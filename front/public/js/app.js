'use strict';

/* App Module */

//Função que indetifica a plataforma que usuário está utilizando
//alet(navigator.platform)
//Caso necessário
//navigator.appCodeName
//navigator.appName
//navigator.appVersion
//navigator.cookieEnabled
//navigator.language
//navigator.onLine
//navigator.platform
//navigator.userAgent

var portalApp = angular.module('portalApp', [
	'ngRoute'
	, 'angular-loading-bar'
	, 'ui.mask'
	, 'ngCookies'
	, 'ngDialog'
	, 'wysiwyg.module'
	, 'colorpicker.module'
	, 'ngDraggable'
	, 'portalServices'
	, 'portalApp.filters'
	, 'portalControllers'
	, 'empresaController'
	, 'historiasController'
	, 'inicialController'
	, 'noticiaController'
	, 'rankingController'
	, 'senhaController'
	, 'isteven-multi-select'
	, 'treinamentoController'
	, 'angularUtils.directives.dirPagination'
	, 'djds4rce.angular-socialshare'
]).constant('toastr', toastr)
.constant('SERVICE', {
	url: 'http://www.portaldainovacao.com.br:8080/'
	//url: 'http://192.168.1.209:8080/'
	, urlImagem: 'http://www.portaldainovacao.com.br/'
	//, urlImagem: 'http://192.168.1.209/'
	, service: 'portal-da-inovacao/rest/'
	, brainstorming: 'brainstorming/'
	, empresa: 'empresa/'
	, faixa: 'faixa/'
	, gamificacao: 'gamificacao/'
	, historia: 'ideia/'
	, noticia: 'noticia/'
	, projeto: 'projeto/'
	, canvas : 'canvas/'
	, solicitacao: 'solicitacao/'
	, solicitacoesPendentes: 'solicitacao/solicitacoes/pendentes/'
  , notificacao: 'notificacao/'
	, usuario: 'usuario/'
	, planoacao: 'planoacao/'
})
.constant('USER_ROLES', {
	all: '*'
	, admin: 'admin'
	, editor: 'editor'
	, guest: 'guest'
});

angular.module('portalApp.filters', [])
	.filter('linebreak', function() {
		return function(text) {
			if(text)
	        return text
	            .replace(/&/g, '&amp;')
	            .replace(/>/g, '&gt;')
	            .replace(/</g, '&lt;');
	    	return '';
    }})
    .filter('to_trusted', ['$sce', function($sce){
	return function(text) {
		return $sce.trustAsHtml(text);
	};
}]);;

angular.module('portalApp').run(function($FB){
  $FB.init('885120678232858');
});

angular.module('portalApp')
    .directive('formatDate',formatDate);
  function formatDate(){
    return {
     require: 'ngModel',
      link: function(scope, elem, attr, modelCtrl) {
        modelCtrl.$formatters.push(function(modelValue){
          return new Date(modelValue);
        })
      }
    }
   }

portalApp.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider',
	function($routeProvider, $locationProvider, cfpLoadingBarProvider) {
		$locationProvider.html5Mode(true).hashPrefix('!') ;
		$routeProvider.
		when('/', {
				templateUrl: './views/inicial.html'
				, controller: 'inicialController'
			})
			.when('/planoacao/atividades/:empresaId/:ideiaId/:planoId', {
				templateUrl: './views/historias-plano-atv.html'
				, controller: 'Controller5w2h'
			})
			.when('/admin/portal', {
				templateUrl: './views/adm-portal.html'
				, controller: 'AdmPortalCtrl'
			})
			.when('/admin/portal/usuarios', {
				templateUrl: './views/adm-portal-usuarios.html'
				, controller: 'AdmPortalUsuariosCtrl'
			})
			.when('/galeria', {
				templateUrl: './views/galeria.html'
				, controller: 'gameController'
			})
			.when('/empresa/:empresaId/analise-perfil', {
				templateUrl: './views/analise-perfil.html'
				, controller: 'BenchmarkingCtrl'
			})
			.when('/analise-perfil/:usuarioId', {
				templateUrl: './views/analise-perfil.html'
				, controller: 'BenchmarkingCtrl'
			})
			.when('/empresa/:empresaId/analise-historia', {
				templateUrl: './views/analise-historia.html'
				, controller: 'BenchmarkingCtrl'
			})
			.when('/empresa/:empresaId/analise-brainstorm', {
				templateUrl: './views/analise-brainstorm.html'
				, controller: 'BenchmarkingCtrl'
			})
			.when('/empresa/:empresaId/analise-dashbord', {
				templateUrl: './views/analise-dashbord.html'
				, controller: 'BenchmarkingCtrl'
			})
			.when('/brainstorm/:empresaId/:ideiaId', {
				templateUrl: './views/brainstorm.html'
				, controller: 'BrainstormCtrl'
			})
			.when('/brainstorm/:empresaId/:ideiaId/:brainstormId/:tipoDoc', {
				templateUrl: './views/brainstorm-doc.html'
				, controller: 'BrainstormCtrl'
			})
			.when('/brainstorm/:empresaId/:ideiaId/brainstorm-list-grupo', {
				templateUrl: './views/brainstorm-list-grupo.html'
				, controller: 'BrainstormGrupoCtrl'
			})
			.when('/brainstorming/:empresaId/:ideiaId/:grupoId/brainstorm-add-grupo', {
				templateUrl: './views/brainstorm-add-grupo.html'
				, controller: 'BrainstormGrupoCtrl'
			})
			.when('/cadastro/empresa', {
				templateUrl: './views/empresa-cadastro.html'
				, controller: 'EmpresaCadCtrl'
			})
			.when('/cadastro/:email', {
				templateUrl: './views/cadastro.html'
				, controller: 'CadastroCtrl'
			})
			.when('/cadastro', {
				templateUrl: './views/cadastro.html'
				, controller: 'CadastroCtrl'
			})
			.when('/canvas/:empresaId/:ideiaId', {
				templateUrl: './views/canvas.html'
				, controller: 'CanvasCtrl'
			})
			.when('/criar-senha/:linkMd5/:email', {
				templateUrl: './views/criar-senha.html'
				, controller: 'senhaController'
			})
			.when('/documentos/:empresaId/:ideiaId', {
				templateUrl: './views/historias-documentos.html'
				, controller: 'HistoriasDashboardCtrl'
			})
			.when('/empresa/listar', {
				templateUrl: './views/empresa-lista.html'
				, controller: 'EmpresaCadCtrl'
			})
			.when('/empresa/:empresaId/visao/:tipoDoc', {
				templateUrl: './views/empresa-visao.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/empresa/:empresaId/missao/:tipoDoc', {
				templateUrl: './views/empresa-missao.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/empresa/:empresaId/edit', {
				templateUrl: './views/empresa-add.html'
				, controller: 'EmpresaEditCtrl'
			})
			.when('/empresa/:empresaId/equipe', {
				templateUrl: './views/empresa-equipe.html'
				, controller: 'EmpresaEquipeCtrl'
			})

			.when('/empresa/:empresaId/equipe-add', {
				templateUrl: './views/empresa-equipe-add.html'
				, controller: 'EmpresaEquipeCtrl'
			})
			.when('/empresa/:empresaId/grupo', {
				templateUrl: './views/empresa-grupo.html'
				, controller: 'EmpresaGrupoCtrl'
			})
			.when('/empresa/:empresaId/grupo/:grupoId', {
				templateUrl: './views/empresa-grupo-lista.html'
				, controller: 'EmpresaGrupoCtrl'
			})
			.when('/empresa/:empresaId', {
				templateUrl: './views/empresa.html'
				, controller: 'EmpresaCadCtrl'
			})
			.when('/empresa/editar/:empresaId', {
				templateUrl: './views/empresa-cadastro.html'
				, controller: 'EmpresaCadCtrl'
			})
			.when('/empresa/equipe/pdi/:empresaId', {
				templateUrl: './views/empresa-pdi.html'
				, controller: 'EmpresaCadCtrl'
			})
			.when('/empresa/:empresaId/missaoPdi/:tipoDoc', {
				templateUrl: './views/empresa-pdi-missao.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/empresa/:empresaId/visaoPdi/:tipoDoc', {
				templateUrl: './views/empresa-pdi-visao.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/empresa/:empresaId/compPdi/:tipoDoc', {
				templateUrl: './views/empresa-pdi-compromisso.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/empresa/:empresaId/needPdi/:tipoDoc', {
				templateUrl: './views/empresa-pdi-necessidade.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/empresa/:empresaId/planPdi/:tipoDoc', {
				templateUrl: './views/empresa-pdi-planejamento.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/empresa/:empresaId/polPdi/:tipoDoc', {
				templateUrl: './views/empresa-pdi-politica.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/empresa/:empresaId/swot', {
				templateUrl: './views/empresa-swot.html'
				, controller: 'SwotController'
			})
			.when('/empresa/:empresaId/swot/adicionar', {
				templateUrl: './views/empresa-swot-add.html'
				, controller: 'SwotController'
			})
			.when('/empresa/:empresaId/:idSwot/:indexSwot/swot/editar', {
				templateUrl: './views/empresa-swot-add.html'
				, controller: 'SwotController'
			})
			.when('/empresa/:empresaId/swot/:idSwot/analise', {
				templateUrl: './views/swot-analise.html'
				, controller: 'SwotController'
			})
			.when('/historias/add/:empresaId', {
				templateUrl: './views/historias-add.html'
				, controller: 'HistoriasAddCtrl'
			})
			.when('/historias/add/:empresaId/:ideiaId', {
				templateUrl: './views/historias-add.html'
				, controller: 'HistoriasAddCtrl'
			})
			.when('/historias/add', {
				templateUrl: './views/historias-add.html'
				, controller: 'HistoriasAddCtrl'
			})
			.when('/historias/equipe/:empresaId/:ideiaId', {
				templateUrl: './views/historias-equipe.html'
				, controller: 'HistoriasEquipeCtrl'
			})
			.when('/historias/:empresaId/:ideiaId/dashboard', {
				templateUrl: './views/historias-dashboard.html'
				, controller: 'HistoriasDashboardCtrl'
			})
			.when('/historias/fechamento/:empresaId/:ideiaId', {
				templateUrl: './views/historias-fechamento.html'
				, controller: 'FechamentoCtrl'
			})
			.when('/historias/fechamento/:empresaId/:ideiaId/documento-fechado', {
				templateUrl: './views/documento-fechado.html'
				, controller: 'FechamentoCtrl'
			})
			.when('/historias/empresa/:empresaId', {
				templateUrl: './views/historias.html'
				, controller: 'HistoriasCtrl'
			})
			.when('/historias/usuario', {
				templateUrl: './views/historias-usuario.html'
				, controller: 'HistoriasCtrl'
			})
			.when('/usuario/painel', {
				templateUrl: './views/usuario-painel.html'
				, controller: 'HistoriasCtrl'
			})
			.when('/login', {
				templateUrl: './views/login.html'
				, controller: 'LoginCtrl'
			})
			.when('/noticia/add', {
				templateUrl: './views/noticia-add.html'
				, controller: 'NoticiaAddCtrl'
			})
			.when('/noticia/edit/:noticiaId', {
				templateUrl: './views/noticia-add.html'
				, controller: 'NoticiaEditCtrl'
			})
			.when('/noticia/:noticiaId', {
				templateUrl: './views/noticia.html'
				, controller: 'NoticiaCtrl'
			})
			.when('/noticias', {
				templateUrl: './views/noticias.html'
				, controller: 'NoticiasCtrl'
			})
			.when('/noticias/pag/:pag', {
				templateUrl: './views/noticias.html'
				, controller: 'NoticiasCtrl'
			})
			.when('/atividades', {
				templateUrl: './views/usuario-atividades.html'
			  , controller: 'AtividadesCtrl'
			})
			.when('/perfil', {
				templateUrl: './views/perfil.html'
				, controller: 'PerfilCtrl'
			})
			.when('/perfil/:alterarSenha', {
				templateUrl: './views/alterarSenha.html'
				, controller: 'senhaController'
			})
			.when('/pesquisa/:busca', {
				templateUrl: './views/pesquisa.html'
				, controller: 'PesquisaCtrl'
			})
			.when('/pesquisa', {
				templateUrl: './views/pesquisa.html'
			})
			.when('/planoacao/:empresaId/:ideiaId', {
				templateUrl: './views/historias-plano.html'
				, controller: 'PlanoAcaoCtrl'
			})
			.when('/planoacao/:empresaId/:ideiaId/adicionar', {
				templateUrl: './views/historias-plano-add.html'
				, controller: 'PlanoAcaoCtrl'
			})
			.when('/planoacao/:empresaId/:ideiaId/:idPlano/editar', {
				templateUrl: './views/historias-plano-edit.html'
				, controller: 'PlanoAcaoCtrl'
			})
			.when('/historias/projeto/:empresaId/:ideiaId', {
				templateUrl: './views/historias-projeto.html'
				, controller: 'FechamentoCtrl'
			})
			.when('/prototipos/:empresaId/:ideiaId/:tipoDoc', {
				templateUrl: './views/historias-prototipos.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/questoes', {
				templateUrl: './views/questoes.html'
				, controller: 'QuestoesCtrl'
			})
			.when('/ranking', {
				templateUrl: './views/ranking.html'
				, controller: 'rankingPortal'
			})
			.when('/recupera-senha', {
				templateUrl: './views/recupera-senha.html'
				, controller: 'senhaController'
			})
			.when('/testes/:empresaId/:ideiaId/:tipoDoc', {
				templateUrl: './views/historias-testes.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/validacoes/:empresaId/:ideiaId/:tipoDoc', {
				templateUrl: './views/historias-validacoes.html'
				, controller: 'EmpresaCtrl'
			})
			.when('/empresa/:empresaId/ranking', {
				templateUrl: './views/ranking-empresa.html'
				, controller: 'rankingEmpresaCtrl'
			})
			.when('/ranking/:empresaId/:ideiaId', {
				templateUrl: './views/ranking-historia.html'
				, controller: 'rankingHistoriaCtrl'
			})
			.when('/treinamento/add', {
				templateUrl: './views/treinamento-add.html'
				, controller: 'TreinamentoAddCtrl'
			})
			.when('/treinamento/:treinamentoId', {
				templateUrl: './views/treinamento.html'
				, controller: 'TreinamentoCtrl'
			})
			.when('/treinamentos', {
				templateUrl: './views/treinamentos.html'
				, controller: 'TreinamentoCtrl'
			})
			.otherwise({
				redirectTo: '/'
			});
	}
]);

portalApp.run(function($rootScope, $location, $cookies, AuthService, SERVICE, core) {
	$rootScope.menuActivePerfil = false;
	$rootScope.menuActiveAmigos = false;
	$rootScope.subMenuMobile = false;
	$rootScope.notificationsActive = false;
	$rootScope.personNotifActive = false;
	$rootScope.qtNotificacao = 0;
	$rootScope.user = {'theme': 'grey'};
	$rootScope.busca = "";
	$rootScope.loaderActive = false;
	$rootScope.menuAberto = false;
	//$rootScope.menuActive = $('body').width() > 1024;
	$rootScope.menuActive = false;
	$rootScope.image = '';
	$rootScope.idUser = '';
		// Set options third-party lib
	toastr.options.timeOut = 4000;
	toastr.options.positionClass = 'toast-top-right';
	toastr.options.preventDuplicates = true;
	toastr.options.progressBar = true;

	//Função para alterar o tamanho do header quando acontece o scroll
	$(window).on('scroll', function() {
		if($(window).scrollTop() > 69){
			$('header').addClass('he-shrink');
		}else{
			$('header').removeClass('he-shrink');
		}
	});

	//Verifica se já tem algum Usuário logado nos Cookies do Navegador.
	if($cookies.get('tecnova')){
		$rootScope.loggedIn = true;

		$rootScope.user = {
			id: $cookies.get('user_id')
			, nome: $cookies.get('user_name')
			, email: $cookies.get('user_email')
			, theme: $cookies.get('user_theme')
			, image: $cookies.get('user_image')
			, perfis: []
		};
		for (var i = $cookies.get('user_perfis_qtd') - 1; i >= 0; i--) {
			$rootScope.user.perfis[i] = $cookies.get('user_perfil_' + i);
		};

		if ($rootScope.user.image.length == 0) {

			$rootScope.image = false;
		}else {

			$rootScope.image = true;

		}

	};

	$rootScope.infoUser = [];
	$rootScope.noImg = false;
	$rootScope.imgUsr = false;
	$rootScope.imgSocial = false;
	$rootScope.linkImage = SERVICE.urlImagem;


	if ($rootScope.user.id != null) {
		(function usuarioPesquisa(){

			var data = {
				'id': $rootScope.user.id
			}
			core.usuarioPesquisa(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$rootScope.infoUser = res.objeto;

					if ($rootScope.infoUser.idFacebook == null && $rootScope.infoUser.idGoogle == null && $rootScope.infoUser.perfilBig == null) {
						$rootScope.noImg = true;
					} else if ($rootScope.infoUser.idFacebook == null && $rootScope.infoUser.idGoogle == null && $rootScope.infoUser.perfilBig != null){
						$rootScope.imgUsr = true;
					} else {
						$rootScope.imgSocial = true;

					}
				}

			})
		})();
	}

	if($rootScope.loggedIn){
		(function loadAmigos(){
			core.loadAmigos(function(resposta){
				if(resposta.erro){
					toastr.error(resposta.mensagem);
				}else{
					$rootScope.amigos = resposta.objeto;
				}
			});
		})();

		(function loadEmpresas(){
			core.loadEmpresas(function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$rootScope.listaEmpresas = res.objeto;
				}
			})
		})();

		(function loadNotifi(){
			core.loadSolicitacaoAmizades(function(resposta){
				if(resposta.erro){
					toastr.error(resposta.mensagem);
				}else{
					$rootScope.solicitacaoAmizades = resposta.objeto;
					$rootScope.qtNotificacao = resposta.objeto.length + $rootScope.qtNotificacao;
				}
			});

			core.loadSolicitacaoPendente(function(resposta){
				if(resposta.erro){
					toastr.error(resposta.mensagem);
				}else{
					$rootScope.solicitacaoPendentes = resposta.objeto;
				}
			});

			core.loadSolicitacaoEmpresa(function(resposta){
				if(resposta.erro){
					toastr.error(resposta.mensagem);
				}else{
					$rootScope.solicitacaoEmpresa = resposta.objeto;
					$rootScope.qtNotificacao = resposta.objeto.length + $rootScope.qtNotificacao;
				}
			});

			core.loadNotificacao(function(resposta){
				if(resposta.erro){
					toastr.error(resposta.mensagem);
				}else{
          $rootScope.lstNotificacao = resposta.objeto;
					$rootScope.qtNotificacao = resposta.objeto.length + $rootScope.qtNotificacao;
				}
			});

		})();
	}

	function checkLoginState() {
		FB.getLoginStatus(function(response) {
			$rootScope.statusChangeCallback(response);
		});
	}

	$rootScope.attachSignin = function (element) {
		$rootScope.auth2.attachClickHandler(element, {},
			function(googleUser) {
				var data = {};
				data.idPlus = googleUser.getBasicProfile().getId();
				data.nome = googleUser.getBasicProfile().getName();
				data.email = googleUser.getBasicProfile().getEmail();
				data.imagem = googleUser.getBasicProfile().getImageUrl();
				data.tipo = "plus";
				$rootScope.login(data);
			}, function(er) {

			}
		);
	}

	$rootScope.apiFace = function(){
		FB.api('/me' , 'GET' , {fields: 'name,id,email,picture.width(9999).height(9999)'}, function(res){
			var data = {};
			data.idFace = res.id;
			data.nome = res.name;
			data.email = res.email;
			data.imagem = res.picture.data.url;
			data.tipo = "face";
			$rootScope.login(data);
		})
	}

	$rootScope.closeMenu = function() {
		$rootScope.menuActive = false;
		$rootScope.menuActivePerfil = false;
		$rootScope.notificationsActive = false;
		$rootScope.personNotifActive = false;
		$rootScope.menuActiveAmigos = false;
	}

	$rootScope.closeMenuMobile = function() {
		if(screen.width < 1024){
			$rootScope.menuActive = false;
			$rootScope.menuActivePerfil = false;
			$rootScope.notificationsActive = false;
			$rootScope.personNotifActive = false;
			$rootScope.menuActiveAmigos = false;
			$rootScope.menuAberto = false;
		}
	}

	$rootScope.hidePerfilMenu = function() {
		$rootScope.notificationsActive = false;
		$rootScope.personNotifActive = false;
		$rootScope.menuActiveAmigos = false;
	}

	$rootScope.hideAmigosMenu = function() {
		$rootScope.notificationsActive = false;
		$rootScope.personNotifActive = false;
		$rootScope.menuActiveAmigos = !$rootScope.menuActiveAmigos;
	}

	$rootScope.login = function(credentials){
		AuthService.login(credentials, function(resposta){
			if(resposta.erro){
				toastr.error(resposta.mensagem);
			}else{
				var resul = resposta.mensagem.substr(0,4).toUpperCase();
				if(resul == "CADA"){
					toastr.success(resposta.mensagem);
					$rootScope.loaderActive = false;
					resposta.objeto.imagem = (credentials.imagem) ? credentials.imagem : "";
					$rootScope.saveUserInfo(resposta.objeto, 'cadastro');
				}else {
					toastr.success(resposta.mensagem);
					$rootScope.loaderActive = false;
					resposta.objeto.imagem = (credentials.imagem) ? credentials.imagem : "";
					$rootScope.saveUserInfo(resposta.objeto, 'login');
				}
			}
		});
	}

	$rootScope.loginEmail = function(credentials){
		credentials.tipo = "email";
		$rootScope.login(credentials);
	}

	$rootScope.loginTeste = function(){
        window.location.href = './views/usuario-painel.html';
    }

	$rootScope.loginFace = function(){
		FB.login(function(res){
			if(res.status == "connected"){
				$rootScope.apiFace();

			} else if (res.status === 'not_authorized') {
				toastr.error("Login não autorizado");
			} else {
				toastr.error("Não foi possível conectar ao Facebook");
			}
		}, {
			scope: 'email'
			, return_scopes: true
		});
		return false;
	}

	$rootScope.logout = function() {
		$rootScope.menuActivePerfil = false;
		$cookies.remove('tecnova');
		$rootScope.loggedIn = false;
		window.location.href = '/';
	}

	$rootScope.openMenu = function() {
		$rootScope.closeMenu();
		$rootScope.menuActive = true;
	}

  $rootScope.LerNotificacao = function(){
    core.LerNotificacao(function(resposta){
       if (resposta.erro == 0){
           $rootScope.qtNotificacao = $rootScope.qtNotificacao - $rootScope.lstNotificacao.length;
           $rootScope.lstNotificacao.splice(0, $rootScope.lstNotificacao.length);
       }
    });
  }

	$rootScope.responderSolicitaoAmizade = function(aceita, usuarioId) {
		var servico = (aceita) ? "aprovar" : "recusar";
		core.responderSolicitacao(servico, usuarioId, function(resposta){
			if(resposta.erro){
				toastr.error(resposta.mensagem);
			}else{
				toastr.success(resposta.mensagem);
				angular.forEach($rootScope.solicitacaoAmizades, function(amigo, i){
					if(amigo.id == usuarioId){
						$rootScope.solicitacaoAmizades.splice(i, 1);
					}
				});
				$rootScope.qtNotificacao = $rootScope.qtNotificacao - 1;
			}
		});
	}

	$rootScope.responderSolicitaoEmpresa = function(aceita, idEmpresa, $index) {
		var servico = (aceita) ? "aprovar" : "recusar";
		core.responderSolicitaoEmpresa(servico, idEmpresa, function(resposta){
			if(resposta.erro){
				toastr.error(resposta.mensagem);
			}else{
				toastr.success(resposta.mensagem);
				$rootScope.solicitacaoEmpresa.splice($index, 1);
				$rootScope.qtNotificacao = $rootScope.qtNotificacao - 1;
			}
		});
	}

	$rootScope.saveUserInfo = function(usuario, tipo){
		if(!$rootScope.loggedIn){
			$rootScope.loggedIn = true;
			$cookies.put('tecnova', usuario);
			$cookies.put('user_id', usuario.id);
			$cookies.put('user_name', usuario.nome);
			$cookies.put('user_email', usuario.email);
			$cookies.put('user_theme', usuario.perfil_tema);
			$cookies.put('user_image', usuario.imagem);
			//$cookies.put('user_perfis_qtd', usuario.perfis.length);

			angular.forEach(usuario.perfis, function(val, key){
				$cookies.put('user_perfil_'+ key, val.sigla);
			});

			setTimeout(function(){
				if(tipo == 'login')
					window.location.href = '/usuario/painel';
				if(tipo == 'cadastro')
					window.location.href = '/usuario/painel';
			}, 800)
		}

        function ajustarInput(str) {
        var adicionar = 6 - str.length;
        for (var i = 0; i < adicionar; i++) str = '0' + str;
            return str.slice(0, 5) + '-' + str.slice(-1);
        }
	}

	$rootScope.searchSubmit = function(busca) {
		if(busca){
			$location.path('/pesquisa/' + busca);
		}
	}

	$rootScope.startApp = function() {
		window.onload = function() {
			gapi.load('auth2', function(){
				$rootScope.auth2 = gapi.auth2.init({
					client_id: '514094463323-agueg0itifu5p5fc8ce1oihkeeevsl5r.apps.googleusercontent.com'
					, cookiepolicy: 'single_host_origin'
				});
				$rootScope.attachSignin(document.getElementById('customBtn'));
				$rootScope.attachSignin(document.getElementById('customBtn-2'));
			});
		};
	};

	$rootScope.showHideMenu = function() {
		$rootScope.menuActivePerfil = false;
		$rootScope.notificationsActive = false;
		$rootScope.personNotifActive = false;
		$rootScope.menuActiveAmigos = false;
		$rootScope.menuAberto = !$rootScope.menuAberto;
	}

	$rootScope.showHidePerfilMenu = function() {
		$rootScope.menuActiveAmigos = false;
		$rootScope.notificationsActive = false;
		$rootScope.menuAberto = false;
		$rootScope.personNotifActive = !$rootScope.personNotifActive;
		$rootScope.menuAtivo = false;
	}




	$rootScope.showHideNotifications = function() {
		$rootScope.menuActiveAmigos = false;
		$rootScope.personNotifActive = false;
		$rootScope.notificationsActive = !$rootScope.notificationsActive;
	}

	$rootScope.statusChangeCallback = function(response) {
		if (response.status === 'connected') {
			$rootScope.apiFace();
		} else if (response.status === 'not_authorized') {
			toastr.error("Login não autorizado");
		} else {
			toastr.error("Não foi possível conectar ao Facebook");
		}
	}

	$rootScope.toggleSubMenuMobile = function() {
		$rootScope.subMenuMobile = !$rootScope.subMenuMobile;
	}

	//Inicia a API do Facebook para a realização do login
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '885120678232858',
			cookie     : true,  // enable cookies to allow the server to access
			status     : true,  // enable cookies to allow the server to access
			xfbml      : true,  // parse social plugins on this page
			version    : 'v2.4' // use version 2.2
		});
	};

	//Inicia o SDK do Facebook para a realização do login
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

});
