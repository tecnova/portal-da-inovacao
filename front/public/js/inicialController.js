'use strict';

/* Controllers */

var inicialController = angular.module('inicialController', ['ngCookies', 'ngMdIcons']);

inicialController.controller('inicialController', ['$rootScope', '$scope', '$location', '$routeParams', '$http', 'SERVICE', 'core',
	function inicialController($rootScope, $scope, $location, $routeParams, $http, SERVICE, core) {

		/*NoticiasCtrl*/
		$scope.addMenu = false;
		$scope.itemsPerPage = 10;
		$scope.imagemUrl = SERVICE.urlImagem;
		$scope.noticias = [];

		if(!$rootScope.projetos){
			$http.get('../public/js/artigos.json').success(function(data) {
				$rootScope.projetos = data;
			})
		}
			$http({
				url: SERVICE.url + SERVICE.service + SERVICE.noticia + 'noticias'
				, method: 'GET'
			}).then(function(response) {
				$scope.noticias = response.data;
			}, function(response){
				toastr.warning("Por favor tente novamente mais tarde.")
				toastr.error("Ocorreu um erro ao carregar notícias.")
			});

		/* Treinamentos - TreinamentoCtrl*/
		$scope.menuCatTrei = false;
		$scope.treinamentoId = {"id": $routeParams.treinamentoId};
		$scope.videoLink = "../public/treinamentos/" + $scope.treinamentoId.id + "/index.html";

		$scope.treinamentoId = {"id": $routeParams.treinamentoId};
		$scope.addMenu = false;
		$scope.menuCatTrei = false;
		if(!$rootScope.projetos){
			$http.get('../public/js/artigos.json').success(function(data) {
				$rootScope.projetos = data;
			})
		}

		if(!$rootScope.treiAnda){
			$http.get('../public/js/treiAnda.json').success(function(data) {
				$rootScope.treiAnda = data;
			})
		}

		$scope.message = "Página de treinamento.";


		$scope.enviarEmailContato = function(contato){
			var data = {
				"email": contato.email,
				"nome": contato.nome,
				"mensagem": contato.mensagem
			}
			core.enviarEmailContato(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
					$scope.contato.mensagem = '';
					$scope.contato.nome = '';
					$scope.contato.email = '';
				}
			});
		}


	}
]);