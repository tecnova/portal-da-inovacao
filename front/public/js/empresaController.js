'use strict';

/* Controllers */

var empresaController = angular.module('empresaController', ['ngCookies' , 'chart.js' , 'ngMdIcons' , 'ngDialog', 'colorpicker.module', 'wysiwyg.module']);

empresaController.controller('EmpresaCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE',
	function EmpresaCtrl($rootScope, $routeParams, $scope, $location, $http, core, SERVICE) {
		$scope.empresa = {'id': $routeParams.empresaId};
		$scope.historia = {"id": $routeParams.ideiaId};
		$scope.perfilUser = '';
		$scope.validaEditor = 0 ;
		$scope.loggeduser = $rootScope.user.id;

		if ($scope.empresa.id > 0) {
			(function loadInfoEmpresa(){
				core.loadEmpresaUsuario($scope.empresa.id, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.empresaInfo = res.objeto;
					}
				})
			})();
		}

		(function loadHistoriasData() {
			var data = {
				'id': $scope.historia.id
			}
			if ($scope.historia.id != null) {
				core.loadHistoriasData(data, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.usuarioCriador = res.objeto.usuario;
						$scope.titleHistoria = res.objeto.titulo;
					}
				})
			}else{
				$scope.titleHistoria = "";
			}
		})();

			(function loadEmpresaUsuario(){
				core.loadEmpresaUsuario($scope.empresa.id, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.perfilUser = res.objeto.perfilUsuario
					}
				})
			})();

		(function loadDocumentos(){
			var data = {
				"idEmpresa": parseInt($scope.empresa.id),
				"tpDocumento": $routeParams.tipoDoc
			}

			core.loadDocumentos(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.documento = res.objeto[0];
				}
			})
		})();

		$scope.addDocumentos = function(tpDocumento, documento){
			var data = {
				"idEmpresa": parseInt($scope.empresa.id),
				"tpDocumento": tpDocumento,
				"documento": documento
			};
			if ($scope.empresaInfo.perfilUsuario != 'USUARIO' || $scope.usuarioCriador == $scope.loggeduser) {
				core.addDocEmpresa(data, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						toastr.success(res.mensagem);
					}
				});
			}else{
				toastr.error("Desculpe, você não tem permissões para alterar esse documento!");
			}

		}
	}
]);

empresaController.controller('EmpresaCadCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE','ngDialog', '$cookies',
	function EmpresaCadCtrl($rootScope, $routeParams, $scope, $location, $http, core, SERVICE, ngDialog, $cookies) {
		$scope.imagemUrl = SERVICE.urlImagem;
		$scope.cnaes = [];
		$scope.faixaFaturamento = [];
		$scope.faixaFuncionario = [];
		$scope.usuariosNaEmpresa = [];
		$scope.infoEmpresas = [];
		$scope.filtroCnae = '';
		$scope.sugestoes = false;
		$scope.perfilUser = '';
		
		$scope.empresa = {'id': $routeParams.empresaId};

		if ($scope.empresa.id > 0) {
			(function loadInfoEmpresa(){
				core.loadEmpresaUsuario($scope.empresa.id, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.empresaInfo = res.objeto;
					}
				})
			})();
		}

		(function loadFaixaFaturamento(){
			core.loadFaixaFaturamento(function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.faixaFaturamento = res;
				}
			})
		})();

		(function loadFaixaFuncionarios(){
			core.loadFaixaFuncionarios(function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.faixaFuncionario = res;
				}
			})
		})();

		if($routeParams.empresaId){
			$scope.empresa = {'idEmpresa': $routeParams.empresaId};
			(function loadEmpresaUsuario(){
				core.loadEmpresaUsuario($scope.empresa.idEmpresa, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.empresa = res.objeto;
						$scope.perfilUser =  $scope.empresa.perfilUsuario;
						delete $scope.empresa.perfilUsuario;
						$scope.empresa['idEmpresa'] = $scope.empresa.idEmpresa;
						(function loadCnaes(){
							core.loadCnaes(function(res){
								if(res.erro > 0){
									toastr.error(res.mensagem);
								}else{
									$scope.cnaes = res;
									angular.forEach($scope.cnaes, function(cnae){
										if($scope.empresa.idCnae == cnae.id){
											$scope.filtroCnae = cnae.codigo + ' - ' + cnae.nome;
											$scope.empresa.idCnae = cnae.id;
										}
									});
								}
							})
						})();
					}
				})
			})();
		}else{
			(function loadCnaes(){
				core.loadCnaes(function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.cnaes = res;
					}
				})
			})();
		};

		(function loadCookies(){
			$scope.CookiesName = "empresaHelp";
			$scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

			if ($scope.showHelp == null) {
					$cookies.put($scope.CookiesName, "true");
					var cookie = $cookies.get('brainstormHelp');
					$scope.showHelp = "true";
			}
		})();

		$scope.help = function() {
			var now = new Date(),
			exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
			$cookies.put("" + $scope.CookiesName + "", "false", {expires: exp});
			$scope.showHelp = "false";
		}

		$scope.excluirUsuarioEmpresa = function(){
			var data = {
				"idEmpresa": $scope.globalIdEmpresa,
				"idAmigoEmpresa": parseInt($rootScope.user.id),
				"idUsuario": parseInt($rootScope.user.id)
			};

			core.excluirUsuarioEmpresa(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
					ngDialog.close();

					for (var i = 0; i < $scope.listaEmpresas.length; i++) {
						if ($scope.listaEmpresas[i].idEmpresa == $scope.globalIdEmpresa) {
							$scope.listaEmpresas.splice(i, 1);
						}
					}
					$location.path('/empresa/listar');
				}
			});
		};

		$scope.loadEmpresasCad = function(CEP){
			core.loadEmpresaUsuario($scope.empresa.idEmpresa, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
	        $scope.empresa.logradouro = res['logradouro'];
	        $scope.empresa.bairro = res['bairro'];
	        $scope.empresa.municipio = res['localidade'];
	        $scope.empresa.uf = res['uf'];
				}
			});
		};

		$scope.loadEmpresas = function(){
			core.loadEmpresas(function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$rootScope.listaEmpresas = res.objeto;
				}
			})
		};

		$scope.pesquisaCEP = function(CEP){
			core.pesquisaCEP(CEP, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
          $scope.empresa.logradouro = res['logradouro'];
          $scope.empresa.bairro = res['bairro'];
          $scope.empresa.municipio = res['localidade'];
          $scope.empresa.uf = res['uf'];
				}
			});
		};

		$scope.adicionaCnae = function(cnae){
			$scope.filtroCnae = cnae.codigo + ' - ' + cnae.nome;
			$scope.empresa.idCnae = cnae.id;
		}

		$scope.validarCNPJ = function(cnpj) {
			cnpj = cnpj.replace(/[^\d]+/g,'');

			if(cnpj == '') return false;
			if (cnpj.length != 14)
				return false;

			if (cnpj == "00000000000000" ||
				cnpj == "11111111111111" ||
				cnpj == "22222222222222" ||
				cnpj == "33333333333333" ||
				cnpj == "44444444444444" ||
				cnpj == "55555555555555" ||
				cnpj == "66666666666666" ||
				cnpj == "77777777777777" ||
				cnpj == "88888888888888" ||
				cnpj == "99999999999999")
				return false;

			var tamanho = cnpj.length - 2
			var numeros = cnpj.substring(0,tamanho);
			var digitos = cnpj.substring(tamanho);
			var soma = 0;
			var pos = tamanho - 7;

			for (var i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(0))
				return false;

			tamanho = tamanho + 1;
			numeros = cnpj.substring(0,tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(1))
				return false;

			return true;
		}

		$scope.cadEmpresa = function(empresa){
			empresa['usuario'] =  parseInt($rootScope.user.id);

			if ($routeParams.empresaId > 0) {
				core.editaEmpresa(empresa, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem)
					}else{
						$scope.empresa = res.objeto;
						toastr.success(res.mensagem);
						$location.path('/empresa/' + empresa.idEmpresa);
					}
				})
			} else{
				if ($scope.validarCNPJ(empresa.cnpj)) {
					core.cadEmpresa(empresa, function(res){
						if(res.erro > 0){
							toastr.error(res.mensagem);
						}else{
							toastr.success(res.mensagem);
							$location.path('/empresa/listar');
							$scope.loadEmpresas();
						}
					})
				}else{
					toastr.error("O CNPJ não é válido!");
				};
			}
		};

		$scope.inativarEmpresa = function(idEmpresa, $index){
			var data = {
				"idEmpresa": idEmpresa,
				"idUsuario": $rootScope.user.id
			};

			core.inativarEmpresa(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					for (var i = $rootScope.listaEmpresas.length - 1; i >= 0; i--) {
						if ($rootScope.listaEmpresas[i].idEmpresa === idEmpresa) {
							$rootScope.listaEmpresas.splice(i, 1);
						};
					};
					toastr.success(res.mensagem);
					ngDialog.close();
					$location.path('/empresa/listar');
				}
			})
		};

		$scope.openExcluirEmpresa = function(idEmpresa, $index){
			ngDialog.open({
				template:'openExcluirEmpresa',
				className: 'ngdialog-theme-default',
				scope: $scope
			});
		};

		$scope.openSairEmpresa = function(empresa){
			$scope.globalIdEmpresa = empresa;

			ngDialog.open({
				template:'openSairEmpresa',
				className: 'ngdialog-theme-default',
				scope: $scope
			});
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};


		$scope.procurandoCnae = function($event){
			var elemento = angular.element($event.target);

			if(elemento.val().length >= 3){
				$scope.sugestoes = true;
			}else{
				$scope.sugestoes = false;
			}
		}
	}
]);

empresaController.controller('EmpresaEquipeCtrl', ['$scope', '$rootScope', '$location', '$http', '$routeParams', 'core', 'ngDialog', "$cookies",
	function EmpresaEquipeCtrl($scope, $rootScope, $location, $http, $routeParams, core, ngDialog, $cookies) {
		$scope.empresa = {'id': $routeParams.empresaId};
		$scope.botaoPerfil = true;
		$scope.botaoExcluir = true;
		$scope.amigosForaEmpresa = $rootScope.amigos;
		$scope.solicitacoesPendentes = [];
		$scope.loggeduser = $rootScope.user.id;
		$scope.dadosUser = [];
		$scope.userAmigos = [0];
		$scope.indexUsuario = 0;
		$scope.perfilUsuario = '';
		$scope.nomeUsuario = '';
		$scope.newPageNumber = 1;
		$scope.itemPagina = 10;
		$scope.newIndiceUsuario = 0;
		$scope.origem = 'EquipeEmpresa';


		if ($scope.empresa.id > 0) {
			(function loadInfoEmpresa(){
				core.loadEmpresaUsuario($scope.empresa.id, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.empresaInfo = res.objeto;
					}
				})
			})();
		}

		(function loadEmpresaUsuario(){
			core.loadEmpresaUsuario($scope.empresa.id, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.perfilUser = res.objeto.perfilUsuario
				}
			})
		})();

		(function loadSolicitacoesPendentes(){
			var data = {
				"id": parseInt($scope.empresa.id)
			};
			core.loadSolicitacoesEmpresaPendentes(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.solicitacoesPendentes = res.objeto;
				}
			});
		})();

		(function loadUsuarioEmpresa(){
			core.loadUsuarioEmpresa($scope.empresa.id, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.usuariosNaEmpresa = res.objeto;
					for (var i = 0; i < $scope.usuariosNaEmpresa.length; i++) {
						if($scope.usuariosNaEmpresa[i].perfil === "USUARIO"){
							$scope.usuariosNaEmpresa[i].perfil = "U";
						}
						if($scope.usuariosNaEmpresa[i].perfil === "LIDER"){
							$scope.usuariosNaEmpresa[i].perfil = "L";
						}
						if($scope.usuariosNaEmpresa[i].perfil === "ADMINISTRADOR"){
							$scope.usuariosNaEmpresa[i].perfil = "A";
						}
					}
				}
			})
		})();

		/* Função para carregar os amigos do usuario logado */
		(function loadAmigosUser(){
			var data = {
				"idUsuario": $rootScope.user.id,
				"idEmpresa": $scope.empresa.id
			}

			core.loadAmigosUser(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.userAmigos = res.objeto;
				}
			})
		})();

		(function loadCookies(){
      $scope.CookiesName = "empresaEquipeHelp";
      $scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

      if ($scope.showHelp == null) {
          $cookies.put($scope.CookiesName, "true");
          var cookie = $cookies.get('brainstormHelp');
          $scope.showHelp = "true";
      }
    })();

    $scope.help = function() {
      var now = new Date(),
      exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
      $cookies.put("" + $scope.CookiesName + "", "false", {expires: exp});
      $scope.showHelp = "false";
    }

		$scope.adicionaAmigoEquipe = function(usuario, newPageNumber, $index){
			var data = {
				"id_empresa": parseInt($scope.empresa.id),
				"id_usuario": parseInt(usuario.idUsuario)
			};

			$scope.newIndiceUsuario = ($index) + (newPageNumber - 1) * $scope.itemPagina;

			core.adicionaAmigoEquipe(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
					$scope.userAmigos.splice($scope.newIndiceUsuario, 1);
					$scope.solicitacoesPendentes.push(usuario);
				}
			});
		};


		/* Função para convidar os usuarios por email*/
		$scope.adicionarPorEmail = function(email){
			var data = {
				'email': email,
				"idEmpresa": parseInt($scope.empresa.id),
				"idUsuario": $rootScope.amigos.length + 1
			};

			if (email != null) {
				core.adicionarPorEmail(data, function(res){
					if (res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						toastr.success(res.mensagem);
						$scope.solicitacoesPendentes.push({'email': email, "id_usuario": $rootScope.amigos.length + 1});
					}
				});
			} else {
				toastr.error("O email não pode estar vazio!");
			}

		};

		$scope.trocarPerfil = function(usuario, perfil, nome, numPage, $index) {
				$scope.modalTrocarPerfil(usuario, perfil, nome, numPage, $index, $scope);
		}

		$scope.clickToClose = function(){
			ngDialog.close();
		};

		/* Função para reenviar os emails */
		$scope.enviarEmail = function(email){
			var data = {
				"email": email
			};
			core.enviarEmail(data,function(res) {
				if (res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
				}
			})
		};

		$scope.mudarPerfilUsuario = function(usuario, idPerfil, indice){
			if ($scope.perfilUser == 'ADMINISTRADOR' || $scope.perfilUser == 'LIDER') {
				var data = {
					"idEmpresa": parseInt($scope.empresa.id),
					"idAmigoEmpresa": parseInt($rootScope.user.id),
					"idUsuario": parseInt(usuario),
					"perfis": [
						{
							"id": parseInt(idPerfil)
						}
					]
				};
				core.mudarPerfilUsuario(data, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						switch(idPerfil){
							case 1:
							var perfilString = "U";
							break;
							case 2:
							var perfilString = "L";
							break;
							case 3:
							var perfilString = "A";
							break;
						}
						toastr.success(res.mensagem);

						$scope.usuariosNaEmpresa[indice].perfil = perfilString;
						$scope.perfilUsuario = perfilString;
					}
					ngDialog.close();
				});
			}else{
				toastr.error("Desculpe, você não tem permissão para trocar o perfil de usuários");
			}
		};		

	$scope.excluirUsuario = function(item, numPage, $index){
		$scope.modalExcluirUsuario(item, numPage, $index, $scope);
	};

	$scope.clickToCloseFunction = function(){
			ngDialog.close();
		};
	}
]);

empresaController.controller('EmpresaGrupoCtrl', ['$scope', '$rootScope', '$location', '$http', '$routeParams', 'core', 'SERVICE', 'ngDialog', '$cookies',
	function EmpresaGrupoCtrl($scope, $rootScope, $location, $http, $routeParams, core, SERVICE , ngDialog, $cookies ) {
		$scope.imagemUrl = SERVICE.urlImagem;
		$scope.grupoId = $routeParams.grupoId;
		$scope.empresa = {'id': $routeParams.empresaId};
		$scope.grupoDados = [];
		$scope.botaoAdicionar = true;
		$scope.botaoInfo = true;
		$scope.usuariosSelecionados = [];
		$scope.usuariosDisponiveis = [];
		$scope.itensPagina = 10;
		$scope.newPageNumberAdd = 1;
		$scope.newPageNumberDelete = 1;
		$scope.origem = 'GrupoEmpresa';
		$scope.empresa = {'id': $routeParams.empresaId};
		$scope.gruposLista = [];

		if ($scope.empresa.id > 0) {
			(function loadInfoEmpresa(){
				core.loadEmpresaUsuario($scope.empresa.id, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.empresaInfo = res.objeto;
					}
				})
			})();
		}

		(function loadEmpresaUsuario(){
			core.loadEmpresaUsuario($scope.empresa.id, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.perfilUser = res.objeto.perfilUsuario;
				}
			})
		})();

		(function buscaGruposEmpresa(){
			core.buscaGruposEmpresa($scope.empresa.id, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.gruposLista = res.objeto;
					for (var i = $scope.gruposLista.length - 1; i >= 0; i--) {
						if ($scope.gruposLista[i].id == $scope.grupoId) {
							$scope.grupoDados = $scope.gruposLista[i];
						}
					}
				}
			})
		})();

		(function loadCookies(){
			$scope.CookiesName = "empresaGrupoHelp";
			$scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

			if ($scope.showHelp == null) {
					$cookies.put($scope.CookiesName, "true");
					var cookie = $cookies.get('brainstormHelp');
					$scope.showHelp = "true";
			}
		})();

		$scope.help = function() {
			var now = new Date(),
			exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
			$cookies.put("" + $scope.CookiesName + "", "false", {expires: exp});
			$scope.showHelp = "false";
		}


		$scope.callGruposEmpresa = function(){
			core.buscaGruposEmpresa($scope.empresa.id, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.gruposLista = res.objeto;
				}
			})
		}

		$scope.adicionaGrupoEmpresa = function(titulo){
			var data = {
				"nome": titulo.grupo,
				"idEmpresa": parseInt($scope.empresa.id)
			};
			if ($scope.perfilUser == "ADMINISTRADOR" || $scope.perfilUser == "LÍDER") {
				core.adicionaGrupoEmpresa(data, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						toastr.success(res.mensagem);
						titulo.grupo = "";
						$scope.callGruposEmpresa();
					}
				});
			}else{
				toastr.error("Somente o administrador ou líder da empresa pode criar novos grupos");
				titulo.grupo = "";
			}
		}		

		$scope.adicionarUsuario = function(id, newPageNumber, indice) {
			$scope.newIndiceUsuario = indice + (newPageNumber - 1) * $scope.itensPagina;
			$scope.adicionarUsuarioCtrl($scope.origem, id, newPageNumber, indice, $scope);
		}

		$scope.excluirGrupo = function(id, $index){
			var data = {
				"id": parseInt(id)
			};
			if ($scope.perfilUser == "ADMINISTRADOR" || $scope.perfilUser == "LIDER") {
			core.excluirGrupo(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
					$location.path('/empresa/' + $scope.empresa.id + '/grupo');
				}
				ngDialog.close();
			});
			}else {
				toastr.error("Somente o administrador ou líder da empresa pode excluir o grupo");
			}
		};
		$scope.clickToOpen = function(usuario, $index){
			ngDialog.open({
				template:'templateId',
				className: 'ngdialog-theme-default',
				scope: $scope});

			};
		$scope.clickToClose = function(){
				ngDialog.close();
			};

		$scope.excluirUsuario = function(item, numPage, indice) {
			$scope.newIndiceUsuario = indice + (numPage - 1) * $scope.itensPagina;
			$scope.modalExcluirUsuario(item, numPage, indice, $scope);
		}

		$scope.alterarGrupo = function(id, nome){
			var data = {
				"id": parseInt(id),
				"nome": nome
			};
			if ($scope.perfilUser == "ADMINISTRADOR" || $scope.perfilUser == "LIDER") {
				core.alterarGrupo(data, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						toastr.success(res.mensagem);
					}
				});
			}else {
				toastr.error("Somente o administrador ou líder da empresa pode editar os grupos");
			}
		};

		if ($scope.grupoId > 0) {
		(function buscaUsuarioGrupoEmpresa(){
			core.buscaUsuarioGrupoEmpresa($scope.grupoId, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.grupoUserLista = res.objeto;
					$scope.usuariosDisponiveis = $scope.grupoUserLista.lstUsuarioEmpresa;
					$scope.usuariosSelecionados = $scope.grupoUserLista.lstUsuarioGrupo;
				}
			})
		})();
		}
		$scope.buscaUsuariosEmpresa = function(){
			core.buscaUsuariosEmpresa($scope.empresa.id, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.usuariosEmpresa = res.objeto;
					for (var i = $scope.usuariosEmpresa.length - 1; i >= 0; i--) {
						for (var j = $scope.grupoUserLista.length - 1; j >= 0; j--) {
							if ($scope.usuariosEmpresa[i].nome == $scope.grupoUserLista[j].nomeUsuario) {
								$scope.usuariosEmpresa[i].grupo = $scope.grupoUserLista[j].idGrupo;
							};
						};
					};
				}
			})
		}
	}

]);
empresaController.controller('SwotController', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE', '$window', 'ngDialog',
	function SwotController($rootScope, $routeParams, $scope, $location, $http, core, SERVICE, $window, ngDialog) {
		$scope.empresa = {'id': $routeParams.empresaId};
		$scope.imagemUrl = SERVICE.urlImagem;
		$scope.lstSwot = [];
	    $scope.lstForca = [];
	    $scope.lstOportunidade = [];
	    $scope.lstFraqueza = [];
	    $scope.lstAmeaca = [];
	    $scope.lstAtaque = [];
		$scope.lstDefesa = [];
		$scope.idSwot = $routeParams.idSwot;
		$scope.ForcaIndex = 0;
		$scope.FraquezaIndex = 0;
		$scope.Ataque = {};
		$scope.lstData = [];
		$scope.nota = 5;


		if ($scope.empresa.id > 0) {
			(function loadInfoEmpresa(){
				core.loadEmpresaUsuario($scope.empresa.id, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.empresaInfo = res.objeto;
					}
				})
			})();
		}

		(function loadSwot(){
			var data = {
				"idEmpresa": $scope.empresa.id
			}
			core.loadSwot(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.lstSwot = res.objeto;
					for (var i = 0; i < $scope.lstSwot.length; i++) {
						$scope.lstSwot[i].dtInicio = Date.parse($scope.lstSwot[i].dtInicio);
						$scope.lstSwot[i].dtPrevisto = Date.parse($scope.lstSwot[i].dtPrevisto);
						if ($scope.lstSwot[i].id == $scope.idSwot) {
							$scope.notaMin = $scope.lstSwot[i].ptMinimo;
							$scope.notaMax = $scope.lstSwot[i].ptMaximo;
							$scope.notaCorte = $scope.lstSwot[i].corte;
						}
					}
					if ($routeParams.indexSwot >= 0) {
						$scope.swot = $scope.lstSwot[$routeParams.indexSwot];
						$scope.swot.dtInicio = new Date($scope.swot.dtInicio);
						$scope.swot.dtPrevisto = new Date($scope.swot.dtPrevisto);
					}
				}
			})
		})();

		(function loadSwotItem(){
			var data = {
				'id': $scope.idSwot
			}
			core.listaSwotItem(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.lstSwotItens = res.objeto;
				}
			})
		})();

		(function loadSwotInfo(){
			var data = {
				"idEmpresa": $scope.empresa.id
			}
			core.loadSwot(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.lstSwotInfo = res.objeto;
					for (var i = 0; i < $scope.lstSwotInfo.length; i++) {
						if ($scope.lstSwotInfo[i].id  == $scope.idSwot) {
							$scope.lstSwotInfo = $scope.lstSwotInfo[i];
							$scope.lstSwotInfo.dtInicio = new Date($scope.lstSwotInfo.dtInicio);
							$scope.lstSwotInfo.dtPrevisto = new Date($scope.lstSwotInfo.dtPrevisto);
						}
					}
				}
			})
		})();

		(function loadSwotQuadrante(){
			var data = {
				'id': $scope.idSwot
			}
			core.listaSwotItem(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.lstSwotItens = res.objeto;
					for (var i = 0; i < $scope.lstSwotItens.length; i++) {
						if ($scope.lstSwotItens[i].tpSwot == "S") {
							$scope.lstForca.push($scope.lstSwotItens[i]);
						}else if($scope.lstSwotItens[i].tpSwot == "O"){
							$scope.lstOportunidade.push($scope.lstSwotItens[i]);
							$scope.lstOportunidade[$scope.lstOportunidade.length - 1].valor = 0;
						}else if($scope.lstSwotItens[i].tpSwot == "W"){
							$scope.lstFraqueza.push($scope.lstSwotItens[i]);
						}else{
							$scope.lstAmeaca.push($scope.lstSwotItens[i]);
						}
					}
				}
			})
		})();

		$scope.AtualizarNotas = function($index, valor){
			$scope.lstOportunidade[$index].valor = valor;
			$scope.CalculaTotalNota();
		}

		$scope.CalculaTotalNota = function(){
			$scope.totalNotas = 0;
			for (var i = 0; i < $scope.lstOportunidade.length; i++) {
				$scope.totalNotas += parseInt($scope.lstOportunidade[i].valor);
			}
		}

		$scope.AtualizarNotasDefesa = function($index, valor){
			$scope.lstAmeaca[$index].valor = valor;
		}

		$scope.trocarForca = function(seta) {
			if (seta == 'P') {
				$scope.ForcaIndex--;
			}else{
				$scope.ForcaIndex++;
			}
		}

		$scope.trocarFraqueza = function(seta) {
			if (seta == 'P') {
				$scope.FraquezaIndex--;
			}else{
				$scope.FraquezaIndex++;
				$scope.nota = 40;
			}
		}

		$scope.salvarNotas = function(){
			if ($scope.ForcaIndex >= 0) {
				$scope.lstAtaque.push({'idForca' : $scope.lstForca[$scope.ForcaIndex].id, 'item ' : $scope.lstOportunidade });
			}

			for (var i = 0; i < $scope.lstOportunidade.length; i++) {
				var data = {
					'idSwotItemS': 0,
					'idSwotItemO': 0,
					'nota' : 0,
					'idUsuario' : 0
				};

				data.idSwotItemS = $scope.lstForca[$scope.ForcaIndex].id;
				data.idSwotItemO = $scope.lstOportunidade[i].id;
				data.nota = $scope.lstOportunidade[i].valor;
				data.idUsuario = $scope.lstOportunidade[i].idUsuario;
				$scope.lstData.push(data);
			}
			core.adicionaNotasSwot($scope.lstData, function(res){
			 if(res.erro > 0){
				 toastr.error(res.mensagem);
			 }else{
				toastr.success(res.mensagem);
			 }
		 });
		}

		$scope.salvarNotasDefesa = function(){
			if ($scope.FraquezaIndex >= 0) {
				$scope.Defesa = {'idFraqueza' : $scope.lstFraqueza[$scope.FraquezaIndex].id, 'item ' : $scope.lstAmeaca }
			}
		}

    $scope.adicionarSwot = function(swot){
       var data = {
        "id": "",
        "idEmpresa": $scope.empresa.id,
        "idUsuario": $rootScope.user.id,
				"titulo": swot.titulo,
        "descricao" : swot.descricao,
        "dtInicio" : swot.dtInicio,
        "dtPrevisto" : swot.dtPrevisto,
        "ptMinimo" : swot.ptMinimo,
        "ptMaximo" : swot.ptMaximo,
				"corte": swot.corte
       };
			 if ($scope.idSwot > 0) {
			 		data.id = $scope.idSwot;
				}

       core.adicionaSwot(data, function(res){
       	if(res.erro > 0){
        	toastr.error(res.mensagem);
        }else{
	       toastr.success(res.mensagem);
				 $scope.inputDisable = 'true';
				 $scope.addShot = 'true';
				 $scope.idSwot = res.objeto;
        }
      });
    }
    $scope.adicionarItemSwot = function(swot, opcao) {
      var data = {
				'idSwot': $scope.idSwot,
				'tpSwot': opcao,
				"idUsuario": $rootScope.user.id,
        'descricao': swot.text
      }
			core.adicionaSwotItem(data, function(res){
			 if(res.erro > 0){
				toastr.error(res.mensagem);
			 }else{
				toastr.success(res.mensagem);
				$scope.lstSwotItens.push(data);
				swot.text = "";
				ngDialog.close();
			 }
		 });
    }

		$scope.openModal = function(opcao) {
			$scope.itemAdd = opcao;
			ngDialog.open({
  			template:'addItemMobile',
  			className: 'ngdialog-theme-default',
  			scope: $scope
      });
		}

		$scope.modalExcluirItem = function(id, $index){
			$scope.idItemSwot = id;
			$scope.indexItem = $index;
			ngDialog.open({
  			template:'excluirItem',
  			className: 'ngdialog-theme-default',
  			scope: $scope
      });

		}

		$scope.modalExcluirSwot = function(id){
			$scope.idSwot = id;
			ngDialog.open({
  			template:'excluirSwot',
  			className: 'ngdialog-theme-default',
  			scope: $scope
      });
		}

		$scope.clickToClose = function(){
			ngDialog.close();
		};

		$scope.excluirSwot = function($index) {
			var data = {
				"id": $scope.idSwot
			}
		 core.excluirSwot(data, function(res){
			 if(res.erro > 0){
				toastr.error(res.mensagem);
			 }else{
				toastr.success(res.mensagem);
				$scope.lstSwot.splice($index, 1);
				ngDialog.close();
			 }
			});
		}

    $scope.excluirItemSwot = function() {
			var data = {
				"id": $scope.idItemSwot
			}
			core.excluirSwotItem(data, function(res){
				if(res.erro > 0){
				 toastr.error(res.mensagem);
				}else{
				 toastr.success(res.mensagem);
				 $scope.lstSwotItens.splice($scope.indexItem, 1);
				 ngDialog.close();
				}
			 });
    }

	}
]);
empresaController.controller('BenchmarkingCtrl', ['$rootScope', '$routeParams', '$scope', '$timeout', '$location', '$http', 'core', 'SERVICE', 'ngDialog',
	function BenchmarkingCtrl($rootScope, $routeParams, $scope,$timeout, $location, $http, core, SERVICE, ngDialog) {
		$scope.imagemUrl = SERVICE.urlImagem;
		$scope.empresa = {'id': $routeParams.empresaId};
		$scope.usuario = {'idUsuario': $routeParams.usuarioId, 'nome': $rootScope.user.nome};
		$scope.amigosForaEmpresa = $rootScope.amigos;
		$scope.solicitacoesPendentes = [];
		$scope.txtTeste = "";
		$scope.loggeduser = $rootScope.user.id;
		$scope.dadosUser = [];
		$scope.userAmigos = [];
		$scope.indexUsuario = 0;
		$scope.perfilUsuario = '';
		$scope.nomeUsuario = '';
		$scope.newPageNumber = 1;
		$scope.varDisable = true;
		$scope.aberta = true;
		$scope.vencida = true;
		$scope.fechada = true;
		$scope.changeColorAberta = true;
		$scope.changeColorVencida = true;


		if ($scope.empresa.id > 0) {
			(function loadInfoEmpresa(){
				core.loadEmpresaUsuario($scope.empresa.id, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.empresaInfo = res.objeto;
						$scope.perfilUser = res.objeto.perfilUsuario
					}
				})
			})();
		}

		(function loadUsuarioEmpresa(){
			core.loadUsuarioEmpresa($scope.empresa.id, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.usuariosNaEmpresa = res.objeto;

					for (var i = 0; i < $scope.usuariosNaEmpresa.length; i++) {
						if($scope.usuariosNaEmpresa[i].perfil === "USUARIO"){
							$scope.usuariosNaEmpresa[i].perfil = "Usuário";
						}
						if($scope.usuariosNaEmpresa[i].perfil === "LIDER"){
							$scope.usuariosNaEmpresa[i].perfil = "Líder";
						}
						if($scope.usuariosNaEmpresa[i].perfil === "ADMINISTRADOR"){
							$scope.usuariosNaEmpresa[i].perfil = "Administrador";
						}
					}
				}
			})
		})();

		/* Função para carregar os amigos do usuario logado */
		(function loadAmigosUser(){
			var data = {
				"idUsuario": $rootScope.user.id,
				"idEmpresa": $scope.empresa.id
			}

			core.loadAmigosUser(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.userAmigos = res.objeto;
				}
			})
		})();

		$scope.labels =["Inovador", "Criativo", "Executor", "Observador", "Planejador", "Participativo"];
		$scope.series = [];
		$scope.dataUsers = [];
		$scope.colours  = ['#97BBCD','#F7464A','#46BFBD','#FDB45C'];
		$scope.options =[
			$scope.responsive = true,
		];

		$scope.loadInfoUsuario = function(usuario){
			var arrayUsers = [];
			var Achou = false;
			var indice = -1;
			var data = {
				"id": usuario.idUsuario
			};

			/* Este bloco verifica se o usuario ja existe e remove o mesmo*/
			for (var i = 0; i < $scope.series.length; i++) {
				if ($scope.series[i] == usuario.nome) {
					Achou = true;
					indice = i;
					break;
				}
			}
			if (Achou == true) {

				$scope.series.splice(indice, 1);
				$scope.dataUsers.splice(indice, 1);

			/* FINAL DO BLOCO - Este bloco verifica se o usuario ja existe e remove o mesmo*/

			}else{
				core.analisePerfilUsuario(data, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						var valueUser = [];
						var inovadorMedia = 0;

						for (var i = 0; i < res.objeto.length; i++) {
							inovadorMedia += res.objeto[i].valor;
						}

						valueUser.push(inovadorMedia / 5);

						for (var i = 0; i < res.objeto.length; i++) {
							valueUser.push(res.objeto[i].valor);
						}

						if ($scope.series.length <= 3) {
							$scope.series.push(usuario.nome);
							$scope.dataUsers.push(valueUser);
							toastr.success(res.mensagem);
						}else{
							toastr.error('Apenas quatro usuários podem ser analisados ao mesmo tempo!');
						}

					}
				});

			};
		$scope.info = [];
		$scope.maxItems= 4;

			};

		(function loadPerfilUsuario(){
			if ($scope.usuario.idUsuario > 0) {
				$scope.loadInfoUsuario($scope.usuario);
			}
		})();

		(function defineSelect(){
			if ($routeParams.empresaId != null) {
				$scope.varDisable = true;
			}else{
				$scope.varDisable = false;
			}
		})();
		$scope.datasets = [];
		$scope.label =[],
		$scope.serie = [],
		$scope.dataUser =  [],
		$scope.colour  = ['#97BBCD','#F7464A','#46BFBD'],
		$scope.option =[
			$scope.responsive = true,
			$scope.datasetFill = false,]
		$scope.lstAberta = [];
		$scope.lstVencida = [];
		$scope.lstFechada = [];

		$scope.loadDataHistoria = function(historia){
			var data = {
				"empresa": $scope.empresa.id
			};

			core.analiseHistoriaEmpresa(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.changeColorAberta = false;
					$scope.changeColorVencida = false;
					$scope.changeColorFechada = false;

					$scope.dataHistoriaEmpresa = res.objeto;
					var _lastMonth = "";

					for (var i = 0; i < $scope.dataHistoriaEmpresa.length; i++) {
						if	(_lastMonth != $scope.dataHistoriaEmpresa[i].descricao){
							_lastMonth = $scope.dataHistoriaEmpresa[i].descricao;
							$scope.lstAberta.push(0);
							$scope.lstVencida.push(0);
							$scope.lstFechada.push(0);
							$scope.label.push($scope.dataHistoriaEmpresa[i].descricao);
						}
						if ($scope.dataHistoriaEmpresa[i].tpStatus == "A") {
							$scope.lstAberta[$scope.lstAberta.length -1] = $scope.dataHistoriaEmpresa[i].valor;

						}else if ($scope.dataHistoriaEmpresa[i].tpStatus == "V"){
							$scope.lstVencida[$scope.lstVencida.length -1] = $scope.dataHistoriaEmpresa[i].valor;

						}else if ($scope.dataHistoriaEmpresa[i].tpStatus == "F"){
							$scope.lstFechada[$scope.lstFechada.length -1] = $scope.dataHistoriaEmpresa[i].valor;
						}
					}

					$scope.dataUser.push($scope.lstAberta);
					$scope.dataUser.push($scope.lstFechada);
					$scope.dataUser.push($scope.lstVencida);
				}

			})
	}();


		$scope.loadGraficoHistoriaAberta = function(historia){
			if ($scope.aberta == true){
				$scope.aberta = false;
				$scope.changeColorAberta = true;
			}else {
				$scope.aberta = true;
				$scope.changeColorAberta = false;
			}
			$scope.loadGraficoHistoria(historia);
		}

		$scope.loadGraficoHistoriaVencida = function(historia){
			if ($scope.vencida == true){
				$scope.vencida = false;
				$scope.changeColorVencida = true;

			}else {
				$scope.vencida = true;
				$scope.changeColorVencida = false;
			}
			$scope.loadGraficoHistoria(historia);
		}

		$scope.loadGraficoHistoriaFechada = function(historia){
			if ($scope.fechada == true){
				$scope.fechada = false;
				$scope.changeColorFechada = true;
			}else {
				$scope.fechada = true;
				$scope.changeColorFechada = false;
			}
			$scope.loadGraficoHistoria(historia);
		}

		$scope.loadGraficoHistoria = function(historia){

			$scope.dataUser = [];

			if ($scope.aberta == true) {
				$scope.dataUser.push($scope.lstAberta);

			}
			if ($scope.vencida == true) {
				$scope.dataUser.push($scope.lstVencida);


			}
			if ($scope.fechada == true) {
				$scope.dataUser.push($scope.lstFechada);

			}

		}


	$scope.labelBrain =[];
	$scope.serieBrain = ['Brainstorm'];
	$scope.dataUserBrain =  [];
	$scope.colourBrain  = ['#97BBCD'];
	$scope.brain = [];
	$scope.optionBrain = [$scope.responsive = true];

	$scope.loadDataBrain = function(empresa){
		var data = {
			"idEmpresa": $scope.empresa.id
		};

		core.analiseBrainstorm(data, function(res){

			if(res.erro > 0){
				toastr.error(res.mensagem);
			}else{
				$scope.dataBrainEmpresa = res.objeto;
				var _lastMonth = "";

			for (var i = 0; i < $scope.dataBrainEmpresa.length; i++) {
					if	(_lastMonth != $scope.dataBrainEmpresa[i].descricao){
						_lastMonth = $scope.dataBrainEmpresa[i].descricao;
						$scope.brain.push(0);
						$scope.labelBrain.push($scope.dataBrainEmpresa[i].descricao);
					}
					if ($scope.dataBrainEmpresa[i].tpStatus == "Selecionado") {
						$scope.brain[$scope.brain.length - 1] = $scope.dataBrainEmpresa[i].valor;
					}
				}
				$scope.dataUserBrain.push($scope.brain);

			}
		})
	}();

}
]);
