'use strict';

/* Controllers */

var noticiaController = angular.module('noticiaController', ['ngCookies' , 'ngMdIcons']);

noticiaController.controller('NoticiaCtrl', ['$rootScope', '$scope', '$location', '$routeParams', '$http', 'SERVICE',
	function NoticiaCtrl($rootScope, $scope, $location, $routeParams, $http, SERVICE) {
		$http({
			url: SERVICE.url + SERVICE.service + SERVICE.noticia + 'busca/noticia'
			, method: 'POST'
			, data: {"id": $routeParams.noticiaId}
		}).then(function(response) {
			$scope.noticia = response.data;
			$('#texto').append(response.data.texto);
			$rootScope.loaderActive = false;
			if(!response.data.ativo){
				$location.path('/noticias');
				toastr.warning("A notícia que você tentou acessar é privada ou não existe.")
			}
		}, function(response){
			$rootScope.loaderActive = false;
		});
	}
]);

noticiaController.controller('NoticiaAddCtrl', ['$rootScope', '$scope', '$location', '$http', 'core', 'SERVICE',
	function NoticiaAddCtrl($rootScope, $scope, $location, $http, core, SERVICE) {
		$scope.autor = 1;
		$scope.test = "";
		$scope.mensagem = "";
		// $scope.salvarNoticia = salvarNoticia;
		$scope.editorOptions = {
			language: 'pt-br'
		};
		$scope.noticia = {
			"titulo": ""
			, "descricao": ""
			, "texto": ""
			, "privado": 0
			, "link": ""
			, "autor": ""
			, "imagem": null
			, "setor": null
			, "categoria": null
		}

		$http.get(SERVICE.url + SERVICE.service + SERVICE.noticia + 'categorias').success(function(data) {
			$scope.categorias = data;
		}).error(function(erro) {
			$scope.categorias = "";
		});

		$http.get(SERVICE.url + SERVICE.service + SERVICE.noticia + 'setores').success(function(data) {
			$scope.setores = data;
		}).error(function(erro) {
			$scope.setores = "";
		});


		$scope.salvarNoticia = function() {
			$rootScope.loaderActive = true;
			var data = $scope.noticia;
			var url = SERVICE.url + SERVICE.service + SERVICE.noticia + 'criar';
			core.cadastrarNoticia(data, url);
		}
	}
]);

noticiaController.controller('NoticiaEditCtrl', ['$rootScope', '$scope', '$routeParams', '$http', 'core', 'SERVICE',
	function NoticiaEditCtrl($rootScope, $scope, $routeParams, $http, core, SERVICE) {
		$scope.autor = 1;
		$scope.test = "";
		$scope.mensagem = "";
		$scope.salvarNoticia = salvarNoticia;
		$rootScope.loaderActive = true;
		$scope.editorOptions = {
			language: 'pt-br'
		};

		$http({
			url: SERVICE.url + SERVICE.service + SERVICE.noticia + 'busca/noticia'
			, method: 'POST'
			, data: {"id": $routeParams.noticiaId}
		}).then(function(response) {
			$scope.noticia = response.data;
			$scope.autor = (response.data.link == "") ? 0 : 1;
			$scope.noticia.setor = response.data.setor.id;
			$scope.noticia.categoria = response.data.categoria.id;
			$scope.noticia.usuario = response.data.usuario.id;
			$rootScope.loaderActive = false;
		}, function(response){
			$rootScope.loaderActive = false;
		});

		$http.get(SERVICE.url + SERVICE.service + SERVICE.noticia + 'categorias').success(function(data) {
			$scope.categorias = data;
		}).error(function(erro) {
			$scope.categorias = "";
		});

		$http.get(SERVICE.url + SERVICE.service + SERVICE.noticia + 'setores').success(function(data) {
			$scope.setores = data;
		}).error(function(erro) {
			$scope.setores = "";
		});

		function salvarNoticia() {
			$rootScope.loaderActive = true;
			var data = $scope.noticia;
			var url = SERVICE.url + SERVICE.service + SERVICE.noticia + 'alterar';
			core.editarNoticia(data, url);
		}
	}
]);

noticiaController.controller('NoticiasCtrl', ['$rootScope', '$scope', '$location', '$http', 'SERVICE',
	function NoticiasCtrl($rootScope, $scope, $location, $http, SERVICE) {

	    $scope.ShowAlert = function () {
	        //if (typeof ($scope.Name) == "undefined" || $scope.Name == "") {
	        //    $window.alert("Please enter your name!");
	        //    return;
	        //}
	        $window.alert("Hello ");
	    }

		$scope.addMenu = false;
		$scope.itemsPerPage = 10;
		$scope.imagemUrl = SERVICE.urlImagem;

		if(!$scope.noticias){
			$http({
				url: SERVICE.url + SERVICE.service + SERVICE.noticia + 'noticias'
				, method: 'GET'
			}).then(function(response) {
				$scope.noticias = response.data;
			}, function(response){
				toastr.warning("Por favor tente novamente mais tarde.")
				toastr.error("Ocorreu um erro ao carregar notícias.")
			});
		}

		$scope.showHideAddMenu = function(){
			$scope.addMenu = !$scope.addMenu;
		}

		$scope.closeAddMenu = function(filtro){
			$scope.addMenu = false;
			if(filtro == 'noticia'){
				$rootScope.busca = 'NOTÍCIA';
			}
			if(filtro == 'edital'){
				$rootScope.busca = 'EDITAL';
			}
		}
	}
]);