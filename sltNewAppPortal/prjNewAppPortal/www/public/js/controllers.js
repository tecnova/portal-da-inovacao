'use strict';

/* Controllers */

var portalControllers = angular.module('portalControllers', ['ngCookies' , 'ngMdIcons']);

portalControllers.controller('AppController', ['$scope', '$rootScope', '$routeParams', 'ngDialog' , 'USER_ROLES', 'AuthService', 'core',
	function($scope, $rootScope, $routeParams, ngDialog, USER_ROLES, AuthService, core){
		$scope.currentUser = null;
		$scope.userRoles = USER_ROLES;
		$scope.isAuthorized = AuthService.isAuthorized;


	    //Verifica o qual o sistema Operacional do Usuário
		if (navigator.userAgent.indexOf('Android') != -1) {
		    $scope.link = "#";
		} else {
		    $scope.link = '';
		}

		/* Função para alterar o perfil do usuario */
		$scope.modalTrocarPerfil = function(usuario, perfil, nome, numPage, $index, $scope) {
			$scope.usuario = usuario;
			$scope.perfilUsuario = perfil;
			$scope.indexUsuario = $index;
			$scope.nomeUsuario = nome;
			$scope.numPage = numPage;

			$scope.newIndiceUsuario = ($scope.indexUsuario) + ($scope.numPage - 1) * $scope.itemPagina;

			ngDialog.open({
				template:'templateId',
				className: 'ngdialog-theme-default',
				scope: $scope});
		}
		/* Função Excluir usuário da lista*/
		$scope.modalExcluirUsuario = function(item, numPage, $index, $scope){
			var data = {
				'usuario': item,
				'pagina': numPage,
				'index': $index,
				'scope': $scope
			}
			ngDialog.open({
				template:'ConfirmTemplateId',
				className: 'ngdialog-theme-default',
				scope: $scope
			});
			$rootScope.data = data;
		};

		$scope.excluirUsuarioCrtl = function(newIndiceUsuario) {
			//recebe o objeto com as informações do controller e do usuario a ser excluido
			var data = $rootScope.data;

			//recebe o scope do controller
			var $scope = data.scope;

			//Verifica de qual tela está vindo solicitação de exclusão do usuário
			switch ($scope.origem) {
				case 'GrupoEmpresa':
					var data = {
						"idUsuario": data.usuario.id,
						"idGrupo": $scope.grupoId
					}
					if ($scope.perfilUser != "USUARIO") {
						core.removeUsuarioGrupoEmpresa(data, function(res){
							if(res.erro > 0){
								toastr.error(res.mensagem);
							}else{
								toastr.success(res.mensagem);
								$scope.usuariosDisponiveis.push($scope.usuariosSelecionados[$scope.newIndiceUsuario]);
								$scope.usuariosSelecionados.splice($scope.newIndiceUsuario, 1);
							}
						});
					}else{
						toastr.error("Somente o administrador ou líder da empresa pode remover um membro do grupo");
					}
					break;
					case 'EquipeHistoria':
						var data = {
							"idUsuario": data.usuario.id,
							"idIdeia": $scope.ideia.id
						};
						core.removerAmigoProjeto(data, function(resposta){
							if(resposta.erro > 0){
								toastr.error(resposta.mensagem);
							}else{
								toastr.success(resposta.mensagem);
								ngDialog.close();
								$scope.usuariosDisponiveis.push($scope.usuariosSelecionados[$scope.newIndiceUsuario]);
								$scope.usuariosSelecionados.splice($scope.newIndiceUsuario, 1);
								$scope.callHistoriaEquipe(0);
							}
						})
						break;
						case '5W2H':
							$scope.usuariosDisponiveis.push($scope.usuariosSelecionados[$scope.newIndiceSugestaoRemove]);
							$scope.usuariosSelecionados.splice($scope.newIndiceSugestaoRemove, 1);
						break;
            case 'EquipeEmpresa':
              var data = {
        				"idEmpresa": $scope.empresa.id,
        				"idAmigoEmpresa": parseInt(data.usuario.idUsuario),
        				"idUsuario": parseInt($rootScope.user.id)
        			};
              core.excluirUsuarioEmpresa(data, function(res){
                if(res.erro > 0){
                  toastr.error(res.mensagem);
                }else{
                  toastr.success(res.mensagem);
                  $scope.usuariosNaEmpresa.splice(indice, 1);
                }
              });
            }
				ngDialog.close();
			}
		$scope.infoUsuario = function(item) {
			$scope.usuario = item;
			ngDialog.open({
				template:'modalUsuarioDescricao',
				className: 'ngdialog-theme-default',
				scope: $scope});
		}

		$scope.adicionarUsuarioCtrl = function(origem, item, newPageNumber, $index, $scope) {
			switch (origem) {
				case 'EquipeHistoria':
					var data = {
						"idUsuario": item,
						"idIdeia": $routeParams.ideiaId
					};
					core.adicionaAmigoEquipeHistoria(data, function(resposta){
						if(resposta.erro > 0){
							toastr.error(resposta.mensagem);
						}else{
							toastr.success(resposta.mensagem);
							$scope.usuariosSelecionados.push($scope.usuariosDisponiveis[$scope.newIndiceUsuario]);
							$scope.usuariosDisponiveis.splice($scope.newIndiceUsuario, 1);
						}
					})
					break;
					case 'GrupoEmpresa':
						var data = {
							"idUsuario": item,
							"idGrupo":  $routeParams.grupoId,
							"idEmpresa": parseInt($scope.empresa.id)
						}
						if ($scope.perfilUser != "USUARIO") {
							core.addUsuarioGrupoEmpresa(data, function(res){
								if(res.erro > 0){
									toastr.error(res.mensagem);
								}else{
									toastr.success(res.mensagem);
									$scope.usuariosSelecionados.push($scope.usuariosDisponiveis[$scope.newIndiceUsuario]);
									$scope.usuariosDisponiveis.splice($scope.newIndiceUsuario, 1);
								}
							});
							}else {
								toastr.error("Somente o administrador ou líder pode adicionar um membro ao grupo");
							}
					break;
					case '5W2H':
						$scope.usuariosSelecionados.push($scope.usuariosDisponiveis[$scope.newIndiceSugestao]);
						$scope.usuariosDisponiveis.splice($scope.newIndiceSugestao, 1);
			}
		}

		//Lista de Brainstorming Controllers
		$scope.modalDescricao = function(descricao){
			$scope.descricaoGlobal = descricao ;
			ngDialog.open({
				template:'modalDescricao',
				className: 'ngdialog-theme-default',
				scope: $scope});
			};

		$scope.setCurrentUser = function(user){
			$scope.currentUser = user;
		}
	}
])

portalControllers.controller('AdmPortalCtrl', ['$rootScope', '$scope', '$location', '$http', 'SERVICE', 'core',
	function AdmPortalCtrl($rootScope, $scope, $location, $http, SERVICE, core) {
	}
]);

portalControllers.controller('AdmPortalUsuariosCtrl', ['$rootScope', '$scope', '$location', '$http', 'SERVICE', 'core',
	function AdmPortalUsuariosCtrl($rootScope, $scope, $location, $http, SERVICE, core) {
		//paginação
		$scope.itemsPerPage = 10;
		$scope.permissaoAlterada = 0;
		$scope.mudarPermissao = mudarPermissao;
		$scope.alterouPermissao = alterouPermissao;
		$scope.usuarios = [];

		$http.get(SERVICE.url + SERVICE.service + SERVICE.usuario + 'perfis').success(function(data) {
			$scope.perfis = data;
		}).error(function(er){
			toastr.error("Ocorreu um erro ao buscar os perfis de usuários.");
		})

		$http.get(SERVICE.url + SERVICE.service + SERVICE.usuario + 'usuarios').success(function(data) {
			if($scope.perfis.length > 0){
				angular.forEach(data, function(usu){
					//Quantidade de perfis que o usuário tem.
					var perfisSelecionados = usu.perfis.length;
					angular.forEach($scope.perfis, function(perfil, key){
						//verifica se o usuário já possui este perfil
						var checked = false;
						for (var i = perfisSelecionados - 1; i >= 0; i--) {
							if(usu.perfis[i].id == perfil.id){
								usu.perfis[i].ticked = true;
								checked = true;

								if(usu.perfis[i].sigla == "US"){
									usu.perfis[i].disabled = true;
								}else{
									usu.perfis[i].disabled = false;
								}
							}
						}
						//verifica se o usuário já possui este perfil
						if(!checked){
							usu.perfis.push({
								"id": perfil.id
								,"nome": perfil.nome
								, "sigla": perfil.sigla
								, "ticked": false
								, "disabled": false
							});
						}
					});
					$scope.usuarios.push(usu);
				});
			}
		}).error(function(er){
			toastr.error("Ocorreu um erro ao buscar os usuários.");
		})

		function mudarPermissao(usuario, idPerfil){
			var index;
			var dt = {
				"id": usuario.id
				, "perfis": []
			};

			angular.forEach(usuario.perfis, function(perfil, i){
				if(perfil.id == idPerfil){
					index = i;
					perfil.ticked = !perfil.ticked;
				}

				if(perfil.ticked == true){
					dt.perfis.push({
						"id": perfil.id
						, "nome": perfil.nome
						, "sigla": perfil.sigla
					})
				}
			})

			$http.post(SERVICE.url + SERVICE.service + SERVICE.usuario + 'altera/perfil', dt).success(function(data) {
				toastr.success("Permissões alteradas com sucesso!");
			}).error(function(er){
				usuario.perfis[index].ticked = !usuario.perfis[index].ticked;
				toastr.error("Ocorreu um erro ao alterar as permissões!");
				toastr.warning("Favor verficar sua conexão com a internet!");
			})
		}

		function alterouPermissao(){
			$scope.permissaoAlterada = 1;
		}
	}
]);

portalControllers.controller('IndexCtrl', ['$rootScope', '$scope', '$location', '$http', 'SERVICE',
	function IndexCtrl($rootScope, $scope, $location, $http, SERVICE) {
		$scope.imagemUrl = SERVICE.urlImagem;

		if(!$scope.noticias){
			$http({
				url: SERVICE.url + SERVICE.service + SERVICE.noticia + 'noticias'
				, method: 'GET'
			}).then(function(response) {
				$scope.noticias = response.data;
			}, function(response){
				toastr.warning("Por favor tente novamente mais tarde.");
				toastr.error("Ocorreu um erro ao carregar notícias.");
			});
		}

		$scope.closeArticle = function(id) {
			$('#con-principal article').removeClass('open-article');
		}

	}
]);

portalControllers.controller('CadastroCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'SERVICE', 'core',
	function CadastroCtrl($rootScope, $routeParams, $scope, $location, $http, SERVICE, core) {
		$scope.message = "Página de cadastro.";
		$scope.usuario = {
			nome: ''
		  , senha: ''
		}
		$scope.usuario.email = ($routeParams.email) ? $routeParams.email : '';
		$scope.cadastrar = function() {
			$rootScope.loaderActive = true;
			var data = $scope.usuario;
			var url = SERVICE.url + SERVICE.service + SERVICE.usuario + 'criar';
			if(data.senha == $scope.confirmaSenha){
				core.salvarUsuario(data, url);
			}else{
				toastr.error("As senhas não conferem.")
				$rootScope.loaderActive = false;
			}
		}
	}
]);

portalControllers.controller('LoginCtrl', ['$scope', '$location', '$rootScope', '$cookies',
	function LoginCtrl($scope, $location, $rootScope, $cookies) {
		$scope.signedIn = false;
		$scope.credentials = {
			email: ''
			, password: ''
		}

		$scope.logarEmail = function() {
			$scope.credentials.tipo = 'email';
			$rootScope.login($scope.credentials);
		};

		$scope.sairGoogle = function() {
			$scope.$apply(function(){
				sairPlus();
			})
		};

		$scope.sairFace = function() {
			FB.logout();
		};

		function sairPlus(){
			gapi.auth.signOut();
		}
	}
]);

portalControllers.controller('PerfilCtrl', ['$rootScope', '$scope', '$location', '$http', 'SERVICE', '$cookies',
	function PerfilCtrl($rootScope, $scope, $location, $http, SERVICE, $cookies) {
		$scope.apagaNoticia = apagaNoticia;
		$scope.filtrarNoticiasAtivas = filtrarNoticiasAtivas;
		$scope.editandoNome = 0;
		$scope.editandoSenha = 0;
		$scope.toogleNome = toogleNome;
		$scope.toogleSenha = toogleSenha;
		$scope.inativarConta = inativarConta;
		$scope.alterarUsuario = alterarUsuario;
		$scope.filtroNoticia = 0;
		$scope.noticias = [];
		$scope.noticiasRetornadas = [];
		$rootScope.user.password = "1234";
		//paginação
		$scope.itemsPerPage = 10;

		$http({
			url: SERVICE.url + SERVICE.service + SERVICE.noticia + 'noticias'
			, method: 'GET'
		}).then(function(response) {
			$scope.noticiasRetornadas = response.data;
			filtrarNoticiasAtivas($scope.filtroNoticia);
		}, function(response){
			toastr.error("Ocorreu um erro inesperado ao buscar as notícias.");
		});

		function apagaNoticia(id, $index){
			var resul = confirm("Você realmente deseja apagar esta notícia? ");
			if(resul){
				$rootScope.loaderActive = true;
				$http({
					url: SERVICE.url + SERVICE.service + SERVICE.noticia + 'inativar'
					, data: {"id": parseInt(id), 'usuario': {"id": $rootScope.user.id}}
					, method: 'POST'
				}).then(function(response) {
					if(response.erro){
						toastr.error(response.data.mensagem);
					}else{
						$scope.noticias.splice($index, 1);
						angular.forEach($scope.noticiasRetornadas, function(val, key){
							if(val.id == id){
								$scope.noticiasRetornadas[key].ativo = false;
							}
						});
						toastr.success(response.data.mensagem);
					}
					$rootScope.loaderActive = false;
				}, function(response){
					toastr.error("Ocorreu um erro ao  apagar a notícia.");
					$rootScope.loaderActive = false;
				});
			}
		}

		function alterarUsuario(tipo, $event){
			$rootScope.loaderActive = true;
			var data = {'id': parseInt($rootScope.user.id)};
			switch(tipo){
				case 'nome':
					data.nome = $rootScope.user.nome;
					toogleNome();
					$cookies.put('user_name', $rootScope.user.nome);
					break;
				case 'senha':
					data.senha = $rootScope.user.password;
					toogleSenha();
					break;
				case 'tema':
					$rootScope.user.theme = angular.element($event.target).data('id');
					data.perfil_tema = $rootScope.user.theme;
					$cookies.put('user_theme', $rootScope.user.theme);
					break;
			}
			$http({
				url: SERVICE.url + SERVICE.service + SERVICE.usuario + 'alterar'
				, data: data
				, method: 'POST'
			}).then(function(response) {
				if(response.erro){
					toastr.error(response.data.mensagem);
				}else{
					toastr.success(response.data.mensagem);
				}
				$rootScope.loaderActive = false;
			}, function(response){
				toastr.error("Ocorreu um erro ao realizar a alteração.");
				$rootScope.loaderActive = false;
			});
		}



		function filtrarNoticiasAtivas(){
			$scope.noticias = [];
			$scope.filtroNoticia = !$scope.filtroNoticia;
			angular.forEach($scope.noticiasRetornadas, function(val){
				if(val.ativo == $scope.filtroNoticia){
					$scope.noticias.push(val);
				}
			});
		}

		function toogleNome(){
			$scope.editandoNome = !$scope.editandoNome;
		}

		function toogleSenha(){
			$scope.editandoSenha = !$scope.editandoSenha;
		}
	}
]);

portalControllers.controller('PesquisaCtrl', ['$rootScope', '$scope', '$location', '$http', '$routeParams', 'core',
	function PesquisaCtrl($rootScope, $scope, $location, $http, $routeParams, core) {
		core.search({'pesquisa': $routeParams.busca, 'id': parseInt($rootScope.user.id)}, function(response){
			if(!response.erro){
				$scope.resultado = response;
			}else{
				toastr.error(response.mensagem);
			}
		})

		$scope.adicionarAmigo = function(id){
			core.addAmigo(id, function(resposta){
				if(resposta.erro){
					toastr.error(resposta.mensagem);
				}else{
					toastr.success(resposta.mensagem);
					angular.forEach($scope.resultado, function(data, ind){
						if(data.id == id){
							$scope.resultado.splice(ind, 1);
							return;
						}
					})
				}
			})
		}
	}
]);

portalControllers.controller('QuestoesCtrl', ['$rootScope', '$scope', '$location', '$http',
	function NoticiasCtrl($rootScope, $scope, $location, $http) {

		if(!$scope.questoes){
			$http.get('../public/js/questoes.json').success(function(data) {
				$scope.questoes = data;
			})
		}
	}
]);
