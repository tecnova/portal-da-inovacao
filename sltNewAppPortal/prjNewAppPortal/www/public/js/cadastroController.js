'use strict';

var senhaController = angular.module('senhaController', ['ngCookies','ngMdIcons']);

senhaController.controller('senhaController', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE',
	function senhaController($rootScope, $routeParams, $scope, $location, $http, core, SERVICE) {

		$scope.alteraSenha = function(usuario){
				var data = {
					"senha": usuario.senha,
					"novaSenha": usuario.novaSenha,
					"id": parseInt($rootScope.user.id)
				};
			if (usuario.novaSenha == usuario.senhaRep) {
				core.alteraSenha(data, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						toastr.success(res.mensagem);
						$location.path('/usuario/painel');
					}
				});
			}else{
				toastr.error("As senhas não correspondem!");
			}
		}

		$scope.emailSenha = function(email){
			var data = {
				'email': email
			};
			core.enviarEmailRecuperaSenha(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
				}
			});
		}
		$scope.enviaNovaSenha = function(usuario){
			var data = {
				"email": usuario.email,
				"link" : $routeParams.linkMd5,
				"senha": usuario.novaSenha
			};
			if (usuario.novaSenha == usuario.repSenha) {
				core.criarNovaSenha(data, function(res){
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						toastr.success(res.mensagem);
						$location.path('../#login');
					}
				})
			}else{
				alert("As senhas não conferem!");
			};
		};
	}

]);

senhaController.controller('gameController', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE', 'ngDialog', '$cookies',
	function gameController($rootScope, $routeParams, $scope, $location, $http, core, SERVICE, ngDialog, $cookies) {

		$scope.badges = [];
		$scope.insignias = [];
		$scope.trofeu = [];
		$scope.lstBadgesAcoes = [];
		$scope.lstBadgesDatas = [];

		(function loadBadges(){
			core.loadBadges(function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.badges = res;
				}
			})
		})();

		(function loadInsignias(){
			core.loadInsignias(function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.insignias = res;
				}
			})
		})();

		(function loadCookies(){
			$scope.CookiesName = "galeriaHelp";
			$scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

			if ($scope.showHelp == null) {
					$cookies.put($scope.CookiesName, "true");
					$scope.showHelp = "true";
			}
		})();

		$scope.help = function() {
			var now = new Date(),
			exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
			$cookies.put("" + $scope.CookiesName + "", 'false', {expires: exp});
			$scope.showHelp = 'false';
		}

		$scope.clickToOpen = function(trofeu){
			$scope.trofeu = trofeu;

			ngDialog.open({
				template:'templateId',
				className: 'ngdialog-theme-default',
				scope: $scope});
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};
	}
]);
