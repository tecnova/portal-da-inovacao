'use strict';

/* Controllers */

var historiasController = angular.module('historiasController', ['ngCookies','ngDialog','ngMdIcons'] );

historiasController.controller('Controller5w2h', ['$routeParams', '$scope', '$rootScope', 'core', 'ngDialog', 'SERVICE',
    function Controller5w2h($routeParams, $scope, $rootScope, core, ngDialog, SERVICE){
      $scope.idHistoria = $routeParams.ideiaId;
      $scope.imagemUrl = SERVICE.urlImagem;
		  $scope.idEmpresa = $routeParams.empresaId;
      $scope.idBrainstormingAtual = '';
      $scope.botaoAdicionar = true;
      $scope.botaoInfo = true;
    	$scope.lstBrainstorming = [];
      $scope.lstBrainstormingUsuario = [];
      $scope.posBrainstorming = -1;
      $scope.idbrGlobal = 0;
      $scope.idacaoGlobal = 0;
      $scope.indexAcao = 0;
      $scope.lstTarefa = [];
      $scope.item = [];
      $scope.usuariosSelecionados = [];
      $scope.usuariosDisponiveis = [];
      $scope.itensPagina = 5;
      $scope.newPageNumberAdd = 1;
      $scope.newPageNumberDelete = 1;
      $scope.btnAddAcao = false;
      $scope.idPlano = $routeParams.planoId;
      $scope.globalUserDisponivel = [];
      $scope.lstPlanoAcao = 0;
      var agora = new Date();
      $scope.item.dtInicio = agora;
      $scope.origem = '5W2H';

      (function loadPlanoAcao() {
        var data = {
          'idHistoria': $scope.idHistoria
        }
        core.loadPlanoAcao(data, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.lstPlanoAcao = res.objeto;
            for (var i = 0; i < $scope.lstPlanoAcao.length; i++) {
              if ($scope.lstPlanoAcao[i].id == $scope.idPlano) {
                $scope.tituloPlano = $scope.lstPlanoAcao[i].assunto;
                $scope.ataquePlano = $scope.lstPlanoAcao[i].ataque;
                $scope.descPlano = $scope.lstPlanoAcao[i].detalhamento;
              }
            }
          }
        })
      })();

    (function loadHistoriaEmpresa(){
			var data = {
				"id": $scope.idHistoria,
				"idUsuario": parseInt($rootScope.user.id),
				"idEmpresa": $scope.idEmpresa
			};
			core.loadHistoriasData(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.historia = res.objeto;
				}
			})
		})();

    (function loadHistoriaEquipe(){
			var data = {
				"idIdeia": $scope.idHistoria
			};
			core.loadHistoriaEquipe(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
          $scope.lstUsuarioDisponivel = angular.copy(res.objeto.lstUsuarioIdeia);
          $scope.globalUserDisponivel = angular.copy(res.objeto.lstUsuarioIdeia);
          $scope.usuariosDisponiveis = res.objeto.lstUsuarioIdeia;
				}
			});
		})();

    if ($scope.idEmpresa > 0) {
      (function loadInfoEmpresa(){
        core.loadEmpresaUsuario($scope.idEmpresa, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.empresaInfo = res.objeto;
          }
        })
      })();
    }

    $scope.openModalAcao = function(idbr, idacao, $index){
			$scope.idbrGlobal = idbr;
      $scope.idacaoGlobal = idacao;
      $scope.indexAcao = $index;

  		ngDialog.open({
  			template:'excluirAcaoDialog',
  			className: 'ngdialog-theme-default',
  			scope: $scope
      });
		};

    $scope.clickToClose = function(){
			ngDialog.close();
		};

		(function loadProjetoAcao(){
			var data = {
				"id": $scope.idPlano
			};

			core.loadProjetoAcao(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
          $scope.lstTarefa = res.objeto;
          $scope.btnAddAcao = true;
				}
			});
    })();

    $scope.NovaAcao = function(){
      $scope.usuariosSelecionados = [];
      $scope.item = [];
    }

    $scope.adicionarUsuario = function(id, newPageNumber, indice) {
      $scope.newIndiceSugestao = indice + (newPageNumber - 1) * $scope.itensPagina;
      $scope.adicionarUsuarioCtrl($scope.origem, id, newPageNumber, indice, $scope);
    }

    $scope.excluirUsuario = function(item, numPage, indice) {
      $scope.newIndiceSugestaoRemove = indice + (numPage - 1) * $scope.itensPagina;
      $scope.modalExcluirUsuario(item, numPage, indice, $scope);
    }

    $scope.TarefaAcaoSalvar = function(item){
      $scope.historia.dataLimite = new Date($scope.historia.dataLimite);
      $scope.item.dtInicio = new Date($scope.item.dtInicio);
      $scope.item.dtTermino = new Date($scope.item.dtTermino);

      if ($scope.item.dtTermino >= $scope.item.dtInicio) {
        if ($scope.item.dtTermino <= $scope.historia.dataLimite) {
          var data = {
            "id" : $scope.idPlano,
            "acao" : $scope.item.acao,
            "motivo" : $scope.item.motivo,
            "local" : $scope.item.local,
            "dtInicio" : $scope.item.dtInicio,
            "dtTermino" : $scope.item.dtTermino,
            "detalhamento" : $scope.item.detalhamento,
            "valor" : $scope.item.valor,
            "lstUsuarioAcao" : $scope.usuariosSelecionados
          }

          if ($scope.item.idprojeto5w2hitem > 0) {
            $scope.lstTarefa[$scope.indexGlobal].acao = $scope.item.acao;
            $scope.lstTarefa[$scope.indexGlobal].motivo = $scope.item.motivo;
            $scope.lstTarefa[$scope.indexGlobal].local = $scope.item.local;
            $scope.lstTarefa[$scope.indexGlobal].dtInicio = $scope.item.dtInicio;
            $scope.lstTarefa[$scope.indexGlobal].dtTermino = $scope.item.dtTermino;
            $scope.lstTarefa[$scope.indexGlobal].detalhamento = $scope.item.detalhamento;
            $scope.lstTarefa[$scope.indexGlobal].valor = $scope.item.valor;
            $scope.lstTarefa[$scope.indexGlobal].lstUsuarioAcao = $scope.item.lstUsuarioAcao;

            //adicionando na variavel data o id da Ação para a alteração
            data.idprojeto5w2hitem = $scope.item.idprojeto5w2hitem;
            data.id = 0;

            core.TarefaAcaoEditar(data, function(res){
              if(res.erro > 0){
                toastr.error(res.mensagem);
              }else{
                toastr.success(res.mensagem);
                $scope.showForm();
                }
              });
          } else {
              core.TarefaAcaoSalvar(data, function(res){
                if(res.erro > 0){
                  toastr.error(res.mensagem);
                }else{
                  toastr.success(res.mensagem);
                  $scope.lstTarefa.push(
                    {
                      "id" : $scope.idPlano,
                      "idprojeto5w2hitem": res.objeto,
                      "acao" : $scope.item.acao,
                      "motivo" : $scope.item.motivo,
                      "local" : $scope.item.local,
                      "dtInicio" : $scope.item.dtInicio,
                      "dtTermino" : $scope.item.dtTermino,
                      "detalhamento" : $scope.item.detalhamento,
                      "valor" : $scope.item.valor,
                      "lstUsuarioAcao" : $scope.usuariosSelecionados
                    });
                  $scope.showForm();
                  $scope.lstUsuarioDisponivel = angular.copy($scope.globalUserDisponivel);
                  }
                });
            }
        }else{
          toastr.error("A data final não pode ser maior que a data limite da história!");
        }
      }else{
        toastr.error("A data final não pode ser menor que a data inicial");
      }
    }

    $scope.CarregaTarefa = function($index){
        $scope.item = angular.copy($scope.lstTarefa[$index]);
        $scope.usuariosSelecionados = $scope.item.lstUsuarioAcao;
        for (var i = 0; i < $scope.usuariosDisponiveis.length; i++) {
            for (var j = 0; j < $scope.usuariosSelecionados.length; j++) {
              if ($scope.usuariosDisponiveis[i].id == $scope.usuariosSelecionados[j].id) {
                $scope.usuariosDisponiveis.splice(i, 1);
              }
            }
        }
        $scope.indexGlobal = $index;
    }

		$scope.excluirAcao = function(planoId, idprojeto5w2hitem, $index){
			var data = {
				"id": planoId,
				"idprojeto5w2hitem": parseInt(idprojeto5w2hitem)
			}
			core.excluirAcao(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.lstTarefa.splice($index, 1);
					toastr.success(res.mensagem);
          ngDialog.close();
				}
			});
		}

		$scope.clickToOpen = function(idAcao){
			ngDialog.open({
				template:'templateId',
				className: 'ngdialog-theme-default',
				scope: $scope});
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};

		$scope.showForm = function(){
			var element = angular.element('#form');
			var actions =  angular.element('#actions');
			if(element.hasClass('showForm')){
				element.removeClass('showForm');
				actions.removeClass('hide-actions');
			}else{
				element.addClass('showForm');
				actions.addClass('hide-actions');
			}
		}
	}
]);

historiasController.controller('AtividadesCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE', 'ngDialog','$cookies',
	function AtividadesCtrl($rootScope, $routeParams, $scope, $location, $http, core, SERVICE, ngDialog , $cookies) {
		$scope.atividades = [0];
		$scope.idAtividade = [];
		$scope.idAcao = '';
		$scope.filter = '+assunto';
		$scope.atv = [];

		(function loadAtividadesUsuario(){
			var data = {
				"idusuario": parseInt($rootScope.user.id)
			};

			core.loadAcoesProjeto(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.atividades = res.objeto;
				}

			})
		})();

    (function loadCookies(){
      $scope.CookiesName = "atividadesHelp";
      $scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

      if ($scope.showHelp == null) {
          $cookies.put($scope.CookiesName, "true");
          $scope.showHelp = "true";
      }
    })();

    $scope.help = function() {
      var now = new Date(),
      exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
      $cookies.put("" + $scope.CookiesName + "", 'false', {expires: exp});
      $scope.showHelp = 'false';
    }

		$scope.clickToOpen = function(atv){

			$scope.atv = atv;

			ngDialog.open({
				template:'templateId',
				className: 'ngdialog-theme-default',
				scope: $scope});
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};

		$scope.salvarFeedbackProjeto = function(feedback, idProjeto){
			var agora = new Date();
			var data = {
				"feedback": feedback,
				"dtFeedback": agora,
				"idprojeto5w2hitem": parseInt(idProjeto),
				"idusuario": parseInt($rootScope.user.id)
			}
			core.salvarFeedbackProjeto(data, function(res){
				if (res.erro > 0) {
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
				}
			ngDialog.close();
			})
		};

		$scope.orderFiltro = function(valor){
			$scope.filter = valor;
		}
	}
]);

historiasController.controller('BrainstormCtrl', ['$routeParams', '$scope', '$rootScope', 'core', 'ngDialog' , '$cookies',
	function BrainstormCtrl($routeParams, $scope, $rootScope, core, ngDialog, $cookies){
    $scope.comentariosBrainstorm = [];
		$scope.brainstorms = [0];
    $scope.usuarioSugestao = 0;
    $scope.globalIdBrainstormingIndex = 0;
		$scope.idEmpresa = $routeParams.empresaId;
    $scope.globalIdBrainstorming = 0;
    $scope.loggeduser = $rootScope.user.id;
		$scope.idBrainstormGlobal = 0;
    $scope.itensPagina = 6;
    $scope.tipoDoc = $routeParams.tipoDoc;
    $scope.newPageNumber = 1;
    $scope.filtroIdeia = 0;
		$scope.historia = {
			"id": $routeParams.ideiaId
		};

    if ($scope.idEmpresa > 0) {
      (function loadInfoEmpresa(){
        core.loadEmpresaUsuario($scope.idEmpresa, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.empresaInfo = res.objeto;
          }
        })
      })();
    }

		(function loadHistoriaEmpresa(){
			var data = {
				"id": $scope.historia.id,
				"idUsuario": $scope.loggeduser,
				"idEmpresa": $scope.idEmpresa
			};

			core.loadHistoriasData(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.historia = res.objeto;
				}
			})
		})();

		(function loadBrainstorm(){
			var data = {
        	"idUsuario": $scope.loggeduser,
			    "idHistoria": parseInt($scope.historia.id)
			};
			core.loadBrainstorming(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
         	$scope.brainstorms = res.objeto;
				}
			})
		})();

    if ($scope.tipoDoc != '') {
      (function loadDocumentos(){
        var data = {
          "tpDocumento": $routeParams.tipoDoc,
          "idBrainstorming": $routeParams.brainstormId
        }
        core.loadDocumentos(data, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.documento = res.objeto[0];
          }
        })
      })();
    }

    (function loadCookies(){
      $scope.CookiesName = "brainstormHelp";
      $scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

      if ($scope.showHelp == null) {
          $cookies.put($scope.CookiesName, "true");
          var cookie = $cookies.get('brainstormHelp');
          $scope.showHelp = "true";
      }
    })();

    (function loadBrainstormingGrupo(){
      var data = {
        "idHistoria": parseInt($scope.historia.id)
      };
      core.loadBrainstormingGrupo(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          $scope.lstGrupo = res.objeto;
        }
      })
    })();

    $scope.addBrainstormGrupo = function(brainstormInfo, grupoId){
      brainstormInfo.idGrupoBrainstorming = grupoId;

			var data = {
				"id": brainstormInfo.id,
				"idGrupoBrainstorming": grupoId
			}

      core.editaGrupoBrainstorming(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
        }
      });

    }

    $scope.addGrupoBrainstorming = function(titulo){
			var data = {
				"idHistoria": parseInt($scope.historia.id),
				"descricao": titulo
			}
			core.adicionaBrainstormingGrupo(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
					$scope.lstGrupo.push({"id": res.objeto, "idHistoria": data.idHistoria, 'descricao': titulo});
          ngDialog.close();
				}
			});
		}

    $scope.defineFiltro = function(idGrupo) {
      $scope.filtroIdeia = idGrupo;
    }

    $scope.help = function() {
      var now = new Date(),
      exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
      $cookies.put("" + $scope.CookiesName + "", "false", {expires: exp});
      $scope.showHelp = "false";
    }

		$scope.addDocumentos = function(tpDocumento, documento){
      var data = {
        "tpDocumento": tpDocumento,
        "documento": documento,
        "idBrainstorming": $routeParams.brainstormId
      };
      core.addDocEmpresa(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
        }
      });
		}

		$scope.addBrainstorm = function(brainstorm){
			var data = {
				"idUsuario": $scope.loggeduser,
				"idHistoria": $scope.historia.id,
				"descricao": brainstorm.addDescricao
			}
			core.adicionaBrainstorming(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
          var agora = new Date();
          $scope.brainstorms.push({'id': res.objeto, 'descricao': brainstorm.addDescricao, 'dataCriacao': agora.getTime(), 'boCurtir': 'N', 'qtCurtir': 0, 'qtComentario': 0 });
					toastr.success(res.mensagem);
          toastr.info('Você ganhou 25 pontos por esta ideia!');
          ngDialog.close();
				}
			});
		}

    $scope.alteraBrainstorm = function(brainstormId, brainstormText, $index){
      var data = {
        "id": parseInt(brainstormId),
        "descricao": brainstormText,
        "idUsuario": $scope.loggeduser
      }
      if (brainstormId > 0) {
        core.alteraBrainstorming(data, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            toastr.success(res.mensagem);
          }
        });
      } else {
        $scope.brainstorms[$index].descricao = brainstormText;
      }
    }

		$scope.curtirBrainstorming = function(brainstormId, newPageNumber, indice){
			var data = {
				"id": brainstormId,
				"idUsuario": $scope.loggeduser
			}

      $scope.newIndiceSugestao = indice + (newPageNumber - 1) * $scope.itensPagina;

      if (brainstormId > 0) {
        core.curtirBrainstorming(data, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            toastr.success(res.mensagem);

            if ($scope.brainstorms[$scope.newIndiceSugestao].boCurtir == 'N') {
              $scope.brainstorms[$scope.newIndiceSugestao].boCurtir = 'S'
            } else if ($scope.brainstorms[$scope.newIndiceSugestao].boCurtir == 'S') {
              $scope.brainstorms[$scope.newIndiceSugestao].boCurtir = 'N'
            }
            $scope.brainstorms[$scope.newIndiceSugestao].qtCurtir = res.objeto;
          }
        });
      }else{
        toastr.error("Algo inesperado ocorreu ao curtir o brainstorming!");
      }
		}

    $scope.removeBrainstormGrupo = function(brainstormInfo){
      brainstormInfo.idGrupoBrainstorming = 0;

      var data = {
        "id": parseInt(brainstormInfo.id),
        "idGrupoBrainstorming": null
      }

      core.editaGrupoBrainstorming(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
        }
      });
    }

		$scope.excluirBrainstorming = function(brainstormId, newPageNumber){
			if($scope.clickToOpen){
          var data = {
            "id": $scope.idBrainstormGlobal,
            "idUsuario": $scope.loggeduser
          };

          $scope.newIndiceSugestao = $scope.indexBrainstormGlobal + (newPageNumber - 1) * $scope.itensPagina;

          core.excluirBrainstorming(data, function(res){
            if(res.erro > 0){
              toastr.error(res.mensagem);
            }else{
              toastr.success(res.mensagem);
              $scope.brainstorms.splice($scope.newIndiceSugestao, 1);
            }
            ngDialog.close();
          });
			}
		}

    $scope.excluirGrupo = function(idGrupo, indice){
      var data = {
        "id": idGrupo
      }
      core.excluirBrainstormingGrupo(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
          $scope.lstGrupo.splice(indice, 1);
        }
      });
    }

    $scope.modalAddIdeia = function(){
      ngDialog.open({
        template:'idAddIdeia',
        className: 'ngdialog-theme-default',
        scope: $scope});
    };

    $scope.modalAddGrupo = function(){
      ngDialog.open({
        template:'idAddGrupo',
        className: 'ngdialog-theme-default',
        scope: $scope});
    };

		$scope.clickToOpen = function(id, newPageNumber, $index){
			$scope.idBrainstormGlobal = id;
      $scope.indexBrainstormGlobal = $index;
      $scope.newPageNumber = newPageNumber;


		ngDialog.open({
			template:'templateId',
			className: 'ngdialog-theme-default',
			scope: $scope});

		};

    $scope.openComentarios = function(idbrainstorming, newPageNumber, $index){
      var data = {
        "idBrainstorming" : idbrainstorming
      }

      $scope.globalIdBrainstorming = idbrainstorming;
      $scope.globalIdBrainstormingIndex = $index;
      $scope.newPageNumber = newPageNumber;

      core.loadBrainstormingComentarios(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          $scope.comentariosBrainstorm = res.objeto;
          for (var i = 0; i < $scope.comentariosBrainstorm.length; i++) {
            $scope.comentariosBrainstorm[i].dtComentario = new Date($scope.comentariosBrainstorm[i].dtComentario);
          }
        }
      });

  		ngDialog.open({
  			template:'idComentarios',
  			className: 'ngdialog-theme-default',
  			scope: $scope
      });
		};

    $scope.salvarComentario = function(com, newPageNumber){
      var agora = new Date();

      var data = {
        "idBrainstorming": $scope.globalIdBrainstorming,
        "idUsuario": $scope.loggeduser,
        "comentario": com.comentarioText
      };

      $scope.newIndiceSugestao = $scope.globalIdBrainstormingIndex + (newPageNumber - 1) * $scope.itensPagina;

      core.salvarComentarioBrainstorming(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
          $scope.comentariosBrainstorm.push({'id': res.objeto, 'comentario': com.comentarioText, 'dtComentario': agora});
          $scope.brainstorms[$scope.newIndiceSugestao].qtComentario = $scope.brainstorms[$scope.newIndiceSugestao].qtComentario + 1;
          com.comentarioText = '';
        }
      });
		};

    $scope.excluirBrainstormingComentario = function(idComentario, $index){
      var data = {
        "id": idComentario,
        "idUsuario": $scope.loggeduser,
      };
      core.excluirBrainstormingComentario(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
          $scope.comentariosBrainstorm.splice($index, 1);
          $scope.brainstorms[$scope.globalIdBrainstormingIndex].qtComentario = $scope.brainstorms[$scope.globalIdBrainstormingIndex].qtComentario - 1;
        }
      });
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};
    $scope.teste = function() {
      alert("Ok");
    }

    }
]);

historiasController.controller('BrainstormGrupoCtrl', ['$routeParams','$location', '$scope', '$rootScope', 'core', 'ngDialog' ,
	function BrainstormGrupoCtrl($routeParams, $location, $scope, $rootScope, core, ngDialog){
    $scope.lstBrainstormingDisponivel = [];
    $scope.lstBrainstormingGrupo = [];
    $scope.grupoDescricao = '';
    $scope.descricaoHistoria = '';
    $scope.loggeduser = $rootScope.user.id;
    $scope.lstGrupo = [0];
    $scope.idEmpresa = $routeParams.empresaId;
    $scope.idGrupo = $routeParams.grupoId;
    $scope.itensPagina = 10;
    $scope.newPageNumber = 1;
    $scope.descricaoGlobal = '';
    $scope.newPageNumberTemas = 1;
    $scope.botaoAdicionar = true;
    $scope.historia = {
			"id": $routeParams.ideiaId
		};

    if ($scope.idEmpresa > 0) {
      (function loadInfoEmpresa(){
        core.loadEmpresaUsuario($scope.idEmpresa, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.empresaInfo = res.objeto;
          }
        })
      })();
    }

    (function loadBrainstormingGrupo(){
      var data = {
        "idHistoria": parseInt($scope.historia.id)
      };
      core.loadBrainstormingGrupo(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          $scope.lstGrupo = res.objeto;
        }
      })
    })();

    (function loadHistoriaEmpresa(){
			var data = {
				"id": $scope.historia.id,
				"idUsuario": $scope.loggeduser,
				"idEmpresa": $scope.idEmpresa
			};
			core.loadHistoriasData(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.historia = res.objeto;
				}
			})
		})();

    if ($routeParams.grupoId > 0) {
      (function loadBrainstormingNoGrupo(){
        var data = {
          "idHistoria": parseInt($scope.historia.id),
          "id": $scope.idGrupo
        }
        core.loadBrainstormingNoGrupo(data, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.lstBrainstormingDisponivel = res.objeto.lstBrainstormingDisponivel;
            $scope.lstBrainstormingGrupo = res.objeto.lstBrainstorming;
            $scope.grupoDescricao = res.objeto.descricao;
            $scope.descricaoHistoria = res.objeto.historia;

          }
        })
      })();
    }

    $scope.adicionaBrainstormingGrupo = function(titulo){
			var data = {
				"idHistoria": parseInt($scope.historia.id),
				"descricao": titulo.grupo
			}
			core.adicionaBrainstormingGrupo(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
					$scope.lstGrupo.push({"id": res.objeto, "idHistoria": data.idHistoria, 'descricao': titulo.grupo});
          titulo.grupo = "";
				}
			});
		}

    $scope.alteraBrainstormingGrupo = function(grupoId, grupoText){
      var data = {
        "id": parseInt(grupoId),
        "idHistoria": parseInt($scope.historia.id),
        "descricao": grupoText
      }
      core.alteraBrainstormingGrupo(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
        }
      });
    }

	$scope.infoBrainstorming = function(descricao){
    $scope.modalDescricao(descricao, $scope);
	};

    $scope.addBrainstormGrupo = function(brainstormId, newPageNumber, indice){
			var data = {
				"id": parseInt(brainstormId),
				"idGrupoBrainstorming": $scope.idGrupo
			}

      $scope.newIndiceSugestao = indice + (newPageNumber - 1) * $scope.itensPagina;

      core.editaGrupoBrainstorming(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
          $scope.lstBrainstormingGrupo.push($scope.lstBrainstormingDisponivel[$scope.newIndiceSugestao]);
          $scope.lstBrainstormingDisponivel.splice($scope.newIndiceSugestao, 1);
        }
      });
    }

    $scope.removeBrainstormGrupo = function(brainstormId, newPageNumberTemas, indice){
      var data = {
        "id": parseInt(brainstormId),
        "idGrupoBrainstorming": null
      }
      $scope.newIndiceSugestao = indice + (newPageNumberTemas - 1) * $scope.itensPagina;
      core.editaGrupoBrainstorming(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
          $scope.lstBrainstormingDisponivel.push($scope.lstBrainstormingGrupo[$scope.newIndiceSugestao]);
          $scope.lstBrainstormingGrupo.splice($scope.newIndiceSugestao, 1);
        }
      });
    }

    $scope.excluirGrupo = function($index){
      var data = {
        "id": $scope.idGrupo
      }
      core.excluirBrainstormingGrupo(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          toastr.success(res.mensagem);
          history.go(-1);
          $scope.lstGrupo.splice($index, 1);
        }
			location.reload();
			ngDialog.close();
      });
    }

	$scope.clickToOpen = function($index){
		ngDialog.open({
			template:'templateId',
			className: 'ngdialog-theme-default',
			scope: $scope});
		};

  $scope.clickToClose = function(){
			ngDialog.close();
		};
  }
]);

historiasController.controller('CanvasCtrl', ['$routeParams', '$scope', '$rootScope', 'core', '$cookies',
	function CanvasCtrl($routeParams, $scope, $rootScope, core, $cookies){
		$scope.descOpen = false;
    $scope.idEmpresa = $routeParams.empresaId;
    $scope.ideiaId = $routeParams.ideiaId;
		$scope.canvas = {
			parceiros: []
			, atividades: []
			, recursos: []
			, propostas: []
			, relacionamento: []
			, canais: []
			, segmentos: []
			, custos: []
			, receitas: []
			, objetivo: ""
			, valor: ""
		};

    (function loadHistoriasData() {
      var data = {
        'id': $routeParams.ideiaId
      }
      core.loadHistoriasData(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          $scope.usuarioCriador = res.objeto.usuario;
          $scope.titleHistoria = res.objeto.titulo;
        }
      })
    })();

    if ($scope.idEmpresa > 0) {
      (function loadInfoEmpresa(){
        core.loadEmpresaUsuario($scope.idEmpresa, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.empresaInfo = res.objeto;
          }
        })
      })();
    }
		/*************************LISTAR - INICIO***************************/
		(function loadCanvas(){
			var dtCanvas = {
			"idIdeia" : $routeParams.ideiaId
			};
			core.loadCanvas(dtCanvas, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					var i = 0;
					for (i = 0; i < res.length; i++) {
						if (res[i].nr_item == 1) {
							$scope.canvas.parceiros.push({id: res[i].id ,descricao: res[i].descricao, nr_item: "1", valor: "0"});
						}else if (res[i].nr_item == 2) {
							$scope.canvas.atividades.push({id: res[i].id ,descricao: res[i].descricao, nr_item: "2", valor: "0"});
						}else if (res[i].nr_item == 3) {
							$scope.canvas.recursos.push({id: res[i].id ,descricao: res[i].descricao, nr_item: "3", valor: "0"});
						}else if (res[i].nr_item == 4) {
							$scope.canvas.propostas.push({id: res[i].id ,descricao: res[i].descricao, nr_item: "4", valor: "0"});
						}else if (res[i].nr_item == 5) {
							$scope.canvas.relacionamento.push({id: res[i].id ,descricao: res[i].descricao, nr_item: "5", valor: "0"});
						}else if (res[i].nr_item == 6) {
							$scope.canvas.canais.push({id: res[i].id ,descricao: res[i].descricao, nr_item: "6", valor: "0"});
						}else if (res[i].nr_item == 7) {
							$scope.canvas.segmentos.push({id: res[i].id ,descricao: res[i].descricao, nr_item: "7", valor: "0"});
						}else if (res[i].nr_item == 8) {
							$scope.canvas.custos.push({id: res[i].id ,descricao: res[i].descricao, nr_item: "8", valor: res[i].valor});
						}else if (res[i].nr_item == 9) {
							$scope.canvas.receitas.push({id: res[i].id ,descricao: res[i].descricao, nr_item: "9", valor: res[i].valor});
						}
					}
				}
			})
		})();

    (function loadCookies(){
      $scope.CookiesName = "canvasHelp";
      $scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

      if ($scope.showHelp == null) {
          $cookies.put($scope.CookiesName, "true");
          var cookie = $cookies.get('brainstormHelp');
          $scope.showHelp = "true";
      }
    })();

    $scope.help = function() {
      var now = new Date(),
      exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
      $cookies.put("" + $scope.CookiesName + "", "false", {expires: exp});
      $scope.showHelp = "false";
    }

		/************************* LISTAR - TERMINO ***************************/

		/************************* INSERIR - INICIO ***************************/
		$scope.addCanvas = function(variavel, data, $event){
      if ($scope.usuarioCriador == $rootScope.user.id || $scope.empresaInfo.perfilUsuario != 'USUARIO') {
			var dtCanvas = {
				"id" : "",
				"idIdeia" : $routeParams.ideiaId,
				"idUsuario" : parseInt($rootScope.user.id),
				"nr_item" : data.nr_item,
				"descricao" : data.descricao,
				"valor" : data.valor,
				"data_cadastro" : ""
			}
			variavel.push(data);

			var textArea = angular.element($event.target).find('textarea');
			textArea[0].value = "";

			var input = angular.element($event.target).find('input');

			if(input.length > 0){
				input[0].value = "";
			}

        //Chamando o Serviço para Gravar no BD
        core.adicionaCanvas(dtCanvas, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            toastr.success(res.mensagem);
            $scope.closeAddNote();
            toastr.info("Você ganhou 10 pontos por adicionar esta sessão! Complete o canvas para ganhar mais pontos!")
          }
        });
      }else{
        toastr.error("Desculpe, você não tem permissão para criar o canvas!");
      }
		}

		/************************* INSERIR - TERMINO ***************************/

		/************************* APAGAR - INICIO ***************************/

		$scope.delCanvas = function(alvo, id, $index){
            var dtCanvas = {
                "id" : id
            }
            core.apagaCanvas(dtCanvas, function(res){
                if(res.erro > 0){
                    toastr.error(res.mensagem);
                }else{
                    toastr.success(res.mensagem);
        			alvo.splice($index, 1);
                }
            });
		}

		/************************* APAGAR - TERMINO ***************************/

		$scope.addNote = function($event){
			var article = angular.element($event.target).parent('a').parent('div');

			if(article.hasClass('add')){
				$('.can-content').removeClass('add');
			}else{
				$('.can-content').removeClass('add');
			article.addClass('add');
			}
		}

		$scope.closeAddNote = function(){
			$('.can-content').removeClass('add');
		}

		$scope.closeProcess = function($event){
			$('.pro-cont').removeClass('active');
		}

		$scope.editProcess = function($event){
			var artigoAtual = angular.element($event.target).parent('article');
			if (!artigoAtual.hasClass('active')) {
				$('.pro-cont').removeClass('active');
				artigoAtual.addClass('active');
			};
		}

		$scope.openDesc = function(){
			$scope.descOpen = !$scope.descOpen;
		}

		$scope.scrollOnClick = function($event, attr){
			$('html,body').stop().animate({scrollTop: $("#" +attr).offset().top - 60}, 800);
			setTimeout(function(){
			$('.pro-bt').removeClass('active');
			angular.element($event.target).addClass('active');
			}, 800);
		};

		$scope.showCanvasMobile = function($event){
			var article = angular.element($event.target).parent('div');
			if(article.hasClass('active')){
				$('.can-content').removeClass('active');
			}else{
				$('.can-content').removeClass('active');
				article.addClass('active');
			}
		}

		$scope.submitForm = function($event){
			var article = angular.element($event.target).parent('section').parent('article');
			article.removeClass('active');
		}

		$scope.textScroll = function($event){
			if(screen.width < 768){
				var scrollTo = $($event.target).offset().top - 70;
				$('html,body').stop().animate({scrollTop: scrollTo}, 800);
			}
		}
		}
]);

historiasController.controller('FechamentoCtrl', ['$scope', '$routeParams', '$rootScope','$location', 'core', 'ngDialog', 'SERVICE', '$window',
	function FechamentoCtrl($scope, $routeParams, $rootScope, $location, core, ngDialog, SERVICE, $window) {
    $scope.infoHistoria = [];
    $scope.imagemUrl = SERVICE.urlImagem;
    $scope.infoBrainstorming = [];
    $scope.resumo = '';
    $scope.empresa = {'id': $routeParams.empresaId};


    if ($scope.empresa.id > 0) {
      (function loadInfoEmpresa(){
        core.loadEmpresaUsuario($scope.empresa.id, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.empresaInfo = res.objeto;
          }
        })
      })();
    }


    (function loadInfoFechamento(){
      var data = {
        "id": $routeParams.ideiaId
      }
			core.loadfechamentoHistorias(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
          console.log(res.objeto);
					$scope.infoHistoria = res.objeto;
          $scope.ListSize = $scope.infoHistoria.lstBrainstorming.length;
          $scope.QtColuna = 4;
          $scope.QtItens = Math.floor($scope.ListSize / $scope.QtColuna);
          $scope.newLstBrainstorming = [];
          $scope.NrColuna = -1;
          $scope.newObjeto = [];

          for (var c = 0; c < $scope.QtColuna; c++) {
            $scope.NrColuna = (c % $scope.QtColuna);

            for (var i = (c * $scope.QtItens); i < ((c * $scope.QtItens) + $scope.QtItens); i++) {
                $scope.newObjeto.push($scope.infoHistoria.lstBrainstorming[i]);
            }
            $scope.newLstBrainstorming.push($scope.newObjeto);
            $scope.newObjeto = [];
          }

          $scope.NrColuna = 0;
          for (var i = ($scope.QtItens * $scope.QtColuna); i < $scope.ListSize; i++) {
            $scope.newLstBrainstorming[$scope.NrColuna].push($scope.infoHistoria.lstBrainstorming[i]) ;
            $scope.NrColuna++;
          }
          switch ($scope.QtColuna) {
            case 1: $scope.class = "col-md-12 col-xs-12";
              break;
            case 2: $scope.class = "col-md-6 col-xs-12";
              break;
            case 3: $scope.class = "col-md-4 col-xs-12";
              break;
            case 4: $scope.class = "col-md-3 col-xs-12";
          }
				}
			})
		})();

    $scope.saveFechamentoHistorias = function(resultado){
      var data = {
        "id": $routeParams.ideiaId,
        "resumo": $scope.resumo,
        "resultado": $scope.resultado
      }
      if ($scope.infoHistoria.usuario == $rootScope.user.id) {
          core.saveFechamentoHistorias(data, function(res){
            if(res.erro > 0){
              toastr.error(res.mensagem);
            }else{
              toastr.success(res.mensagem);
              if ($scope.resultado == 'destaque') {
                toastr.info("Você completou essa história como 'Destaque' ganhou 80 pontos por isso!");
              } else if ($scope.resultado == 'sucesso'){
                toastr.info("Por completar essa história como 'Sucesso', você ganhou 70 pontos!");
              } else if ($scope.resultado == 'inconclusivo'){
                toastr.info("A sua história foi encerrada como 'Inconclusiva', mas você ganhou 60 pontos por chegar até aqui!");
              } else if ($scope.resultado == 'cancelado'){
                toastr.info("A sua história foi cancelada. Por realizar o fechamento, você ganhou 30 pontos!");
              }
              $scope.clickToClose();
              $location.path('historias/' + $routeParams.empresaId + '/' + $routeParams.ideiaId + '/dashboard');
            }
          })
      }else{
        toastr.error("Desculpe, você não tem permissão para finalizar essa história!");
      }
		};

    $scope.selecionaBrainstorming = function(idBrainstorming){
      var data = {
        "id": idBrainstorming,
      }
      core.selecionarBrainstormFechamento(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
				}
			})
		};
    
    $scope.printIt = function(){
       var table = document.getElementById('printArea').innerHTML;
       var myWindow = $window.open('', '', 'width=800, height=600');
       myWindow.document.write(table);
       myWindow.print();
    };
    
    $scope.desselecionaBrainstorming = function(idBrainstorming){
      var data = {
        "id": idBrainstorming,
      }
      core.desselecionarBrainstormFechamento(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
				}
			})
		};

    $scope.openEncerraHistoria = function(resumo, resultado){
      $scope.resumo = resumo;
      $scope.resultado = resultado;

  		ngDialog.open({
  			template:'openEncerraHistoria',
  			className: 'ngdialog-theme-default',
  			scope: $scope
      });
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};
	}
]);

historiasController.controller('HistoriasCtrl', ['$rootScope', '$routeParams', '$scope', '$filter', '$location', '$http', 'core', 'SERVICE', '$cookies','ngDialog', '$route',
	function HistoriasCtrl($rootScope, $routeParams, $scope, $filter, $location, $http, core, SERVICE, $cookies, ngDialog, $route) {
		$scope.empresaId = $routeParams.empresaId;
		$scope.imagemUrl = SERVICE.urlImagem;
		$scope.alterarUsuario = alterarUsuario;
		$scope.editandoNome = 0;
		$scope.toogleNome = toogleNome;
		$scope.empresaInfo = [];
		$scope.infoUser = [];
		$scope.validaLogin = false;
		$scope.filtro = '-prioridade';
		$scope.noImg = false;
		$scope.imgUsr = false;
		$scope.imgSocial = false;
		$scope.validaUser = false;
		$scope.histVencidas = [];
		$scope.histFechadas = [];
		$scope.histAberta = [];
		$scope.itensNaPagina = 6;
		$scope.newPageNumber = 1;
		$scope.usuario = {
		"id" : $rootScope.user.id
		};
    var agora = new Date();

		(function usuarioPesquisa(){
			core.usuarioPesquisa($scope.usuario, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.infoUser = res.objeto;

					if ($scope.infoUser.idFacebook == null && $scope.infoUser.idGoogle == null && $scope.infoUser.perfilBig == null) {
						$scope.noImg = true;
						$scope.validaLogin = true;
						$scope.validaUser = true;
					} else if ($scope.infoUser.idFacebook == null && $scope.infoUser.idGoogle == null && $scope.infoUser.perfilBig != null){
						$scope.imgUsr = true;
						$scope.validaLogin = true;
						$scope.validaUser = true;
					} else if ($scope.infoUser.idFacebook == null && $scope.infoUser.idGoogle == null){
						$scope.validaUser = true;
						$scope.validaLogin = true;
					} else {
						$scope.imgSocial = true;
						$scope.validaUser = false;
						$scope.validaLogin = false;
					}
				}
			})
		})();

		function alterarUsuario(tipo, $event){
			var data = {'id': $scope.usuario.id};
			switch(tipo){
				case 'nome':
					data.nome = $rootScope.user.nome;
					toogleNome();
					$cookies.put('user_name', $rootScope.user.nome);
          toogleNome();
					break;
			}
			$http({
				url: SERVICE.url + SERVICE.service + SERVICE.usuario + 'alterar'
				, data: data
				, method: 'POST'
			}).then(function(response) {
				if(response.erro){
					toastr.error(response.data.mensagem);
				}else{
					toastr.success(response.data.mensagem);
          toogleNome();
				}
			}, function(response){
				toastr.error("Ocorreu um erro ao realizar a alteração.");
			});
		}

		function toogleNome(){
			$scope.editandoNome = !$scope.editandoNome;
		}

    (function loadCookies(){
      $scope.CookiesName = "painelHelp";
      $scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

      if ($scope.showHelp == null) {
          $cookies.put($scope.CookiesName, "true");
          $scope.showHelp = "true";
      }
    })();

    (function loadCookiesHistorias(){
      $scope.CookiesName = "historiasHelp";
      $scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

      if ($scope.showHelp == null) {
          $cookies.put($scope.CookiesName, "true");
          $scope.showHelp = "true";
      }
    })();

    $scope.help = function() {
      var now = new Date(),
      exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
      $cookies.put("" + $scope.CookiesName + "", 'false', {expires: exp});
      $scope.showHelp = 'false';
    }

    $scope.adicionarImagem = function(usuarioImagem) {
      var formData = new FormData();
			formData.append('uploadedFile', usuarioImagem);
			formData.append('idUsuario', $scope.usuario.id);
      core.adicionarImagemUsuario(formData, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					toastr.success(res.mensagem);
					location.reload();
				}
			})
    }

		$scope.criaEstrelas = function(prioridade){
			$scope.estrelas = '[';

			for (var i = 1; i <= prioridade; i++) {
				if (i == prioridade) {
					$scope.estrelas = $scope.estrelas + i + ']'
				}else{
					$scope.estrelas = $scope.estrelas + i + ','
				}
			};
			return $scope.estrelas;
		};

    if ($scope.empresaId > 0) {
      (function loadInfoEmpresa(){
        core.loadEmpresaUsuario($scope.empresaId, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.empresaInfo = res.objeto;
          }
        })
      })();
    }

		$scope.inativarConta = function(){
			var resul = $scope.clickToOpen;
			if(resul){
				$rootScope.loaderActive = true;
				$http({
					url: SERVICE.url + SERVICE.service + SERVICE.usuario + 'inativar'
					, data: {"id": $scope.usuario.id}
					, method: 'POST'
				}).then(function(response) {
					if(response.erro){
						toastr.error(response.data.mensagem);
					}else{
						toastr.success(response.data.mensagem);
						$rootScope.logout();
					}
					$rootScope.loaderActive = false;
				}, function(response){
					toastr.error("Ocorreu um erro ao excluir sua conta.");
					$rootScope.loaderActive = false;
				});
			}
		};

		$scope.clickToOpen = function(){
		ngDialog.open({
			template:'templateId',
			className: 'ngdialog-theme-default',
			scope: $scope});
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};

		(function loadHistorias(){
			var data = {
				"idEmpresa": $scope.empresaId,
				"idUsuario": $scope.usuario.id
			};

			core.loadHistorias(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.historias = [];
					var historias = [];
					for (var i = res.objeto.length - 1; i >= 0; i--) {
						if(historias){
							var jaPossuiNaHistorias = false;
							for (var j = historias.length - 1; j >= 0; j--) {
								if(historias[j].id === res.objeto[i].idIdeia){
									historias[j].usuario.push({"nome": res.objeto[i].usuario});
									jaPossuiNaHistorias = true;
								}
							}
							if(!jaPossuiNaHistorias){
								historias.push({
									"id":res.objeto[i].idIdeia
									, "nome": res.objeto[i].tituloIdeia
									, "idEmpresa": res.objeto[i].idEmpresa
									, "dataLimite": res.objeto[i].dataLimite
									, "prioridade": res.objeto[i].prioridade
                					, "boFechado": res.objeto[i].boFechado
									, "estrelas": $scope.criaEstrelas(res.objeto[i].prioridade)
									, "usuario": [{
										"nome": res.objeto[i].usuario
									}]
								})
							}
						}else{
							historias = {
								"id":res.objeto[i].idIdeia
								, "nome": res.objeto[i].tituloIdeia
								, "idEmpresa": res.objeto[i].idEmpresa
								, "dataLimite": res.objeto[i].dataLimite
								, "prioridade": res.objeto[i].prioridade
               					, "boFechado": res.objeto[i].boFechado
								, "estrelas": $scope.criaEstrelas(res.objeto[i].prioridade)
								, "usuario": [{
									"nome": res.objeto[i].usuario
								}]
							}
						}
					};
					//termina TUTOOO
					$scope.historias = historias;

          for (var i = 0; i < $scope.historias.length; i++) {
            $scope.historias[i].dataLimite = new Date($scope.historias[i].dataLimite);

            if ($scope.historias[i].dataLimite > agora && $scope.historias[i].boFechado == 'N') {
              $scope.histAberta.push($scope.historias[i]);
            } else if ($scope.historias[i].dataLimite <= agora && $scope.historias[i].boFechado == 'N') {
              $scope.histVencidas.push($scope.historias[i]);
            } else {
              $scope.histFechadas.push($scope.historias[i]);
            }
          }
					$scope.empresaUser = $rootScope.listaEmpresas;
				}
			});
		})();

		$scope.alteraFiltro = function(valor){
			$scope.filtro = valor;
		}
	}
]);

historiasController.controller('HistoriasAddCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE', 'ngDialog',
	function HistoriasAddCtrl($rootScope, $routeParams, $scope, $location, $http, core, SERVICE ,ngDialog) {
		$scope.imagemUrl = SERVICE.urlImagem;
		$scope.idUsuario = $rootScope.user.id;
   		$scope.empresaHistoria = 0;
   		$scope.empresaImg = 0;
		$scope.linksLista = [];
    	$scope.varDisable = true;
		$scope.imagensOk = [];
    	$scope.empresaImg = $routeParams.empresaId;
    var agora = new Date();
		$scope.ideia = {
			'idEmpresa': $routeParams.empresaId,
      'id': $routeParams.ideiaId
		};
		$scope.editorOptions = {
			language: 'pt-br'
			, height: '230px'
		};
    $scope.ideia.realDataCriacao = agora;
    $scope.ideia.prioridade = 0;
    $scope.ideia.boPlanoAcao = 'S';
    $scope.ideia.boBrainstorming = 'S';
    $scope.ideia.boCanvas = 'S';

    if ($routeParams.ideiaId > 0) {
      (function loadHistoriasData(){
        var data = {
          "id": parseInt($routeParams.ideiaId),
          "idUsuario": parseInt($rootScope.user.id),
          "idEmpresa": parseInt($routeParams.empresaId)
        };
        core.loadHistoriasData(data, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.ideia = res.objeto;
            for (var i = 0 ; i < $scope.ideia.lstImagem.length; i++) {
              $scope.nomeImagem.push($scope.ideia.lstImagem[i]);
            };

            for (var i = 0 ; i < $scope.ideia.lstLink.length; i++) {
              if ($scope.ideia.lstLink[i].length > 1) {
                $scope.linksLista.push($scope.ideia.lstLink[i]);
              };
            };
          }
        })
      })();
    }

    (function defineCampoSelect(){
      if ($routeParams.empresaId != null) {
        $scope.varDisable = true;
      }else{
        $scope.varDisable = false;
      }
		})();

    (function excluirImagemTemp(){
      if ($scope.empresaImg > 0) {
        var data = {
          'usuario': $rootScope.user.id,
          'empresa': $scope.empresaImg
        }

        core.excluirHistoriasImagemTemp(data, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }
        })
      }
    })();

    $scope.definePrioridade = function(x){
      $scope.ideia.prioridade = x;
    };

    $scope.excluirHistoriasImagemTemp = function(){
      if ($scope.empresaImg > 0) {
        var data = {
          'usuario': $rootScope.user.id,
          'empresa': $scope.empresaImg
        }
        core.excluirHistoriasImagemTemp(data, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }
        })
      }
    };

    $scope.excluirHistoriasImagemTemp = function(){
      var data = {
        'usuario': $rootScope.user.id,
        'empresa': $scope.empresaImg
      }
      core.excluirHistoriasImagemTemp(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }
      })
    };

		$scope.excluirHistoriasImagem = function($index){
			if ($scope.clickToOpen)  {
				$scope.imagensOk.splice($index, 1);
			};
		};

		$scope.clickToOpen = function(){
		ngDialog.open({
			template:'templateId',
			className: 'ngdialog-theme-default',
			scope: $scope});
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};

    $scope.selectEmpresaId = function(id){
			$scope.empresaImg = id;
      $scope.excluirHistoriasImagemTemp();
		};

		$scope.salvarHistoria = function(){
			$scope.dataCriacao = new Date($scope.ideia.dataCriacao);
			$scope.dataLimite = new Date($scope.ideia.dataLimite);

      if ($routeParams.empresaId == null) {
        $scope.empresaHistoria = $scope.ideia.idEmpresa;
        var pathData = 'historias/usuario'
      } else {
        $scope.empresaHistoria = $routeParams.empresaId;
        var pathData = 'historias/empresa/' + $routeParams.empresaId;
      }
      if ($scope.ideia.titulo != null) {
        if ($scope.ideia.dataLimite >= $scope.ideia.dataCriacao) {
		  if($scope.ideia.prioridade > 0){
	      if ($scope.empresaHistoria > 0) {
	        var data = {
            'id': $scope.ideia.id,
            'titulo': $scope.ideia.titulo,
            'descricao': $scope.ideia.descricao,
            'usuario': $rootScope.user.id,
            'empresa': $scope.empresaHistoria,
            'ativo': 1,
            'dataCriacao': $scope.dataCriacao,
            'dataLimite': $scope.dataLimite,
            'prioridade': $scope.ideia.prioridade,
            'tipoIdeia': 'E',
            'boCanvas': $scope.ideia.boCanvas,
            'boBrainstorming': $scope.ideia.boBrainstorming,
            'boPlanoAcao': $scope.ideia.boPlanoAcao,
            'lstLink': $scope.linksLista
	        };

	          core.salvarHistoria(data, function(resposta){
	            if(resposta.erro > 0){
	              toastr.error(resposta.mensagem);
	            }else{
	              toastr.success(resposta.mensagem);
	              toastr.info("Você ganhou 100 pontos pela criação desta história!");
	              $location.path(pathData);
	            }
	          })
	        }else{
	          toastr.error("Você deve selecionar uma empresa!");
	        }
		}else{

			toastr.error("Você deve selecionar a prioridade");
		}
            }else{
				toastr.error("A data limite não pode ser menor que a data atual");
		}
  			}else{
  				toastr.error("O título da história deve ser informado!");
	}
    }
		$scope.salvarHistoriasLink = function(link){
			if (link != null  && link.length > 0) {
				$scope.linksLista.push(link);
				$scope.ideia.link = '';
			} else {
				toastr.error("Você deve informar um link!");
			}
		}

		$scope.salvarHistoriasImagem = function(imagem, empresa){
			var fd = new FormData();

			fd.append('uploadedFile', imagem);
			fd.append('idUsuario', $rootScope.user.id);
			fd.append('idEmpresa', $scope.empresaImg);

      if ($scope.empresaImg > 0) {
        core.salvarHistoriasImagem(fd, function(resposta){
          if(resposta.erro > 0){
            toastr.error(resposta.mensagem);
          }else{
            toastr.success(resposta.mensagem);
            $scope.imagensOk.push(resposta.objeto);
          }
        })
      } else {
        toastr.error("Você precisa selecionar uma empresa para salvar!");
      }

		}
	}
]);

historiasController.controller('HistoriasDashboardCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE', 'ngDialog',
	function HistoriasDashboardCtrl($rootScope, $routeParams, $scope, $location, $http, core, SERVICE , ngDialog) {
		$scope.empresaId = $routeParams.empresaId;
		$scope.imagemUrl = SERVICE.urlImagem;
    $scope.dialogImgUrl = '';
    $scope.dialogHistId = '';
    $scope.dialogImgName = '';
		$scope.nomeImagem = [];
		$scope.linksHistoria = [];
    $scope.userId = $rootScope.user.id;
		$scope.historia = {
			"id": $routeParams.ideiaId
		};

		(function loadHistoriaEmpresa(){
			var data = {
				"id": parseInt($routeParams.ideiaId),
				"idUsuario": parseInt($rootScope.user.id),
				"idEmpresa": parseInt($routeParams.empresaId)
			};

			core.loadHistoriasData(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.historia = res.objeto;
          $scope.criadorHistoria = $scope.historia.usuario;
					for (var i = 0 ; i < $scope.historia.lstImagem.length; i++) {
						$scope.nomeImagem.push($scope.historia.lstImagem[i]);
					};

					for (var i = 0 ; i < $scope.historia.lstLink.length; i++) {
						$scope.linksHistoria.push($scope.historia.lstLink[i]);
					};
				}
			})
		})();

    if ($routeParams.empresaId > 0) {
      (function loadInfoEmpresa(){
        core.loadEmpresaUsuario($routeParams.empresaId, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.empresaInfo = res.objeto;
          }
        })
      })();
    }

		$scope.clickToOpen = function(){
  		ngDialog.open({
  			template:'templateId',
  			className: 'ngdialog-theme-default',
  			scope: $scope});
		};

    $scope.clickToOpenSair = function(){
  		ngDialog.open({
  			template:'templateSair',
  			className: 'ngdialog-theme-default',
  			scope: $scope});
		};

    $scope.clickToOpenImg = function(imagemUrl, historiaId, img){
      $scope.dialogImgUrl = imagemUrl;
      $scope.dialogHistId = historiaId;
      $scope.dialogImgName = img;

  		ngDialog.open({
  			template:'showImgBig',
  			className: 'ngdialog-theme-default',
  			scope: $scope});
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};

    $scope.sairHistoria = function(){
			var data = {
				"idUsuario": $rootScope.user.id,
				"idIdeia": $scope.historia.id
			};
			core.removerAmigoProjeto(data, function(resposta){
				if(resposta.erro > 0){
					toastr.error(resposta.mensagem);
				}else{
					toastr.success(resposta.mensagem);
          ngDialog.close();
          $location.path('historias/empresa/' + $routeParams.empresaId);
				}
			})
		}

		$scope.excluirHistorias = function(){
			if($scope.clickToOpen){
				var data = {
					'id': parseInt($scope.historia.id)
				};
        if ($scope.criadorHistoria == $scope.userId) {
            core.excluirHistorias(data, function(res){
                if(res.erro > 0){
                  toastr.error(res.mensagem);
                }else{
                  toastr.success(res.mensagem);
                  $location.path('/historias/usuario');
                }
            ngDialog.close();
          })
        }else {
          toastr.error("Esse usuário não pode excluir essa História");
        }
			}
		}
	}
]);


historiasController.controller('HistoriasEquipeCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE', 'ngDialog',
	function HistoriasEquipeCtrl($rootScope, $routeParams, $scope, $location, $http, core, SERVICE, ngDialog) {
		$scope.amigosFora = [];
    $scope.imagemUrl = SERVICE.urlImagem;
    $scope.loggeduser = $rootScope.user.id;
    $scope.botaoInfo = true;
    $scope.botaoAdicionar = true;
    $scope.origem = 'EquipeHistoria';
		$scope.pesquisaFora = '';
    $scope.idGrupoClass = 0;
    $scope.filtroGrupo = 0;
    $scope.newPageNumberDelete = 1;
    $scope.newPageNumberAdd = 1;
    $scope.itensPagina = 10;
		$scope.ideia = {
			'id': $routeParams.ideiaId
		};
    $scope.empresa = {
			'id': $routeParams.empresaId
		};

    (function loadHistoriasData() {
			var data = {
				'id': $scope.ideia.id
			}
			core.loadHistoriasData(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.criadorHistoria = res.objeto.usuario;
          $scope.titleHistoria = res.objeto.titulo;
				}
			})
		})();

    if ($scope.empresa.id > 0) {
      (function loadInfoEmpresa(){
        core.loadEmpresaUsuario($scope.empresa.id, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.empresaInfo = res.objeto;
          }
        })
      })();
    }

    (function buscaGruposEmpresa(){
			core.buscaGruposEmpresa($scope.empresa.id, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.gruposLista = res.objeto;
          $scope.gruposLista.push({"id": 0, "idEmpresa": $scope.empresa.id, "nome": "Todos"});
				}
			})
		})();

		(function loadHistoriaEquipe(){
			var data = {
				"idIdeia": $scope.ideia.id
			};

			core.loadHistoriaEquipe(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.usuariosSelecionados = res.objeto.lstUsuarioIdeia;
          $scope.usuariosDisponiveis = res.objeto.lstUsuarioEmpresa;
				}
			});
		})();

    $scope.callHistoriaEquipe = function(idGrupo){
      var data = {
				"idIdeia": $scope.ideia.id,
        "idGrupoUsuario": idGrupo
		    };
      core.loadHistoriaEquipe(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.usuariosSelecionados = res.objeto.lstUsuarioIdeia;
          $scope.usuariosDisponiveis = res.objeto.lstUsuarioEmpresa;
          $scope.idGrupoClass = idGrupo;
				}
			});
		}

		$scope.adicionarUsuario = function(id, newPageNumber, indice){
      $scope.newIndiceUsuario = indice + (newPageNumber - 1) * $scope.itensPagina;
      $scope.adicionarUsuarioCtrl($scope.origem, id, newPageNumber, indice, $scope);
    }

    $scope.excluirUsuario = function(item, newPageNumber, $index) {
      $scope.newIndiceUsuario = $index + (newPageNumber - 1) * $scope.itensPagina;
      $scope.modalExcluirUsuario(item, newPageNumber, $index, $scope);
    }
	}
]);

historiasController.controller('PlanoAcaoCtrl', ['$rootScope', '$routeParams', '$scope', 'core', 'SERVICE', '$location', 'ngDialog', '$cookies',
	function PlanoAcaoCtrl($rootScope, $routeParams, $scope, core, SERVICE, $location, ngDialog, $cookies) {
    $scope.imagemUrl = SERVICE.urlImagem;
    $scope.idGrupoBrainstroming = 0;
    $scope.idbrainstorming = 0;
    $scope.plano = [];
    $scope.loggeduser = $rootScope.user.id;
    $scope.lstPlanoAcao = 0;
    $scope.planoEdit = [];
    $scope.empresaId = $routeParams.empresaId;
    $scope.historia = {
			"id": $routeParams.ideiaId
		};

    if ($scope.empresaId > 0) {
      (function loadInfoEmpresa(){
        core.loadEmpresaUsuario($scope.empresaId, function(res){
          if(res.erro > 0){
            toastr.error(res.mensagem);
          }else{
            $scope.empresaInfo = res.objeto;
          }
        })
      })();
    }

    (function loadHistoriasData() {
      var data = {
        'id': $scope.historia.id
      }
      core.loadHistoriasData(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          $scope.infoHistoria = res.objeto;
        }
      })
    })();

    (function loadPlanoAcao() {
      var data = {
        'idHistoria': $scope.historia.id
      }
      core.loadPlanoAcao(data, function(res){
        if(res.erro > 0){
          toastr.error(res.mensagem);
        }else{
          $scope.lstPlanoAcao = res.objeto;
          if ($routeParams.idPlano > 0 ) {
            for (var i = 0; i < $scope.lstPlanoAcao.length; i++) {
              if ($scope.lstPlanoAcao[i].id == $routeParams.idPlano) {
                $scope.planoEdit =  $scope.lstPlanoAcao[i];
              }
            }
            if ($scope.planoEdit.idbrainstorming > 0) {
              $scope.brainstorming = true;
              $scope.temas = false;
            } else if ($scope.planoEdit.idGrupoBrainstroming > 0){
              $scope.temas = true;
              $scope.brainstorming = false;
            }
          }
        }
      })
    })();

    (function loadplanoBrainstorm(){
			var data = {
        		"idUsuario": $rootScope.user.id,
				    "idHistoria": parseInt($scope.historia.id)
			};
			core.loadBrainstorming(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
          $scope.planoBrainstorm = res.objeto;
				}
			})
		})();

		(function loadplanoTemas(){
			var data = {
				"idHistoria": parseInt($scope.historia.id)
			};
			core.loadBrainstormingGrupo(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.planoTemas = res.objeto;
				}
			})
		})();

    (function loadCookies(){
      $scope.CookiesName = "planoAcaoHelp";
      $scope.showHelp = $cookies.get("" + $scope.CookiesName + "");

      if ($scope.showHelp == null) {
          $cookies.put($scope.CookiesName, "true");
          var cookie = $cookies.get('brainstormHelp');
          $scope.showHelp = "true";
      }
    })();

    $scope.help = function() {
      var now = new Date(),
      exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
      $cookies.put("" + $scope.CookiesName + "", "false", {expires: exp});
      $scope.showHelp = "false";
    }

  $scope.clearActive = function() {
      var brainstorms = angular.element('.brainstorms');
      brainstorms.removeClass('activeList');

      var temas = angular.element('.temas');
      temas.removeClass('activeList');

      $scope.idbrainstorming = 0;
      $scope.idGrupoBrainstroming = 0;
      $scope.plano.ataque = '';
    }

    $scope.infoBrainstorming = function(descricao){
      $scope.modalDescricao(descricao, $scope);
    };

    $scope.selectBrainstorm = function(idBrainstorming, descricao){
        $scope.idBrainstorming = idBrainstorming;
        $scope.idGrupoBrainstroming = 0;

        if ($routeParams.idPlano > 0) {
          $scope.planoEdit.ataque = descricao;
        }else {
          $scope.plano.ataque = descricao;
        }
    };

    $scope.selectTema = function(idTema, descricao){
        $scope.idGrupoBrainstroming = idTema;
        $scope.idBrainstorming = 0;
        var brainstorms = angular.element('.brainstorms');
        brainstorms.removeClass('activeList');
        if ($routeParams.idPlano > 0) {
          $scope.planoEdit.ataque = descricao;
        }else {
          $scope.plano.ataque = descricao;
        }
		};
      $scope.excluirPlanoAcao = function($index){
	        var data = {
	          "id": $scope.idPlano
	        }
		    if($scope.clickToOpen){
		        core.excluirPlanoAcao(data, function(resposta){
		          if(resposta.erro > 0){
		            toastr.error(resposta.mensagem);
		          }else{
		            toastr.success(resposta.mensagem);
		            $scope.lstPlanoAcao.splice($index, 1);
		          }
		          ngDialog.close();
		        })
			};
     	}
		$scope.clickToOpen = function(id, $index){
			ngDialog.open({
				template:'templateId',
				className: 'ngdialog-theme-default',
				scope: $scope});
        $scope.idPlano = id;
		};

		$scope.clickToClose = function(){
			ngDialog.close();
		};
    $scope.savePlanoAcao = function(plano){
      var data = {
        "assunto": plano.assunto,
        "ataque": plano.ataque,
        "detalhamento": plano.descricao,
        "idGrupoBrainstroming": $scope.idGrupoBrainstroming,
        "idbrainstorming": $scope.idBrainstorming,
        "idHistoria": $scope.historia.id,
        "idusuario": $rootScope.user.id
      };
      if ($scope.idGrupoBrainstroming > 0 || $scope.idBrainstorming > 0 || plano.ataque != null) {
        core.savePlanoAcao(data, function(resposta){
          if(resposta.erro > 0){
            toastr.error(resposta.mensagem);
          }else{
            toastr.success(resposta.mensagem);
            $location.path('planoacao/atividades/' + $scope.empresaId + '/' + $scope.historia.id + "/" + resposta.objeto);
          }
        })
      } else {
        toastr.info("Você deve escolher ataque! Pode ser uma ideia, um tema do brainstorming ou então digitar no campo 'Ataque'");
      }
    }

    $scope.editPlanoAcao = function(planoEdit){
      if (planoEdit.idusuario == $rootScope.user.id || $scope.empresaInfo.perfilUsuario == 'ADMINISTRADOR') {
        var data = {
          "assunto": planoEdit.assunto,
          "idusuario": $rootScope.user.id,
          "ataque": planoEdit.ataque,
          "detalhamento": planoEdit.detalhamento,
          "idGrupoBrainstroming": $scope.idGrupoBrainstroming,
          "idbrainstorming": $scope.idbrainstorming,
          "idHistoria": $scope.historia.id,
          "id": planoEdit.id
        };
        if ($scope.idGrupoBrainstroming > 0 || $scope.idbrainstorming > 0 || planoEdit.ataque != null) {
          core.editPlanoAcao(data, function(resposta){
            if(resposta.erro > 0){
              toastr.error(resposta.mensagem);
            }else{
              toastr.success(resposta.mensagem);
              $location.path('planoacao/' + $scope.empresaId + '/' + $scope.historia.id);
            }
          })
        } else {
          alert("Você deve escolher ataque! Pode ser uma ideia, um tema do brainstorming ou então digitar no campo 'Ataque'");
        }
      }else{
        toastr.error("Você não tem permissão para editar este plano de ação!");
      }
    }
	}
]);
