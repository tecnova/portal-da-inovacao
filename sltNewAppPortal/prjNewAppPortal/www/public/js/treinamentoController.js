'use strict';

/* Controllers de Treinamento */

var treinamentoController = angular.module('treinamentoController', ['ngCookies']);

treinamentoController.controller('TreinamentoCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE',
	function TreinamentoCtrl($rootScope, $routeParams, $scope, $location, $http, core, SERVICE) {
		$scope.menuCatTrei = false;
		$scope.treinamentoId = {"id": $routeParams.treinamentoId};
		$scope.videoLink = "../public/treinamentos/" + $scope.treinamentoId.id + "/index.html";
		$scope.video = [];

		(function loadTreinamentos(){
			core.loadTreinamentos(function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$rootScope.treiAnda = res;
					for (var i = res.length - 1; i >= 0; i--) {
						if (res[i].treinamentoId == $routeParams.treinamentoId) {
							$scope.video = res[i];

							$scope.dataUrl = 'http://www.portaldainovacao.com.br/treinamento/' + $scope.video.treinamentoId;
							$scope.dataImg = 'http://www.portaldainovacao.com.br/imagens/treinamentos/' + $scope.video.treinamentoId + '.jpg';
							$scope.hideMenu = true;
						};
					};
					var menu = angular.element('#con-treinamento');
					if ($scope.hideMenu) {
						menu.addClass('hideMenu');
					}
				}
			})
		})();
	}
]);
