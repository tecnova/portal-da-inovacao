-- CANVAS -- 
CREATE DEFINER=`root`@`localhost` TRIGGER canvas_after_insert
	AFTER INSERT on Canvas
FOR EACH ROW
BEGIN

	-- Trabalhando com a Gamificação
    SELECT id, valor into @idAtiv, @Valor FROM atividade WHERE tpAtividade = 'tpCanvasItemCriar';
    Set @idEmpresa = (Select id_empresa FROM ideia WHERE id = NEW.Id_Ideia);
    
	Insert Into 
		atividade_usuario
			(
				 idAtividade
				,idEmpresa
				,idUsuario
				,dtAtividade
				,idHistoria
                ,idCanvas
                ,Pontuacao
			)
		values
			(
				 @idAtiv
				,@idEmpresa
				,NEW.Id_Usuario
				,now()
				,NEW.Id_Ideia
                ,NEW.id
                ,@valor
			);

	/* Verifica se completou o Canvas */
	Set @Cont = 1;
	Set @Achou = 1; /* 0 = False --- 1 = True */
    
	While @Cont <= 9 Do
		SET @ContReg = (Select Count(1) From Canvas Where Id_Ideia = NEW.Id_Ideia and Nr_Item = @Cont);
    
		if @ContReg = 0 then
			Set @Achou = 0;
        END IF;
    
		Set @Cont = @Cont + 1;
        
    END While;

    if (@Achou = 1) THEN

        SELECT id, valor into @idAtiv, @valor FROM atividade WHERE tpAtividade = 'tpCanvasCompletar';

		Insert Into 
			atividade_usuario
				(
					 idAtividade
					,idEmpresa
					,idUsuario
					,dtAtividade
					,idHistoria
					,idCanvas
                    ,Pontuacao
				)
			values
				(
					 @idAtiv
					,@idEmpresa
					,NEW.Id_Usuario
					,now()
					,NEW.Id_Ideia
					,NEW.id
                    ,@valor
				);
    
    END IF;
END;

CREATE DEFINER=`root`@`localhost` TRIGGER canvas_before_delete
	BEFORE DELETE on Canvas
FOR EACH ROW
BEGIN

	-- Excluindo os lançamentos da gamificação
	
	DELETE FROM atividade_usuario
		WHERE 
			idCanvas = old.id;
			
END;
