CREATE DEFINER=`root`@`localhost` TRIGGER brainstorming_curtir_after_insert
	AFTER INSERT on brainstorming_curtir
FOR EACH ROW
BEGIN

    SELECT 
		bra.id_usuario,
        usr.Nome
	Into
		@idUsuario,
        @Nome
	From
        brainstorming bra,
        usuario usr
	Where 
        bra.id = New.idBrainstorming
	and bra.id_usuario = usr.id;
    
	Insert Into 
		requisicao
			(
				 Mensagem
				,idUsuario
				,boLeitura
			)
		values
			(
				 "" + @Nome + " curtiu sua sugestão no brainstorming "
				,New.idUsuario
				,"N"
			);

END