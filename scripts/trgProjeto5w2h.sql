-- Projeto5w2h --
CREATE DEFINER=`root`@`localhost` TRIGGER `portal_inovacao`.`projeto5w2h_BEFORE_DELETE` BEFORE DELETE ON `projeto5w2h` FOR EACH ROW
BEGIN
	Delete from projeto5w2hitem where id > 0 and idprojeto5w2h = old.id;
END;

