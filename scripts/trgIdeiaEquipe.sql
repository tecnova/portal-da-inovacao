CREATE DEFINER=`root`@`localhost` TRIGGER ideia_equipe_after_insert
	AFTER INSERT on ideia_equipe
FOR EACH ROW
BEGIN

	Insert Into 
		requisicao
			(
				 Mensagem
				,idUsuario
				,boLeitura
			)
		values
			(
				 "Você foi convidado para participar de uma nova história"
				,New.id_usuario
				,"N"
			);

END;
