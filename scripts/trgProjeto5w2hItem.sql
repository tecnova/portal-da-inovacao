-- Projeto5w2hItem

CREATE DEFINER=`root`@`localhost` TRIGGER `portal_inovacao`.`projeto5w2hitem_BEFORE_DELETE` BEFORE DELETE ON `projeto5w2hitem` FOR EACH ROW
BEGIN
	Delete From projeto5w2hitemequipe Where id > 0 and idprojeto5w2hitem = old.id;
END