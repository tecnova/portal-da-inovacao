-- Ideia --
CREATE DEFINER=`root`@`localhost` TRIGGER ideia_inclui_ideia_equipe
	AFTER INSERT on ideia
FOR EACH ROW
BEGIN

	-- Inserindo o usuario na equipe
	Insert Into ideia_equipe (id_ideia , id_usuario) Values (NEW.id, NEW.id_usuario);

	-- Trabalhando com Gamificacão
    SELECT id, valor into @idAtiv, @valor FROM atividade WHERE tpAtividade = 'tpHistoriaCriar';
	Insert Into 
		atividade_usuario
			(
				 idAtividade
				,idEmpresa
				,idUsuario
				,dtAtividade
				,idHistoria
				,Pontuacao
			)
		values
			(
				 @idAtiv
				,NEW.id_empresa
				,NEW.id_usuario
				,now()
				,NEW.id
				,@valor
			);
END;

CREATE DEFINER=`root`@`localhost` TRIGGER ideia_exclui_ideia_equipe
	BEFORE DELETE on ideia
FOR EACH ROW
BEGIN

	-- Excluindo Usuario da Equipe
	Delete From ideia_equipe Where id_ideia = old.id and id_usuario = old.id_usuario and id > 0;
	
	-- Trabalhando com a Gamificacao
	Delete From atividade_usuario Where idusuario = old.id_usuario and idHistoria = old.id and id > 0;
	
	
END;

