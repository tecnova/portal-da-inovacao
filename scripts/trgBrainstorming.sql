-- Brainstorming --
CREATE DEFINER=`root`@`localhost` TRIGGER brainstorming_after_insert
	AFTER INSERT on brainstorming
FOR EACH ROW
BEGIN

    SELECT id, valor into @idAtiv, @valor FROM atividade WHERE tpAtividade = 'tpBrainstormingIdeiaCriar';
    Set @idEmpresa = (Select id_empresa FROM ideia WHERE id = NEW.id_historia);
    
	Insert Into 
		atividade_usuario
			(
				 idAtividade
				,idEmpresa
				,idUsuario
				,dtAtividade
				,idHistoria
                ,idBrainstorming
				,Pontuacao
			)
		values
			(
				 @idAtiv
				,@idEmpresa
				,NEW.id_usuario
				,now()
				,NEW.id_historia
                ,NEW.id
				,@valor
			);

END;

CREATE DEFINER=`root`@`localhost` TRIGGER brainstorming_before_delete
	BEFORE DELETE on brainstorming
FOR EACH ROW
BEGIN

	-- Excluindo os lançamentos da gamificação
	
	DELETE FROM atividade_usuario
		WHERE 
			idBrainstorming = old.id;
			
END;