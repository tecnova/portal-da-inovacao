-- Projeto5w2hItemEquipe --

CREATE DEFINER=`root`@`localhost` TRIGGER projeto5w2hitemequipe_after_insert
	AFTER INSERT on projeto5w2hitemequipe
FOR EACH ROW
BEGIN

    SELECT id, valor into @idAtiv, @valor FROM atividade WHERE tpAtividade = 'tpProjeto5w2hAcao';
    Set @idProjeto5w2h = (Select idProjeto5w2h From projeto5w2hitem Where id = NEW.idprojeto5w2hitem);
    Set @idBrainstorming = (Select id_brainstorming From projeto5w2h where id = @idProjeto5w2h);
    Set @idHistoria = (Select id_historia From brainstorming where id = @idBrainstorming);
    Set @idEmpresa = (Select id_empresa FROM ideia WHERE id = @idHistoria);
    
	Insert Into 
		atividade_usuario
			(
				 idAtividade
				,idEmpresa
				,idUsuario
				,dtAtividade
				,idHistoria
                ,idProjeto5w2hItem
				,Pontuacao
			)
		values
			(
				 @idAtiv
				,@idEmpresa
				,NEW.idusuario
				,now()
				,@idHistoria
                ,NEW.idprojeto5w2hitem
				,@valor
			);

END;

CREATE DEFINER=`root`@`localhost` TRIGGER projeto5w2hitemequipe_before_delete
	BEFORE DELETE on projeto5w2hitemequipe
FOR EACH ROW
BEGIN

	-- Excluindo os lançamentos da gamificação
	
	DELETE FROM atividade_usuario
		WHERE 
			idProjeto5w2hItem = old.idProjeto5w2hItem and id > 0;
			
END;

