<section class="art-form">
	<h5>Descreva abaixo o problema encontrado.</h5>
	<form action="javascript:;">
		<textarea name="" id=""></textarea>
		<button>Salvar</button>
		<div>
			<div class="pro-feed">
				<a href="#"><i class="fa fa-comment-o"></i><span class="pro-comment">15</span></a>
				<a href="#">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star-o"></i>
				</a>
			</div>
		</div>
	</form>
</section>
<section class="art-form">
	<h5>Liste-os abaixo:</h5>
	<form action="javascript:;">
		<input type="text">
		<button>Add</button>
		<div class="content">
			<ul>
				<li>Perda de dados - <i class="fa fa-thumbs-o-up"></i> <i class="fa fa-thumbs-o-down"></i></li>
				<li>Poucas pessoas envolvidas - <i class="fa fa-thumbs-o-up"></i> <i class="fa fa-thumbs-o-down"></i></li>
				<li>Falta de incentivo - <i class="fa fa-thumbs-o-up"></i> <i class="fa fa-thumbs-o-down"></i></li>
			</ul>
		</div>
		<div>
			<div class="pro-feed">
				<a href="#"><i class="fa fa-comment-o"></i><span class="pro-comment">10</span></a>
				<a href="#">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star-o"></i>
				</a>
			</div>
		</div>
	</form>
</section>