'use strict';

var rankingController = angular.module('rankingController', ['ngCookies' , 'ngMdIcons']);

rankingController.controller('rankingEmpresaCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE',
	function rankingEmpresaCtrl($rootScope, $routeParams, $scope, $location, $http, core, SERVICE) {
		$scope.empresa = {'id': $routeParams.empresaId};
		$scope.rankingEmpresa = [];
		$scope.imagemUrl = SERVICE.urlImagem;
		$scope.razaoSocial = '';

		(function loadEmpresaUsuario() {
				var data = $scope.empresa.id;

				core.loadEmpresaUsuario(data, function(res) {
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.razaoSocial = res.objeto.razaoSocial;
						$scope.empresaImagem = res.objeto.imagem;
					}
				})
		})();

		(function buscaRankingEmpresa(){
			var	data = {
				'idEmpresa': parseInt($scope.empresa.id)
			}
			core.buscaRankingEmpresa(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.rankingEmpresa = res.objeto;
				}
			})
		})();
	}
]);

rankingController.controller('rankingHistoriaCtrl', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE',
	function rankingHistoriaCtrl($rootScope, $routeParams, $scope, $location, $http, core, SERVICE) {
		$scope.historia = {"id": $routeParams.ideiaId};
		$scope.rankingHistoria = [];
		$scope.titleHistoria = "";
		$scope.empresa = {'id': $routeParams.empresaId};

		(function loadEmpresaUsuario() {
				var data = $scope.empresa.id;

				core.loadEmpresaUsuario(data, function(res) {
					if(res.erro > 0){
						toastr.error(res.mensagem);
					}else{
						$scope.razaoSocial = res.objeto.razaoSocial;
					}
				})
		})();

		(function buscaRankingHistoria(){
			var data = {
				"idHistoria": parseInt($scope.historia.id)
			};
			core.buscaRankingHistoria(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.rankingHistoria = res.objeto;
				}
			})
		})();

		(function loadHistoriasData() {
			var data = {
				'id': $scope.historia.id
			}
			core.loadHistoriasData(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.titleHistoria = res.objeto.titulo;
				}
			})
		})();
	}
]);

rankingController.controller('rankingPortal', ['$rootScope', '$routeParams', '$scope', '$location', '$http', 'core', 'SERVICE',
	function rankingPortal($rootScope, $routeParams, $scope, $location, $http, core, SERVICE) {
		$scope.rankingUserPortal = [];
		$scope.rankingEmpresaPortal = [];

		(function buscaRankingUsuariosPortal(){
			var data = {
				"idUsuario": parseInt($rootScope.user.id)
			}
			core.buscaRankingUsuariosPortal(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.rankingUserPortal = res.objeto;
				}
			})
		})();

		(function buscaRankingEmpresasPortal(){
			var data = {
				"idEmpresa": parseInt($routeParams.empresaId)
			}
			core.buscaRankingEmpresasPortal(data, function(res){
				if(res.erro > 0){
					toastr.error(res.mensagem);
				}else{
					$scope.rankingEmpresaPortal = res.objeto;
				}
			})
		})();
	}
]);
