'use strict';

/* Services */

var portalServices = angular.module('portalServices', ['ngMdIcons']);
var base  = angular.element('#base');
  if (navigator.userAgent.indexOf('Android') != -1) {
    base.remove();
  }


portalServices.service('Session', function(){
	this.create = function(sessionId, userId, userRole){
		this.id = sessionId;
		this.userId = userId;
		this.userRole = userRole;
	};

	this.destroy = function(){
		this.id = null;
		this.userId = null;
		this.userRole = null;
	};
})

portalServices.directive('fileModel', ['$parse',
	function($parse){
		return {
			restrict: 'A'
			, link: function(scope, element, attr){
				var model = $parse(attr.fileModel);
				var modelSetter = model.assign;

				element.bind('change', function(){
					scope.$apply(function(){
						if(element[0].files[0].type == "image/jpeg"){
							modelSetter(scope, element[0].files[0]);
							if($("#res-msg").hasClass('error')){
								$("#res-msg").removeClass('error');
							}
							$("#res-msg").html("Imagem válida.");
							$("#res-msg").addClass('success');
						}else{
							modelSetter(scope, null);
							angular.element(element).val("");
							$("#res-msg").html("Imagem inválida.");
							if($("#res-msg").hasClass('success')){
								$("#res-msg").removeClass('success');
							}
							$("#res-msg").addClass('error');
						}
					});
				});
			}
		}
	}
]);

portalServices.directive('empresaFile', ['$parse', '$http', 'SERVICE',
	function($parse, $http, SERVICE){
		return {
			restrict: 'A'
			, link: function(scope, element, attr){
				var model = $parse(attr.fileModel);
				var modelSetter = model.assign;

				element.bind('change', function(){
					scope.$apply(function(){
						var id = scope.empresa.idEmpresa;
						if(element[0].files[0].type == "image/jpeg"){
							scope.empresa.idEmpresa = 0;
							var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'imagem/salvar';
							var fd = new FormData();
							fd.append('uploadedFile', element[0].files[0]);
							fd.append('id', id);

							$http.post(url, fd, {headers: {'Content-Type': undefined}})
								.success(function(res){
									if(res.erro > 0){
										toastr.error(res.mensagem)
									}else{
										scope.empresa.imagem = true;
										toastr.success(res.mensagem);
									}
									scope.empresa.idEmpresa = id;
								}).error(function(err) {
									scope.empresa.idEmpresa = id;
									toastr.warning('Tente novamente mais tarde.');
									toastr.error('Ocorreu algo ao salvar a foto.');
								});

						}else{
							angular.element(element).val("");
							toastr.error("Imagem inválida.");
						}
					});
				});
			}
		}
	}
]);

portalServices.directive('usuarioFile', ['$rootScope', '$parse', '$http', 'SERVICE',
	function($rootScope, $parse, $http, SERVICE){
		return {
			restrict: 'A'
			, link: function(scope, element, attr){
				var model = $parse(attr.fileModel);
				var modelSetter = model.assign;

				element.bind('change', function(){
					scope.$apply(function(){
						if(element[0].files[0].type == "image/jpeg"){
							var url = SERVICE.url + SERVICE.service  + 'usuario/uploadimagem';

							var formData = new FormData();
							formData.append('uploadedFile', element[0].files[0]);
							formData.append('idUsuario', $rootScope.user.id);


							$http.post(url, formData, {headers: {'Content-Type': undefined}})
								.success(function(res){
									if(res.erro > 0){
										toastr.error(res.mensagem)
									}else{
										toastr.success(res.mensagem);
										location.reload();
									}
								}).error(function(err) {
									toastr.warning('Tente novamente mais tarde.');
									toastr.error('Não foi possível salvar a foto.');
								});

						}else{
							angular.element(element).val("");
							toastr.error("Imagem inválida.");
						}
					});
				});
			}
		}
	}
]);

portalServices.directive('onLongPress', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $elm, $attrs) {
			$elm.bind('touchstart', function(evt) {
				// Locally scoped variable that will keep track of the long press
				$scope.longPress = true;

				// We'll set a timeout for 600 ms for a long press
				$timeout(function() {
					if ($scope.longPress) {
						// If the touchend event hasn't fired,
						// apply the function given in on the element's on-long-press attribute
						$scope.$apply(function() {
							$scope.$eval($attrs.onLongPress)
						});
					}
				}, 300);
			});

			$elm.bind('touchend', function(evt) {
				// Prevent the onLongPress event from firing
				$scope.longPress = false;
				// If there is an on-touch-end function attached to this element, apply it
				if ($attrs.onTouchEnd) {
					$scope.$apply(function() {
						$scope.$eval($attrs.onTouchEnd)
					});
				}
			});
		}
	};
})

portalServices.service('core', ['$rootScope', '$http', '$cookies', 'SERVICE', function($rootScope, $http, $cookies, SERVICE){
	this.addAmigo = function (id, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.solicitacao + 'solicitar';
		var data = {
			"amigo": {
				"id": id
			},
			"usuario": {
				"id": $rootScope.user.id
			}
		};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao adicionar o amigo."});
		});
	}
	this.addUsuarioGrupoEmpresa = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'grupo/adicionar/usuario';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao adicionar o usuário."});
		});
	}
	this.addUsuarioGrupoHistoria = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'equipe/inserirgrupo';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível inserir o grupo na história."});
		});
	}
	this.removeUsuarioGrupoEmpresa = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'grupo/remover/usuario';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível remover o usuário."});
		});
	}
	this.adicionaAmigoEquipe = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.solicitacao + 'equipe/solicitar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao realizar esta solicitação."});
		});
	}
	this.adicionaAmigoEquipeHistoria = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'equipe/inserir';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao realizar esta solicitação."});
		});
	}
	this.adicionaCanvas = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.canvas + 'criar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao adicionar o canvas."});
		});
	}
	this.adicionaSwot = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'swot/salvar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao criar o SWOT."});
		});
	}
	this.analiseSwot = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'swot/analise';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao exibir a análise."});
		});
	}
	this.adicionaNotasSwot = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'swot/InsereNota';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao salvar as notas."});
		});
	}
	this.excluirSwot = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'swot/delete';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível excluir o SWOT."});
		});
	}
	this.adicionaSwotItem = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'swot/AdicionaItem';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao criar o item do SWOT."});
		});
	}

	this.excluirSwotItem = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'swot/DeleteItem';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível excluir o item do SWOT."});
		});
	}
	this.analisePerfilUsuario = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'usuario/analise';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar as informações."});
		});
	}
		this.analiseHistoriaEmpresa = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'ideia/analise';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar as informações."});
		});
	}
		this.analiseBrainstorm = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'brainstorming/analise';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar as informações."});
		});
	}
	this.listaSwotItem = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'swot/ListaItem';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao criar o item do SWOT."});
		});
	}
	this.loadSwot = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'swot/lista';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi posssível listar os itens do SWOT."});
		});
	}
	this.adicionarPorEmail = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'email/convite';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response){
			fn({"erro": 1, 'mensagem':"Não foi possível enviar o e-mail"});
		})
	}
	this.apagaCanvas = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.canvas + 'apagar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
    			fn({"erro": 0, "mensagem":"Item Excluido com Sucesso."});
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir o canvas."});
		});
	}
	this.TarefaAcaoSalvar = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.planoacao + 'adicionaracao';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao criar o projeto."});
		});
	}
	this.TarefaAcaoEditar = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.planoacao + 'alteraracao';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível editar a atividade."});
		});
	}
	this.adicionarImagemUsuario = function (data, fn){
		var url = SERVICE.url + SERVICE.service  + 'usuario/uploadimagem';
		$http.post(url, data, {
			headers: {'Content-Type': undefined}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível adicionar a imagem"});
		});
	}
	this.adicionaBrainstorming = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.brainstorming + 'criar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao criar a ideia."});
		});
	}
	this.adicionaBrainstormingGrupo = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'grupo/' + SERVICE.brainstorming + 'criar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao criar o grupo."});
		});
	}
	this.addDocEmpresa = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'documento/' + 'salvar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao criar o documento."});
		});
	}
	this.adicionaGrupoEmpresa = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'grupo/criar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao criar o grupo."});
		});
	}
	this.adicionaUsuarioProjeto = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.projeto + 'incluir/' + 'usuario';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao criar o grupo."});
		});
	}
	this.alteraBrainstorming = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.brainstorming + 'alterar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao alterar a ideia."});
		});
	}
	this.editaGrupoBrainstorming = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.brainstorming + 'alteragrupo';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível alterar a ideia."});
		});
	}
	this.alteraBrainstormingGrupo = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'grupo/' + SERVICE.brainstorming + 'alterar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao alterar o grupo."});
		});
	}
	this.alterarGrupo = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'grupo/alterar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao alterar o grupo."});
		});
	}
	this.alteraSenha = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.usuario + 'alterasenha';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao alterar a senha."});
		});
	}
	this.buscaGruposEmpresa = function (id, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'grupo/lista/grupos/empresa';
		var data = {"id": parseInt(id)};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar os grupos da empresa."});
		});
	}
	this.buscaUsuarioGrupoEmpresa = function (id, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'grupo/lista/usuario';
		var data = {"id": parseInt(id)};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado as pessoas do grupo."});
		});
	}
	this.buscaGrupoUsuarioEmpresa = function (id, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'grupo/lista/grupo/usuario';
		var data = {"id": parseInt(id)};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado as pessoas do grupo."});
		});
	}
	this.buscaRankingEmpresa = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.gamificacao + SERVICE.empresa
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar o ranking da empresa."});
		});
	}
	this.buscaRankingHistoria = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.gamificacao + 'historia'
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar o ranking da historia."});
		});
	}
	this.buscaRankingUsuariosPortal = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.gamificacao + 'portal/' + 'usuario'
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar o ranking do portal."});
		});
	}
	this.buscaRankingEmpresasPortal = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.gamificacao + 'portal/' + SERVICE.empresa
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar o ranking das empresas do portal."});
		});
	}
	this.buscaUsuariosEmpresa = function (id, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'usuarios';
		var data = {"id": parseInt(id)};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar as pessoas da empresa."});
		});
	}
	this.buscarNoticiaPorId = function (id, url){
		return function(){
			var data = {"id": id};
			$http.post(url, data, {
				headers: {'Content-Type': "application/json"}
			}).success(function(response){
				$rootScope.loaderActive = false;
				return response;
			}).error(function(response) {
				$rootScope.loaderActive = false;
			});
		}
	}
	this.cadEmpresa = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'criar';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao cadastrar a empresa."});
		});
	}
	this.cadastrarNoticia = function(data, uploadUrl){
		var fd = new FormData();
		data.setor = (data.setor > 0) ? data.setor : 0;
		data.categoria = (data.categoria > 0) ? data.categoria : 0;
		data.privado = (data.privado > 0) ? true : false;

		fd.append('titulo', encodeURIComponent(data.titulo));
		fd.append('descricao', encodeURIComponent(data.descricao));
		fd.append('isPrivado', data.privado);
		fd.append('texto', data.texto);
		fd.append('autor', encodeURIComponent(data.autor));
		fd.append('link', data.link);
		fd.append('setor', data.setor);
		fd.append('categoria', data.categoria);
		fd.append('usuario', 1);

		if(data.imagem){
			fd.append('uploadedFile', data.imagem);
		}


		$http.post(uploadUrl, fd, {
			headers: {'Content-Type': undefined}
		}).success(function(response){
			if(response.erro){
				toastr.error(response.mensagem);
			}else{
				toastr.success(response.mensagem);
				setTimeout(function(){
					window.location.href = '/noticias';
				}, 800)
			}
			$rootScope.loaderActive = false;
		}).error(function(response) {
			toastr.error("Ocorreu algo inesperado ao salvar a notícia.");
			$rootScope.loaderActive = false;
		});
	}
	this.criarNovaSenha = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'usuario/novasenha'
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao salvar a senha."});
		});
	}
	this.curtirBrainstorming = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'brainstorming/curtir'
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao salvar a senha."});
		});
	}
	this.editaEmpresa = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'alterar';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao alterar a empresa."});
		});
	}
	this.editarNoticia = function(data, uploadUrl){
		var fd = new FormData();
		data.setor = (data.setor > 0) ? data.setor : 0;
		data.categoria = (data.categoria > 0) ? data.categoria : 0;
		data.privado = (data.privado > 0) ? true : false;

		fd.append('id', data.id);
		fd.append('titulo', encodeURIComponent(data.titulo));
		fd.append('descricao', encodeURIComponent(data.descricao));
		fd.append('isPrivado', data.privado);
		fd.append('texto', data.texto);
		fd.append('autor', encodeURIComponent(data.autor));
		fd.append('link', data.link);
		fd.append('setor', data.setor);
		fd.append('categoria', data.categoria);
		fd.append('usuario', $rootScope.user.id);


		$http.post(uploadUrl, fd, {
			headers: {'Content-Type': undefined}
		}).success(function(response){
			if(response.erro){
				toastr.error(response.mensagem);
			}else{
				toastr.success(response.mensagem);
				setTimeout(function(){
					window.location.href = '/noticias';
				}, 800)
			}
			$rootScope.loaderActive = false;
		}).error(function(response) {
			toastr.error("Ocorreu algo inesperado ao salvar a notícia.");
			$rootScope.loaderActive = false;
		});
	}
	this.editaProjeto = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.projeto + 'alterar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao editar o projeto."});
		});
	}
	this.enviarEmail = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'email/email';
		var email = data.email;
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn({"erro": 0, 'mensagem':"E-mail enviado"});
			}else{
				fn({"erro": 1, 'mensagem':"1 - Não foi possível enviar o e-mail"});
			}
		}).error(function(response){
			fn({"erro": 1, 'mensagem':"Não foi possível enviar o e-mail"});
		})
	}
	this.enviarEmailContato = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'email/contato'
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao enviar o email."});
		});
	}
	this.enviarEmailRecuperaSenha = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'email/recuperar'
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao enviar o email."});
		});
	}
	this.excluirAcao = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.planoacao + 'excluiracao';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir a ação."});
		});
	}
	this.excluirBrainstorming = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.brainstorming + 'excluir';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir a ideia."});
		});
	}
	this.excluirBrainstormingComentario = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.brainstorming + 'excluircomentario';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível excluir o comentário da ideia."});
		});
	}
	this.excluirBrainstormingGrupo = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'grupo/' + SERVICE.brainstorming + 'excluir';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir o grupo."});
		});
	}
	this.excluirGrupo = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'grupo/excluir';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir o grupo."});
		});
	}
	this.excluirHistorias = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'inativar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir a história."});
		});
	}
	this.loadfechamentoHistorias = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'fechamento';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar as informações de fechamento da história."});
		});
	}
	this.saveFechamentoHistorias = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'fechar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao salvar o fechamento da história."});
		});
	}
	this.savePlanoAcao = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'planoacao/salvar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao salvar o plano de ação."});
		});
	}
	this.editPlanoAcao = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'planoacao/alterar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao editar o plano de ação."});
		});
	}
	this.excluirPlanoAcao = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'planoacao/excluir/projeto';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir o plano de ação."});
		});
	}
	this.loadPlanoAcao = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'planoacao/lista';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao salvar o plano de ação."});
		});
	}
	this.excluirHistoriasLink = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'link/remover';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir o link da história."});
		});
	}
	this.excluirHistoriasImagem = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'imagem/excluir';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir a imagem da história."});
		});
	}
	this.excluirHistoriasImagemTemp = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'deletetemp';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir as imagens temporárias."});
		});
	}
	this.excluirProjeto = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.projeto + 'excluir/' + 'projeto';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir o projeto."});
		});
	}
	this.excluirUsuarioEmpresa = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'amigos/remover';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado!."});
		});
	}
	this.excluirUsuarioProjeto = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.projeto + 'excluir/' + 'equipe';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir o usuário."});
		});
	}
	this.inativarEmpresa = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'inativar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao excluir a empresa."});
		});
	}
	this.loadAcoesProjeto = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.projeto + 'acao';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível buscar as ações."});
		});
	}
	this.loadAmigos = function(fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.solicitacao + 'amigos/usuario';
		$http.post(url, {"id": parseInt($rootScope.user.id)}, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar os amigos."});
		});
	}
	this.loadProjetoBrainstorming = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.projeto + 'brainstorming';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar a ideia."});
		});
	}
	/* Projeto 5w2h
	this.loadProjetoAcao = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.projeto + 'acao';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar a ideia."});
		});
	}*/
	this.loadProjetoAcao = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.planoacao + 'acao';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível carregar as atividades do plano de acao."});
		});
	}
	this.loadBrainstorming = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.brainstorming + 'lista/historia';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar a ideia."});
		});
	}
	this.loadBrainstormingComentarios = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.brainstorming + 'lista/comentario';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível carregar os comentários do brainstorming."});
		});
	}
	this.loadBrainstormingGrupo = function (data, fn){
		var url = SERVICE.url + SERVICE.service + 'grupo/' + SERVICE.brainstorming + 'lista';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar os grupos."});
		});
	}
	this.loadBrainstormingNoGrupo = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.brainstorming + 'lista/grupo';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar os grupos."});
		});
	}
	this.loadCanvas = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.canvas + 'lista';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar o canvas."});
		});
	}
	this.loadCnaes = function(fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'cnaes';
		$http.get(url).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar os cnaes."});
		});
	}
	this.loadDocumentos = function(data, fn){
		var url = SERVICE.url + SERVICE.service + 'documento/' + 'lista';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar o documento"});
		});
	}
	this.loadEmpresas = function(fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'lista/empresas';
		var id = {"id": parseInt($rootScope.user.id)};

		$http.post(url, id, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar as empresas."});
		});
	}
	this.loadEmpresaUsuario = function (id, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'lista/empresa';
		var data = {
			"idEmpresa": parseInt(id),
			"idUsuario": parseInt($rootScope.user.id)
		};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao realizar esta solicitação."});
		});
	}
	this.loadFaixaFaturamento = function(fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.faixa + 'faturamento/' + 'todos';
		$http.get(url).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar a faixa de faturamento."});
		});
	}
	this.loadFaixaFuncionarios = function(fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.faixa + 'funcionario/' + 'todos';
		$http.get(url).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar a faixa de funcionarios."});
		});
	}
	this.loadHistoriasData = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'buscar';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar a história."});
		});
	}
	this.loadHistorias = function(data, fn){
        var url = "";
        if (data.idEmpresa == null){
    		url = SERVICE.url + SERVICE.service + SERVICE.historia + 'usuario';
        }else{
    		url = SERVICE.url + SERVICE.service + SERVICE.historia + 'usuario/empresa';
        }

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar a história."});
		});
	}
	this.loadHistoriasEmpresa = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'usuario/empresa';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar a história."});
		});
	}
	this.loadHistoriaEquipe = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'equipe/listar';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar os amigos da empresa."});
		});
	}
	this.loadHistoriasImagem = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'imagens';

		$http.post(url, data, {
			headers: {'Content-Type': 'application/json'}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar as imagens da historia."});
		});
	}
	this.loadHistoriasLinks = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'link/lista';

		$http.post(url, data, {
			headers: {'Content-Type': 'application/json'}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao buscar os links da história."});
		});
	}
	this.loadNotificacao = function (fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.notificacao + 'lista';
		var data = {
			"id": parseInt($rootScope.user.id)
		};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao realizar esta solicitação."});
		});
	}
	this.LerNotificacao = function (fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.notificacao + 'ler';
		var data = {
			"idUsuario": parseInt($rootScope.user.id)
		};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao realizar esta solicitação."});
		});
	}
	this.loadSolicitacaoAmizades = function(fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.solicitacoesPendentes + 'amigo';
		$http.post(url, {"amigo": {"id": parseInt($rootScope.user.id)}}, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar as solitações."});
		});
	}
	this.loadSolicitacaoEmpresa = function (fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'notificacao';
		var data = {
			"id": parseInt($rootScope.user.id)
		};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao realizar esta solicitação."});
		});
	}
	this.loadSolicitacoesEmpresaPendentes = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.solicitacao + 'equipe/solicitacoes/pendentes';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar as solicitações pendentes."});
		});
	}
	this.loadSolicitacaoPendente = function(fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.solicitacoesPendentes + 'usuario';
		$http.post(url, {"usuario": {"id": parseInt($rootScope.user.id)}}, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar as solicitações pendentes."});
		});
	}
	this.loadTreinamentos = function(fn){
		var url = '../public/js/treiAnda.json';
		$http.get(url).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar os treinamentos."});
		});
	}
	this.loadBadges = function(fn){
		var url = '../public/js/badges.json';
		$http.get(url).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar os badges."});
		});
	}
	this.loadInsignias = function(fn){
		var url = '../public/js/insignias.json';
		$http.get(url).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao carregar as insignias."});
		});
	}
	this.loadUsuarioEmpresa = function (id, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'amigos/empresa';
		var data = {
			"idEmpresa": parseInt(id),
			"idUsuario": parseInt($rootScope.user.id)
		};
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao realizar esta solicitação."});
		});
	}
	this.loadAmigosUser = function (data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'amigos';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível realizar essa solicitação"});
		});
	}

	this.mudarPerfilUsuario = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + 'amigos/perfil';

		$http.post(url, data, {
			headers: {'Content-Type': 'application/json'}

		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao alterar o perfil do usuário."});
		});
	}
	this.pesquisaCEP = function(CEP, fn){
		$http.get('http://viacep.com.br/ws/' + CEP + '/json/'
		).success(function(response){
			if(fn){
				fn(response);
			}else{
				fn({"erro": 1, 'mensagem':"Erro ao retornar Pesquisa CEP"});
			}
		}).error(function(response){
			fn({"erro": 1, 'mensagem':"Não foi possível pesquisar o CEP"});
		})
	}
	this.removerAmigoProjeto = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'equipe/excluir';

		$http.post(url, data, {
			headers: {'Content-Type': 'application/json'}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao alterar o perfil do usuário."});
		});
	}
	this.responderSolicitacao = function(servico, userId, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.solicitacao + servico;
		var data = {
			"amigo": {
				"id": $rootScope.user.id
			},
			"usuario":{
				"id": userId
			}
		};

		if(servico == "aprovar"){
			data['aprovado'] = true;
		}else{
			data['recusado'] = true;
		}
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao "+servico+" a solicitação."});
		});
	}
	this.responderSolicitaoEmpresa = function(servico, empresaId, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.empresa + "solicitacao/" + servico;
		var data = {
			"idUsuario": $rootScope.user.id,
			"idEmpresa": empresaId
		};

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao "+servico+" a solicitação."});
		});
	}
	this.salvarComentarioBrainstorming = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.brainstorming + 'comentar';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível salvar o comentário na ideia!."});
		});
	}
	this.salvarFeedbackProjeto = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.projeto + 'acao/feedback';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao salvar o feedback da ação."});
		});
	}
	this.salvarUsuario = function(data, uploadUrl){
		$http.post(uploadUrl, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(response.erro){
				toastr.error(response.mensagem);
			}else {
				toastr.success(response.mensagem);
				$rootScope.saveUserInfo(response.objeto, 'cadastro');
			}
			$rootScope.loaderActive = false;
		}).error(function(response) {
			toastr.error("Ocorreu algo inesperado ao cadastrar o usuário.");
			$rootScope.loaderActive = false;
		});
	}
	this.salvarHistoria = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'salvar';

		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível salvar a ideia."});
		});
	}
	this.salvarHistoriasImagem = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'uploadimagem';

		$http.post(url, data, {
			headers: {'Content-Type': undefined}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo ao incluir a imagem da história."});
		});
	}
	this.salvarHistoriasLink = function(data, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.historia + 'link/criar';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao salvar o link da ideia."});
		});
	}
	this.search = function(busca, fn){
		var url = SERVICE.url + SERVICE.service + SERVICE.solicitacao + 'pesquisa/usuarios';
		$http.post(url, busca, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			if(fn){
				fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao realizar a pesquisa."});
			}
		});
	}
	this.selecionarBrainstormFechamento = function(busca, fn){
		var url = SERVICE.url + SERVICE.service + 'brainstorming/selecionar';
		$http.post(url, busca, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			if(fn){
				fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao selecionar o brainstorm."});
			}
		});
	}
	this.desselecionarBrainstormFechamento = function(busca, fn){
		var url = SERVICE.url + SERVICE.service + 'brainstorming/desselecionar';
		$http.post(url, busca, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			if(fn){
				fn({"erro": 1, "mensagem":"Ocorreu algo inesperado ao desmarcar o brainstorming."});
			}
		});
	}
	this.usuarioPesquisa = function(data, fn){
		var url = SERVICE.url + SERVICE.service  + 'usuario/pesquisa';
		$http.post(url, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){
			if(fn){
				fn(response);
			}
		}).error(function(response) {
			fn({"erro": 1, "mensagem":"Não foi possível buscar as informações do usuário"});
		});
	}
}]);

portalServices.service('AuthService', function($http, Session, SERVICE, $location, $rootScope ,$cookies){
	this.login = function(credentials, fn){
		var urlServico = SERVICE.url + SERVICE.service + SERVICE.usuario + "criar";
		var data = {};
		if($rootScope.user.id > 0){
			data.id = $rootScope.user.id;
		}
		if(credentials.nome){
			data.nome = credentials.nome;
		}
		data.email = credentials.email;

        if(credentials.imagem){
    		data.url = credentials.imagem;
        }

		switch (credentials.tipo){
			case 'face':
				data.id_face = credentials.idFace;
				break;
			case 'plus':
				data.id_google = credentials.idPlus;
				break;
			case 'email':
				data.senha = credentials.password;
				if(credentials.id_face){
					data.id_face = credentials.id_face;
				}
				if(credentials.id_google){
					data.id_google = credentials.id_google;
				}

				break;
		}

		$http.post(urlServico, data, {
			headers: {'Content-Type': "application/json"}
		}).success(function(response){


			if (response.erro < 1) {
				if(fn){
					if ((!response.objeto.imagem) && (credentials.tipo == 'email')){
						credentials.imagem = response.objeto.imagem;
						credentials.imagemSm = response.objeto.perfilSmall;
						credentials.imagemBg = response.objeto.perfilBig;
					}
					data.url = credentials.imagem;
					fn(response);
				}
			}else{
				toastr.error(response.mensagem);
			};
		}).error(function(response) {
			if(fn){
				fn({"erro": 1, "mensagem": "Ocorreu algo inesperado ao realizar o login."});
			}
		});

	};
});
