/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.portal.inovacao.dto;

/**
 *
 * @author notebook-inova
 */
public class ImagemDTO {
    
    String fileName;
    Long idUsuario;
    Long idEmpresa;
    Long idIdeia;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Long getIdIdeia() {
        return idIdeia;
    }

    public void setIdIdeia(Long idIdeia) {
        this.idIdeia = idIdeia;
    }

    
    
}
