package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.portal.inovacao.model.Noticia;
import br.com.portal.inovacao.model.NoticiaCategoria;
import br.com.portal.inovacao.model.NoticiaSetor;

public class NoticiaDAO {
	
	@Inject
	private EntityManager manager;
	
	public NoticiaDAO() {}
	
	public void salva(Noticia noticia) {
		manager.getTransaction().begin();
		manager.persist(noticia);
		manager.getTransaction().commit();
	}

	public void atualiza(Noticia noticia) {
		manager.getTransaction().begin();
		manager.merge(noticia);
		manager.getTransaction().commit();
	}

	public void apaga(Noticia noticia) {
		manager.getTransaction().begin();
		manager.remove(manager.getReference(Noticia.class, noticia.getId()));
		manager.getTransaction().commit();
	}
	
	public void flush(Noticia noticia) {
		manager.getTransaction().begin();
		manager.persist(noticia);
		manager.flush();
	}
	
	public void inativaNoticia(Noticia noticia) {
		manager.getTransaction().begin();
		manager.merge(noticia);
		manager.getTransaction().commit();
	}
	
	public NoticiaCategoria pesquisaCategoriaPorId(Long id) {
		return manager.find(NoticiaCategoria.class, id);
	}

	public NoticiaSetor pesquisaSetorPorId(Long id) {
		return manager.find(NoticiaSetor.class, id);
	}

	public Noticia pesquisaNoticiaPorId(Long id) {
		return manager.find(Noticia.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<NoticiaCategoria> listaCategorias() {
		return manager.createQuery("from NoticiaCategoria").getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<NoticiaCategoria> listaSetores() {
		return manager.createQuery("from NoticiaSetor").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Noticia> listaNoticias() {
		return manager.createQuery("from Noticia").getResultList();
	}
}