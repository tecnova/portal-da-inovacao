package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.Date;

public class CriaNoticia implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String titulo;
	private String descricao;
	private String texto;
	private String link;
	private String autor;
	private Date data;
	private Long usuario;
	private Long setor;
	private Long categoria;
	private Long imagem;
	private boolean privado;

	public CriaNoticia() {}

	public CriaNoticia(Long id, String titulo, String descricao, String texto, String link, String autor, Date data, Long usuario, Long setor,
			Long categoria, Long imagem, boolean privado) {
		this.id = id;
		this.titulo = titulo;
		this.descricao = descricao;
		this.texto = texto;
		this.link = link;
		this.autor = autor;
		this.data = data;
		this.usuario = usuario;
		this.setor = setor;
		this.categoria = categoria;
		this.imagem = imagem;
		this.privado = privado;
	}

	public Long getId() {
		return id;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getTexto() {
		return texto;
	}

	public String getLink() {
		return link;
	}

	public String getAutor() {
		return autor;
	}

	public Date getData() {
		return data;
	}

	public Long getUsuario() {
		return usuario;
	}

	public Long getSetor() {
		return setor;
	}

	public Long getCategoria() {
		return categoria;
	}

	public Long getImagem() {
		return imagem;
	}

	public boolean isPrivado() {
		return privado;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setUsuario(Long usuario) {
		this.usuario = usuario;
	}

	public void setSetor(Long setor) {
		this.setor = setor;
	}

	public void setCategoria(Long categoria) {
		this.categoria = categoria;
	}

	public void setImagem(Long imagem) {
		this.imagem = imagem;
	}

	public void setPrivado(boolean privado) {
		this.privado = privado;
	}
}
