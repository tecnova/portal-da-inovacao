package br.com.portal.inovacao.service;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import br.com.portal.inovacao.dto.EmailDTO;
import br.com.portal.inovacao.utils.Retorno;

public class ConviteEmail extends Thread {

    private static final int CODIGO_SUCESSO = 0;
    private static final int CODIGO_ERRO = 1;

    private static final String MSG_EMAIL_ENVIADO_COM_SUCESSO = "Email enviado com sucesso!";
    private static final String MSG_ERRO_AO_ENVIAR_EMAIL = "Erro ao enviar email.";

    public Retorno run(EmailDTO dto) {
        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            Session session = Session.getInstance(props, new ConviteEmail.MailAuthenticator());
            MimeMessage msg = new MimeMessage(session);

            String mensagem = "Ola, <br><br> "
                            + "Voce está recebendo um convite para se cadastrar no Portal da Inovacao. <br>"
                            + "Acesse o link http://www.portaldainovacao.com.br/cadastro/" + dto.getEmail() + " para se cadastrar.";

            msg.setContent(mensagem, "text/html");
            msg.setSubject("Portal da Inovação");
            msg.setFrom(new InternetAddress("accion.tecnova@gmail.com"));
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(dto.getEmail()));
            msg.setSentDate(new Date(System.currentTimeMillis()));

            Transport.send(msg);

            return new Retorno(CODIGO_SUCESSO, MSG_EMAIL_ENVIADO_COM_SUCESSO);
        } catch (Exception e) {
            return new Retorno(CODIGO_ERRO, e.getMessage());
        }
    }

    public Retorno RecuperarSenha(EmailDTO dto, String _Link) {
        try {
            
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            Session session = Session.getInstance(props, new ConviteEmail.MailAuthenticator());
            MimeMessage msg = new MimeMessage(session);

            String mensagem = "Ola, <br><br>"
                            + "Clique no Link Abaixo para recuperar a senha. <br>"
                            + "Acesse o link http://www.portaldainovacao.com.br/criar-senha/" + _Link + "/" + dto.getEmail() + " para se cadastrar.";

            msg.setContent(mensagem, "text/html");
            msg.setSubject("Portal da Inovação");
            msg.setFrom(new InternetAddress("accion.tecnova@gmail.com"));
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(dto.getEmail()));
            msg.setSentDate(new Date(System.currentTimeMillis()));

            Transport.send(msg);
            
            return new Retorno(CODIGO_SUCESSO, MSG_EMAIL_ENVIADO_COM_SUCESSO);
            
        } catch (Exception e) {
            return new Retorno(CODIGO_ERRO, e.getMessage());
        }
    }
    
    public Retorno ContatoPagina(EmailDTO dto) {
        try {
            
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            Session session = Session.getInstance(props, new ConviteEmail.MailAuthenticator());
            MimeMessage msg = new MimeMessage(session);

            String mensagem = "Ola, <br><br>"
                            + "Recebemos um Contato pelo Portal da Inovação. <br> <br> <br>"
                            + "Nome: " + dto.getNome() + " <br> "
                            + "Email: " + dto.getEmail() + " <br> "
                            + "Mensagem : " + dto.getMensagem() + " <br> ";
            

            msg.setContent(mensagem, "text/html");
            msg.setSubject("Contato pelo Portal da Inovação");
            msg.setFrom(new InternetAddress("accion.tecnova@gmail.com"));
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress("accion.tecnova@gmail.com"));
            msg.setSentDate(new Date(System.currentTimeMillis()));

            Transport.send(msg);
            
            return new Retorno(CODIGO_SUCESSO, MSG_EMAIL_ENVIADO_COM_SUCESSO);
            
        } catch (Exception e) {
            return new Retorno(CODIGO_ERRO, e.getMessage());
        }
    }

    private class MailAuthenticator extends Authenticator {

        private PasswordAuthentication authentication;

        public MailAuthenticator() {
            String username = "accion.tecnova@gmail.com";
            String password = "aw2000(accion";
            authentication = new PasswordAuthentication(username, password);
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }
}