package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class EmpresaPerfilUsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idEmpresa;
	private String nomeUsuario;
	private String perfilEmpresaUsuario;
	private String razaoSocial;

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public String getPerfilEmpresaUsuario() {
		return perfilEmpresaUsuario;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public void setPerfilEmpresaUsuario(String perfilEmpresaUsuario) {
		this.perfilEmpresaUsuario = perfilEmpresaUsuario;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
}
