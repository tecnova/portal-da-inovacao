package br.com.portal.inovacao.facade;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.inject.Inject;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.portal.inovacao.dao.EmpresaDAO;
import br.com.portal.inovacao.model.Empresa;
import br.com.portal.inovacao.utils.Retorno;

public class EmpresaImagemFacade {
	
	private static int CODIGO_SUCESSO = 0;
	private static int CODIGO_ERRO = 1;
	
	private static final String MSG_IMAGEM_SALVA_COM_SUCESSO = "Imagem salva com sucesso.";
	private static final String MSG_OCORREU_UM_ERRO_AO_SALVAR_IMAGEM = "Ocorreu um erro ao salvar imagem.";
	private static final String MSG_EMPRESA_NAO_ENCONTRADA = "Empresa não encontrada.";

	private static String UPLOAD_FILE_SERVER = "/var/www/html/imagens/empresa/";

	@Inject
	private EmpresaDAO empresaDAO;

	 private boolean criaDiretorio(Map<String, List<InputPart>> map) {
            try {
                return new File(UPLOAD_FILE_SERVER + map.get("id").get(0).getBodyAsString()).mkdirs();
           } catch (IOException e) {
                e.printStackTrace();
           }
            return false;
	 }

	public Retorno salvaImagem(MultipartFormDataInput multipartFormDataInput) {
            Map<String, List<InputPart>> map = multipartFormDataInput.getFormDataMap();

            try {
                String idEmpresa = map.get("id").get(0).getBodyAsString();
                Empresa empresa = empresaDAO.pesquisaPorId(Long.parseLong(idEmpresa));

                if (empresa == null)
                    return new Retorno(CODIGO_ERRO, MSG_EMPRESA_NAO_ENCONTRADA);

//                Retorno _Retorno = new Retorno();
//                
//                _Retorno.setErro(CODIGO_ERRO);
//                _Retorno.setMensagem("Estou tentando Criar um Diretório ");
//                _Retorno.setObjeto(criaDiretorio(map));
//
//                return _Retorno;
                
                criaDiretorio(map);
                
                String fileName = "logo.jpg";

                redimensionaImagem(enviaImagemParaServidor(fileName, map), 768, 1024);

                empresa.setImagem(true);
                empresaDAO.salvar(empresa);

                return new Retorno(CODIGO_SUCESSO, MSG_IMAGEM_SALVA_COM_SUCESSO, fileName);
            } catch (IOException e) {
                return new Retorno(CODIGO_ERRO, MSG_OCORREU_UM_ERRO_AO_SALVAR_IMAGEM);
            }
	}

	private String enviaImagemParaServidor(String fileName, Map<String, List<InputPart>> map) throws IOException {
            InputStream inputStream = null;
            List<InputPart> lstInputPart = map.get("uploadedFile");
            String fileServer = null;

            if (lstInputPart != null) {
                for (InputPart inputPart : lstInputPart) {
                    if (null != fileName && !"".equalsIgnoreCase(fileName)) {
                        inputStream = inputPart.getBody(InputStream.class, null);
                        fileServer = gravaImagem(inputStream, fileName, map);
                        inputStream.close();
                    }
                }
            }
            return fileServer;
	}

	private String gravaImagem(InputStream inputStream, String fileName, Map<String, List<InputPart>> map) throws IOException {
            OutputStream outputStream = null;
            String qualifiedUploadFilePath = UPLOAD_FILE_SERVER + map.get("id").get(0).getBodyAsString() + "/" + fileName;

            try {
                outputStream = new FileOutputStream(new File(qualifiedUploadFilePath));
                int read = 0;
                byte[] bytes = new byte[1024];
                while ((read = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
                }
                outputStream.flush();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally {
                outputStream.close();
            }
            return qualifiedUploadFilePath;
	}
	
	public static void redimensionaImagem(String caminhoImg, Integer imgLargura, Integer imgAltura) throws IOException {  
            BufferedImage imagem = ImageIO.read(new File(caminhoImg));  

            Double novaImgLargura = (double) imagem.getWidth();  
            Double novaImgAltura = (double) imagem.getHeight();  

            Double imgProporcao = null;  
            if (novaImgLargura >= imgLargura) {  
                imgProporcao = (novaImgAltura / novaImgLargura);  
                novaImgLargura = (double) imgLargura;  
                novaImgAltura = (novaImgLargura * imgProporcao);  
                while (novaImgAltura > imgAltura) {  
                    novaImgLargura = (double) (--imgLargura);  
                    novaImgAltura = (novaImgLargura * imgProporcao);  
                }  
            } else if (novaImgAltura >= imgAltura) {  
                imgProporcao = (novaImgLargura / novaImgAltura);  
                novaImgAltura = (double) imgAltura;  
                while (novaImgLargura > imgLargura) {  
                    novaImgAltura = (double) (--imgAltura);  
                    novaImgLargura = (novaImgAltura * imgProporcao);  
                }  
            }  

            BufferedImage novaImagem = new BufferedImage(novaImgLargura.intValue(), novaImgAltura.intValue(), BufferedImage.TYPE_INT_RGB);  
            Graphics g = novaImagem.getGraphics();  
            g.drawImage(imagem.getScaledInstance(novaImgLargura.intValue(), novaImgAltura.intValue(), 10000), 0, 0, null);  
            g.dispose();  

            ImageIO.write(novaImagem, "JPG", new File(caminhoImg));  
        } 
}
