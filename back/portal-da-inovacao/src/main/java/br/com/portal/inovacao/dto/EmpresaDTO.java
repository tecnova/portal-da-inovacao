package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class EmpresaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idEmpresa;
    private String cnpj;
    private String razaoSocial;
    private String logradouro;
    private String numero;
    private String complemento;
    private String bairro;
    private String municipio;
    private String uf;
    private String cep;
    private Long idCnae;
    private Long idFaixaFuncionario;
    private Long idFaixaFaturamento;
    private Long usuario;
    private boolean imagem;
    private String telefone;

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public String getMunicipio() {
        return municipio;
    }

    public String getUf() {
        return uf;
    }

    public String getCep() {
        return cep;
    }

    public Long getIdCnae() {
        return idCnae;
    }

    public Long getIdFaixaFuncionario() {
        return idFaixaFuncionario;
    }

    public Long getIdFaixaFaturamento() {
        return idFaixaFaturamento;
    }

    public Long getUsuario() {
        return usuario;
    }

    public boolean isImagem() {
        return imagem;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public void setIdCnae(Long idCnae) {
        this.idCnae = idCnae;
    }

    public void setIdFaixaFuncionario(Long idFaixaFuncionario) {
        this.idFaixaFuncionario = idFaixaFuncionario;
    }

    public void setIdFaixaFaturamento(Long idFaixaFaturamento) {
        this.idFaixaFaturamento = idFaixaFaturamento;
    }

    public void setUsuario(Long usuario) {
        this.usuario = usuario;
    }

    public void setImagem(boolean imagem) {
        this.imagem = imagem;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
        
        
}
