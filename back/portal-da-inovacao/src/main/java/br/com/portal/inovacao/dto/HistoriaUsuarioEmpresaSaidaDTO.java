package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class HistoriaUsuarioEmpresaSaidaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idIdeia;
    private String tituloIdeia;
    private String descricaoIdeia;
    private Long idUsuario;
    private Long idEmpresa;
    private String usuario;
    private Date dataLimite;
    private Long prioridade;
    private List<UsuarioDTO> lstUsuario;

    public Long getIdIdeia() {
        return idIdeia;
    }

    public void setIdIdeia(Long idIdeia) {
        this.idIdeia = idIdeia;
    }

    public String getTituloIdeia() {
        return tituloIdeia;
    }

    public void setTituloIdeia(String tituloIdeia) {
        this.tituloIdeia = tituloIdeia;
    }

    
    public String getDescricaoIdeia() {
        return descricaoIdeia;
    }

    public void setDescricaoIdeia(String descricaoIdeia) {
        this.descricaoIdeia = descricaoIdeia;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

        
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getDataLimite() {
        return dataLimite;
    }

    public void setDataLimite(Date dataLimite) {
        this.dataLimite = dataLimite;
    }

    public Long getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(Long prioridade) {
        this.prioridade = prioridade;
    }

    public List<UsuarioDTO> getLstUsuario() {
        return lstUsuario;
    }

    public void setLstUsuario(List<UsuarioDTO> lstUsuario) {
        this.lstUsuario = lstUsuario;
    }
    
    
}
