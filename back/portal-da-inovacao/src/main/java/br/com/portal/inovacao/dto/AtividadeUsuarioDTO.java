package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.Date;

public class AtividadeUsuarioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private int idAtividade;
    private int idEmpresa;
    private int idUsuario;
    private Date dtAtividade;
    private int idHistoria;
    private int idBrainstorming;
    private int idCanvas;
    private int idNoticia;
    private int idTreinamento;
    private int idDocumento;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAtividade() {
        return idAtividade;
    }

    public void setIdAtividade(int idAtividade) {
        this.idAtividade = idAtividade;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getDtAtividade() {
        return dtAtividade;
    }

    public void setDtAtividade(Date dtAtividade) {
        this.dtAtividade = dtAtividade;
    }

    public int getIdHistoria() {
        return idHistoria;
    }

    public void setIdHistoria(int idHistoria) {
        this.idHistoria = idHistoria;
    }

    public int getIdBrainstorming() {
        return idBrainstorming;
    }

    public void setIdBrainstorming(int idBrainstorming) {
        this.idBrainstorming = idBrainstorming;
    }

    public int getIdCanvas() {
        return idCanvas;
    }

    public void setIdCanvas(int idCanvas) {
        this.idCanvas = idCanvas;
    }

    public int getIdNoticia() {
        return idNoticia;
    }

    public void setIdNoticia(int idNoticia) {
        this.idNoticia = idNoticia;
    }

    public int getIdTreinamento() {
        return idTreinamento;
    }

    public void setIdTreinamento(int idTreinamento) {
        this.idTreinamento = idTreinamento;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

   
    
}