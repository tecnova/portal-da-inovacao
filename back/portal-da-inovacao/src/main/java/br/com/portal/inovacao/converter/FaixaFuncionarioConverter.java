package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.FaixaFuncionarioDTO;
import br.com.portal.inovacao.model.FaixaFuncionario;

public class FaixaFuncionarioConverter {
	
	public static FaixaFuncionarioDTO converterFaixaFuncionario(FaixaFuncionario faixaFuncionario) {
		FaixaFuncionarioDTO funcionarioDTO = new FaixaFuncionarioDTO();
		funcionarioDTO.setId(faixaFuncionario.getId());
		funcionarioDTO.setDescricao(faixaFuncionario.getDescricao());
		funcionarioDTO.setQuantidadeInicial(faixaFuncionario.getQuantidadeInicial());
		funcionarioDTO.setQuantidadeFinal(faixaFuncionario.getQuantidadeFinal());
		return funcionarioDTO;
	}
}
