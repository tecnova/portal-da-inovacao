package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.portal.inovacao.model.FaixaFuncionario;

public class FaixaFuncionarioDAO {
	
	@Inject
	private EntityManager em;
	
	public List<FaixaFuncionario> todos() {
		return em.createQuery("from FaixaFuncionario", FaixaFuncionario.class).getResultList();
	}
	
	public FaixaFuncionario pesquisaPorId(Long id) {
		return em.find(FaixaFuncionario.class, id);
	}
}
