package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.List;

public class GrupoBrainstormingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long idHistoria;
    private String descricao;
    private List<BrainstormingDTO> lstBrainstorming;
    private List<BrainstormingDTO> lstBrainstormingDisponivel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdHistoria() {
        return idHistoria;
    }

    public void setIdHistoria(Long idHistoria) {
        this.idHistoria = idHistoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<BrainstormingDTO> getLstBrainstorming() {
        return lstBrainstorming;
    }

    public void setLstBrainstorming(List<BrainstormingDTO> lstBrainstorming) {
        this.lstBrainstorming = lstBrainstorming;
    }

    public List<BrainstormingDTO> getLstBrainstormingDisponivel() {
        return lstBrainstormingDisponivel;
    }

    public void setLstBrainstormingDisponivel(List<BrainstormingDTO> lstBrainstormingDisponivel) {
        this.lstBrainstormingDisponivel = lstBrainstormingDisponivel;
    }
    
    
}
