package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.EmpresaPerfilUsuarioDTO;
import br.com.portal.inovacao.model.EmpresaEquipe;

public class EmpresaPerfilUsuarioConverter {
	
	public static EmpresaPerfilUsuarioDTO converterEmpresaPerfilUsuario(EmpresaEquipe empresaEquipe) {
		EmpresaPerfilUsuarioDTO dto = new EmpresaPerfilUsuarioDTO();
		dto.setIdEmpresa(empresaEquipe.getEmpresa().getId());
		dto.setRazaoSocial(empresaEquipe.getEmpresa().getRazaoSocial());
		dto.setNomeUsuario(empresaEquipe.getUsuario().getNome());
		dto.setPerfilEmpresaUsuario(empresaEquipe.getPerfilEmpresa().getNome());
		return dto;
	}
}