package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.UsuarioConverter.converterUsuario;
import static br.com.portal.inovacao.converter.UsuarioPerfilConverter.converterUsuarioPerfil;
import static br.com.portal.inovacao.utils.ConverteSenhaParaMD5.convertPasswordToMD5;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.PerfilDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.AlteraUsuario;
import br.com.portal.inovacao.dto.CriaUsuarioEntrada;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.PerfilDTO;
import br.com.portal.inovacao.dto.UsuarioPerfilEntrada;
import br.com.portal.inovacao.model.Perfil;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.utils.ConverteSenhaParaMD5;
import br.com.portal.inovacao.utils.Retorno;

public class UsuarioFacade {
	
	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;
	private static final String MSG_NAO_FOI_ENCONTRADO_USUARIO_PARA_ALTERAR_PERFIL = "Não foi encontrado Usuário para alterar perfil.";
	private static final String MSG_NAO_FOI_ENCONTRADO_NENHUM_USUARIO_PARA_SER_REMOVIDO = "Não foi encontrado nenhum Usuário para ser removido.";
	private static final String MSG_PERFIL_ATUALIZADO_COM_SUCESSO = "Perfil atualizado com sucesso!";
	private static final String MSG_USUARIO_EXCLUIDO_COM_SUCESSO = "Usuário Excluído com sucesso";
	private static final String MSG_USUARIO_ALTERADO_COM_SUCESSO = "Usuário alterado com sucesso!";
	private static final String MSG_USUARIO_NAO_EXISTE_NO_BANCO_DE_DADOS = "Usuário não existe no banco de dados!";
	private static final String MGS_LOGIN_INVALIDO = "Login inválido!";
	private static final String MSG_LOGIN_EFETUADO_COM_SUCESSO = "Login Efetuado com sucesso!";
	private static final String MSG_CADASTRO_EFETUADO_COM_SUCESSO = "Cadastro efetuado com sucesso!";
	private static final String MSG_CONTA_VINCULADA_COM_SUCESSO = "Conta vinculada com sucesso!";
	private static final String MSG_CONTA_JA_ESTA_VINCULADA = "Conta já está vinculada.";
	private static final String MSG_EMAIL_DO_USUARIO_DEVE_SER_PREENCHIDO = "Email do Usuário deve ser preenchido.";
	private static final String MSG_NOME_DO_USUARIO_DEVE_SER_PREENCHIDO = "Nome do Usuário deve ser preenchido.";
	private static final String MSG_SENHA_DEVE_SER_PREENCHIDA = "Senha deve ser preenchida.";

	@Inject
	private UsuarioDAO dao;

	@Inject
	private PerfilDAO perfilDao;

	public Retorno salvar(CriaUsuarioEntrada dto) throws Exception {
            
            if (dto.getId_face() != null)
                return usuarioFacebook(dto);

            if (dto.getId_google() != null)
                return usuarioGooglePlus(dto);

            if (dto.getNome() != null) {
                return usuarioEmailSenha(dto);
            }else{
                return loginEmailSenha(dto);
            }

	}

	private Retorno usuarioFacebook(CriaUsuarioEntrada dto) {
            if (dto.getId() != null) {
                List<Usuario> idFacebook = dao.pesquisaIdFacebook(dto.getId_face());

                if (idFacebook != null)
                        return new Retorno(CODIGO_ERRO, MSG_CONTA_JA_ESTA_VINCULADA);

                Usuario usuario = dao.pesquisaPorId(dto.getId());
                usuario.setId_face(dto.getId_face());
                dao.atualiza(usuario);

                return new Retorno(CODIGO_SUCESSO, MSG_CONTA_VINCULADA_COM_SUCESSO);
            }

            List<Usuario> idFacebook = dao.pesquisaIdFacebook(dto.getId_face());
            if (!idFacebook.isEmpty()) {
                return new Retorno(0, MSG_LOGIN_EFETUADO_COM_SUCESSO, converterUsuario(idFacebook.get(0)));
            }

            List<Usuario> email = dao.pesquisaEmail(dto.getEmail());
            if (!email.isEmpty()) {
                email.get(0).setId_face(dto.getId_face());
                dao.atualiza(email.get(0));
                return new Retorno(CODIGO_SUCESSO, MSG_LOGIN_EFETUADO_COM_SUCESSO, converterUsuario(email.get(0)));
            }

            Usuario usuarioFacebook = criaUsuarioFacebook(dto);
            dao.salva(usuarioFacebook);

            return new Retorno(CODIGO_SUCESSO, MSG_CADASTRO_EFETUADO_COM_SUCESSO, converterUsuario(usuarioFacebook));
	}

	private Retorno usuarioGooglePlus(CriaUsuarioEntrada dto) {
            if (dto.getId() != null) {
                List<Usuario> idGooglePlus = dao.pesquisaIdGooglePlus(dto.getId_face());

                if (idGooglePlus != null)
                        return new Retorno(CODIGO_ERRO, MSG_CONTA_JA_ESTA_VINCULADA);

                Usuario usuario = dao.pesquisaPorId(dto.getId());
                usuario.setId_google(dto.getId_google());
                dao.atualiza(usuario);

                return new Retorno(CODIGO_SUCESSO, MSG_CONTA_VINCULADA_COM_SUCESSO);
            }

            List<Usuario> idGooglePlus = dao.pesquisaIdGooglePlus(dto.getId_google());
            if (!idGooglePlus.isEmpty()) {
                return new Retorno(CODIGO_SUCESSO, MSG_LOGIN_EFETUADO_COM_SUCESSO, converterUsuario(idGooglePlus.get(0)));
            }

            List<Usuario> email = dao.pesquisaEmail(dto.getEmail());
            if (!email.isEmpty()) {
                email.get(0).setId_google(dto.getId_google());
                dao.atualiza(email.get(0));
                return new Retorno(CODIGO_SUCESSO, MSG_LOGIN_EFETUADO_COM_SUCESSO, converterUsuario(email.get(0)));
            }

            Usuario usuarioGooglePlus = criaUsuarioGooglePlus(dto);
            dao.salva(usuarioGooglePlus);

            return new Retorno(CODIGO_SUCESSO, MSG_CADASTRO_EFETUADO_COM_SUCESSO, converterUsuario(usuarioGooglePlus));
	}

	private Retorno loginEmailSenha(CriaUsuarioEntrada dto) throws NoSuchAlgorithmException {
            List<Usuario> loginESenha = dao.pesquisaLoginESenha(dto.getEmail(), convertPasswordToMD5(dto.getSenha()));

            if (!loginESenha.isEmpty() && loginESenha.get(0).isAtivo()) {
                    return new Retorno(CODIGO_SUCESSO, MSG_LOGIN_EFETUADO_COM_SUCESSO, converterUsuario(loginESenha.get(0)));
            } else {
                    return new Retorno(CODIGO_ERRO, MGS_LOGIN_INVALIDO);
            }
	}

	private Retorno usuarioEmailSenha(CriaUsuarioEntrada dto) throws NoSuchAlgorithmException {
            List<Usuario> email = dao.pesquisaEmail(dto.getEmail());

            if (!email.isEmpty()) {
                email.get(0).setSenha(convertPasswordToMD5(dto.getSenha()));
                dao.atualiza(email.get(0));
                return new Retorno(CODIGO_SUCESSO, MSG_LOGIN_EFETUADO_COM_SUCESSO, converterUsuario(email.get(0)));
            }

            Usuario usuarioEmail;
            try {
                usuarioEmail = criaUsuarioEmail(dto);
                dao.salva(usuarioEmail);
                return new Retorno(CODIGO_SUCESSO, MSG_LOGIN_EFETUADO_COM_SUCESSO, usuarioEmail);
            } catch (Exception e) {
                return new Retorno(CODIGO_ERRO, e.getMessage());
            }
	}

	private void validaDadosDeEntrada(CriaUsuarioEntrada dto) throws Exception {
            if (dto.getNome() == null || dto.getNome().equals(""))
                throw new Exception(MSG_NOME_DO_USUARIO_DEVE_SER_PREENCHIDO);
            if (dto.getEmail() == null || dto.getEmail().equals(""))
                throw new Exception(MSG_EMAIL_DO_USUARIO_DEVE_SER_PREENCHIDO);
            if (dto.getSenha() == null || dto.getSenha().equals(""))
                throw new Exception(MSG_SENHA_DEVE_SER_PREENCHIDA);
	}

	private Usuario criaUsuarioEmail(CriaUsuarioEntrada dto) throws Exception {
            validaDadosDeEntrada(dto);
            Usuario usuario = dadosComunsUsuario(dto);
            usuario.setSenha(ConverteSenhaParaMD5.convertPasswordToMD5(dto.getSenha()));
            return usuario;
	}

	private Usuario dadosComunsUsuario(CriaUsuarioEntrada dto) {
            Date date = new Date();
            java.sql.Date data = new java.sql.Date(date.getTime());

            Usuario usuario = new Usuario();
            usuario.setPerfis(new ArrayList<Perfil>());
            List<Perfil> perfilUsuario = perfilDao.pesquisaPerfilUsuario("US");

            usuario.setNome(dto.getNome());
            usuario.setEmail(dto.getEmail());
            usuario.setData(data);
            usuario.setPerfil_tema("blue");
            usuario.setAtivo(true);
            usuario.getPerfis().addAll(perfilUsuario);
            return usuario;
	}
	
	public Retorno alterar(AlteraUsuario dto) {
		Usuario usuario = dao.pesquisaPorId(dto.getId());
		
		if (usuario == null)
			return new Retorno(CODIGO_ERRO, MSG_USUARIO_NAO_EXISTE_NO_BANCO_DE_DADOS);
		
		if (dto.getNome() != null)
			usuario.setNome(dto.getNome());
		
		if (dto.getSenha() != null)
			usuario.setSenha(dto.getSenha());
		
		if (dto.getPerfil_tema() != null)
			usuario.setPerfil_tema(dto.getPerfil_tema());
			
		dao.atualiza(usuario);
		
		return new Retorno(CODIGO_SUCESSO, MSG_USUARIO_ALTERADO_COM_SUCESSO);
	}

	public Retorno inativar(InformaIdDTO dto) {
		Usuario usuario = dao.pesquisaPorId(dto.getId());

		if (usuario != null) {
			usuario.setAtivo(false);
			dao.atualiza(usuario);
			return new Retorno(CODIGO_SUCESSO, MSG_USUARIO_EXCLUIDO_COM_SUCESSO);
		}
		return new Retorno(CODIGO_ERRO, MSG_NAO_FOI_ENCONTRADO_NENHUM_USUARIO_PARA_SER_REMOVIDO);
	}

	private Usuario criaUsuarioFacebook(CriaUsuarioEntrada dto) {
		Usuario usuario = dadosComunsUsuario(dto);
		usuario.setId_face(dto.getId_face());
		return usuario;
	}

	private Usuario criaUsuarioGooglePlus(CriaUsuarioEntrada dto) {
		Usuario usuario = dadosComunsUsuario(dto);
		usuario.setId_google(dto.getId_google());
		return usuario;
	}

	public List<UsuarioPerfilEntrada> usuarios() {
		return converterUsuarioPerfil(dao.todos());
	}

	public List<Perfil> perfis() {
		return perfilDao.todos();
	}
	
	public Retorno alteraPerfil(UsuarioPerfilEntrada usuarioPerfil) {
		//Front vai ter que mandar tudo
		Usuario usuario = dao.pesquisaPorId(usuarioPerfil.getId());
		
		if (usuario != null) {
			List<Perfil> perfis = new ArrayList<>();
			for (PerfilDTO dto : usuarioPerfil.getPerfis()) {
				Perfil perfil = perfilDao.pesquisaPorId(dto.getId());
				perfis.add(perfil);
			}
			usuario.setPerfis(perfis);
			dao.atualiza(usuario);
			return new Retorno(CODIGO_SUCESSO, MSG_PERFIL_ATUALIZADO_COM_SUCESSO, converterUsuarioPerfil(usuario));
		}
		return new Retorno(CODIGO_ERRO, MSG_NAO_FOI_ENCONTRADO_USUARIO_PARA_ALTERAR_PERFIL);
	}
}
