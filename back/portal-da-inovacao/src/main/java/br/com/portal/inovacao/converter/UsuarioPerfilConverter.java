package br.com.portal.inovacao.converter;

import static br.com.portal.inovacao.converter.PerfilConverter.converterPerfil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.portal.inovacao.dto.PerfilDTO;
import br.com.portal.inovacao.dto.UsuarioPerfilEntrada;
import br.com.portal.inovacao.model.Perfil;
import br.com.portal.inovacao.model.Usuario;

public class UsuarioPerfilConverter {
	
	public static List<UsuarioPerfilEntrada> converterUsuarioPerfil(List<Usuario> usuarios) {
		List<UsuarioPerfilEntrada> usuariosPerfis = new ArrayList<UsuarioPerfilEntrada>();
		
		for (Usuario usuario : usuarios) 
			usuariosPerfis.add(converterUsuarioPerfil(usuario));
	
		return usuariosPerfis;
	}

	public static UsuarioPerfilEntrada converterUsuarioPerfil(Usuario usuario) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		UsuarioPerfilEntrada usuarioPerfil = new UsuarioPerfilEntrada();
		usuarioPerfil.setId(usuario.getId());
		usuarioPerfil.setNome(usuario.getNome());
		usuarioPerfil.setEmail(usuario.getEmail());
		usuarioPerfil.setDataCadastro(dateFormat.format(usuario.getData()));
		
		List<PerfilDTO> perfilDtos = new ArrayList<>();
		for (Perfil perfil : usuario.getPerfis()) {
			perfilDtos.add(converterPerfil(perfil));
		}
		usuarioPerfil.setPerfis(perfilDtos);
		return usuarioPerfil;
	}
}
