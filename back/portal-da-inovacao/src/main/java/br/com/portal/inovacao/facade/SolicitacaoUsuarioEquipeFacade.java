package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.EmpresaEquipeSolicitacaoConverter.converterSolicitacaoEmpresaEquipe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.EmpresaDAO;
import br.com.portal.inovacao.dao.EmpresaEquipeDAO;
import br.com.portal.inovacao.dao.PerfilEmpresaDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.EmpresaEquipeSolicitacaoSaidaDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.SolicitacaoUsuarioEquipeDTO;
import br.com.portal.inovacao.model.Empresa;
import br.com.portal.inovacao.model.EmpresaEquipe;
import br.com.portal.inovacao.model.PerfilEmpresa;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.utils.Retorno;

public class SolicitacaoUsuarioEquipeFacade {
	
	@Inject
	private PerfilEmpresaDAO perfilEmpresaDAO;
	
	@Inject
	private EmpresaDAO empresaDAO;
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	@Inject
	private EmpresaEquipeDAO empresaEquipeDAO;
	
	private static int CODIGO_SUCESSO = 0;
	private static int CODIGO_ERRO = 1;
	
	private static String MSG_SOLICITACAO_ENVIADA_COM_SUCESSO = "Solicitação enviada com sucesso!";
	private static String MSG_SOLICITACAO_JA_ENVIADA = "Solicitação já foi enviada.";
	
	public Retorno criaSolicitacao(SolicitacaoUsuarioEquipeDTO dto) {
		try {
			validaSeSolicitacaoJaFoiEnviada(dto);
			EmpresaEquipe empresaEquipe = criaEmpresaEquipe(dto);
			empresaEquipeDAO.salvar(empresaEquipe);
			return new Retorno(CODIGO_SUCESSO, MSG_SOLICITACAO_ENVIADA_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	
	private EmpresaEquipe criaEmpresaEquipe(SolicitacaoUsuarioEquipeDTO dto) {
		List<PerfilEmpresa> perfil = perfilEmpresaDAO.pesquisaPerfilAdministrador("US");
		Empresa empresa = empresaDAO.pesquisaPorId(dto.getId_empresa());
		Usuario usuario = usuarioDAO.pesquisaPorId(dto.getId_usuario());
		
		EmpresaEquipe empresaEquipe = new EmpresaEquipe();
		empresaEquipe.setDataSolicitacao(new Date());
		empresaEquipe.setPerfilEmpresa(perfil.get(0));
		empresaEquipe.setEmpresa(empresa);
		empresaEquipe.setUsuario(usuario);
		empresaEquipe.setEnvioEmail(false);
		empresaEquipe.setSolicitacaoAprovada(false);
		return empresaEquipe;
	}
	
	private void validaSeSolicitacaoJaFoiEnviada(SolicitacaoUsuarioEquipeDTO dto) throws Exception {
            List<EmpresaEquipe> existeSolicitacao = empresaEquipeDAO.pesquisaSeExisteSolicitacao(dto.getId_usuario(), dto.getId_empresa());
            try {
                if (!existeSolicitacao.isEmpty())
                    throw new Exception(MSG_SOLICITACAO_JA_ENVIADA);
            } catch (Exception e) {
                    throw new Exception(e.getMessage());
            }
	}
	
	public Retorno solicitacoesPendentes(InformaIdDTO idEmpresa) {
		List<EmpresaEquipe> solicitacoesPendentes = empresaEquipeDAO.pesquisaSolicitacoesPendentes(idEmpresa.getId());
		List<EmpresaEquipeSolicitacaoSaidaDTO> dtos = new ArrayList<EmpresaEquipeSolicitacaoSaidaDTO>();
		
		for (EmpresaEquipe empresaEquipe : solicitacoesPendentes) {
			dtos.add(converterSolicitacaoEmpresaEquipe(empresaEquipe));
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
}
