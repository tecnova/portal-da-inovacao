package br.com.portal.inovacao.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.portal.inovacao.model.NoticiaLog;

public class NoticiaLogDAO {
	
	@Inject
	private EntityManager manager;
	
	public void salva(NoticiaLog noticiaLog) {
		manager.getTransaction().begin();
		manager.persist(noticiaLog);
		manager.getTransaction().commit();
	}
}
