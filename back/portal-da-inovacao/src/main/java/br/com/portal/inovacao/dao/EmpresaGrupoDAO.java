package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.EmpresaGrupo;

public class EmpresaGrupoDAO {
	
	@Inject
	private EntityManager em;
	
	public void salvar(EmpresaGrupo empresaGrupo) {
		em.getTransaction().begin();
		em.merge(empresaGrupo);
		em.getTransaction().commit();
	}
	
	public void excluir(EmpresaGrupo empresaGrupo) {
		em.getTransaction().begin();
		em.remove(empresaGrupo);		
		em.getTransaction().commit();
	}
	
	public EmpresaGrupo pesquisaPorId(Long id) {
		return em.find(EmpresaGrupo.class, id);
	}
	
	@SuppressWarnings("rawtypes")
	public List buscaGrupoEUsuarios(Long idEmpresa) {
		Query query = em.createNativeQuery("SELECT u.nome as nome_usuario, eg.id as id_grupo, eg.nome as nome_grupo "
				+ "FROM usuario u "
				+ "JOIN usuario_grupo ug ON ug.id_usuario = u.id "
				+ "JOIN empresa_grupo eg ON eg.id = ug.id_grupo "
				+ "WHERE eg.id_empresa = :idEmpresa");
		query.setParameter("idEmpresa", idEmpresa);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<EmpresaGrupo> pesquisaPorNome(String nomeGrupo) {
		Query query = em.createQuery("from EmpresaGrupo where nome = :nomeGrupo", EmpresaGrupo.class);
		query.setParameter("nomeGrupo", nomeGrupo);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<EmpresaGrupo> buscaGruposDaEmpresa(Long idEmpresa) {
		Query query = em.createQuery("from EmpresaGrupo where id_empresa = :idEmpresa", EmpresaGrupo.class);
		query.setParameter("idEmpresa", idEmpresa);
		return query.getResultList();
	}
}
