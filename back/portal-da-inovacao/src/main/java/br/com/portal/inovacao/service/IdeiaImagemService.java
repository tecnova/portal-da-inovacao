package br.com.portal.inovacao.service;

import br.com.portal.inovacao.dto.ImagemDTO;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.facade.IdeiaImagemFacade;
import br.com.portal.inovacao.utils.Retorno;
import java.io.File;

@Path("ideia/imagem")
public class IdeiaImagemService {
	
	@Inject
	private IdeiaImagemFacade facade;
	
	@POST
	@Consumes(MULTIPART_FORM_DATA)
	@Produces(APPLICATION_JSON)
	@Path("salvar")
	public Retorno salvaImagem(MultipartFormDataInput multipartFormDataInput) {
		return facade.salvaImagem(multipartFormDataInput);
	}
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("listar")
	public Retorno listaImagens(InformaIdDTO idIdeia) {
		return facade.listaImagensDoServidor(idIdeia);
	}
        
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("excluir")
	public Retorno apagaImagem(ImagemDTO imagem) {
            try{
                File file = new File("/var/www/html/imagens/ideia/" + imagem.getId().toString() + "/" + imagem.getUrl() );
                file.delete();

                Retorno _retorno = new Retorno();
                
                _retorno.setErro(0);
                _retorno.setMensagem("Imagem excluida com Sucesso !!");
                
                return _retorno;
            }
            catch(Exception e)
            {
                Retorno _retorno = new Retorno();
                
                _retorno.setErro(1);
                _retorno.setMensagem(_retorno.getMensagem());
                
                return _retorno;
            }
            
            
	}
        
}
