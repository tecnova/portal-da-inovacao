package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.PerfilEmpresa;

public class PerfilEmpresaDAO {
	
	@Inject
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<PerfilEmpresa> pesquisaPerfilAdministrador(String sigla) {
		return em.createQuery("from PerfilEmpresa where sigla = '" + sigla + "'").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<PerfilEmpresa> pesquisaPorNome(String nome) {
		Query query = em.createQuery("from PerfilEmpresa where nome = :nome");
		query.setParameter("nome", nome);
		return query.getResultList();
	}
	
	public PerfilEmpresa pesquisaPorId(Long id) {
		return em.find(PerfilEmpresa.class, id);
	}
}
