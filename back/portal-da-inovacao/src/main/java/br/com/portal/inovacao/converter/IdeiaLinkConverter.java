package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.IdeiaLinkDTO;
import br.com.portal.inovacao.model.IdeiaLink;

public class IdeiaLinkConverter {
	
	public static IdeiaLinkDTO converterIdeiaLink(IdeiaLink ideiaLink) {
		
		IdeiaLinkDTO dto = new IdeiaLinkDTO();
		dto.setId(ideiaLink.getId());
		dto.setLink(ideiaLink.getLink());
		dto.setIdeia(ideiaLink.getIdeia().getId());
		return dto;
	}
}
