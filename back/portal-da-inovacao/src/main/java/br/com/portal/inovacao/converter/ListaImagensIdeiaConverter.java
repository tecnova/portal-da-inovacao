package br.com.portal.inovacao.converter;

import java.util.ArrayList;
import java.util.List;

import br.com.portal.inovacao.utils.Retorno;

public class ListaImagensIdeiaConverter {
	
	public static Retorno converterListaImagensIdeia(List<String> arquivos) {
		List<String> arquivosConvertidos = new ArrayList<String>();
		
		for (String nomesArquivos : arquivos) {
			arquivosConvertidos.add(nomesArquivos);
		}
		return new Retorno(0, arquivosConvertidos);
	}
}
