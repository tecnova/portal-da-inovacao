package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.EmpresaGrupoConverter.converteEmpresaGrupo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.bytecode.buildtime.spi.ExecutionException;

import br.com.portal.inovacao.dao.EmpresaDAO;
import br.com.portal.inovacao.dao.EmpresaGrupoDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.EmpresaGrupoDTO;
import br.com.portal.inovacao.dto.GrupoUsuarioSaidaDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.model.Empresa;
import br.com.portal.inovacao.model.EmpresaGrupo;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.utils.Retorno;

public class EmpresaGrupoFacade {

	@Inject
	private EmpresaGrupoDAO empresaGrupoDAO;
	
	@Inject
	private EmpresaDAO empresaDAO;
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	private static final int CODIGO_ERRO = 1;
	private static final int CODIGO_SUCESSO = 0;

	private static final String MSG_NOME_DO_GRUPO_DE_SER_PREENCHIDO = "Nome do grupo de ser preenchido.";
	private static final String MSG_GRUPO_CRIADO_COM_SUCESSO = "Grupo criado com sucesso!";
	private static final String MSG_GRUPO_EXCLUIDO_COM_SUCESSO = "Grupo excluido com sucesso!";
	private static final String MSG_NOME_DO_GRUPO_ALTERADO_COM_SUCESSO = "Nome do grupo alterado com sucesso!";
	
	private EmpresaGrupo criaEmpresaGrupo(EmpresaGrupoDTO empresaGrupoDTO) {
		Empresa empresa = empresaDAO.pesquisaPorId(empresaGrupoDTO.getIdEmpresa());

		EmpresaGrupo empresaGrupo = new EmpresaGrupo();
		empresaGrupo.setNome(empresaGrupoDTO.getNome());
		empresaGrupo.setIdEmpresa(empresa);
		return empresaGrupo;
	}
	
	public Retorno criaGrupo(EmpresaGrupoDTO empresaGrupoDTO) {
		try {
			validaDadosDeEntrada(empresaGrupoDTO);
			EmpresaGrupo empresaGrupo = criaEmpresaGrupo(empresaGrupoDTO);
			empresaGrupoDAO.salvar(empresaGrupo);
			return new Retorno(CODIGO_SUCESSO, MSG_GRUPO_CRIADO_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	
	private void validaDadosDeEntrada(EmpresaGrupoDTO empresaGrupoDTO){
		if (empresaGrupoDTO.getNome() == null || empresaGrupoDTO.getNome().trim().equals(""))
			throw new ExecutionException(MSG_NOME_DO_GRUPO_DE_SER_PREENCHIDO);
	}
	
	public Retorno excluiGrupo(InformaIdDTO dto) {
		EmpresaGrupo empresaGrupo = empresaGrupoDAO.pesquisaPorId(dto.getId());
		try {
			empresaGrupoDAO.excluir(empresaGrupo);
			return new Retorno(CODIGO_SUCESSO, MSG_GRUPO_EXCLUIDO_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	
	public Retorno alteraGrupo(EmpresaGrupoDTO dto) {
		EmpresaGrupo empresaGrupo = empresaGrupoDAO.pesquisaPorId(dto.getId());
		try {
			if (dto.getNome() != null)
				empresaGrupo.setNome(dto.getNome());
				empresaGrupoDAO.salvar(empresaGrupo);
			return new Retorno(CODIGO_SUCESSO, MSG_NOME_DO_GRUPO_ALTERADO_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	
	@SuppressWarnings("rawtypes")
	public Retorno listaGrupoEUsuarios(InformaIdDTO idEmpresa) {
		List grupoEUsuarios = empresaGrupoDAO.buscaGrupoEUsuarios(idEmpresa.getId());
		List<GrupoUsuarioSaidaDTO> dtos = new ArrayList<>();
		
		for (int i = 0; i < grupoEUsuarios.size(); i++) {
			Object[] a = (Object[]) grupoEUsuarios.get(i);
			GrupoUsuarioSaidaDTO grupoUsuarioSaidaDTO = new GrupoUsuarioSaidaDTO();			
			
			for (Object object : a) {
				if (object instanceof BigInteger) {
					EmpresaGrupo empresaGrupo = empresaGrupoDAO.pesquisaPorId(((BigInteger)object).longValue());
					grupoUsuarioSaidaDTO.setIdGrupo(empresaGrupo.getId());
				} else {
					List<Usuario> nomeUsuario = usuarioDAO.pesquisaPorNome((String)object);
					List<EmpresaGrupo> nomeGrupo = empresaGrupoDAO.pesquisaPorNome((String)object);
									
//					if (!nomeUsuario.isEmpty())
//						grupoUsuarioSaidaDTO.setNomeUsuario(nomeUsuario.get(0).getNome());
					
					if (!nomeGrupo.isEmpty())
						grupoUsuarioSaidaDTO.setGrupo(nomeGrupo.get(0).getNome());
				}
			}
			dtos.add(grupoUsuarioSaidaDTO);
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
	
	public Retorno listaGruposDaEmpresa(InformaIdDTO idEmpresa) {
		List<EmpresaGrupo> gruposDaEmpresa = empresaGrupoDAO.buscaGruposDaEmpresa(idEmpresa.getId());
		List<EmpresaGrupoDTO> dtos = new ArrayList<EmpresaGrupoDTO>();
		
		for (EmpresaGrupo empresaGrupo : gruposDaEmpresa) {
			dtos.add(converteEmpresaGrupo(empresaGrupo));
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
}
