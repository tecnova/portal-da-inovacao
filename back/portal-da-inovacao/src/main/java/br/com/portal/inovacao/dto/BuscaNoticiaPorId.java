package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class BuscaNoticiaPorId implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	public BuscaNoticiaPorId() {}
	
	public BuscaNoticiaPorId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}