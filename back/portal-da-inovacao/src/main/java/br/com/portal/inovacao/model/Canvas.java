package br.com.portal.inovacao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "Canvas")
public class Canvas {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name="Id_Ideia")
	private Ideia idIdeia;
	
	@ManyToOne
	@JoinColumn(name="Id_Usuario")
	private Usuario idUsuario;
	
	@Column(name="Nr_Item")
	private Long nr_item;
	
	@Column(length=250, name="Descricao")
	private String descricao;
	
	@Column(name="Valor")
	private Double valor;
	
	@Column(name="Data_Cadastro")
	private Date data_cadastro;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Ideia getIdIdeia() {
		return idIdeia;
	}

	public void setIdIdeia(Ideia idIdeia) {
		this.idIdeia = idIdeia;
	}

	public Usuario getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Usuario idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getNr_item() {
		return nr_item;
	}

	public void setNr_item(Long nr_item) {
		this.nr_item = nr_item;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String string) {
		this.descricao = string;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double double1) {
		this.valor = double1;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idIdeia == null) ? 0 : idIdeia.hashCode());
		result = prime * result + ((idUsuario == null) ? 0 : idUsuario.hashCode());
		result = prime * result + ((nr_item == null) ? 0 : nr_item.hashCode());
		result = prime * result	+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		result = prime * result + ((data_cadastro == null) ? 0 : data_cadastro.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Canvas other = (Canvas) obj;
		if (data_cadastro == null) {
			if (other.data_cadastro != null)
				return false;
		} else if (!data_cadastro.equals(other.data_cadastro))
			return false;
		
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		
		if (idUsuario == null) {
			if (other.idUsuario != null)
				return false;
		} else if (!idUsuario.equals(other.idUsuario))
			return false;
		
		if (nr_item == null) {
			if (other.nr_item != null)
				return false;
		} else if (!nr_item.equals(other.nr_item))
			return false;
		
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;

		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;

		if (data_cadastro == null) {
			if (other.data_cadastro != null)
				return false;
		} else if (!data_cadastro.equals(other.data_cadastro))
			return false;

		return true;
	}

}
