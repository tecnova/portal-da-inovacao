package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.portal.inovacao.model.EmpresaEquipe;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;

public class EmpresaEquipeDAO {
	
	@Inject
	private EntityManager em;
	
	private static final String ID_PERFIL_EMPRESA_ADMINISTRADOR = "3"; 
	
	public void salvar(EmpresaEquipe empresaEquipe) {
		em.getTransaction().begin();
		em.merge(empresaEquipe);
		em.getTransaction().commit();
	}
	
	@SuppressWarnings("rawtypes")
	public List buscaEmpresaDoUsuarioComPerfil(Long id) {
		Query query = em.createNativeQuery("SELECT u.nome as nome_usuario, e.razao_social, e.id as id_empresa, pe.nome as perfil_empresa_usuario "
				+ "FROM usuario u "
				+ "JOIN empresa_equipe ee ON ee.id_usuario = u.id "
				+ "JOIN empresa e ON ee.id_empresa = e.id "
				+ "JOIN perfil_empresa pe ON ee.id_perfil_empresa = pe.id "
				+ "WHERE u.id = :id and e.is_empresa_ativa = 1");
		query.setParameter("id", id);
		return query.getResultList();
	} 
	
	@SuppressWarnings("unchecked")
	public List<EmpresaEquipe> pesquisaSeExisteSolicitacao(Long idUsuario, Long idEmpresa) {
            
            List<EmpresaEquipe> _List;
            
            Query query;
            
            query = em.createQuery("from EmpresaEquipe where id_usuario = :idUsuario and id_empresa = :idEmpresa and data_aprovacao is null and data_recusa is null");
            query.setParameter("idUsuario", idUsuario);
            query.setParameter("idEmpresa", idEmpresa);
            
            _List = query.getResultList();
            
            if (_List.size() == 0){
                
                //Verifica se ja foi recusado um convite
                query = em.createQuery("from EmpresaEquipe where id_usuario = :idUsuario and id_empresa = :idEmpresa and not data_recusa is null");
                query.setParameter("idUsuario", idUsuario);
                query.setParameter("idEmpresa", idEmpresa);
                
                _List = query.getResultList();
                
                if (_List.size() > 0) {
                    //Exclui o convite enviado anteriormente
                    
                    String _SQL;
                    
                    _SQL = "";
                    _SQL = _SQL + "Delete ";
                    _SQL = _SQL + "From ";
                    _SQL = _SQL + "     empresa_equipe ";
                    _SQL = _SQL + "Where ";
                    _SQL = _SQL + "     id_usuario = " + idUsuario + " ";
                    _SQL = _SQL + " and id_empresa = " + idEmpresa + " ";
                    
                    ConectaBanco _Banco = new ConectaBanco();

                    Retorno _Retorno;
                    
                    _Retorno = _Banco.Commando(_SQL);

                    if (_Retorno.getErro() == 0 ){
                        _List.clear();
                    }
                }
            }

            return _List;

        }
	
	@SuppressWarnings("unchecked")
	public List<EmpresaEquipe> pesquisaSolicitacoesPendentes(Long id) {
		Query query = em.createQuery("from EmpresaEquipe where id_empresa = :id and solicitacao_aprovada = 0 and data_aprovacao is null and data_recusa is null");
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<EmpresaEquipe> buscaEmpresaDoUsuario(Long idUsuario, Long idEmpresa) {
		Query query = em.createQuery("from EmpresaEquipe where id_usuario = :idUsuario and id_empresa = :idEmpresa", EmpresaEquipe.class);
		query.setParameter("idUsuario", idUsuario);
		query.setParameter("idEmpresa", idEmpresa);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<EmpresaEquipe> buscaPerfilAdministrador(Long idUsuario, Long idEmpresa) {
		Query query = em.createQuery("from EmpresaEquipe where id_perfil_empresa = " + ID_PERFIL_EMPRESA_ADMINISTRADOR + " and id_usuario = :idUsuario and id_empresa = :idEmpresa", EmpresaEquipe.class);
		query.setParameter("idUsuario", idUsuario);
		query.setParameter("idEmpresa", idEmpresa);
		return query.getResultList();
	}
	
	@SuppressWarnings("rawtypes")
	public List buscaAmigosNaEmpresa(Long idEmpresa) {
		Query query = em.createNativeQuery("SELECT u.id as id_usuario, u.nome as nome_usuario, u.email as email_usuario, pe.nome as perfil_empresa " 
				+ "FROM usuario u " 
				+ "JOIN empresa_equipe ee ON ee.id_usuario = u.id " 
				+ "JOIN perfil_empresa pe ON ee.id_perfil_empresa = pe.id " 
				+ "WHERE ee.id_empresa = :idEmpresa and ee.data_aprovacao is not null");
		query.setParameter("idEmpresa", idEmpresa);
		return query.getResultList();
	}
	
	public List<EmpresaEquipe> buscaUsuarioNaEmpresa(Long idEmpresa, Long idUsuario) {
		TypedQuery<EmpresaEquipe> query = em.createQuery("from EmpresaEquipe where id_empresa = :idEmpresa and id_usuario = :idUsuario", EmpresaEquipe.class);
		query.setParameter("idEmpresa", idEmpresa);
		query.setParameter("idUsuario", idUsuario);
		return query.getResultList();
	}
	
	public void excluir(EmpresaEquipe empresaEquipe) {
		em.getTransaction().begin();
		em.remove(empresaEquipe);
		em.getTransaction().commit();
	}
	
	@SuppressWarnings("rawtypes")
	public List notificaSolicitacaoEquipeEmpresa(Long idUsuario) {
		Query query = em.createNativeQuery("SELECT e.id, e.razao_social "
				+ "FROM empresa_equipe ee "
				+ "JOIN empresa e ON ee.id_empresa = e.id "
				+ "WHERE ee.id_usuario = :idUsuario "
				+ "AND ee.data_aprovacao is null "
				+ "AND ee.data_recusa is null");
		query.setParameter("idUsuario", idUsuario);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<EmpresaEquipe> listaUsuariosDaEmpresa(Long idEmpresa) {
		Query query = em.createQuery("from EmpresaEquipe where id_empresa = :idEmpresa", EmpresaEquipe.class);
		query.setParameter("idEmpresa", idEmpresa);
		return query.getResultList();
	}
}
