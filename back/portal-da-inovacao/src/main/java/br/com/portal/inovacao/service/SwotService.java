package br.com.portal.inovacao.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.EmpresaUsuarioDTO;
import br.com.portal.inovacao.dto.HistoriaUsuarioEmpresaSaidaDTO;
import br.com.portal.inovacao.dto.SwotDTO;
import br.com.portal.inovacao.dto.SwotItemDTO;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("swot")
public class SwotService {
	
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("salvar")
    public Retorno criaSwot(SwotDTO dto) throws UnsupportedEncodingException {

        Retorno _Retorno;

        ConectaBanco _Banco = new ConectaBanco();

        
        if (dto.getId() == null){
            
            String _SQL;
            
            _SQL = "";
            _SQL = _SQL + "Insert Into ";
            _SQL = _SQL + "   swot ";
            _SQL = _SQL + "      ( ";
            _SQL = _SQL + "        idEmpresa ";
            _SQL = _SQL + "       ,idUsuario ";
            _SQL = _SQL + "       ,Descricao ";
            _SQL = _SQL + "       ,dtInicio  ";
            _SQL = _SQL + "       ,dtPrevisto";
            _SQL = _SQL + "       ,ptMinimo  ";
            _SQL = _SQL + "       ,ptMaximo  ";
            _SQL = _SQL + "      ) ";
            _SQL = _SQL + "    Values ";
            _SQL = _SQL + "      (  ";
            _SQL = _SQL + "        '" + dto.getIdEmpresa() + "' ";
            _SQL = _SQL + "       , " + dto.getIdUsuario() + " ";
            _SQL = _SQL + "       ,'" + dto.getDescricao() + "' ";
            _SQL = _SQL + "       ,'" + dto.getDtInicio() + "' ";
            _SQL = _SQL + "       ,'" + dto.getDtPrevisto() + "' ";
            _SQL = _SQL + "       , " + dto.getPtMinimo() + " ";
            _SQL = _SQL + "       , " + dto.getPtMaximo() + " ";
            _SQL = _SQL + "      ) ";
            
            
            return _Banco.Commando(_SQL);         
            
        }else{
            
            String _SQL;
            
            _SQL = "";
            _SQL = _SQL + "Update ";
            _SQL = _SQL + "   swot set";
            _SQL = _SQL + "        idEmpresa = '" + dto.getIdEmpresa() + "' ";
            _SQL = _SQL + "       ,Descricao = '" + dto.getDescricao() + "' ";
            _SQL = _SQL + "       ,dtInicio  = '" + dto.getDtInicio() + "' ";
            _SQL = _SQL + "       ,dtPrevisto= '" + dto.getDtPrevisto() + "' ";
            _SQL = _SQL + "       ,ptMinimo  =  " + dto.getPtMinimo() + " ";
            _SQL = _SQL + "       ,ptMaximo  =  " + dto.getPtMaximo()+ " ";
            _SQL = _SQL + " Where ";
            _SQL = _SQL + "       id = " + dto.getId() + " ";
                    
            return _Banco.Commando(_SQL);                     
        }

    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("AdicionaItem")
    public Retorno addItemSwot(SwotItemDTO dto) throws UnsupportedEncodingException {

        Retorno _Retorno;

        ConectaBanco _Banco = new ConectaBanco();
        
        String _SQL;

        _SQL = "";
        _SQL = _SQL + "Insert Into ";
        _SQL = _SQL + "   swotitem ";
        _SQL = _SQL + "      ( ";
        _SQL = _SQL + "        idSwot ";
        _SQL = _SQL + "       ,tpSwot ";
        _SQL = _SQL + "       ,Descricao ";
        _SQL = _SQL + "       ,idUsuario  ";
        _SQL = _SQL + "      ) ";
        _SQL = _SQL + "    Values ";
        _SQL = _SQL + "      (  ";
        _SQL = _SQL + "        '" + dto.getIdSwot() + "' ";
        _SQL = _SQL + "       ,'" + dto.getTpSwot() + "' ";
        _SQL = _SQL + "       ,'" + dto.getDescricao() + "' ";
        _SQL = _SQL + "       , " + dto.getIdUsuario() + " ";
        _SQL = _SQL + "      ) ";

        return _Banco.Commando(_SQL);     
            
    }
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("alteraDataTermino")
    public Retorno alteraDataTermino(SwotDTO dto) {
        
        ConectaBanco _Banco = new ConectaBanco();
        
        String _SQL;

        try{
                
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            
            _SQL = "";
            _SQL = _SQL + "Update ";
            _SQL = _SQL + "    swot ";
            _SQL = _SQL + "Set ";
            _SQL = _SQL + "    dtTermino = '" + dateFormat.format(dto.getDtTermino()) + "' ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "    id = " + dto.getId() + " ";

            return _Banco.Commando(_SQL); 
                
        }catch(Exception ex){
            return new Retorno(1, "Ocorreu um erro ao alterar a data swot!");
        }
        
    }    
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("usuario")
    public Retorno buscaIdeiaPorUsuario(EmpresaUsuarioDTO dto) {

        try {
            
            ConectaBanco _Banco = new ConectaBanco();

            String _SQL;

            //Procurando o Id do Projeto a Partir do id_brainstorming

            _SQL = "";
            _SQL = _SQL + "SELECT " 
                        + "i.id as id_historia, " 
                        + "i.titulo as titulo_historia, " 
                        + "i.descricao as descricao_historia, " 
                        + "i.data_limite, "
                        + "i.prioridade, "
                        + "u.id as idUsuario, "
                        + "u.nome "
                 + "FROM ideia i "
                        + "LEFT JOIN ideia_equipe ie ON ie.id_ideia = i.id "
                        + "LEFT JOIN usuario u ON ie.id_usuario = u.id "
                + "WHERE "
                        + "i.is_ideia_ativa = 1 "
                    + "and i.id in (SELECT id_ideia from ideia_equipe where id_usuario = " + dto.getIdUsuario() + ") ";

            ResultSet _item = _Banco.Consulta(_SQL);

            List<HistoriaUsuarioEmpresaSaidaDTO> dtos = new ArrayList<>();

            try{
                while (_item.next()) {

                    HistoriaUsuarioEmpresaSaidaDTO _itemdto = new HistoriaUsuarioEmpresaSaidaDTO();

                    _itemdto.setIdIdeia(_item.getLong("id_historia"));
                    _itemdto.setTituloIdeia(_item.getString("titulo_historia"));
                    _itemdto.setDescricaoIdeia(_item.getString("descricao_historia"));
                    _itemdto.setDataLimite(_item.getDate("data_limite"));
                    _itemdto.setPrioridade(_item.getLong("prioridade"));
                    _itemdto.setIdUsuario(_item.getLong("idUsuario"));
                    _itemdto.setUsuario(_item.getString("nome"));

                    dtos.add(_itemdto);

                }

                return new Retorno(0, "Listagem Gerada com Sucesso", dtos);

            }catch(Exception ex){
                return new Retorno(0, ex.getMessage());
            }

        } catch (Exception ex) {
            return new Retorno(1, "Ta dando Nada Não " + ex.getMessage());
        }  
    }

}
