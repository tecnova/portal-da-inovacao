package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.BrainstormingConverter.converterBrainstorming;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import java.sql.*;

import br.com.portal.inovacao.dao.BrainstormingDAO;
import br.com.portal.inovacao.dao.GrupoBrainstormingDAO;
import br.com.portal.inovacao.dao.IdeiaDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.BrainstormingDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.model.Brainstorming;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;
import java.text.SimpleDateFormat;

public class BrainstormingFacade {
           
	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;
	
	private static final String MSG_DESCRICAO_DEVE_SER_PREENCHIDO = "Descrição deve ser preenchido.";
	private static final String MSG_BRAINSTORMING_SALVO_COM_SUCESSO = "Brainstorming salvo com sucesso!";
	private static final String MSG_BRAINSTORMING_ALTERADO_COM_SUCESSO = "Brainstorming alterado com sucesso!";
	private static final String MSG_BRAINSTORMING_NAO_ENCONTRADO = "Brainstorming não encontrado.";
	private static final String MSG_ERRO_AO_ALTERAR_BRAINSTORMING = "Erro ao alterar brainstorming.";
	private static final String MSG_BRAINSTORMING_EXCLUIDO_COM_SUCESSO = "Brainstorming excluido com sucesso!";
	private static final String MSG_ERRO_AO_EXCLUIR_BRAINSTORMING = "Erro ao excluir brainstorming.";

               
	@Inject
	private BrainstormingDAO brainstormingDAO;
	
	@Inject
	private IdeiaDAO ideiaDAO;
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	@Inject
	private GrupoBrainstormingDAO grupoBrainstormingDAO;
	
	private Brainstorming criaBrainstormingObj(BrainstormingDTO dto) {
		Brainstorming brainstorming = new Brainstorming();
		brainstorming.setDescricao(dto.getDescricao());
		brainstorming.setIdHistoria(ideiaDAO.pesquisaPorId(dto.getIdHistoria()));
		brainstorming.setIdUsuario(usuarioDAO.pesquisaPorId(dto.getIdUsuario()));
                if (dto.getIdGrupoBrainstorming() != null){
                    brainstorming.setIdGrupoBrainstorming(grupoBrainstormingDAO.pesquisaPorId(dto.getIdGrupoBrainstorming()));
                }
		brainstorming.setDataCriacao(new Date());
		return brainstorming;
	}
	
	public Retorno criaBrainstorming(BrainstormingDTO dto) {
		try {
			validaDadosDeEntrada(dto);
			Brainstorming brainstorming = criaBrainstormingObj(dto);
			brainstormingDAO.salvar(brainstorming);
			return new Retorno(CODIGO_SUCESSO, MSG_BRAINSTORMING_SALVO_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	
	private void validaDadosDeEntrada(BrainstormingDTO dto) throws Exception {
		if (dto.getDescricao() == null || dto.getDescricao().trim().equals(""))
			throw new Exception(MSG_DESCRICAO_DEVE_SER_PREENCHIDO);
	}
	

	public Retorno listaBrainstormingPorGrupo(InformaIdDTO idGrupo) {
		List<Brainstorming> listaBrainstormingPorGrupo = brainstormingDAO.listaBrainstormingPorGrupo(idGrupo.getId());
		List<BrainstormingDTO> dtos = new ArrayList<BrainstormingDTO>();
		
		for (Brainstorming brainstorming : listaBrainstormingPorGrupo) {
			dtos.add(converterBrainstorming(brainstorming));
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
}
