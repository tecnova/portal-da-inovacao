package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.FaixaFaturamentoConverter.converterFaixaFaturamento;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.FaixaFaturamentoDAO;
import br.com.portal.inovacao.dto.FaixaFaturamentoDTO;
import br.com.portal.inovacao.model.FaixaFaturamento;

public class FaixaFaturamentoFacade {
	
	@Inject
	private FaixaFaturamentoDAO faixaFaturamentoDAO;
	
	public List<FaixaFaturamentoDTO> listaFaixaFaturamento() {
		List<FaixaFaturamento> faturamentos = faixaFaturamentoDAO.todos();
		List<FaixaFaturamentoDTO> dtos = new ArrayList<FaixaFaturamentoDTO>();
		
		for (FaixaFaturamento faixaFaturamento : faturamentos) {
			dtos.add(converterFaixaFaturamento(faixaFaturamento));
		}
		return dtos;
	}
}
