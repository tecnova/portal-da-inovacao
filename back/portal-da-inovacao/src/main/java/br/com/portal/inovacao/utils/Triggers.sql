
USE `portal_inovacao`;

/* ****************** 
	brainstorming
******************** */

-- After Insert
DELIMITER $$
USE `portal_inovacao` $$
DROP TRIGGER IF EXISTS `portal_inovacao`.`brainstorming_after_insert` $$

CREATE DEFINER=`root`@`localhost` TRIGGER brainstorming_after_insert
	AFTER INSERT on brainstorming
FOR EACH ROW
BEGIN

    SELECT id, valor into @idAtiv, @valor FROM atividade WHERE tpAtividade = 'tpBrainstormingIdeiaCriar';
    Set @idEmpresa = (Select id_empresa FROM ideia WHERE id = NEW.id_historia);
    
	Insert Into 
		atividade_usuario
			(
				 idAtividade
				,idEmpresa
				,idUsuario
				,dtAtividade
				,idHistoria
                ,idBrainstorming
				,Pontuacao
			)
		values
			(
				 @idAtiv
				,@idEmpresa
				,NEW.id_usuario
				,now()
				,NEW.id_historia
                ,NEW.id
				,@valor
			);

END $$

DELIMITER ;

-- Before delete
DELIMITER $$
USE `portal_inovacao` $$
DROP TRIGGER IF EXISTS `portal_inovacao`.`brainstorming_before_delete` $$

CREATE DEFINER=`root`@`localhost` TRIGGER brainstorming_before_delete
	BEFORE DELETE on brainstorming
FOR EACH ROW
BEGIN

	-- Excluindo os lançamentos da gamificação
	
	DELETE FROM atividade_usuario
		WHERE 
			idBrainstorming = old.id;
			
END $$

DELIMITER ;

/* ****************** 
	Canvas
******************** */

-- After Insert
DELIMITER $$
USE `portal_inovacao`$$
DROP TRIGGER IF EXISTS `portal_inovacao`.`canvas_after_insert` $$

CREATE DEFINER=`root`@`localhost` TRIGGER canvas_after_insert
	AFTER INSERT on Canvas
FOR EACH ROW
BEGIN

	-- Trabalhando com a Gamificação
    SELECT id, valor into @idAtiv, @Valor FROM atividade WHERE tpAtividade = 'tpCanvasItemCriar';
    Set @idEmpresa = (Select id_empresa FROM ideia WHERE id = NEW.Id_Ideia);
    
	Insert Into 
		atividade_usuario
			(
				 idAtividade
				,idEmpresa
				,idUsuario
				,dtAtividade
				,idHistoria
                ,idCanvas
                ,Pontuacao
			)
		values
			(
				 @idAtiv
				,@idEmpresa
				,NEW.Id_Usuario
				,now()
				,NEW.Id_Ideia
                ,NEW.id
                ,@valor
			);

	/* Verifica se completou o Canvas */
	Set @Cont = 1;
	Set @Achou = 1; /* 0 = False --- 1 = True */
    
	While @Cont <= 9 Do
		SET @ContReg = (Select Count(1) From Canvas Where Id_Ideia = NEW.Id_Ideia and Nr_Item = @Cont);
    
		if @ContReg = 0 then
			Set @Achou = 0;
        END IF;
    
		Set @Cont = @Cont + 1;
        
    END While;

    if (@Achou = 1) THEN

        SELECT id, valor into @idAtiv, @valor FROM atividade WHERE tpAtividade = 'tpCanvasCompletar';

		Insert Into 
			atividade_usuario
				(
					 idAtividade
					,idEmpresa
					,idUsuario
					,dtAtividade
					,idHistoria
					,idCanvas
                    ,Pontuacao
				)
			values
				(
					 @idAtiv
					,@idEmpresa
					,NEW.Id_Usuario
					,now()
					,NEW.Id_Ideia
					,NEW.id
                    ,@valor
				);
    
    END IF;
END $$

DELIMITER ;

-- Before delete
DELIMITER $$
USE `portal_inovacao` $$
DROP TRIGGER IF EXISTS `portal_inovacao`.`canvas_before_delete` $$

CREATE DEFINER=`root`@`localhost` TRIGGER canvas_before_delete
	BEFORE DELETE on Canvas
FOR EACH ROW
BEGIN

	-- Excluindo os lançamentos da gamificação
	
	DELETE FROM atividade_usuario
		WHERE 
			idCanvas = old.id;
			
END $$

DELIMITER ;

/* ****************** 
	Ideia / Historia
******************** */

-- After Insert ;
DELIMITER $$
USE `portal_inovacao`$$
DROP TRIGGER IF EXISTS `portal_inovacao`.`ideia_inclui_ideia_equipe` $$

CREATE DEFINER=`root`@`localhost` TRIGGER ideia_inclui_ideia_equipe
	AFTER INSERT on ideia
FOR EACH ROW
BEGIN

	-- Inserindo o usuario na equipe
	Insert Into ideia_equipe (id_ideia , id_usuario) Values (NEW.id, NEW.id_usuario);

	-- Trabalhando com Gamificacão
    SELECT id, valor into @idAtiv, @valor FROM atividade WHERE tpAtividade = 'tpHistoriaCriar';
	Insert Into 
		atividade_usuario
			(
				 idAtividade
				,idEmpresa
				,idUsuario
				,dtAtividade
				,idHistoria
				,Pontuacao
			)
		values
			(
				 @idAtiv
				,NEW.id_empresa
				,NEW.id_usuario
				,now()
				,NEW.id
				,@valor
			);
END $$

DELIMITER ;

-- Before Delete;

DELIMITER $$
USE `portal_inovacao`$$
DROP TRIGGER IF EXISTS `portal_inovacao`.`ideia_exclui_ideia_equipe` $$

CREATE DEFINER=`root`@`localhost` TRIGGER ideia_exclui_ideia_equipe
	BEFORE DELETE on ideia
FOR EACH ROW
BEGIN

	-- Excluindo Usuario da Equipe
	Delete From ideia_equipe Where id_ideia = old.id and id_usuario = old.id_usuario;
	
	-- Trabalhando com a Gamificacao
	Delete From atividade_usuario Where id_usuario = old.id_usuario and idHistoria = old.id;
	
	
END $$

DELIMITER;

/* ****************** 
	projeto5w2h
******************** */

-- Before delete

DELIMITER $$
USE `portal_inovacao`$$
DROP TRIGGER IF EXISTS `portal_inovacao`.`projeto5w2h_BEFORE_DELETE` $$

CREATE DEFINER=`root`@`localhost` TRIGGER `portal_inovacao`.`projeto5w2h_BEFORE_DELETE` BEFORE DELETE ON `projeto5w2h` FOR EACH ROW
BEGIN
	Delete from projeto5w2hitem where id > 0 and idprojeto5w2h = old.id;
END $$

DELIMITER;

/* ****************** 
	projeto5w2hitem
******************** */

-- Before delete

DELIMITER $$
USE `portal_inovacao`$$
DROP TRIGGER IF EXISTS `portal_inovacao`.`projeto5w2hitem_BEFORE_DELETE` $$

CREATE DEFINER=`root`@`localhost` TRIGGER `portal_inovacao`.`projeto5w2hitem_BEFORE_DELETE` BEFORE DELETE ON `projeto5w2hitem` FOR EACH ROW
BEGIN
	Delete From projeto5w2hitemequipe Where id > 0 and idprojeto5w2hitem = old.id;
END $$

DELIMITER;

/* ****************** 
	projeto5w2hitemequipe
******************** */

-- After Insert

DELIMITER $$
USE `portal_inovacao`$$
DROP TRIGGER IF EXISTS `portal_inovacao`.`projeto5w2hitemequipe_after_insert` $$

CREATE DEFINER=`root`@`localhost` TRIGGER projeto5w2hitemequipe_after_insert
	AFTER INSERT on projeto5w2hitemequipe
FOR EACH ROW
BEGIN

    SELECT id, valor into @idAtiv, @valor FROM atividade WHERE tpAtividade = 'tpProjeto5w2hAcao';
    Set @idProjeto5w2h = (Select idProjeto5w2h From projeto5w2hitem Where id = NEW.idprojeto5w2hitem);
    Set @idBrainstorming = (Select id_brainstorming From projeto5w2h where id = @idProjeto5w2h);
    Set @idHistoria = (Select id_historia From brainstorming where id = @idBrainstorming);
    Set @idEmpresa = (Select id_empresa FROM ideia WHERE id = @idHistoria);
    
	Insert Into 
		atividade_usuario
			(
				 idAtividade
				,idEmpresa
				,idUsuario
				,dtAtividade
				,idHistoria
                ,idProjeto5w2hItem
				,Pontuacao
			)
		values
			(
				 @idAtiv
				,@idEmpresa
				,NEW.idusuario
				,now()
				,@idHistoria
                ,NEW.idprojeto5w2hitem
				,@valor
			);

END $$

DELIMITER;

-- Before delete
DELIMITER $$
USE `portal_inovacao` $$
DROP TRIGGER IF EXISTS `portal_inovacao`.`projeto5w2hitemequipe_before_delete` $$

CREATE DEFINER=`root`@`localhost` TRIGGER projeto5w2hitemequipe_before_delete
	BEFORE DELETE on projeto5w2hitemequipe
FOR EACH ROW
BEGIN

	-- Excluindo os lançamentos da gamificação
	
	DELETE FROM atividade_usuario
		WHERE 
			idProjeto5w2hItem = old.idProjeto5w2hItem;
			
END $$

DELIMITER ;