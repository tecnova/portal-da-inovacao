package br.com.portal.inovacao.dto;

import br.com.portal.inovacao.model.Usuario;

public class InativaNoticia extends InformaIdDTO {

	private static final long serialVersionUID = 1L;

	private Usuario usuario;

	public InativaNoticia() {
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}