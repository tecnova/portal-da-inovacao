package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.PerfilDTO;
import br.com.portal.inovacao.model.Perfil;

public class PerfilConverter {
	
	public static PerfilDTO converterPerfil(Perfil perfil) {
		PerfilDTO dto = new PerfilDTO();
		dto.setId(perfil.getId());
		dto.setNome(perfil.getNome());
		dto.setSigla(perfil.getSigla());
		return dto;
	}
}
