package br.com.portal.inovacao.service;

import br.com.portal.inovacao.dto.AmigoEmpresaDTO;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.AmigoEmpresaUsuarioDTO;
import br.com.portal.inovacao.dto.CnaeDTO;
import br.com.portal.inovacao.dto.EmpresaDTO;
import br.com.portal.inovacao.dto.EmpresaUsuarioDTO;
import br.com.portal.inovacao.dto.EmpresaUsuarioSaidaDTO;
import br.com.portal.inovacao.dto.HistoriaUsuarioEmpresaSaidaDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.PerfilAmigoEmpresaDTO;
import br.com.portal.inovacao.facade.EmpresaFacade;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;
import java.sql.ResultSet;
import java.util.ArrayList;

@Path("empresa")
public class EmpresaService {
	
	@Inject
	private EmpresaFacade facade;
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("criar")
	public Retorno criaEmpresa(EmpresaDTO dto) {
            return facade.salvaEmpresa(dto);
	}
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("alterar")
	public Retorno alteraEmpresa(EmpresaDTO dto) {
            return facade.alteraEmpresa(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("inativar")
	public Retorno inativaEmpresa(EmpresaUsuarioDTO dto) {
            
            Retorno _Retorno;
            
            ConectaBanco _Banco = new ConectaBanco();
            
            String _SQL;
            _SQL = "";
            _SQL = _SQL + "Select ";
            _SQL = _SQL + "     1 ";
            _SQL = _SQL + "From  ";
            _SQL = _SQL + "     empresa emp ";
            _SQL = _SQL + "    ,perfil_empresa pem ";
            _SQL = _SQL + "    ,empresa_equipe eeq ";
            _SQL = _SQL + "where  ";
            _SQL = _SQL + "     emp.id = " + dto.getIdEmpresa() + " ";
            _SQL = _SQL + " and eeq.id_usuario = " + dto.getIdUsuario() + " ";
            _SQL = _SQL + " and emp.id = eeq.id_empresa ";
            _SQL = _SQL + " and eeq.id_perfil_empresa = pem.id ";
            _SQL = _SQL + " and pem.Sigla = 'AD' ";

            ResultSet rs = _Banco.Consulta(_SQL);

            try{
                
                if (!rs.first()){
                    return new Retorno(1,"Esse Usuário não pode inativar essa empresa. ");
                }else{
                    
                    _SQL = "";
                    _SQL = _SQL + "update ";
                    _SQL = _SQL + "     empresa ";
                    _SQL = _SQL + "set  ";
                    _SQL = _SQL + "     is_empresa_ativa = 0 " ;
                    _SQL =  _SQL + "where  ";
                    _SQL =  _SQL + "     id = " + dto.getIdEmpresa() + " ";

                    _Retorno = _Banco.Commando(_SQL);
                    
                    if (_Retorno.getErro() == 1){
                        return new Retorno(1, "aqui" + _Retorno.getMensagem());
                    }else{
                        return new Retorno(0, "Empresa Inativada com Sucesso !!");
                    }
                }
                
            }catch(Exception ex){
                return new Retorno(1, ex.getMessage());
            }

        }

	@GET
	@Produces(APPLICATION_JSON)
	@Path("cnaes")
	public List<CnaeDTO> listaCnaes() {
		return facade.listaCnaes();
	}
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista/empresas")
	public Retorno listaEmpresasPerfilUsuario(InformaIdDTO dto) {
            return facade.buscaEmpresasEPerfilDoUsuario(dto);
	}
	
        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("listar")
	public Retorno listarEmpresaDoUsuario(EmpresaUsuarioDTO dto) {
		
            String _SQL;
            
            _SQL = "";
            _SQL = _SQL + "Select ";
            _SQL = _SQL + "   emp.* ";
            _SQL = _SQL + "From ";
            _SQL = _SQL + "   empresa emp ";
            _SQL = _SQL + "";
            
            return new Retorno(1, "Serviço não esta pronto !!");
	}
        
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista/empresa")
	public Retorno listaEmpresaDoUsuario(EmpresaUsuarioDTO dto) {
            
            EmpresaUsuarioSaidaDTO _item;
            List<EmpresaUsuarioSaidaDTO> _dtoss = new ArrayList<>();

            String _SQL;
            ConectaBanco _Banco = new ConectaBanco();
            
            _SQL = "";
            _SQL = _SQL + "Select ";
            _SQL = _SQL + "    emp.*, ";
            _SQL = _SQL + "    per.nome as perfil ";
            _SQL = _SQL + "From ";
            _SQL = _SQL + "    empresa emp ";
            _SQL = _SQL + "   ,empresa_equipe eeq ";
            _SQL = _SQL + "   ,perfil_empresa per ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "    emp.id = eeq.id_empresa ";
            _SQL = _SQL + "and per.id  = eeq.id_perfil_empresa ";
            _SQL = _SQL + "and eeq.id_usuario = " + dto.getIdUsuario() + " ";
            _SQL = _SQL + "and emp.id = " + dto.getIdEmpresa() + " ";
            

            try{
                
                ResultSet rs = _Banco.Consulta(_SQL);

                _item = new EmpresaUsuarioSaidaDTO();

                if (rs.first()){
                    _item.setIdEmpresa(rs.getLong("id"));
                    _item.setRazaoSocial(rs.getString("razao_social"));
                    _item.setCnpj(rs.getString("cnpj"));
                    _item.setLogradouro(rs.getString("logradouro"));
                    _item.setMunicipio(rs.getString("municipio"));
                    _item.setUf(rs.getString("uf"));
                    _item.setNumero(rs.getString("numero"));
                    _item.setComplemento(rs.getString("complemento"));
                    _item.setCep(rs.getString("CEP"));
                    _item.setBairro(rs.getString("bairro"));
                    _item.setIdCnae(rs.getLong("id_cnae"));
                    _item.setIdFaixaFaturamento(rs.getLong("id_faixa_faturamento"));
                    _item.setIdFaixaFuncionario(rs.getLong("id_faixa_funcionario"));
                    _item.setImagem(rs.getBoolean("is_empresa_imagem"));
                    _item.setPerfilUsuario(rs.getString("perfil"));
                }
                return new Retorno(0, "Listagem Gerada com Sucesso", _item);

            }catch(Exception ex){
                return new Retorno(1, ex.getMessage());
            }              
            
//            try{
//                while (rs.next()) {
//
//                    _item = new EmpresaUsuarioSaidaDTO();
//
//                    _item.setIdEmpresa(rs.getLong("id"));
//                    _item.setRazaoSocial(rs.getString("razao_social"));
//                    _item.setCnpj(rs.getString(rs.getString("cnpj")));
//                    _item.setLogradouro(rs.getString("logradouro"));
//                    _item.setMunicipio(rs.getString("municipio"));
//                    _item.setUf(rs.getString("uf"));
//                    _item.setNumero(rs.getString("numero"));
//                    _item.setComplemento(rs.getString("complemento"));
//                    _item.setCep(rs.getString("CEP"));
//                    _item.setBairro(rs.getString("bairro"));
//                    _item.setIdCnae(rs.getLong("id_cnae"));
//                    _item.setIdFaixaFaturamento(rs.getLong("id_faixa_faturamento"));
//                    _item.setIdFaixaFuncionario(rs.getLong("id_faixa_funcionario"));
//                    _item.setImagem(rs.getBoolean("is_empresa_imagem"));
//                    _item.setPerfilUsuario(rs.getString("perfil"));
//
//                    _dtoss.add(_item);
//
//                }
//
//                return new Retorno(0, "Listagem Gerada com Sucesso", _dtoss);
//
//            }catch(Exception ex){
//                return new Retorno(0, ex.getMessage());
//            }            
         
            // return facade.buscaEmpresaDoUsuario(dto);            
        }


	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("amigos")
	public Retorno listaAmigos(EmpresaUsuarioDTO dto) {
            
            AmigoEmpresaDTO _item;
            List<AmigoEmpresaDTO> _dtoss = new ArrayList<AmigoEmpresaDTO>();

            String _SQL;
            ConectaBanco _Banco = new ConectaBanco();
            
            _SQL = "";
            _SQL = _SQL + "SELECT  ";
            _SQL = _SQL + "    * ";
            _SQL = _SQL + "FROM ";
            _SQL = _SQL + "    usuario usr ";
            _SQL = _SQL + "WHERE ";
            _SQL = _SQL + "   (usr.id in (SELECT  ";
            _SQL = _SQL + "                 usa.id_amigo AS idAmigo ";
            _SQL = _SQL + "              FROM ";
            _SQL = _SQL + "                 usuario_amigo usa ";
            _SQL = _SQL + "		 WHERE ";
            _SQL = _SQL + "		    usa.id_usuario = " + dto.getIdUsuario() + " ) ";
            _SQL = _SQL + "or usr.id in (SELECT  ";
            _SQL = _SQL + "		    usa2.id_usuario AS idAmigo ";
            _SQL = _SQL + "		 FROM ";
            _SQL = _SQL + "		    usuario_amigo usa2 ";
            _SQL = _SQL + "		 WHERE ";
            _SQL = _SQL + "		    usa2.id_amigo = " + dto.getIdUsuario() + " )) ";
            
            if (dto.getIdEmpresa() != null){
                _SQL = _SQL + "and usr.id not in (Select ";
                _SQL = _SQL + "		    eeq.id_usuario AS idAmigo ";
                _SQL = _SQL + "		 FROM ";
                _SQL = _SQL + "		    empresa_equipe eeq ";
                _SQL = _SQL + "		 WHERE ";
                _SQL = _SQL + "		    eeq.id_empresa = " + dto.getIdEmpresa() + " ) ";
            }
            
            ResultSet rs = _Banco.Consulta(_SQL);

            try{
                while (rs.next()) {

                    _item = new AmigoEmpresaDTO();

                    _item.setIdUsuario(rs.getLong("id"));
                    _item.setNome(rs.getString("nome"));
                    _item.setEmail(rs.getString("email"));
                    _item.setPerfil("");

                    _dtoss.add(_item);

                }

                return new Retorno(0, "Listagem Gerada com Sucesso", _dtoss);

            }catch(Exception ex){
                return new Retorno(0, ex.getMessage());
            }            
	}
        
        
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("amigos/empresa")
	public Retorno listaAmigosEmpresa(EmpresaUsuarioDTO dto) {
            
            AmigoEmpresaDTO _item;
            List<AmigoEmpresaDTO> _dtoss = new ArrayList<AmigoEmpresaDTO>();

            String _SQL;
            ConectaBanco _Banco = new ConectaBanco();
            
            _SQL = "";
            _SQL = _SQL + "Select ";
            _SQL = _SQL + "    usu.* ";
            _SQL = _SQL + "   ,pem.nome as perfil ";
            _SQL = _SQL + "   ,eeq.solicitacao_aprovada ";
            _SQL = _SQL + "From ";
            _SQL = _SQL + "    usuario usu ";
            _SQL = _SQL + "   ,empresa_equipe eeq ";
            _SQL = _SQL + "   ,perfil_empresa pem ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "    usu.id = eeq.id_usuario ";
            _SQL = _SQL + "and eeq.id_perfil_empresa = pem.id  ";
            _SQL = _SQL + "and eeq.id_empresa = " + dto.getIdEmpresa() + " ";
            
            ResultSet rs = _Banco.Consulta(_SQL);

            try{
                while (rs.next()) {

                    _item = new AmigoEmpresaDTO();

                    _item.setIdUsuario(rs.getLong("id"));
                    _item.setNome(rs.getString("nome"));
                    _item.setEmail(rs.getString("email"));
                    _item.setPerfil(rs.getString("perfil"));
                    
                    if (rs.getInt("solicitacao_aprovada") == 0){
                        _item.setBoConfirmado("S");
                    }else{
                        _item.setBoConfirmado("N");
                    }

                    _dtoss.add(_item);

                }

                return new Retorno(0, "Listagem Gerada com Sucesso", _dtoss);

            }catch(Exception ex){
                return new Retorno(0, ex.getMessage());
            }            
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("amigos/perfil")
	public Retorno alteraPerfilAmigosEmpresa(PerfilAmigoEmpresaDTO dto) {

            ConectaBanco _Banco = new ConectaBanco();

            String _SQL;

            try{

                _SQL = "";
                _SQL = _SQL + "Update ";
                _SQL = _SQL + "    empresa_equipe ";
                _SQL = _SQL + "Set ";
                _SQL = _SQL + "    id_perfil_empresa = " + dto.getPerfis().get(0).getId() + " ";
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "    id_empresa = " + dto.getIdEmpresa() + " ";
                _SQL = _SQL + "and id_usuario = " + dto.getIdUsuario() + " ";

                return _Banco.Commando(_SQL); 

            }catch(Exception ex){
                return new Retorno(1, "Ocorreu um erro ao enviar o convite!");
            }
        
        }

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("amigos/remover")
	public Retorno removeAmigosEmpresa(AmigoEmpresaUsuarioDTO dto) {
		return facade.removeAmigoDaEmpresa(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("solicitacao/aprovar")
	public Retorno aceitaSolicitacao(EmpresaUsuarioDTO dto) {
		return facade.aprovaSolicitacaoGrupo(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("solicitacao/recusar")
	public Retorno recusaSolicitacao(EmpresaUsuarioDTO dto) {
		return facade.recusaSolicitacaoGrupo(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("notificacao")
	public Retorno notificaSolicitacaoEquipeEmpresa(InformaIdDTO dto) {
		return facade.notificaSolicitacaoEquipeEmpresa(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("usuarios")
	public Retorno listaUsuariosDaEmpresa(InformaIdDTO dto) {
		return facade.listaUsuariosDaEmpresa(dto);
	}
}
