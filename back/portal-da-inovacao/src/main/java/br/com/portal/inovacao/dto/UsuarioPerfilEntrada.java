package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.List;

public class UsuarioPerfilEntrada implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String nome;
    private String email;
    private String dataCadastro;
    private String imagem;
    private List<PerfilDTO> perfis;

    public UsuarioPerfilEntrada() {}

    public UsuarioPerfilEntrada(Long id, String nome, String email, String dataCadastro, List<PerfilDTO> perfis) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.dataCadastro = dataCadastro;
        this.perfis = perfis;
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getDataCadastro() {
        return dataCadastro;
    }

    public List<PerfilDTO> getPerfis() {
        return perfis;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDataCadastro(String dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public void setPerfis(List<PerfilDTO> perfis) {
        this.perfis = perfis;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }
        
        
}
