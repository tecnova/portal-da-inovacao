package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class NotificaSolicitacaoEquipeEmpresaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idEmpresa;
	private String razaoSocial;

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
}
