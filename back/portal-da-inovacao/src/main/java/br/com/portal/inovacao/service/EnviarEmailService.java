package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.EmailDTO;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

@Path("email")
public class EnviarEmailService {
	
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("enviar")
    public Retorno enviaEmail(EmailDTO email) {
        ConviteEmail conviteEmail = new ConviteEmail();
        return conviteEmail.run(email);
    }

    
        
}
