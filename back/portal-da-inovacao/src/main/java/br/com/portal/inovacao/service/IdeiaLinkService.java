package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.IdeiaLinkDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.facade.IdeiaLinkFacade;
import br.com.portal.inovacao.utils.Retorno;

@Path("ideia/link")
public class IdeiaLinkService {
	
	@Inject
	private IdeiaLinkFacade facade;
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("criar")
	public Retorno criaIdeiaLink(IdeiaLinkDTO dto) {
		return facade.criaIdeiaLink(dto);
	}
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("remover")
	public Retorno removeLink(InformaIdDTO dto) {
		return facade.excluiLink(dto);
	}
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista")
	public Retorno listaLinksPorIdeia(IdeiaLinkDTO dto) {
		return facade.listaLinksDaIdeia(dto);
	}
}
