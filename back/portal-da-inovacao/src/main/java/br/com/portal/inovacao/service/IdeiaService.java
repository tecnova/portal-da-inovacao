package br.com.portal.inovacao.service;

import static br.com.portal.inovacao.converter.ListaImagensIdeiaConverter.converterListaImagensIdeia;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.AlteraIdeiaDTO;
import br.com.portal.inovacao.dto.EmpresaUsuarioDTO;
import br.com.portal.inovacao.dto.HistoriaUsuarioEmpresaSaidaDTO;
import br.com.portal.inovacao.dto.IdeiaDTO;
import br.com.portal.inovacao.dto.IdeiaEquipeDTO;
import br.com.portal.inovacao.dto.ImagemDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.facade.IdeiaFacade;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import javax.imageio.ImageIO;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

@Path("ideia")
public class IdeiaService {
	
    private static final String UPLOAD_FILE_SERVER = "/var/www/html/imagens/ideia/";
    private String UPLOAD_FILE_SERVER_TEMP = "/var/www/html/imagens/temp/";
    private static final int CODIGO_SUCESSO = 0;
    private static final int CODIGO_ERRO = 1;

    private static final String MSG_IMAGEM_SALVA_COM_SUCESSO = "Imagem salva com sucesso.";
    private static final String MSG_OCORREU_UM_ERRO_AO_SALVAR_IMAGEM = "Ocorreu um erro ao salvar imagem.";
    private static final String MSG_IDEIA_INEXISTENTE = "Idéia Inexistente.";    
    
    private String _Mensagem;
    
    @Inject
    private IdeiaFacade facade;
    private Object ideiaDAO;

    private boolean criaDiretorio(String _folder) {
       try {
           return new File(_folder).mkdirs();
      } catch (Exception e) {
           e.printStackTrace();
      }
       return false;
    }
    
    private boolean excluiTemp(String _folder){
        File folder = new File(_folder);
        if (folder.isDirectory()) {
                File[] sun = folder.listFiles();
                for (File toDelete : sun) {
                        toDelete.delete();
                }
        }    
        
        return true;
    }
    
    public boolean moverArquivo(String _oldFolder, String _newFolder) {  
        File folder = new File(_oldFolder);
        if (folder.isDirectory()) {
            File[] sun = folder.listFiles();
            for (File _file : sun) {
                _file.renameTo(new File(_newFolder, _file.getName()));
            }
        }    
        return true;  
    }  

    public String copiarArquivo(String _oldFolder, String _newFolder) {  
        try {
            File folder = new File(_oldFolder);
            if (folder.isDirectory()) {
                File[] sun = folder.listFiles();
                for (File _file : sun) {
                    copy(new File(_oldFolder + _file.getName()), new File(_newFolder + _file.getName()), true);
                }
            }    
        }catch (Exception ex){
            return ex.getMessage();
        }
        return "";  
    }  
    
    /** 
     * Copia arquivos de um local para o outro 
     * @param origem - Arquivo de origem 
     * @param destino - Arquivo de destino 
     * @param overwrite - Confirmação para sobrescrever os arquivos 
     * @throws IOException 
     */ 
    public static void copy(File origem, File destino, boolean overwrite) throws IOException{ 
       Date date = new Date();
       if (destino.exists() && !overwrite){ 
          System.err.println(destino.getName()+" já existe, ignorando..."); 
          return; 
       } 
       FileInputStream fisOrigem = new FileInputStream(origem); 
       FileOutputStream fisDestino = new FileOutputStream(destino); 
       FileChannel fcOrigem = fisOrigem.getChannel();   
       FileChannel fcDestino = fisDestino.getChannel();   
       fcOrigem.transferTo(0, fcOrigem.size(), fcDestino);   
       fisOrigem.close();   
       fisDestino.close(); 
       Long time = new Date().getTime() - date.getTime();
    }    
    
    private String enviaImagemParaServidor(String fileName, Map<String, List<InputPart>> map, String _folder) throws IOException {
    
        InputStream inputStream = null;
        List<InputPart> lstInputPart = map.get("uploadedFile");
        String fileServer = null;

        if (lstInputPart != null) {
            for (InputPart inputPart : lstInputPart) {
                if (null != fileName && !"".equalsIgnoreCase(fileName)) {
                    inputStream = inputPart.getBody(InputStream.class, null);
                    fileServer = gravaImagem(inputStream, fileName, _folder);
                    inputStream.close();
                }
            }
        }
        return fileServer;
    }

    private String gravaImagem(InputStream inputStream, String fileName, String _folder) throws IOException {
        
        OutputStream outputStream = null;
        String qualifiedUploadFilePath = _folder + fileName;

        try {
            outputStream = new FileOutputStream(new File(qualifiedUploadFilePath));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
            }
            outputStream.flush();
        } catch (Exception e) {
            return e.getMessage();
        } finally {
            outputStream.close();
        }
        return qualifiedUploadFilePath;
    }
	
    public static void redimensionaImagem(String caminhoImg, Integer imgLargura, Integer imgAltura) throws IOException {  
        BufferedImage imagem = ImageIO.read(new File(caminhoImg));  

        Double novaImgLargura = (double) imagem.getWidth();  
        Double novaImgAltura = (double) imagem.getHeight();  

        Double imgProporcao = null;  
        if (novaImgLargura >= imgLargura) {  
            imgProporcao = (novaImgAltura / novaImgLargura);  
            novaImgLargura = (double) imgLargura;  
            novaImgAltura = (novaImgLargura * imgProporcao);  
            while (novaImgAltura > imgAltura) {  
                novaImgLargura = (double) (--imgLargura);  
                novaImgAltura = (novaImgLargura * imgProporcao);  
            }  
        } else if (novaImgAltura >= imgAltura) {  
            imgProporcao = (novaImgLargura / novaImgAltura);  
            novaImgAltura = (double) imgAltura;  
            while (novaImgLargura > imgLargura) {  
                novaImgAltura = (double) (--imgAltura);  
                novaImgLargura = (novaImgAltura * imgProporcao);  
            }  
        }  

        BufferedImage novaImagem = new BufferedImage(novaImgLargura.intValue(), novaImgAltura.intValue(), BufferedImage.TYPE_INT_RGB);  
        Graphics g = novaImagem.getGraphics();  
        g.drawImage(imagem.getScaledInstance(novaImgLargura.intValue(), novaImgAltura.intValue(), 10000), 0, 0, null);  
        g.dispose();  

        ImageIO.write(novaImagem, "JPG", new File(caminhoImg));  
    } 
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("imagens")    
    public Retorno listaImagensDoServidor(InformaIdDTO idIdeia) {
        
        //Copiar para o Temp
        File diretorio = new File(UPLOAD_FILE_SERVER + idIdeia.getId());
        List<String> nomesArquivos = new ArrayList<String>();

        for (File file : diretorio.listFiles()) {
                nomesArquivos.add(file.getName());
        }
        return converterListaImagensIdeia(nomesArquivos);
    }    
         
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("salvar")
    public Retorno criaIdeia(IdeiaDTO dto) throws UnsupportedEncodingException {

        String _SQL;

        try {
            
            ConectaBanco _Banco = new ConectaBanco();
            Retorno _Retorno = new Retorno();

            //Salvando na tabela ideia 
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            
            Long _idIdeia;
            Long _id = dto.getId();
            String _Titulo = dto.getTitulo();
            String _Descricao = dto.getDescricao();
            Long _idUsuario = dto.getUsuario();
            Long _Ativo = dto.getAtivo();
            String _dtCriacao = dateFormat.format(dateFormat.parse(dto.getDataCriacao()));
            String _dtLimite = dateFormat.format(dateFormat.parse(dto.getDataLimite()));
            Long _Prioridade = dto.getPrioridade();
            String _TipoIdeia = dto.getTipoIdeia();
            Long _idEmpresa = dto.getEmpresa();
            String _Link = dto.getLink();
            List<String> _lstLink = dto.getLstLink();
            
            if (dto.getId() != null){
                
                _SQL = "";
                _SQL = _SQL + "Update ";
                _SQL = _SQL + "    ideia ";
                _SQL = _SQL + "Set ";
                _SQL = _SQL + "     Titulo = '" + _Titulo + "' ";
                _SQL = _SQL + "    ,Descricao  = '" + _Descricao + "' ";
                _SQL = _SQL + "    ,id_usuario = " + _idUsuario + " ";
                _SQL = _SQL + "    ,is_ideia_ativa =  " + _Ativo + " ";
                _SQL = _SQL + "    ,data_criacao = '" + _dtCriacao + "' ";
                _SQL = _SQL + "    ,data_limite = '" + _dtLimite + "' ";
                _SQL = _SQL + "    ,Prioridade = " + _Prioridade + " ";
                _SQL = _SQL + "    ,TipoIdeia = '" + _TipoIdeia + "' ";
                _SQL = _SQL + "    ,id_empresa = " + _idEmpresa + " ";
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "     id = " + _id + " ";

                _Retorno = _Banco.Commando(_SQL);

                if (_Retorno.getErro() > 0 ){
                    return new Retorno(1, " Update " + _Retorno.getMensagem());
                }else{
                    _idIdeia = _id;
                }            
            }else{
                _SQL = "";
                _SQL = _SQL + "Insert Into ";
                _SQL = _SQL + " ideia ";
                _SQL = _SQL + "   ( ";
                _SQL = _SQL + "     Titulo ";
                _SQL = _SQL + "    ,Descricao  ";
                _SQL = _SQL + "    ,id_usuario  ";
                _SQL = _SQL + "    ,is_ideia_ativa ";
                _SQL = _SQL + "    ,data_criacao ";
                _SQL = _SQL + "    ,data_limite ";
                _SQL = _SQL + "    ,Prioridade ";
                _SQL = _SQL + "    ,TipoIdeia ";
                _SQL = _SQL + "    ,id_empresa ";
                _SQL = _SQL + "   ) ";
                _SQL = _SQL + " Values ";
                _SQL = _SQL + "   (  ";
                _SQL = _SQL + "     '" + _Titulo + "' ";
                _SQL = _SQL + "    ,'" + _Descricao + "' ";
                _SQL = _SQL + "    , " + _idUsuario + " ";
                _SQL = _SQL + "    , " + _Ativo + " ";
                _SQL = _SQL + "    ,'" + _dtCriacao + "' ";
                _SQL = _SQL + "    ,'" + _dtLimite + "' ";
                _SQL = _SQL + "    , " + _Prioridade + " ";
                _SQL = _SQL + "    ,'" + _TipoIdeia + "' ";
                _SQL = _SQL + "    , " + _idEmpresa + " ";
                _SQL = _SQL + "   ) ";

                _Retorno = _Banco.InsertIntoReturnID(_SQL);

                if (_Retorno.getErro() > 0 ){
                    return new Retorno(1, " Insert Into " + _Retorno.getMensagem());
                }else{
                    _idIdeia = (long)_Retorno.getObjeto();
                }            
            }
            
            //Transferir imagem do Temp para pasta Definitiva
            if ( criaDiretorio(UPLOAD_FILE_SERVER + _idIdeia) != true){
                _Mensagem = _Mensagem + "Erro ao Criar Diretorio";
            };
            if (excluiTemp(UPLOAD_FILE_SERVER + _idIdeia) != true){
                _Mensagem = _Mensagem + "Erro ao Excluir Temp";
            };
           
            if (moverArquivo(UPLOAD_FILE_SERVER_TEMP + _idEmpresa + "/" + _idUsuario, UPLOAD_FILE_SERVER + _idIdeia) != true){
                _Mensagem = _Mensagem + "Erro ao Mover Arquivo ";
            };
            
            //Realizando um loop para pegar todos os links e separar
            //Separando os links individuais
            
            
            _SQL = "";
            _SQL = _SQL + "Delete ";
            _SQL = _SQL + "From ";
            _SQL = _SQL + "  ideia_link ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "  ideia_id = " + _idIdeia + " ";

            _Retorno = _Banco.Commando(_SQL);
                
            for (int i = 0; i < _lstLink.size(); i++){
            
                _SQL = "";
                _SQL = _SQL + "Insert Into ";
                _SQL = _SQL + " ideia_link ";
                _SQL = _SQL + "   ( ";
                _SQL = _SQL + "     ideia_id ";
                _SQL = _SQL + "    ,Link  ";
                _SQL = _SQL + "   ) ";
                _SQL = _SQL + " Values ";
                _SQL = _SQL + "   (  ";
                _SQL = _SQL + "      " + _idIdeia + " ";
                _SQL = _SQL + "    ,'" + _lstLink.get(i) + "' ";
                _SQL = _SQL + "   ) ";

                _Retorno = _Banco.InsertIntoReturnID(_SQL);
            }

            return new Retorno(0, "História Cadastrada com Sucesso !!");
            
        }catch(Exception ex){
            
            if (ex.getMessage() != null){
                return new Retorno(0, ex.getMessage());
            }else{
                return new Retorno(0, "História Cadastrada com Sucesso !!");
            }
                 
        }
    
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("deletetemp")
    public Retorno deletetemp(IdeiaDTO dto) {
        try{
            
            Retorno _retorno = new Retorno();
            
            excluiTemp(UPLOAD_FILE_SERVER_TEMP + dto.getEmpresa() + "/" + dto.getUsuario());

            _retorno.setMensagem("Imagem excluida com Sucesso !!");

            return _retorno;
        }
        catch(Exception e)
        {
            Retorno _retorno = new Retorno();

            _retorno.setErro(1);
            _retorno.setMensagem(_retorno.getMensagem());

            return _retorno;
        }


    }   
    
    @POST
    @Consumes(MULTIPART_FORM_DATA)
    @Produces(APPLICATION_JSON)
    @Path("uploadimagem")
    public Retorno salvaImagem(MultipartFormDataInput fdt) {
        Map<String, List<InputPart>> map = fdt.getFormDataMap();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");

        try {
            String idUsuario = map.get("idUsuario").get(0).getBodyAsString();
            String idEmpresa = map.get("idEmpresa").get(0).getBodyAsString();

            criaDiretorio(UPLOAD_FILE_SERVER_TEMP + idEmpresa + "/" + idUsuario + "/");

            String fileName = dateFormat.format(new Date()) + ".jpg";

            redimensionaImagem(enviaImagemParaServidor(fileName, map, UPLOAD_FILE_SERVER_TEMP + idEmpresa + "/" + idUsuario + "/"), 768, 1024);

            return new Retorno(CODIGO_SUCESSO, MSG_IMAGEM_SALVA_COM_SUCESSO, fileName);
        } catch (IOException e) {
                return new Retorno(CODIGO_ERRO, MSG_OCORREU_UM_ERRO_AO_SALVAR_IMAGEM);
        }
    }
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("imagem/excluir")
    public Retorno apagaImagem(ImagemDTO dto) {
        try{
            File file = new File(UPLOAD_FILE_SERVER_TEMP + dto.getIdEmpresa() + "/" + dto.getIdUsuario() + "/" + dto.getFileName() );
            file.delete();

            Retorno _retorno = new Retorno();

            _retorno.setErro(0);
            _retorno.setMensagem("Imagem excluida com Sucesso !!");

            return _retorno;
        }
        catch(Exception e)
        {
            Retorno _retorno = new Retorno();

            _retorno.setErro(1);
            _retorno.setMensagem(_retorno.getMensagem());

            return _retorno;
        }


    }    
        
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("alterar")
    public Retorno alteraIdeia(AlteraIdeiaDTO dto) throws UnsupportedEncodingException {
        return facade.alteraIdeia(dto);
    }
	
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("alteraPrioridade")
    public Retorno alteraPrioridade(AlteraIdeiaDTO dto) {
        
        ConectaBanco _Banco = new ConectaBanco();
        
        String _SQL;

        try{
                
            _SQL = "";
            _SQL = _SQL + "Update ";
            _SQL = _SQL + "    ideia ";
            _SQL = _SQL + "Set ";
            _SQL = _SQL + "    prioridade = " + dto.getPrioridade() + " ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "    id = " + dto.getId() + " ";

            return _Banco.Commando(_SQL); 
                
        }catch(Exception ex){
            return new Retorno(1, "Ocorreu um erro ao enviar o convite!");
        }
        
    }
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("alteraDataLimite")
    public Retorno alteraDataLimite(AlteraIdeiaDTO dto) {
        
        ConectaBanco _Banco = new ConectaBanco();
        
        String _SQL;

        try{
                
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            
            _SQL = "";
            _SQL = _SQL + "Update ";
            _SQL = _SQL + "    ideia ";
            _SQL = _SQL + "Set ";
            _SQL = _SQL + "    data_limite = '" + dateFormat.format(dto.getDataLimite()) + "' ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "    id = " + dto.getId() + " ";

            return _Banco.Commando(_SQL); 
                
        }catch(Exception ex){
            return new Retorno(1, "Ocorreu um erro ao enviar o convite!");
        }
        
    }    
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("inativar")
    public Retorno inativaIdeia(InformaIdDTO dto) throws UnsupportedEncodingException {
        
        ConectaBanco _Banco = new ConectaBanco();
        
        String _SQL;

        try{
                
            _SQL = "";
            _SQL = _SQL + "Update ";
            _SQL = _SQL + "    ideia ";
            _SQL = _SQL + "Set ";
            _SQL = _SQL + "    is_ideia_ativa = 0 ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "    id = " + dto.getId() + " ";

            return _Banco.Commando(_SQL); 
                
        }catch(Exception ex){
            return new Retorno(1, "Ocorreu um erro ao excluir a ideia ");
        }

    }

    @GET
    @Produces(APPLICATION_JSON)
    @Path("listar")
    public List<IdeiaDTO> listaIdeia() throws UnsupportedEncodingException {
        return facade.listaIdeias();
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("buscar")
    public Retorno buscaIdeiaPorId(IdeiaEquipeDTO dto) {

        try {
            
            ConectaBanco _Banco = new ConectaBanco();

            String _SQL;

            //Procurando o Id do Projeto a Partir do id_brainstorming

            _SQL = "";
            _SQL = _SQL + "SELECT " 
                            + " i.* "
                        + "FROM  "
                            + "ideia i "
                        + "WHERE "
                            + "i.id = " + dto.getId() + " ";

            ResultSet rs = _Banco.Consulta(_SQL);

            IdeiaDTO _ideia = new IdeiaDTO();
            
            
            if (rs.first()){
                
                List<String> _ideiaImagem = new ArrayList<>();
                List<String> _ideiaLink = new ArrayList<>();
                
                SimpleDateFormat dateReturn = new SimpleDateFormat("yyyy-MM-dd");
                
		_ideia.setId(rs.getLong("id"));
		_ideia.setTitulo(rs.getString("Titulo"));
		_ideia.setDescricao(rs.getString("Descricao"));
		_ideia.setUsuario(rs.getLong("id_usuario"));
		_ideia.setDataCriacao(dateReturn.format(rs.getDate("data_criacao")));
                
                if (rs.getDate("data_limite") != null){
                    _ideia.setDataLimite(dateReturn.format(rs.getDate("data_limite")));
                }else{
                    _ideia.setDataLimite(null);
                }
                    
		_ideia.setTipoIdeia(rs.getString("tipoIdeia"));
		_ideia.setEmpresa(rs.getLong("id_empresa"));
                _ideia.setPrioridade(rs.getLong("prioridade"));

                if ((dto.getIdEmpresa() != null) && (dto.getIdUsuario() != null)) {

                    criaDiretorio(UPLOAD_FILE_SERVER_TEMP + dto.getIdEmpresa() + "/" + dto.getIdUsuario());

                    //Copiar para o Temp
                    if (excluiTemp(UPLOAD_FILE_SERVER_TEMP + dto.getIdEmpresa() + "/" + dto.getIdUsuario()) != true){
                        return new Retorno(1, "Erro ao Excluir o Temp");
                    };
                    
                    String _Resposta;
                    _Resposta = copiarArquivo(UPLOAD_FILE_SERVER + dto.getId() + "/", UPLOAD_FILE_SERVER_TEMP + dto.getIdEmpresa() + "/" + dto.getIdUsuario() + "/");
                    if (_Resposta.equals("") != true) {
                        return new Retorno(1, _Resposta);
                    };
                    File diretorio = new File(UPLOAD_FILE_SERVER_TEMP + dto.getIdEmpresa() + "/" + dto.getIdUsuario());

                    for (File file : diretorio.listFiles()) {
                        _ideiaImagem.add(file.getName());
                    }

                    _SQL = "";
                    _SQL = _SQL + "SELECT " 
                                    + " i.* "
                                + "FROM  "
                                    + "ideia_link i "
                                + "WHERE "
                                    + "i.ideia_id = " + dto.getId() + " ";

                    rs = _Banco.Consulta(_SQL);

                    while (rs.next()){
                        _ideiaLink.add(rs.getString("link"));
                    }

                    _ideia.setLstImagem(_ideiaImagem);
                    _ideia.setLstLink(_ideiaLink);
                    
                }
                return new Retorno(0, "Listagem Gerada com Sucesso", _ideia);
                
            }else{
                return new Retorno(1, "Não foi Encontrado Informaçãoes para essa ideia" );
            }


        }catch(Exception ex){
            return new Retorno(1, ex.getMessage());
        }
        
        //return facade.buscaIdeiaPorId(dto);
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("usuario")
    public Retorno buscaIdeiaPorUsuario(EmpresaUsuarioDTO dto) {

        try {
            
            ConectaBanco _Banco = new ConectaBanco();

            String _SQL;

            //Procurando o Id do Projeto a Partir do id_brainstorming

            _SQL = "";
            _SQL = _SQL + "SELECT " 
                        + "i.id as id_historia, " 
                        + "i.titulo as titulo_historia, " 
                        + "i.descricao as descricao_historia, " 
                        + "i.data_limite, "
                        + "i.prioridade, "
                        + "i.id_empresa as idEmpresa, "
                        + "u.id as idUsuario, "
                        + "u.nome "
                 + "FROM ideia i "
                        + "LEFT JOIN ideia_equipe ie ON ie.id_ideia = i.id "
                        + "LEFT JOIN usuario u ON ie.id_usuario = u.id "
                + "WHERE "
                        + "i.is_ideia_ativa = 1 "
                    + "and i.id in (SELECT id_ideia from ideia_equipe where id_usuario = " + dto.getIdUsuario() + ") ";

            ResultSet _item = _Banco.Consulta(_SQL);

            List<HistoriaUsuarioEmpresaSaidaDTO> dtos = new ArrayList<>();

            try{
                while (_item.next()) {

                    HistoriaUsuarioEmpresaSaidaDTO _itemdto = new HistoriaUsuarioEmpresaSaidaDTO();

                    _itemdto.setIdIdeia(_item.getLong("id_historia"));
                    _itemdto.setTituloIdeia(_item.getString("titulo_historia"));
                    _itemdto.setDescricaoIdeia(_item.getString("descricao_historia"));
                    _itemdto.setDataLimite(_item.getDate("data_limite"));
                    _itemdto.setPrioridade(_item.getLong("prioridade"));
                    _itemdto.setIdUsuario(_item.getLong("idUsuario"));
                    _itemdto.setUsuario(_item.getString("nome"));
                    _itemdto.setIdEmpresa(_item.getLong("idEmpresa"));

                    dtos.add(_itemdto);

                }

                return new Retorno(0, "Listagem Gerada com Sucesso", dtos);

            }catch(Exception ex){
                return new Retorno(0, ex.getMessage());
            }

        } catch (Exception ex) {
            return new Retorno(1, "Ta dando Nada Não " + ex.getMessage());
        }  
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("usuario/empresa")
    public Retorno listaHistoriaPorUsuarioEmpresa(EmpresaUsuarioDTO dto) {

        try {

            ConectaBanco _Banco = new ConectaBanco();

            String _SQL;

            //Procurando o Id do Projeto a Partir do id_brainstorming

            _SQL = "";
            _SQL = _SQL + "SELECT " 
                        + "i.id as id_historia, " 
                        + "i.titulo as titulo_historia, " 
                        + "i.descricao as descricao_historia, " 
                        + "i.data_limite, "
                        + "i.prioridade, "
                        + "u.id as idUsuario, "
                        + "i.id_empresa as idEmpresa, "
                        + "u.nome "
                 + "FROM ideia i "
                        + "LEFT JOIN ideia_equipe ie ON ie.id_ideia = i.id "
                        + "LEFT JOIN usuario u ON ie.id_usuario = u.id "
                + "WHERE "
                        + "i.is_ideia_ativa = 1 "
                    + "and i.id in (SELECT id_ideia from ideia_equipe where id_empresa = " + dto.getIdEmpresa() + " and id_usuario = " + dto.getIdUsuario() + ") ";

            ResultSet _item = _Banco.Consulta(_SQL);

            List<HistoriaUsuarioEmpresaSaidaDTO> dtos = new ArrayList<>();

            try{
                while (_item.next()) {

                    HistoriaUsuarioEmpresaSaidaDTO _itemdto = new HistoriaUsuarioEmpresaSaidaDTO();

                    _itemdto.setIdIdeia(_item.getLong("id_historia"));
                    _itemdto.setTituloIdeia(_item.getString("titulo_historia"));
                    _itemdto.setDescricaoIdeia(_item.getString("descricao_historia"));
                    _itemdto.setDataLimite(_item.getDate("data_limite"));
                    _itemdto.setPrioridade(_item.getLong("prioridade"));
                    _itemdto.setIdUsuario(_item.getLong("idUsuario"));
                    _itemdto.setUsuario(_item.getString("nome"));
                    _itemdto.setIdEmpresa(_item.getLong("idEmpresa"));

                    dtos.add(_itemdto);

                }

                return new Retorno(0, "Listagem Gerada com Sucesso", dtos);

            }catch(Exception ex){
                return new Retorno(0, ex.getMessage());
            }

        } catch (Exception ex) {
            return new Retorno(1, "Ta dando Nada Não " + ex.getMessage());
        }  
        
    
    }
}
