package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class FaixaFuncionarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private Integer quantidadeInicial;
	private Integer quantidadeFinal;

	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public Integer getQuantidadeInicial() {
		return quantidadeInicial;
	}

	public Integer getQuantidadeFinal() {
		return quantidadeFinal;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setQuantidadeInicial(Integer quantidadeInicial) {
		this.quantidadeInicial = quantidadeInicial;
	}

	public void setQuantidadeFinal(Integer quantidadeFinal) {
		this.quantidadeFinal = quantidadeFinal;
	}
}
