package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.NotificaSolicitacaoEquipeEmpresaDTO;
import br.com.portal.inovacao.model.EmpresaEquipe;

public class NotificaSolicitacaoEquipeEmpresaConverter {
	
	public static NotificaSolicitacaoEquipeEmpresaDTO converterNotificacaoEquipeEmpresa(EmpresaEquipe empresaEquipe) {
		NotificaSolicitacaoEquipeEmpresaDTO dto = new NotificaSolicitacaoEquipeEmpresaDTO();
		dto.setIdEmpresa(empresaEquipe.getId());
		dto.setRazaoSocial(empresaEquipe.getEmpresa().getRazaoSocial());
		return dto;
	} 
}
