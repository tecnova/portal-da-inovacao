package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class AdicionaUsuarioNoGrupoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long idUsuario;
	private Long idGrupo;
	private Long idEmpresa;

	public Long getId() {
		return id;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public Long getIdGrupo() {
		return idGrupo;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
