package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.PesquisaUsuario;
import br.com.portal.inovacao.dto.SolicitacaoAmigoDTO;
import br.com.portal.inovacao.facade.UsuarioAmigoFacade;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.utils.Retorno;

@Path("solicitacao")
public class SolicitacaoAmigoService {
	
	@Inject
	private UsuarioAmigoFacade facade;
	
	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("solicitar")
	public Retorno fazSolicitacao(SolicitacaoAmigoDTO solicitacaoAmigo) {
		return facade.criaSolicitacao(solicitacaoAmigo);
	}
	
	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("solicitacoes/pendentes/usuario")
	public Retorno solicitacoesPendentesDoUsuario(SolicitacaoAmigoDTO solicitacaoAmigo) {
		return facade.solicitacoesPendentesDoUsuario(solicitacaoAmigo);
	}

	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("solicitacoes/pendentes/amigo")
	public Retorno solicitacoesPendentesDoAmigo(SolicitacaoAmigoDTO solicitacaoAmigo) {
		return facade.solicitacoesPendentesDoAmigo(solicitacaoAmigo);
	}
	
	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("aprovar")
	public Retorno aprovaSolicitacaoAmizade(SolicitacaoAmigoDTO solicitacaoAprovada) {
		return facade.aprovaSolicitacaoAmizade(solicitacaoAprovada);
	}

	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("recusar")
	public Retorno recusaSolicitacaoAmizade(SolicitacaoAmigoDTO solicitacaoAprovada) {
		return facade.recusaSolicitacaoAmizade(solicitacaoAprovada);
	}
	
	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("amigos")
	public Retorno buscaAmigos(SolicitacaoAmigoDTO idUsuario) {
		return facade.listaAmigos(idUsuario);
	}

	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("pesquisa/usuarios")
	public List<Usuario> pesquisaUsuarios(PesquisaUsuario pesquisa) throws UnsupportedEncodingException {
		return facade.pesquisaUsuarios(pesquisa);
	}
	
	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("amigos/usuario")
	public Retorno mostraAmigosDoUsuario(InformaIdDTO dto) {
		return facade.mostraAmigosDoUsuario(dto);
	}
}