package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.utils.Retorno;
import br.com.portal.inovacao.facade.CanvasFacade;
import br.com.portal.inovacao.dto.CanvasDTO;
import br.com.portal.inovacao.utils.ConectaBanco;
import java.util.List;

@Path("canvas")
public class CanvasService {
	
	@Inject
	private CanvasFacade facade;
	
    /**
     *
     * @param dto
     * @return
     */
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("criar")
    public Retorno criaCanvas(CanvasDTO dto) {
            return facade.criaCanvas(dto);
            //return new Retorno(0, "Retornei Alguma Coisa");
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("apagar")
    public Retorno apagaCanvas(CanvasDTO dto) {
        
        String _SQL;
        _SQL = "";
        _SQL = _SQL + "Delete From ";
        _SQL = _SQL + "    Canvas   ";
        _SQL = _SQL + "Where ";
        _SQL = _SQL + "    id = " + dto.getId() + " ";
        
        ConectaBanco _Banco = new ConectaBanco();
        
        return _Banco.Commando(_SQL);
        
    }
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("lista")
    public List<CanvasDTO> listaCanvas(CanvasDTO dto) {
            return facade.listaCanvas(dto);
            //return new Retorno(0, "Retornei Alguma Coisa");
    }
    

}
