package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class Projeto5w2hDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private long idbrainstorming;
    private String ataque;
    private String assunto;
    private long idprojeto5w2h;
    private String acao;
    private String motivo;
    private String local;
    private String dtInicio;
    private String dtTermino;
    private String detalhamento;
    private double valor;
    private long idprojeto5w2hitem;
    private String equipe;
    private long idusuario;
    private String feedback;
    private String dtFeedback;
    private List<UsuarioDTO> lstUsuarioAcao;
    private List<UsuarioDTO> lstUsuarioHistoria;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdbrainstorming() {
        return idbrainstorming;
    }

    public void setIdbrainstorming(long idbrainstorming) {
        this.idbrainstorming = idbrainstorming;
    }

    public String getAtaque() {
        return ataque;
    }

    public void setAtaque(String ataque) {
        this.ataque = ataque;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getDtInicio() {
        return dtInicio;
    }

    public void setDtInicio(String dtInicio) {
        this.dtInicio = dtInicio;
    }

    public String getDtTermino() {
        return dtTermino;
    }

    public void setDtTermino(String dtTermino) {
        this.dtTermino = dtTermino;
    }

    public String getDetalhamento() {
        return detalhamento;
    }

    public void setDetalhamento(String detalhamento) {
        this.detalhamento = detalhamento;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public long getIdprojeto5w2hitem() {
        return idprojeto5w2hitem;
    }

    public void setIdprojeto5w2hitem(long idprojeto5w2hitem) {
        this.idprojeto5w2hitem = idprojeto5w2hitem;
    }
    
    public long getIdprojeto5w2h() {
        return idprojeto5w2h;
    }

    public void setIdprojeto5w2h(long idprojeto5w2h) {
        this.idprojeto5w2h = idprojeto5w2h;
    }

    public long getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(long idusuario) {
        this.idusuario = idusuario;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getDtFeedback() {
        return dtFeedback;
    }

    public void setDtFeedback(String dtFeedback) {
        this.dtFeedback = dtFeedback;
    }

    public List<UsuarioDTO> getLstUsuarioAcao() {
        return lstUsuarioAcao;
    }

    public void setLstUsuarioAcao(List<UsuarioDTO> lstUsuarioAcao) {
        this.lstUsuarioAcao = lstUsuarioAcao;
    }

    public List<UsuarioDTO> getLstUsuarioHistoria() {
        return lstUsuarioHistoria;
    }

    public void setLstUsuarioHistoria(List<UsuarioDTO> lstUsuarioHistoria) {
        this.lstUsuarioHistoria = lstUsuarioHistoria;
    }
    
    
}
