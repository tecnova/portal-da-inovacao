package br.com.portal.inovacao.util.jpa;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ProdutorEntityManager {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("PortalDaInovacaoPU");

	@Produces
	@RequestScoped
	private EntityManager criaEntityManager() {
		return factory.createEntityManager();
	}

	@SuppressWarnings("unused")
	private void finaliza(@Disposes EntityManager manager) {
		manager.close();
	}
}
