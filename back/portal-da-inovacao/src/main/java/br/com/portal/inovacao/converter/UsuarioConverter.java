package br.com.portal.inovacao.converter;

import static br.com.portal.inovacao.converter.PerfilConverter.converterPerfil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.portal.inovacao.dto.CriaUsuarioSaida;
import br.com.portal.inovacao.dto.PerfilDTO;
import br.com.portal.inovacao.model.Perfil;
import br.com.portal.inovacao.model.Usuario;

public class UsuarioConverter {
	
    public static CriaUsuarioSaida converterUsuario(Usuario usuario) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        CriaUsuarioSaida saida = new CriaUsuarioSaida();
        saida.setId(usuario.getId());
        saida.setNome(usuario.getNome());
        saida.setEmail(usuario.getEmail());
        saida.setId_face(usuario.getId_face());
        saida.setId_google(usuario.getId_google());
        saida.setPerfil_tema(usuario.getPerfil_tema());
        saida.setData(dateFormat.format(usuario.getData()));

        List<PerfilDTO> perfilDTO = new ArrayList<>();

        for (Perfil perfil : usuario.getPerfis()) {
                perfilDTO.add(converterPerfil(perfil));
        }
        saida.setPerfis(perfilDTO);

        return saida;
    }
}
