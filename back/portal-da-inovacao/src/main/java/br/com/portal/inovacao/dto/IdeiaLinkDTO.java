package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class IdeiaLinkDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long ideia;
	private String link;

	public Long getId() {
		return id;
	}

	public Long getIdeia() {
		return ideia;
	}

	public String getLink() {
		return link;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIdeia(Long ideia) {
		this.ideia = ideia;
	}

	public void setLink(String link) {
		this.link = link;
	}
}