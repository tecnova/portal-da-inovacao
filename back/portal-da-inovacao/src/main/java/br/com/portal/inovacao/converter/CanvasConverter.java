package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.CanvasDTO;
import br.com.portal.inovacao.model.Canvas;

public class CanvasConverter {
	
	public static CanvasDTO converterCanvas(Canvas canvas) {
		CanvasDTO canvasDTO = new CanvasDTO();
		canvasDTO.setId(canvas.getId());
		canvasDTO.setIdIdeia(canvas.getIdIdeia().getId());
		canvasDTO.setIdUsuario(canvas.getIdUsuario().getId());
		canvasDTO.setNr_item(canvas.getNr_item());
		canvasDTO.setDescricao(canvas.getDescricao());
		canvasDTO.setValor(canvas.getValor());
		canvasDTO.setData_cadastro(canvas.getData_cadastro());
		return canvasDTO;
	}
}
