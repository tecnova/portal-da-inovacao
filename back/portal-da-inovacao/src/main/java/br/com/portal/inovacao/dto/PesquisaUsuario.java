package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class PesquisaUsuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String pesquisa;

	public PesquisaUsuario() {
	}

	public PesquisaUsuario(Long id, String pesquisa) {
		this.id = id;
		this.pesquisa = pesquisa;
	}

	public Long getId() {
		return id;
	}

	public String getPesquisa() {
		return pesquisa;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPesquisa(String pesquisa) {
		this.pesquisa = pesquisa;
	}
}
