package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.portal.inovacao.facade.EmpresaImagemFacade;
import br.com.portal.inovacao.utils.Retorno;

@Path("empresa/imagem")
public class EmpresaImagemService {
	
	@Inject
	private EmpresaImagemFacade facade;
	
	@POST
	@Consumes(MULTIPART_FORM_DATA)
	@Produces(APPLICATION_JSON)
	@Path("salvar")
	public Retorno salvaImagem(MultipartFormDataInput multipartFormDataInput) {
		return facade.salvaImagem(multipartFormDataInput);
	}
}
