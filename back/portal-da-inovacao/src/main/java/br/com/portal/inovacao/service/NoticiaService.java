package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.portal.inovacao.dao.NoticiaDAO;
import br.com.portal.inovacao.dto.BuscaNoticiaPorId;
import br.com.portal.inovacao.dto.InativaNoticia;
import br.com.portal.inovacao.facade.NoticiaFacade;
import br.com.portal.inovacao.model.Noticia;
import br.com.portal.inovacao.model.NoticiaCategoria;
import br.com.portal.inovacao.utils.Retorno;

@Path("noticia")
public class NoticiaService {

	@Inject
	private NoticiaFacade facade;

	@Inject
	private NoticiaDAO dao;
	
	@POST
	@Consumes(MULTIPART_FORM_DATA)
	@Produces(APPLICATION_JSON)
	@Path("criar")
	public Retorno criaNoticia(MultipartFormDataInput multipartFormDataInput) throws Exception {
		return facade.salvaNoticia(multipartFormDataInput);
	}
	
	@POST
	@Consumes(MULTIPART_FORM_DATA )
	@Produces(APPLICATION_JSON)
	@Path("alterar")
	public Retorno alteraNoticia(MultipartFormDataInput multipartFormDataInput) throws IOException, Exception {
		return facade.alteraNoticia(multipartFormDataInput);
	}

	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("inativar")
	public Retorno inativaNoticia(InativaNoticia noticia) {
		return facade.inativaNoticia(noticia);
	}

	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	@Path("busca/noticia")
	public Noticia noticiaPorId(BuscaNoticiaPorId noticia) {
		return facade.noticiaPorId(noticia);
	}

	@GET
	@Produces(APPLICATION_JSON)
	@Path("categorias")
	public List<NoticiaCategoria> listaCategorias() {
		return dao.listaCategorias();
	}

	@GET
	@Produces(APPLICATION_JSON)
	@Path("setores")
	public List<NoticiaCategoria> listaSetores() {
		return dao.listaSetores();
	}

	@GET
	@Produces(APPLICATION_JSON)
	@Path("noticias")
	public List<Noticia> listaNoticias() {
		return dao.listaNoticias();
	}
        
  
        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("teste")
	public Retorno teste() {
		//return facade.criaCanvas(dto);
                return new Retorno(0, "Retornei Alguma Coisa");
	}

}
