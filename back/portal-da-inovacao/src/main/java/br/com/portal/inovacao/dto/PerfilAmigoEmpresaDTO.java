package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.List;

public class PerfilAmigoEmpresaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idUsuario;
	private Long idAmigoEmpresa;
	private Long idEmpresa;
	private List<PerfilDTO> perfis;

	public Long getIdUsuario() {
		return idUsuario;
	}

	public Long getIdAmigoEmpresa() {
		return idAmigoEmpresa;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public List<PerfilDTO> getPerfis() {
		return perfis;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setIdAmigoEmpresa(Long idAmigoEmpresa) {
		this.idAmigoEmpresa = idAmigoEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public void setPerfis(List<PerfilDTO> perfis) {
		this.perfis = perfis;
	}
}
