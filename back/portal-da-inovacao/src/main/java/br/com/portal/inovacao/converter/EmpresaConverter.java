package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.EmpresaDTO;
import br.com.portal.inovacao.model.Empresa;

public class EmpresaConverter {
	
	public static EmpresaDTO converterEmpresa(Empresa empresa) {
		EmpresaDTO dto = new EmpresaDTO();
		dto.setIdEmpresa(empresa.getId());
		dto.setCnpj(empresa.getCnpj());
		dto.setRazaoSocial(empresa.getRazaoSocial());
		dto.setLogradouro(empresa.getLogradouro());
		dto.setNumero(empresa.getNumero());
		dto.setComplemento(empresa.getComplemento());
		dto.setBairro(empresa.getBairro());
		dto.setMunicipio(empresa.getMunicipio());
		dto.setUf(empresa.getUf());
		dto.setCep(empresa.getUf());
		dto.setIdCnae(empresa.getIdCnae());
		dto.setIdFaixaFuncionario(empresa.getIdFaixaFuncionario());
		dto.setIdFaixaFaturamento(empresa.getIdFaixaFaturamento());
		dto.setImagem(empresa.isImagem());
                dto.setTelefone(empresa.getTelefone());
		return dto;
	}
}
