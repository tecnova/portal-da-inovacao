package br.com.portal.inovacao.service;

import br.com.portal.inovacao.dto.BrainstormingDTO;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.GrupoBrainstormingDTO;
import br.com.portal.inovacao.facade.GrupoBrainstormingFacade;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Path("grupo/brainstorming")
public class GrupoBrainstormingService {
	
	@Inject
	private GrupoBrainstormingFacade facade;
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("criar")
	public Retorno criaGrupoBrainstorming(GrupoBrainstormingDTO dto) {
		return facade.criaGrupo(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("alterar")
	public Retorno alteraGrupoBrainstorming(GrupoBrainstormingDTO dto) {
		return facade.alteraGrupo(dto);
	}
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("excluir")
	public Retorno excluiGrupoBrainstorming(GrupoBrainstormingDTO dto) {
            
            ConectaBanco _Banco = new ConectaBanco();
            
            String _SQL;
            
            _SQL = "";
            _SQL = _SQL + "Select ";
            _SQL = _SQL + "   1 ";
            _SQL = _SQL + "From  ";
            _SQL = _SQL + "   brainstorming ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "   id_grupobrainstorming = " + dto.getId();
                
            ResultSet rs = _Banco.Consulta(_SQL);

            try{
                if (rs.first()){
                    return new Retorno(1, "Retire primeiramente o item do Brainstorming que se encontra dentro do grupo.!");
                }else{
                    return facade.excluiGrupo(dto);
                }
            }catch(Exception ex){
                return new Retorno(1, "Não foi possível excluir o grupo.");
            }
            
	}
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista")
	public Retorno listaGrupoBrainstormingPorHistoria(GrupoBrainstormingDTO dto) {

            
            List<GrupoBrainstormingDTO> _Result = new ArrayList();
            
            GrupoBrainstormingDTO _item;
            ResultSet rs;
            ConectaBanco _Banco = new ConectaBanco();
            
            String _SQL;
            
            _SQL = "";
            _SQL = _SQL + "SELECT  ";
            _SQL = _SQL + "    * ";
            _SQL = _SQL + "FROM ";
            _SQL = _SQL + "    grupo_brainstorming ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "	id_historia = " + dto.getIdHistoria();
                
            rs = _Banco.Consulta(_SQL);

            try{
                while (rs.next()){
                    
                    _item = new GrupoBrainstormingDTO();
                    
                    _item.setId(rs.getLong("id"));
                    _item.setDescricao(rs.getString("descricao"));
                    _item.setIdHistoria(rs.getLong("id_historia"));
                    
                    _Result.add(_item);
                    
                }

                return new Retorno(0, "Listagem Efetuada com Sucesso", _Result);

            }catch(Exception ex){
                return new Retorno(1, "Não foi possível Listar os Itens");
            }            
        }
	
}
