package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.EmailDTO;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.ConverteSenhaParaMD5;
import br.com.portal.inovacao.utils.Retorno;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("email")
public class EmailService {
	
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("enviar")
    public Retorno enviaEmail(EmailDTO email) {
        ConviteEmail conviteEmail = new ConviteEmail();
        return conviteEmail.run(email);
    }
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("convite")
    public Retorno conviteEmail(EmailDTO dto) {
        
        ConectaBanco _Banco = new ConectaBanco();
        Date _data = new Date();
        Retorno _Retorno;
        
        String _SQL;
        _SQL = "";
        _SQL = _SQL + "Select";
        _SQL = _SQL + "    id ";
        _SQL = _SQL + "From ";
        _SQL = _SQL + "    usuario ";
        _SQL = _SQL + "Where ";
        _SQL = _SQL + "    email = '" + dto.getEmail() + "' ";
        
        ResultSet rs = _Banco.Consulta(_SQL);

        try{
            
            Long _id;
            _id = Long.parseLong("0");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            
            if (!rs.first()){
                
                _SQL = "";
                _SQL = _SQL + "Insert Into ";
                _SQL = _SQL + "   usuario  ";
                _SQL = _SQL + "      ( ";
                _SQL = _SQL + "        email ";
                _SQL = _SQL + "       ,nome  ";
                _SQL = _SQL + "       ,data_criacao ";
                _SQL = _SQL + "       ,is_usuario_ativo ";
                _SQL = _SQL + "      ) ";
                _SQL = _SQL + "   values  ";
                _SQL = _SQL + "      (  ";
                _SQL = _SQL + "         '" + dto.getEmail() + "' ";
                _SQL = _SQL + "        ,'" + dto.getEmail() + "' ";
                _SQL = _SQL + "        ,'" + dateFormat.format(_data) + "' ";
                _SQL = _SQL + "        ,0 ";
                _SQL = _SQL + "       )";
                
                _Retorno = _Banco.InsertIntoReturnID(_SQL);

                
                if (_Retorno.getErro() == 0){
                    _id = (Long)_Retorno.getObjeto();
                }
                
            }else{
                _id = rs.getLong("id");
            }

            if (_id > 0){
                _SQL = "";
                _SQL = _SQL + "Insert Into ";
                _SQL = _SQL + "   empresa_equipe  ";
                _SQL = _SQL + "      ( ";
                _SQL = _SQL + "        id_empresa ";
                _SQL = _SQL + "       ,id_perfil_empresa ";
                _SQL = _SQL + "       ,id_usuario ";
                _SQL = _SQL + "       ,data_solicitacao ";
                _SQL = _SQL + "       ,is_email_enviado ";
                _SQL = _SQL + "       ,solicitacao_aprovada ";
                _SQL = _SQL + "      ) ";
                _SQL = _SQL + "   values  ";
                _SQL = _SQL + "      (  ";
                _SQL = _SQL + "          " + dto.getIdEmpresa() + " ";
                _SQL = _SQL + "        ,1 ";
                _SQL = _SQL + "        ," + _id + " ";
                _SQL = _SQL + "        ,'" + dateFormat.format(_data) + "' ";
                _SQL = _SQL + "        ,1 ";
                _SQL = _SQL + "        ,0 ";
                _SQL = _SQL + "       )";

                _Retorno = _Banco.Commando(_SQL);

                if (_Retorno.getErro() == 1){
                    return new Retorno(1, _Retorno.getMensagem());
                }else{
                    ConviteEmail conviteEmail = new ConviteEmail();
                    _Retorno = conviteEmail.run(dto);
                    return new Retorno(0, _Retorno.getMensagem());
                }

            }else{
                return new Retorno(1, "Impossível Vincular usuário a Empresa. ");
            }
            
        }catch(Exception ex){
            return new Retorno(1, "Ocorreu um erro ao enviar o convite!");
        }
                
    }
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("recuperar")
    public Retorno RecuperarSenha(EmailDTO dto) {
        
        ConectaBanco _Banco = new ConectaBanco();
        Date _data = new Date();
        Retorno _Retorno;
        
        String _SQL;
        _SQL = "";
        _SQL = _SQL + "Select";
        _SQL = _SQL + "    1 ";
        _SQL = _SQL + "From ";
        _SQL = _SQL + "    usuario ";
        _SQL = _SQL + "Where ";
        _SQL = _SQL + "    email = '" + dto.getEmail() + "' ";
        
        ResultSet rs = _Banco.Consulta(_SQL);

        try{
            if (rs.first()){
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String _Link;
                
                Date today = Calendar.getInstance().getTime();
                
                _Link = ConverteSenhaParaMD5.convertPasswordToMD5(dateFormat.format(today));
                //_Link = ConverteSenhaParaMD5.convertPasswordToMD5(dateFormat.toString());
                
                _SQL = "";
                _SQL = _SQL + "Update ";
                _SQL = _SQL + "    usuario ";
                _SQL = _SQL + "Set ";
                _SQL = _SQL + "    Link = '" + _Link + "' ";
                _SQL = _SQL + "   ,senha ='' "; 
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "    email = '" + dto.getEmail() + "' ";
                
                _Retorno = _Banco.Commando(_SQL);
                
                if (_Retorno.getErro() == 0) {
                    ConviteEmail _convite = new ConviteEmail();
                    _Retorno = _convite.RecuperarSenha(dto, _Link);
                }
                
                return _Retorno;
                
                
            }else{
                return new Retorno(1, "E-mail não encontrado");
            }
        }catch(Exception ex){
            return new Retorno(1, "Ocorreu um erro ao enviar o convite!");
        }
                
    }
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("contato")
    public Retorno ContatoPagina(EmailDTO dto) {
        
        try{

            Retorno _Retorno;

            ConviteEmail _convite = new ConviteEmail();
            _Retorno = _convite.ContatoPagina(dto);

            return new Retorno(0, "Mensagem Enviada");
        }catch(Exception ex){
            return new Retorno(1, "Ocorreu um erro ao enviar o convite!");
        }
    }
    

}