package br.com.portal.inovacao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "empresa")
public class Empresa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String cnpj;

	@Column(name = "razao_social")
	private String razaoSocial;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String municipio;
	private String uf;
	private String cep;
        private String telefone;

	@Column(name = "id_cnae")
	private Long idCnae;

	@Column(name = "id_faixa_funcionario")
	private Long idFaixaFuncionario;

	@Column(name = "id_faixa_faturamento")
	private Long idFaixaFaturamento;

	@Column(name = "is_empresa_ativa")
	private boolean ativo;

	@Column(name = "is_empresa_imagem")
	private boolean imagem;

	public Long getId() {
		return id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public String getMunicipio() {
		return municipio;
	}

	public String getUf() {
		return uf;
	}

	public String getCep() {
		return cep;
	}

	public Long getIdCnae() {
		return idCnae;
	}

	public Long getIdFaixaFuncionario() {
		return idFaixaFuncionario;
	}

	public Long getIdFaixaFaturamento() {
		return idFaixaFaturamento;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public boolean isImagem() {
		return imagem;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public void setIdCnae(Long idCnae) {
		this.idCnae = idCnae;
	}

	public void setIdFaixaFuncionario(Long idFaixaFuncionario) {
		this.idFaixaFuncionario = idFaixaFuncionario;
	}

	public void setIdFaixaFaturamento(Long idFaixaFaturamento) {
		this.idFaixaFaturamento = idFaixaFaturamento;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void setImagem(boolean imagem) {
		this.imagem = imagem;
	}

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

        
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (ativo ? 1231 : 1237);
        result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
        result = prime * result + ((cep == null) ? 0 : cep.hashCode());
        result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
        result = prime * result	+ ((complemento == null) ? 0 : complemento.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((idCnae == null) ? 0 : idCnae.hashCode());
        result = prime * result + ((idFaixaFaturamento == null) ? 0 : idFaixaFaturamento.hashCode());
        result = prime * result + ((idFaixaFuncionario == null) ? 0 : idFaixaFuncionario.hashCode());
        result = prime * result + (imagem ? 1231 : 1237);
        result = prime * result	+ ((logradouro == null) ? 0 : logradouro.hashCode());
        result = prime * result	+ ((municipio == null) ? 0 : municipio.hashCode());
        result = prime * result + ((numero == null) ? 0 : numero.hashCode());
        result = prime * result	+ ((razaoSocial == null) ? 0 : razaoSocial.hashCode());
        result = prime * result + ((uf == null) ? 0 : uf.hashCode());
        result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
        return result;
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (ativo != other.ativo)
			return false;
		if (bairro == null) {
			if (other.bairro != null)
				return false;
		} else if (!bairro.equals(other.bairro))
			return false;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (complemento == null) {
			if (other.complemento != null)
				return false;
		} else if (!complemento.equals(other.complemento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idCnae == null) {
			if (other.idCnae != null)
				return false;
		} else if (!idCnae.equals(other.idCnae))
			return false;
		if (idFaixaFaturamento == null) {
			if (other.idFaixaFaturamento != null)
				return false;
		} else if (!idFaixaFaturamento.equals(other.idFaixaFaturamento))
			return false;
		if (idFaixaFuncionario == null) {
			if (other.idFaixaFuncionario != null)
				return false;
		} else if (!idFaixaFuncionario.equals(other.idFaixaFuncionario))
			return false;
		if (imagem != other.imagem)
			return false;
		if (logradouro == null) {
			if (other.logradouro != null)
				return false;
		} else if (!logradouro.equals(other.logradouro))
			return false;
		if (municipio == null) {
			if (other.municipio != null)
				return false;
		} else if (!municipio.equals(other.municipio))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (razaoSocial == null) {
			if (other.razaoSocial != null)
				return false;
		} else if (!razaoSocial.equals(other.razaoSocial))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
                
		return true;
	}
}
