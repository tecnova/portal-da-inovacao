package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.Ideia;

public class IdeiaDAO {
	
	@Inject
	private EntityManager manager;
	
	public IdeiaDAO() {}
	
	public void salva(Ideia ideia) {
		manager.getTransaction().begin();
		manager.persist(ideia);
		manager.getTransaction().commit();
	}
	
	public Ideia pesquisaPorId(Long id) {
		return manager.find(Ideia.class, id);
	}
	
	public void atualiza(Ideia ideia) {
		manager.getTransaction().begin();
		manager.merge(ideia);
		manager.getTransaction().commit();
	}
	
	public List<Ideia> todos() {
		return manager.createQuery("from Ideia", Ideia.class).getResultList();
	}
	
	@SuppressWarnings("rawtypes")
	public List<Ideia> buscaHistoriaPorUsuario(Long idUsuario) {
		Query query = manager.createNativeQuery("SELECT " 
                                                             + "i.id as id_historia, " 
                                                             + "i.descricao as descricao_historia, " 
                                                             + "i.data_limite, "
                                                             + "i.prioridade, "
                                                             + "u.nome "
                                                      + "FROM ideia i "
                                                             + "LEFT JOIN ideia_equipe ie ON ie.id_ideia = i.id "
                                                             + "LEFT JOIN usuario u ON ie.id_usuario = u.id "
                                                      + "WHERE i.id in (SELECT id_ideia from ideia_equipe where id_usuario = :idUsuario) ");
		query.setParameter("idUsuario", idUsuario);
		return query.getResultList();
	}
	
	@SuppressWarnings("rawtypes")
	public List buscaHistoriaPorUsuarioEmpresa(Long idEmpresa, Long idUsuario) {
		Query query = manager.createNativeQuery("SELECT " 
                                                             + "i.id as id_historia, " 
                                                             + "i.descricao as descricao_historia, " 
                                                             + "i.data_limite, "
                                                             + "i.prioridade, "
                                                             + "u.nome "
                                                      + "FROM ideia i "
                                                             + "LEFT JOIN ideia_equipe ie ON ie.id_ideia = i.id "
                                                             + "LEFT JOIN usuario u ON ie.id_usuario = u.id "
                                                     + "WHERE i.id in (SELECT id_ideia from ideia_equipe where id_empresa = :idEmpresa and id_usuario = :idUsuario)");
		query.setParameter("idEmpresa", idEmpresa);
		query.setParameter("idUsuario", idUsuario);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Ideia> pesquisaPorTitulo(String titulo) {
		Query query = manager.createQuery("from Ideia where titulo = :titulo", Ideia.class);
		query.setParameter("titulo", titulo);
		return query.getResultList();
	} 
}