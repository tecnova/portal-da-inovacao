package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.IdeiaConverter.converterIdeia;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.EmpresaDAO;
import br.com.portal.inovacao.dao.IdeiaDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.AlteraIdeiaDTO;
import br.com.portal.inovacao.dto.EmpresaUsuarioDTO;
import br.com.portal.inovacao.dto.HistoriaUsuarioEmpresaSaidaDTO;
import br.com.portal.inovacao.dto.IdeiaDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.model.Empresa;
import br.com.portal.inovacao.model.Ideia;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.utils.Retorno;
import java.text.SimpleDateFormat;

public class IdeiaFacade {

	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;

	private static final String MSG_IDEIA_SALVA_COM_SUCESSO = "Idéia salva com sucesso.";
	private static final String MSG_TITULO_DEVE_SER_PREENCHIDO = "Título deve ser preenchido.";
	private static final String MSG_IDEIA_ALTERADA_COM_SUCESSO = "Idéia alterada com sucesso.";
	private static final String MSG_NAO_FOI_ENCONTRADA_NENHUMA_IDEIA_PARA_SER_ALTERADA = "Não foi encontrada nenhuma idéia para ser alterada.";
	private static final String MSG_NAO_FOI_ENCONTRADA_NENHUMA_IDEIA_PARA_SER_REMOVIDA = "Não foi encontrada nenhuma idéia para ser removida.";
	private static final String MSG_IDEIA_EXCLUIDA_COM_SUCESSO = "Idéia excluida com sucesso.";
	private static final String MSG_EMPRESA_NAO_ENCONTRADA = "Empresa não encontrada.";

	@Inject
	private IdeiaDAO ideiaDAO;

	@Inject
	private UsuarioDAO usuarioDAO;
	
	@Inject
	private EmpresaDAO empresaDAO;

	public Retorno criaIdeia(IdeiaDTO dto) throws UnsupportedEncodingException {
		
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            
            Empresa empresa = empresaDAO.pesquisaPorId(dto.getEmpresa());

            if (empresa == null)
                    return new Retorno(CODIGO_ERRO, MSG_EMPRESA_NAO_ENCONTRADA);

            if (dto.getTitulo() != null && !(dto.getTitulo().equals("")) && empresa != null) {
                
                try {

                    Ideia ideia = new Ideia();
                    ideia.setTitulo(dto.getTitulo());
                    ideia.setDescricao(dto.getDescricao());
                    ideia.setUsuario(usuarioDAO.pesquisaPorId(dto.getUsuario()));
                    ideia.setAtivo(true);
                    ideia.setDataCriacao(new Date());
                    ideia.setDataLimite(dateFormat.parse(dto.getDataLimite()));
                    ideia.setPrioridade(dto.getPrioridade());
                    ideia.setTipoIdeia(dto.getTipoIdeia());
                    ideia.setIdEmpresa(empresa);
                    ideiaDAO.salva(ideia);
                    return new Retorno(CODIGO_SUCESSO, MSG_IDEIA_SALVA_COM_SUCESSO, converterIdeia(ideia));
                
                }catch (Exception ex) {
                    return new Retorno(1, ex.getMessage());
                }  

            } else
                return new Retorno(CODIGO_ERRO, MSG_TITULO_DEVE_SER_PREENCHIDO);
            
	}
	
	public Retorno alteraIdeia(AlteraIdeiaDTO dto) throws UnsupportedEncodingException {
		Ideia ideia = ideiaDAO.pesquisaPorId(dto.getId());
		
		if (ideia != null) {
			if (dto.getTitulo() != null)
				ideia.setTitulo(dto.getTitulo());
			
			if (dto.getDescricao() != null)
				ideia.setDescricao(dto.getDescricao());
			
			if (dto.getTipoIdeia() != null)
				ideia.setTipoIdeia(dto.getTipoIdeia());

			ideiaDAO.atualiza(ideia);
			return new Retorno(CODIGO_SUCESSO, MSG_IDEIA_ALTERADA_COM_SUCESSO);
		}	
		 else
			return new Retorno(CODIGO_ERRO, MSG_NAO_FOI_ENCONTRADA_NENHUMA_IDEIA_PARA_SER_ALTERADA);
	}
	
	public Retorno inativaIdeia(InformaIdDTO dto) {
		Ideia ideia = ideiaDAO.pesquisaPorId(dto.getId());
		
		if (ideia != null) {
			ideia.setAtivo(false);
			ideiaDAO.atualiza(ideia);
			return new Retorno(CODIGO_SUCESSO, MSG_IDEIA_EXCLUIDA_COM_SUCESSO);
		} else {
			return new Retorno(CODIGO_ERRO, MSG_NAO_FOI_ENCONTRADA_NENHUMA_IDEIA_PARA_SER_REMOVIDA);
		}
	}
	
	public List<IdeiaDTO> listaIdeias() {
		List<Ideia> ideias = ideiaDAO.todos();
		List<IdeiaDTO> ideiasConvertidas = new ArrayList<IdeiaDTO>();
		
		for (Ideia ideia : ideias) {
			ideiasConvertidas.add(converterIdeia(ideia));
		}
		return ideiasConvertidas;
	}

	public Retorno buscaIdeiaPorId(InformaIdDTO dto) {
		return new Retorno(CODIGO_SUCESSO, converterIdeia(ideiaDAO.pesquisaPorId(dto.getId())));
	}
	
	public Retorno buscaIdeiaPorUsuario(EmpresaUsuarioDTO ideiadto) {
		List historiaPorUsuario = ideiaDAO.buscaHistoriaPorUsuario(ideiadto.getIdUsuario());
		List<HistoriaUsuarioEmpresaSaidaDTO> dtos = new ArrayList<HistoriaUsuarioEmpresaSaidaDTO>();
		
		for (int i = 0; i < historiaPorUsuario.size(); i++) {
			Object[] a = (Object[]) historiaPorUsuario.get(i);
			
			HistoriaUsuarioEmpresaSaidaDTO dto = new HistoriaUsuarioEmpresaSaidaDTO();
			for (Object object : a) {
				if (object instanceof BigInteger) {
					Ideia ideia = ideiaDAO.pesquisaPorId(((BigInteger)object).longValue());
					dto.setIdIdeia(ideia.getId());
					dto.setDescricaoIdeia(ideia.getTitulo());
				} else {
					List<Usuario> nomeUsuario = usuarioDAO.pesquisaPorNome((String)object);
						
					if (!nomeUsuario.isEmpty()){
                                                dto.setIdUsuario(nomeUsuario.get(0).getId());
						dto.setUsuario(nomeUsuario.get(0).getNome());
                                        }
				}
			}
			dtos.add(dto);
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
	
	@SuppressWarnings("rawtypes")
	public Retorno listaHistoriaPorUsuarioEmpresa(EmpresaUsuarioDTO empresaUsuarioDTO) {
            
            
            
		List historiaPorUsuarioEmpresa = ideiaDAO.buscaHistoriaPorUsuarioEmpresa(empresaUsuarioDTO.getIdEmpresa(), empresaUsuarioDTO.getIdUsuario());
		List<HistoriaUsuarioEmpresaSaidaDTO> dtos = new ArrayList<HistoriaUsuarioEmpresaSaidaDTO>();
		
		for (int i = 0; i < historiaPorUsuarioEmpresa.size(); i++) {
			Object[] a = (Object[]) historiaPorUsuarioEmpresa.get(i);
			
			HistoriaUsuarioEmpresaSaidaDTO dto = new HistoriaUsuarioEmpresaSaidaDTO();
			for (Object object : a) {
				if (object instanceof BigInteger) {
					Ideia ideia = ideiaDAO.pesquisaPorId(((BigInteger)object).longValue());
					dto.setIdIdeia(ideia.getId());
					dto.setDescricaoIdeia(ideia.getTitulo());
                                        dto.setDataLimite(null);
                                        //dto.setDataLimite(ideia.getDataLimite());
                                        dto.setPrioridade(ideia.getPrioridade());
				} else {
					List<Usuario> nomeUsuario = usuarioDAO.pesquisaPorNome((String)object);
						
					if (!nomeUsuario.isEmpty()){
                                                dto.setIdUsuario(nomeUsuario.get(0).getId());
						dto.setUsuario(nomeUsuario.get(0).getNome());
                                        }
				}
			}
			dtos.add(dto);
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
}