package br.com.portal.inovacao.service;

import br.com.portal.inovacao.dto.DocumentoDTO;
import br.com.portal.inovacao.utils.ConectaBanco;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.utils.Retorno;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Path("documento")
public class DocumentoService {
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("salvar")
	public Retorno grava(DocumentoDTO dto) {

            try {

                ConectaBanco _Banco = new ConectaBanco();
                
                if ((dto.getDocumento() != null) && (dto.getTpDocumento() != null))
                {
                    
                    String _SQL;
                    _SQL = "";
                    _SQL =  _SQL + "Select ";
                    _SQL =  _SQL + "     * ";
                    _SQL =  _SQL + "From  ";
                    _SQL =  _SQL + "     documentoitem ";
                    _SQL =  _SQL + "where  ";
                    _SQL =  _SQL + "     tpDocumento = '" + dto.getTpDocumento() + "' ";

                    if (dto.getIdHistoria() != null ){
                        _SQL =  _SQL + " and id_historia = " + dto.getIdHistoria() + " ";
                    }else if (dto.getIdEmpresa() != null){
                        _SQL =  _SQL + " and id_empresa = " + dto.getIdEmpresa() + " ";
                    }else {
                        return new Retorno(1, "Parametros Inválidos !!");
                    } 
                    
                    ResultSet rs;
            
                    rs = _Banco.Consulta(_SQL);
                                
                    if (rs.next()){

                        _SQL = "";
                        _SQL = _SQL + "Update ";
                        _SQL = _SQL + "    documentoitem ";
                        _SQL = _SQL + "Set "; 
                        _SQL = _SQL + "    Documento = '" + dto.getDocumento() + "' ";
                        _SQL = _SQL + "Where ";
                        _SQL = _SQL + "    id = " + rs.getInt("id") + " ";

                        Retorno _Retorno;

                        _Retorno = _Banco.Commando(_SQL);

                        if (_Retorno.getErro() == 0){
                            return new Retorno(0, "Documento Alterado com Sucesso");
                        }else{
                            return new Retorno(1, _Retorno.getMensagem());
                        }
                        
                    }else{
                        
                        _SQL = "";
                        _SQL = _SQL + "Insert Into ";
                        _SQL = _SQL + " documentoitem ";
                        _SQL = _SQL + "   ( ";
                        _SQL = _SQL + "     tpDocumento ";
                        _SQL = _SQL + "    ,Documento  ";
                        _SQL = _SQL + "    ,id_Empresa  ";
                        _SQL = _SQL + "    ,id_Historia ";
                        _SQL = _SQL + "   ) ";
                        _SQL = _SQL + " Values ";
                        _SQL = _SQL + "   ( ";
                        _SQL = _SQL + "     '" + dto.getTpDocumento() + "' ";
                        _SQL = _SQL + "    ,'" + dto.getDocumento() + "' ";
                        _SQL = _SQL + "    , " + dto.getIdEmpresa() + " ";
                        _SQL = _SQL + "    , " + dto.getIdHistoria() + " ";
                        _SQL = _SQL + "   ) ";

                        Retorno _Retorno;

                        _Retorno = _Banco.Commando(_SQL);

                        if (_Retorno.getErro() == 0){
                            return new Retorno(0, "Documento Salvo com Sucesso");
                        }else{
                            return new Retorno(1, _Retorno.getMensagem());
                        }                        
                    }

                }
                else{
                    return new Retorno(1, "Falta Informações para Salvar o Documento");
                }
            
            } catch (Exception ex) {
                return new Retorno(1, ex.getMessage());
            }            
	}

        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("alterar")
	public Retorno alterar(DocumentoDTO dto) {

            try {

                ConectaBanco _Banco = new ConectaBanco();
                
                if ((dto.getDocumento() != null) || (dto.getTpDocumento() != null))
                {
                    
                    String _SQL;
                    _SQL = "";
                    _SQL = _SQL + "Update ";
                    _SQL = _SQL + "    documentoitem ";
                    _SQL = _SQL + "Set "; 
                    _SQL = _SQL + "    Documento = '" + dto.getDocumento() + "' ";
                    _SQL = _SQL + "Where ";
                    _SQL = _SQL + "    id = " + dto.getId() + " ";

                    Retorno _Retorno;
                    
                    _Retorno = _Banco.Commando(_SQL);
                    
                    if (_Retorno.getErro() == 0){
                        return new Retorno(0, "Documento Alterado com Sucesso");
                    }else{
                        return new Retorno(1, _Retorno.getMensagem());
                    }

                }
                else{
                    return new Retorno(1, "Falta Informações para Gravar o Documento");
                }
            
            } catch (Exception ex) {
                return new Retorno(1, ex.getMessage());
            }            
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista")
	public Retorno Pesquisa(DocumentoDTO dto) {

            String _SQL;
            _SQL = "";
            _SQL =  _SQL + "Select ";
            _SQL =  _SQL + "     * ";
            _SQL =  _SQL + "From  ";
            _SQL =  _SQL + "     documentoitem ";
            _SQL =  _SQL + "where  ";
            _SQL =  _SQL + "     tpDocumento = '" + dto.getTpDocumento() + "' ";
            
            if (dto.getIdHistoria() != null ){
                _SQL =  _SQL + " and id_historia = " + dto.getIdHistoria() + " ";
            }else if (dto.getIdEmpresa() != null){
                _SQL =  _SQL + " and id_empresa = " + dto.getIdEmpresa() + " ";
            } 
            ResultSet rs;

            ConectaBanco _Banco = new ConectaBanco();

            rs = _Banco.Consulta(_SQL);
            
            List<DocumentoDTO> dtos = new ArrayList<>();

            try{
                while (rs.next()) {
                    DocumentoDTO item = new DocumentoDTO();
                    item.setId(rs.getLong("id"));
                    item.setTpDocumento(dto.getTpDocumento());
                    item.setDocumento(rs.getString("Documento"));
                    item.setIdHistoria(rs.getLong("id_Historia"));
                    item.setIdEmpresa(rs.getLong("id_Empresa"));
                    dtos.add(item);
                }
            }catch(Exception ex){
                return new Retorno(1, ex.getMessage());
            }

            return new Retorno(0, dtos);

	}
        
        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("teste")
	public Retorno teste() {
		//return facade.criaCanvas(dto);
                return new Retorno(0, "Retornei Alguma Coisa");
	}

	
}
