package br.com.portal.inovacao.facade;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.converter.GrupoBrainstormingConverter;
import br.com.portal.inovacao.dao.GrupoBrainstormingDAO;
import br.com.portal.inovacao.dao.IdeiaDAO;
import br.com.portal.inovacao.dto.GrupoBrainstormingDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.model.GrupoBrainstorming;
import br.com.portal.inovacao.utils.Retorno;

public class GrupoBrainstormingFacade {

	@Inject
	private GrupoBrainstormingDAO grupoBrainstormingDAO;
	
	@Inject
	private IdeiaDAO ideiaDAO;
	
	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;
	
	private static final String MSG_NOME_DEVE_SER_PREENCHIDO = "Nome deve ser preenchido.";
	private static final String MSG_GRUPO_SALVO_COM_SUCESSO = "Grupo salvo com sucesso!";
	private static final String MSG_GRUPO_EXCLUIDO_COM_SUCESSO = "Grupo excluido com sucesso!";
	private static final String MSG_GRUPO_ALTERADO_COM_SUCESSO = "Grupo alterado com sucesso!";

	private GrupoBrainstorming criaGrupoBrainstorming(GrupoBrainstormingDTO grupoBrainstormingDTO) {
		GrupoBrainstorming grupoBrainstorming = new GrupoBrainstorming();
		grupoBrainstorming.setDescricao(grupoBrainstormingDTO.getDescricao());
		grupoBrainstorming.setIdHistoria(ideiaDAO.pesquisaPorId(grupoBrainstormingDTO.getIdHistoria()));
		return grupoBrainstorming;
	}
	
	private GrupoBrainstorming alteraGrupoBrainstorming(GrupoBrainstormingDTO grupoBrainstormingDTO) {
		GrupoBrainstorming grupoBrainstorming = new GrupoBrainstorming();
		grupoBrainstorming.setId(grupoBrainstormingDTO.getId());
		grupoBrainstorming.setDescricao(grupoBrainstormingDTO.getDescricao());
		grupoBrainstorming.setIdHistoria(ideiaDAO.pesquisaPorId(grupoBrainstormingDTO.getIdHistoria()));
		return grupoBrainstorming;
	}
	private GrupoBrainstorming excluiGrupoBrainstorming(GrupoBrainstormingDTO grupoBrainstormingDTO) {
		GrupoBrainstorming grupoBrainstorming = new GrupoBrainstorming();
		grupoBrainstorming.setId(grupoBrainstormingDTO.getId());
		return grupoBrainstorming;
	}
	
	private void validaDadosDeEntrada(GrupoBrainstormingDTO dto) throws Exception {
		if (dto.getDescricao() == null || dto.getDescricao().trim().equals(""))
			throw new Exception(MSG_NOME_DEVE_SER_PREENCHIDO);
	}
	
	public Retorno criaGrupo(GrupoBrainstormingDTO dto) {
		try {
			validaDadosDeEntrada(dto);
			GrupoBrainstorming grupoBrainstorming = criaGrupoBrainstorming(dto);
			
			grupoBrainstormingDAO.salvar(grupoBrainstorming);
			return new Retorno(CODIGO_SUCESSO, MSG_GRUPO_SALVO_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	
	public Retorno alteraGrupo(GrupoBrainstormingDTO dto) {
		try {
			validaDadosDeEntrada(dto);
			GrupoBrainstorming grupoBrainstorming = alteraGrupoBrainstorming(dto);
			
			grupoBrainstormingDAO.alterar(grupoBrainstorming);
			return new Retorno(CODIGO_SUCESSO, MSG_GRUPO_ALTERADO_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	
	public Retorno excluiGrupo(GrupoBrainstormingDTO dto) {
		try {
			GrupoBrainstorming grupoBrainstorming = excluiGrupoBrainstorming(dto);
			
			grupoBrainstormingDAO.excluir(grupoBrainstorming);
			return new Retorno(CODIGO_SUCESSO, MSG_GRUPO_EXCLUIDO_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	public Retorno listaGrupoBrainstormingPorHistoria(InformaIdDTO idHistoria) {
		List<GrupoBrainstorming> grupoBrainstormingPorHistoria = grupoBrainstormingDAO.listaGrupoBrainstormingPorHistoria(idHistoria.getId());
		List<GrupoBrainstormingDTO> dtos = new ArrayList<GrupoBrainstormingDTO>();
		
		for (GrupoBrainstorming brainstorming : grupoBrainstormingPorHistoria) {
			dtos.add(GrupoBrainstormingConverter.converteGrupoBrainstorming(brainstorming));
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
}
