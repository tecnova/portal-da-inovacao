package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.List;

public class CriaUsuarioSaida implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String email;
	private String senha;
	private String id_face;
	private String id_google;
	private String perfil_tema;
	private String data;
	private List<PerfilDTO> perfis;
	
	public CriaUsuarioSaida() {}
	
	public CriaUsuarioSaida(Long id, String nome, String email, String senha, String id_face, String id_google, String perfil_tema, String data, List<PerfilDTO> perfis) {
            this.id = id;
            this.nome = nome;
            this.email = email;
            this.senha = senha;
            this.id_face = id_face;
            this.id_google = id_google;
            this.perfil_tema = perfil_tema;
            this.data = data;
            this.perfis = perfis;
	}
	
	public List<PerfilDTO> getPerfis() {
            return perfis;
	}

	public void setPerfis(List<PerfilDTO> perfis) {
		this.perfis = perfis;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getEmail() {
		return email;
	}

	public String getSenha() {
		return senha;
	}

	public String getId_face() {
		return id_face;
	}

	public String getId_google() {
		return id_google;
	}

	public String getPerfil_tema() {
		return perfil_tema;
	}

	public String getData() {
		return data;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setId_face(String id_face) {
		this.id_face = id_face;
	}

	public void setId_google(String id_google) {
		this.id_google = id_google;
	}

	public void setPerfil_tema(String perfil_tema) {
		this.perfil_tema = perfil_tema;
	}

	public void setData(String data) {
		this.data = data;
	}
}
