package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.AtividadeUsuarioDTO;
import br.com.portal.inovacao.dto.RankingDTO;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;

import java.sql.*;
import java.util.ArrayList;

@Path("gamificacao")
public class GamificacaoService {
	
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("empresa")
    public Retorno ListaGamificacaoEmpresaUsuario(AtividadeUsuarioDTO atividade) throws SQLException  {
        
        Retorno _Retorno = new Retorno();
        
        int i = 0 ;
        String _SQL;
        ResultSet _dto;
        List<RankingDTO> _lstRanking = new ArrayList<>();
        
        _SQL = "";
        _SQL = _SQL + "Select ";
        _SQL = _SQL + "    aUsu.idUsuario ";
        _SQL = _SQL + "   ,usu.Nome ";
        _SQL = _SQL + "   ,sum(aUsu.Pontuacao) as Pontuacao ";
        _SQL = _SQL + "From ";
        _SQL = _SQL + "    atividade_usuario aUsu ";
        _SQL = _SQL + "   ,usuario usu ";
        _SQL = _SQL +"Where ";
        _SQL = _SQL +"     aUsu.idUsuario = usu.id ";
        _SQL = _SQL +" and aUsu.idEmpresa = " + atividade.getIdEmpresa() + " ";
        _SQL = _SQL +"Group By ";
        _SQL = _SQL +"     aUsu.idUsuario ";
        _SQL = _SQL +"    ,usu.Nome ";
        _SQL = _SQL +"Order By Pontuacao DESC ";
        
        ConectaBanco _Banco = new ConectaBanco();
        
        _dto =_Banco.Consulta(_SQL);
        
        while (_dto.next()){
            
            try {
                
                i++;
                
                RankingDTO _Ranking = new RankingDTO();
                _Ranking.setPosicao(i);
                _Ranking.setId(_dto.getInt("idUsuario"));
                _Ranking.setNome(_dto.getString("nome"));
                _Ranking.setValor(_dto.getInt("Pontuacao"));
                _lstRanking.add(_Ranking);
                
            }catch (Exception e) {
                
                _Retorno.setErro(1);
                _Retorno.setMensagem("N�o foi poss�vel trazer o ranking");
                _Retorno.setObjeto(null);
            }
        }
        
        _Retorno.setErro(0);
        _Retorno.setMensagem("Pesquisa Realizada com Sucesso.");
        _Retorno.setObjeto(_lstRanking);
        
        return _Retorno;
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("historia")
    public Retorno ListaGamificacaoProjetoUsuario(AtividadeUsuarioDTO atividade) throws SQLException  {

        Retorno _Retorno = new Retorno();
        
        int i = 0 ;
        String _SQL;
        ResultSet _dto;
        List<RankingDTO> _lstRanking = new ArrayList<>();
        
        _SQL = "";
        _SQL = _SQL + "Select ";
        _SQL = _SQL + "    aUsu.idUsuario ";
        _SQL = _SQL + "   ,usu.Nome ";
        _SQL = _SQL + "   ,sum(aUsu.Pontuacao) as Pontuacao ";
        _SQL = _SQL + "From ";
        _SQL = _SQL + "    atividade_usuario aUsu ";
        _SQL = _SQL + "   ,usuario usu ";
        _SQL = _SQL +"Where ";
        _SQL = _SQL +"     aUsu.idUsuario = usu.id ";
        _SQL = _SQL +" and aUsu.idHistoria = " + atividade.getIdHistoria() + " ";
        _SQL = _SQL +"Group By ";
        _SQL = _SQL +"     aUsu.idUsuario ";
        _SQL = _SQL +"    ,usu.Nome ";
        _SQL = _SQL +"Order By Pontuacao DESC ";
        
        ConectaBanco _Banco = new ConectaBanco();
        
        _dto =_Banco.Consulta(_SQL);
        
        while (_dto.next()){
            
            try {
                
                i++;
                
                RankingDTO _Ranking = new RankingDTO();
                _Ranking.setPosicao(i);
                _Ranking.setId(_dto.getInt("idUsuario"));
                _Ranking.setNome(_dto.getString("nome"));
                _Ranking.setValor(_dto.getInt("Pontuacao"));
                _lstRanking.add(_Ranking);
                
            }catch (Exception e) {
                _Retorno.setErro(1);
                _Retorno.setMensagem("Não foi possível trazer o ranking");
                _Retorno.setObjeto(null);                
            }
        }
        
        _Retorno.setErro(0);
        _Retorno.setMensagem("Pesquisa Realizada com Sucesso.");
        _Retorno.setObjeto(_lstRanking);
        
        return _Retorno;
        
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("portal/usuario")
    public Retorno ListaGamificacaoPortalUsuario(AtividadeUsuarioDTO atividade) throws SQLException  {
        
        Retorno _Retorno = new Retorno();
        
        int i = 0 ;
        String _SQL;
        ResultSet _dto;
        List<RankingDTO> _lstRanking = new ArrayList<>();
        
        _SQL = "";
        _SQL = _SQL + "Select ";
        _SQL = _SQL + "    aUsu.idUsuario ";
        _SQL = _SQL + "   ,usu.Nome ";
        _SQL = _SQL + "   ,sum(aUsu.Pontuacao) as Pontuacao ";
        _SQL = _SQL + "From ";
        _SQL = _SQL + "    atividade_usuario aUsu ";
        _SQL = _SQL + "   ,usuario usu ";
        _SQL = _SQL +"Where ";
        _SQL = _SQL +"     aUsu.idUsuario = usu.id ";
        _SQL = _SQL +"Group By ";
        _SQL = _SQL +"     aUsu.idUsuario ";
        _SQL = _SQL +"    ,usu.Nome ";
        _SQL = _SQL +"Order By Pontuacao DESC ";
        
        ConectaBanco _Banco = new ConectaBanco();
        
        _dto =_Banco.Consulta(_SQL);
        
        while (_dto.next()){
            
            try {
                
                i++;
                
                RankingDTO _Ranking = new RankingDTO();
                _Ranking.setPosicao(i);
                _Ranking.setId(_dto.getInt("idUsuario"));
                _Ranking.setNome(_dto.getString("nome"));
                _Ranking.setValor(_dto.getInt("Pontuacao"));
                _lstRanking.add(_Ranking);
                
            }catch (Exception e) {
                _Retorno.setErro(1);
                _Retorno.setMensagem("Não foi possível trazer o ranking");
                _Retorno.setObjeto(null);                
            }
        }
        
        _Retorno.setErro(0);
        _Retorno.setMensagem("Pesquisa Realizada com Sucesso.");
        _Retorno.setObjeto(_lstRanking);
        
        return _Retorno;

    }
	
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("portal/empresa")
    public Retorno ListaGamificacaoPortalEmpresa(AtividadeUsuarioDTO atividade) throws SQLException  {

        Retorno _Retorno = new Retorno();
        
        int i = 0 ;
        String _SQL;
        ResultSet _dto;
        List<RankingDTO> _lstRanking = new ArrayList<>();
        
        _SQL = "";
        _SQL = _SQL + "Select ";
        _SQL = _SQL + "    aUsu.idEmpresa ";
        _SQL = _SQL + "   ,emp.razao_social ";
        _SQL = _SQL + "   ,sum(aUsu.Pontuacao) as Pontuacao ";
        _SQL = _SQL + "From ";
        _SQL = _SQL + "    atividade_usuario aUsu ";
        _SQL = _SQL + "   ,empresa emp ";
        _SQL = _SQL +"Where ";
        _SQL = _SQL +"     aUsu.idEmpresa = emp.id ";
        _SQL = _SQL +"Group By ";
        _SQL = _SQL +"     aUsu.idEmpresa ";
        _SQL = _SQL +"    ,emp.razao_social ";
        _SQL = _SQL +"Order By Pontuacao DESC ";
        
        ConectaBanco _Banco = new ConectaBanco();
        
        _dto =_Banco.Consulta(_SQL);
        
        while (_dto.next()){
            
            try {
                
                i++;
                
                RankingDTO _Ranking = new RankingDTO();
                _Ranking.setPosicao(i);
                _Ranking.setId(_dto.getInt("idEmpresa"));
                _Ranking.setNome(_dto.getString("razao_social"));
                _Ranking.setValor(_dto.getInt("Pontuacao"));
                _lstRanking.add(_Ranking);
                
            }catch (Exception e) {
                _Retorno.setErro(1);
                _Retorno.setMensagem("Não foi possível trazer o ranking");
                _Retorno.setObjeto(null);                   
            }
        }
        
        _Retorno.setErro(0);
        _Retorno.setMensagem("Pesquisa Realizada com Sucesso.");
        _Retorno.setObjeto(_lstRanking);
        
        return _Retorno; 
    
    }

}
