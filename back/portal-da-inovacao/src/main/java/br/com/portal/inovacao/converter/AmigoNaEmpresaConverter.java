package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.AmigoEmpresaDTO;
import br.com.portal.inovacao.model.EmpresaEquipe;
import br.com.portal.inovacao.model.Usuario;

public class AmigoNaEmpresaConverter {
	
	public static AmigoEmpresaDTO converteAmigoEmpresa(Usuario usuario, EmpresaEquipe empresaEquipe) {
		AmigoEmpresaDTO dto = new AmigoEmpresaDTO();
		dto.setIdUsuario(usuario.getId());
		dto.setNome(usuario.getNome());
		dto.setEmail(usuario.getEmail());
		dto.setPerfil(empresaEquipe.getPerfilEmpresa().getNome());
		return dto;
	}
}
