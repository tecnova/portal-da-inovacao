package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.IdeiaEquipeConverter.converterUsuarioIdeiaEquipe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.converter.IdeiaConverter;
import br.com.portal.inovacao.dao.IdeiaDAO;
import br.com.portal.inovacao.dao.IdeiaEquipeDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.IdeiaDTO;
import br.com.portal.inovacao.dto.IdeiaEquipeDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.UsuarioDTO;
import br.com.portal.inovacao.model.Ideia;
import br.com.portal.inovacao.model.IdeiaEquipe;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.utils.Retorno;

public class IdeiaEquipeFacade {

	@Inject
	private	IdeiaEquipeDAO ideiaEquipeDAO;
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	@Inject
	private IdeiaDAO ideiaDAO;
	
	private static int CODIGO_SUCESSO = 0;
	private static int CODIGO_ERRO = 1;
	
	private static final String MSG_ID_IDEIA_OBRIGATORIO = "Id da idéia é obrigatório.";
	private static final String MSG_ID_USUARIO_OBRIGATORIO = "Id do usuário é obrigatório.";
	private static final String MSG_USUARIO_JA_ADICIONADO = "Usuário já adicionado.";
	
	public Retorno adicionaUsuarioNaIdeiaEquipe(IdeiaEquipeDTO dto) {
		try {
			validaDadosDeEntrada(dto);
			validaSeUsuarioJaEstaInseridoNaIdeia(dto);
			IdeiaEquipe ideiaEquipe = criaIdeiaEquipe(dto);

			ideiaEquipeDAO.salvar(ideiaEquipe);
			return new Retorno(CODIGO_SUCESSO, ideiaEquipe.getUsuario().getNome() + " adicionado com sucesso!", converterUsuarioIdeiaEquipe(ideiaEquipe));
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}

	private void validaSeUsuarioJaEstaInseridoNaIdeia(IdeiaEquipeDTO dto) throws Exception {
		if (quantidadeDoMesmoUsuarioNaIdeia(dto) >= 1)
			throw new Exception(MSG_USUARIO_JA_ADICIONADO);
	}

	private void validaDadosDeEntrada(IdeiaEquipeDTO dto) throws Exception {
		if (dto.getIdIdeia() == null || dto.getIdIdeia().equals(""))
			throw new Exception(MSG_ID_IDEIA_OBRIGATORIO);
		
		if (dto.getIdUsuario() == null || dto.getIdUsuario().equals(""))
			throw new Exception(MSG_ID_USUARIO_OBRIGATORIO);
	}

	private IdeiaEquipe criaIdeiaEquipe(IdeiaEquipeDTO dto) {
		Usuario usuario = usuarioDAO.pesquisaPorId(dto.getIdUsuario());
		Ideia ideia = ideiaDAO.pesquisaPorId(dto.getIdIdeia());
		
		IdeiaEquipe ideiaEquipe = new IdeiaEquipe();
		ideiaEquipe.setUsuario(usuario);
		ideiaEquipe.setIdeia(ideia);
		return ideiaEquipe;
	}
	
	private int quantidadeDoMesmoUsuarioNaIdeia(IdeiaEquipeDTO dto) {
		return ideiaEquipeDAO.quantidadeDoMesmoUsuarioNaIdeia(dto.getIdIdeia(), dto.getIdUsuario());
	}
	
	public Retorno excluiUsuarioDaIdeia(IdeiaEquipeDTO dto) {
		try {
			validaDadosDeEntrada(dto);
			ideiaEquipeDAO.excluir(dto);
			return new Retorno(CODIGO_SUCESSO, "Usu�rio removido com sucesso!");
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	
	public Retorno buscaUsuariosDaIdeia(InformaIdDTO dto) {
		List<IdeiaEquipe> usuariosDaIdeia = ideiaEquipeDAO.buscaUsuariosDaIdeia(dto.getId());
		List<UsuarioDTO> dtos = new ArrayList<UsuarioDTO>();
		
		for (IdeiaEquipe ideiaEquipe: usuariosDaIdeia) {
			dtos.add(converterUsuarioIdeiaEquipe(ideiaEquipe));
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}

	public Retorno buscaIdeiasDoUsuario(InformaIdDTO dto) {
		List<IdeiaEquipe> ideiasDoUsuario = ideiaEquipeDAO.buscaIdeiasDoUsuario(dto.getId());
		List<IdeiaDTO> dtos = new ArrayList<IdeiaDTO>();
		
		for (IdeiaEquipe ideiaDoUsuario : ideiasDoUsuario) {
			Ideia ideia = ideiaDAO.pesquisaPorId(ideiaDoUsuario.getIdeia().getId());
			dtos.add(IdeiaConverter.converterIdeia(ideia));
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
}
