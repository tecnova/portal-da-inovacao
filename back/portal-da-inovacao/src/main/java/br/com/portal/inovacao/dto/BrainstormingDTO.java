package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.List;

public class BrainstormingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long idUsuario;
    private Long idHistoria;
    private String descricao;
    private String dataCriacao;
    private Long idGrupoBrainstorming;
    private String boCurtir;
    private Long qtCurtir;
    private List<UsuarioDTO> lstUsuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdHistoria() {
        return idHistoria;
    }

    public void setIdHistoria(Long idHistoria) {
        this.idHistoria = idHistoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getIdGrupoBrainstorming() {
        //return (long)1;
        return idGrupoBrainstorming;
    }

    public void setIdGrupoBrainstorming(Long idGrupoBrainstorming) {
        this.idGrupoBrainstorming = idGrupoBrainstorming;
    }

    public String getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public List<UsuarioDTO> getLstUsuario() {
        return lstUsuario;
    }

    public void setLstUsuario(List<UsuarioDTO> lstUsuario) {
        this.lstUsuario = lstUsuario;
    }

    public String getBoCurtir() {
        return boCurtir;
    }

    public void setBoCurtir(String boCurtir) {
        this.boCurtir = boCurtir;
    }

    public Long getQtCurtir() {
        return qtCurtir;
    }

    public void setQtCurtir(Long qtCurtir) {
        this.qtCurtir = qtCurtir;
    }

    
        
}
