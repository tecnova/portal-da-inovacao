package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.GrupoBrainstormingDTO;
import br.com.portal.inovacao.model.GrupoBrainstorming;

public class GrupoBrainstormingConverter {
	
	public static GrupoBrainstormingDTO converteGrupoBrainstorming(GrupoBrainstorming grupoBrainstorming) {
		GrupoBrainstormingDTO dto = new GrupoBrainstormingDTO();
		dto.setId(grupoBrainstorming.getId());
		dto.setDescricao(grupoBrainstorming.getDescricao());
		dto.setIdHistoria(grupoBrainstorming.getIdHistoria().getId());
		return dto;
	}
}
