package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.Date;

public class SwotDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long idUsuario;
    private Long idEmpresa;
    private String titulo;
    private String descricao;
    private String dtInicio;
    private String dtPrevisto;
    private String dtTermino;
    private Long ptMinimo;
    private Long ptMaximo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDtInicio() {
        return dtInicio;
    }

    public void setDtInicio(String dtInicio) {
        this.dtInicio = dtInicio;
    }

    public String getDtPrevisto() {
        return dtPrevisto;
    }

    public void setDtPrevisto(String dtPrevisto) {
        this.dtPrevisto = dtPrevisto;
    }

    public String getDtTermino() {
        return dtTermino;
    }

    public void setDtTermino(String dtTermino) {
        this.dtTermino = dtTermino;
    }

    public Long getPtMinimo() {
        return ptMinimo;
    }

    public void setPtMinimo(Long ptMinimo) {
        this.ptMinimo = ptMinimo;
    }

    public Long getPtMaximo() {
        return ptMaximo;
    }

    public void setPtMaximo(Long ptMaximo) {
        this.ptMaximo = ptMaximo;
    }

    
}