package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class SwotItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long idSwot;
    private String tpSwot;
    private String descricao;
    private Long idUsuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdSwot() {
        return idSwot;
    }

    public void setIdSwot(Long idSwot) {
        this.idSwot = idSwot;
    }

    public String getTpSwot() {
        return tpSwot;
    }

    public void setTpSwot(String tpSwot) {
        this.tpSwot = tpSwot;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }


    
}