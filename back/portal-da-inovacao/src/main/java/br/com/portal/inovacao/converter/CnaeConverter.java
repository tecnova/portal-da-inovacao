package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.CnaeDTO;
import br.com.portal.inovacao.model.Cnae;

public class CnaeConverter {
	
	public static CnaeDTO converterCnae(Cnae cnae) {
		CnaeDTO cnaeDTO = new CnaeDTO();
		cnaeDTO.setId(cnae.getId());
		cnaeDTO.setCodigo(cnae.getCodigo());
		cnaeDTO.setNome(cnae.getNome());
		return cnaeDTO;
	}
}
