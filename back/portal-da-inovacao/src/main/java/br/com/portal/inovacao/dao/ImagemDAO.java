package br.com.portal.inovacao.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.portal.inovacao.model.Imagem;

public class ImagemDAO {
	
	@Inject
	private EntityManager manager;
	
	public ImagemDAO() {}
	
	public void salva(Imagem imagem) {
		manager.getTransaction().begin();
		manager.persist(imagem);
		manager.getTransaction().commit();
	}
	
	public Imagem pesquisaImagemPorId(Long id) {
		return manager.find(Imagem.class, id);
	}
	
	public void flush(Imagem imagem) {
		manager.getTransaction().begin();
		manager.persist(imagem);
		manager.flush();
	}
}
