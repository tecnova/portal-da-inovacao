package br.com.portal.inovacao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario_amigo")
public class UsuarioAmigo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	@OneToOne
	@JoinColumn(name = "id_amigo")
	private Usuario amigo;

	@Column(name = "data_solicitacao")
	private Date dataSolicitacao;

	@Column(name = "data_aprovacao")
	private Date dataAprovacao;

	@Column(name = "data_recusa")
	private Date dataRecusa;

	@Column(name = "solicitacao_aprovada")
	private boolean aprovado;

	@Column(name = "solicitacao_recusada")
	private boolean recusado;

	public Long getId() {
		return id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public Usuario getAmigo() {
		return amigo;
	}

	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}

	public Date getDataAprovacao() {
		return dataAprovacao;
	}

	public Date getDataRecusa() {
		return dataRecusa;
	}

	public boolean isAprovado() {
		return aprovado;
	}

	public boolean isRecusado() {
		return recusado;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setAmigo(Usuario amigo) {
		this.amigo = amigo;
	}

	public void setDataSolicitacao(Date dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	public void setDataAprovacao(Date dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}

	public void setDataRecusa(Date dataRecusa) {
		this.dataRecusa = dataRecusa;
	}

	public void setAprovado(boolean aprovado) {
		this.aprovado = aprovado;
	}

	public void setRecusado(boolean recusado) {
		this.recusado = recusado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amigo == null) ? 0 : amigo.hashCode());
		result = prime * result + (aprovado ? 1231 : 1237);
		result = prime * result	+ ((dataAprovacao == null) ? 0 : dataAprovacao.hashCode());
		result = prime * result	+ ((dataRecusa == null) ? 0 : dataRecusa.hashCode());
		result = prime * result	+ ((dataSolicitacao == null) ? 0 : dataSolicitacao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (recusado ? 1231 : 1237);
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioAmigo other = (UsuarioAmigo) obj;
		if (amigo == null) {
			if (other.amigo != null)
				return false;
		} else if (!amigo.equals(other.amigo))
			return false;
		if (aprovado != other.aprovado)
			return false;
		if (dataAprovacao == null) {
			if (other.dataAprovacao != null)
				return false;
		} else if (!dataAprovacao.equals(other.dataAprovacao))
			return false;
		if (dataRecusa == null) {
			if (other.dataRecusa != null)
				return false;
		} else if (!dataRecusa.equals(other.dataRecusa))
			return false;
		if (dataSolicitacao == null) {
			if (other.dataSolicitacao != null)
				return false;
		} else if (!dataSolicitacao.equals(other.dataSolicitacao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (recusado != other.recusado)
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UsuarioAmigo [id=" + id + ", id_Usuario=" + usuario
				+ ", id_Amigo=" + amigo + ", dataSolicitacao="
				+ dataSolicitacao + ", dataAprovacao=" + dataAprovacao
				+ ", dataRecusa=" + dataRecusa + ", aprovado=" + aprovado
				+ ", recusado=" + recusado + "]";
	}
}
