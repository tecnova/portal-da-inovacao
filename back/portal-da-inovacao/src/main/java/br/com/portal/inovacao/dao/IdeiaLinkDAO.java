package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.IdeiaLink;

public class IdeiaLinkDAO {
	
	@Inject
	private EntityManager manager;
	
	public IdeiaLinkDAO() {}
	
	public void salva(IdeiaLink ideiaLink) {
		manager.getTransaction().begin();
		manager.persist(ideiaLink);
		manager.getTransaction().commit();
	}
	
	public void atualiza(IdeiaLink ideiaLink) {
		manager.getTransaction().begin();
		manager.merge(ideiaLink);
		manager.getTransaction().commit();
	}
	
	public void remove(IdeiaLink ideiaLink) {
		manager.getTransaction().begin();
		manager.remove(ideiaLink);
		manager.getTransaction().commit();
	}
	
	public IdeiaLink pesquisaPorId(Long id) {
		return manager.find(IdeiaLink.class, id);
	}
	
	public List<IdeiaLink> todos() {
		return manager.createQuery("FROM IdeiaLink", IdeiaLink.class).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<IdeiaLink> pesquisaLinksPorIdeia(Long idIdeia) {
		Query query = manager.createQuery("FROM IdeiaLink where ideia_id = :idIdeia");
		query.setParameter("idIdeia", idIdeia);
		return query.getResultList();
	}
}
