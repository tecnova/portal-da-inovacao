package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import br.com.portal.inovacao.model.Usuario;

public class SolicitacaoAmigoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Usuario usuario;
	private Usuario amigo;
	private Date dataSolicitacao;
	private Date dataAprovacao;
	private Date dataRecusa;
	private boolean aprovado;
	private boolean recusado;
	private List<SolicitacaoAmigoDTO> solicitacoes;
	
	public List<SolicitacaoAmigoDTO> getSolicitacoes() {
		return solicitacoes;
	}

	public void setSolicitacoes(List<SolicitacaoAmigoDTO> solicitacoes) {
		this.solicitacoes = solicitacoes;
	}

	public SolicitacaoAmigoDTO() {
	}

	public SolicitacaoAmigoDTO(Long id, Usuario usuario, Usuario amigo, Date dataSolicitacao, Date dataAprovacao, Date dataRecusa, boolean resposta, boolean recusado) {
		this.id = id;
		this.usuario = usuario;
		this.amigo = amigo;
		this.dataSolicitacao = dataSolicitacao;
		this.dataAprovacao = dataAprovacao;
		this.dataRecusa = dataRecusa;
		this.aprovado = resposta;
		this.recusado = recusado;
	}

	public Long getId() {
		return id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public Usuario getAmigo() {
		return amigo;
	}

	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}

	public Date getDataAprovacao() {
		return dataAprovacao;
	}

	public Date getDataRecusa() {
		return dataRecusa;
	}

	public boolean isAprovado() {
		return aprovado;
	}

	public boolean isRecusado() {
		return recusado;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setAmigo(Usuario amigo) {
		this.amigo = amigo;
	}

	public void setDataSolicitacao(Date dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	public void setDataAprovacao(Date dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}

	public void setDataRecusa(Date dataRecusa) {
		this.dataRecusa = dataRecusa;
	}

	public void setAprovado(boolean aprovado) {
		this.aprovado = aprovado;
	}

	public void setRecusado(boolean recusado) {
		this.recusado = recusado;
	}
}
