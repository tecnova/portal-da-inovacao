package br.com.portal.inovacao.converter;

import java.util.ArrayList;
import java.util.List;

import br.com.portal.inovacao.dto.UsuarioDTO;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.model.UsuarioAmigo;

public class UsuarioAmigoConverter {
	
	public static UsuarioDTO converterAmigo(Usuario amigo) {
		UsuarioDTO saida = new UsuarioDTO();
		saida.setId(amigo.getId().toString());
		saida.setNome(amigo.getNome());
		saida.setEmail(amigo.getEmail());
		return saida;
	}
	
	public static List<UsuarioDTO> converterSolicitacoesPendentesAmigo(List<UsuarioAmigo> usuariosAmigos) {
		List<UsuarioDTO> amigos = new ArrayList<UsuarioDTO>();
		
		for (UsuarioAmigo usuarioAmigo : usuariosAmigos) {
			UsuarioDTO saida = new UsuarioDTO();
			saida.setId(usuarioAmigo.getUsuario().getId().toString());
			saida.setNome(usuarioAmigo.getUsuario().getNome());
			saida.setEmail(usuarioAmigo.getUsuario().getEmail());
			amigos.add(saida);
		}
		return amigos;
	}

	public static List<UsuarioDTO> converterSolicitacoesPendentesUsuario(List<UsuarioAmigo> usuariosAmigos) {
		List<UsuarioDTO> amigos = new ArrayList<UsuarioDTO>();
		
		for (UsuarioAmigo usuarioAmigo : usuariosAmigos) {
			UsuarioDTO saida = new UsuarioDTO();
			saida.setId(usuarioAmigo.getAmigo().getId().toString());
			saida.setNome(usuarioAmigo.getAmigo().getNome());
			saida.setEmail(usuarioAmigo.getAmigo().getEmail());
			amigos.add(saida);
		}
		return amigos;
	}
	
	public static List<UsuarioDTO> converterAmigos(List<UsuarioAmigo> usuarioAmigos) {
		return converterSolicitacoesPendentesUsuario(usuarioAmigos);
	}
}
