package br.com.portal.inovacao.service;


import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.AlteraUsuario;
import br.com.portal.inovacao.dto.CriaUsuarioEntrada;
import br.com.portal.inovacao.dto.ImagemDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.UsuarioDTO;
import br.com.portal.inovacao.dto.UsuarioPerfilEntrada;
import br.com.portal.inovacao.facade.UsuarioFacade;
import br.com.portal.inovacao.model.Perfil;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.ConverteSenhaParaMD5;
import br.com.portal.inovacao.utils.Retorno;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.imageio.ImageIO;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

@Path("usuario")
public class UsuarioService {
	
    @Inject
    private UsuarioFacade facade;

    private static String UPLOAD_FILE_SERVER = "/var/www/html/imagens/perfil/";
    private static final int CODIGO_SUCESSO = 0;
    private static final int CODIGO_ERRO = 1;

    private static final String MSG_IMAGEM_SALVA_COM_SUCESSO = "Imagem salva com sucesso.";
    private static final String MSG_OCORREU_UM_ERRO_AO_SALVAR_IMAGEM = "Ocorreu um erro ao salvar imagem.";

    private boolean criaDiretorio(String _folder) {
       try {
           return new File(_folder).mkdirs();
      } catch (Exception e) {
           e.printStackTrace();
      }
       return false;
    }

    public static void redimensionaImagem(String caminhoImg, Integer imgLargura, Integer imgAltura) throws IOException {  
        BufferedImage imagem = ImageIO.read(new File(caminhoImg));  

        Double novaImgLargura = (double) imagem.getWidth();  
        Double novaImgAltura = (double) imagem.getHeight();  

        Double imgProporcao = null;  
        if (novaImgLargura >= imgLargura) {  
            imgProporcao = (novaImgAltura / novaImgLargura);  
            novaImgLargura = (double) imgLargura;  
            novaImgAltura = (novaImgLargura * imgProporcao);  
            while (novaImgAltura > imgAltura) {  
                novaImgLargura = (double) (--imgLargura);  
                novaImgAltura = (novaImgLargura * imgProporcao);  
            }  
        } else if (novaImgAltura >= imgAltura) {  
            imgProporcao = (novaImgLargura / novaImgAltura);  
            novaImgAltura = (double) imgAltura;  
            while (novaImgLargura > imgLargura) {  
                novaImgAltura = (double) (--imgAltura);  
                novaImgLargura = (novaImgAltura * imgProporcao);  
            }  
        }  

        BufferedImage novaImagem = new BufferedImage(novaImgLargura.intValue(), novaImgAltura.intValue(), BufferedImage.TYPE_INT_RGB);  
        Graphics g = novaImagem.getGraphics();  
        g.drawImage(imagem.getScaledInstance(novaImgLargura.intValue(), novaImgAltura.intValue(), 10000), 0, 0, null);  
        g.dispose();  

        ImageIO.write(novaImagem, "JPG", new File(caminhoImg));  
    } 
    
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("criar")
    public Retorno cria(CriaUsuarioEntrada usuario) throws Exception {
        
        Retorno _Retorno = new Retorno();

        _Retorno = facade.salvar(usuario);

        if (_Retorno.getErro() == 0){
            
            ConectaBanco _Banco = new ConectaBanco();

            String _SQL;
            _SQL = "";
            _SQL = _SQL + "Select";
            _SQL = _SQL + "    is_usuario_ativo ";
            _SQL = _SQL + "   ,nome ";
            _SQL = _SQL + "   ,email ";
            _SQL = _SQL + "From ";
            _SQL = _SQL + "    usuario ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "    email = '" + usuario.getEmail() + "' ";

            ResultSet rs = _Banco.Consulta(_SQL);

            
            if (rs.first()){
                
                if (rs.getBoolean("is_usuario_ativo") == false) {

                    String _nome;
                    String _email;
                    
                    _nome = rs.getString("nome");
                    _email = rs.getString("email");
                    
                    if (_nome.equals(_email)) {

                        _SQL = "";
                        _SQL = _SQL + "Update ";
                        _SQL = _SQL + "    usuario ";
                        _SQL = _SQL + "Set ";
                        _SQL = _SQL + "    nome = '" + usuario.getNome() + "' ";
                        _SQL = _SQL + "   ,is_usuario_ativo = 1 ";
                        _SQL = _SQL + "Where ";
                        _SQL = _SQL + "    email = '" + usuario.getEmail() + "' ";

                        return _Banco.Commando(_SQL); 
                        
                    }else{
                        return new Retorno(1, "Esse usuário esta desativado ", rs.getString("nome") + " " + rs.getString("email"));
                    }
                    
                }else{
                    return new Retorno(0, "Login Efetuado com Sucesso", _Retorno.getObjeto());
                }
                
            }else{
                return new Retorno(0, "Login Efetuado com Sucesso", _Retorno.getObjeto());
            }
        }else{
            return _Retorno;
        }
        
    }

    @POST
    @Consumes(MULTIPART_FORM_DATA)
    @Produces(APPLICATION_JSON)
    @Path("uploadimagem")
    public Retorno salvaImagem(MultipartFormDataInput fdt) {
        Map<String, List<InputPart>> map = fdt.getFormDataMap();

        try {
            
            String idUsuario = map.get("idUsuario").get(0).getBodyAsString();

            criaDiretorio(UPLOAD_FILE_SERVER + idUsuario + "/");

            String fileNameSmall = "perfilSmall.jpg";
            String fileNameBig = "perfilBig.jpg";

            redimensionaImagem(enviaImagemParaServidor(fileNameSmall, map, UPLOAD_FILE_SERVER + idUsuario + "/"), 50, 50);
            redimensionaImagem(enviaImagemParaServidor(fileNameBig, map, UPLOAD_FILE_SERVER + idUsuario + "/"), 200, 200);

            return new Retorno(CODIGO_SUCESSO, MSG_IMAGEM_SALVA_COM_SUCESSO);
            
        } catch (IOException e) {
                return new Retorno(CODIGO_ERRO, MSG_OCORREU_UM_ERRO_AO_SALVAR_IMAGEM);
        }
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("pesquisa")
    public Retorno pesquisa(InformaIdDTO _usuario) {

        
        try{
            
            SimpleDateFormat _dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            
            UsuarioDTO _user;

            ConectaBanco _Banco = new ConectaBanco();

            String _SQL;
            _SQL = "";
            _SQL = _SQL + "Select";
            _SQL = _SQL + "    * ";
            _SQL = _SQL + "From ";
            _SQL = _SQL + "    usuario ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "    id = " + _usuario.getId() + " ";

            ResultSet rs = _Banco.Consulta(_SQL);

            if (rs.first()){
                
                _user = new UsuarioDTO();
                _user.setId(rs.getString("id"));
                _user.setNome(rs.getString("nome"));
                _user.setEmail(rs.getString("email"));
                _user.setIdFacebook(rs.getString("id_facebook"));
                _user.setIdGoogle(rs.getString("id_google_plus"));
                _user.setDtCriacao(_dateFormat.format(rs.getDate("data_criacao")));
                
                return new Retorno(0, "Pesquisa realizada com sucesso", _user);
            }else{
                return new Retorno(1, "Usuario não encontrado");
            }
            
        }catch(Exception ex){
            return new Retorno(1, "Não foi possível realizar a pesquisa");
        }
            
        
    }

    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("alterar")
    public Retorno altera(AlteraUsuario usuario) {
            return facade.alterar(usuario);
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("alterasenha")
    public Retorno alteraSenha(AlteraUsuario usuario) {
        
        ConectaBanco _Banco = new ConectaBanco();
        
        String _SQL;

        try{
            
            _SQL = "";
            _SQL = _SQL + "Select";
            _SQL = _SQL + "    1 ";
            _SQL = _SQL + "From ";
            _SQL = _SQL + "    usuario ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "    Senha = '" + ConverteSenhaParaMD5.convertPasswordToMD5(usuario.getSenha()) + "' ";
            _SQL = _SQL + "and id = " + usuario.getId() + " ";

            ResultSet rs = _Banco.Consulta(_SQL);

            if (rs.first()){
                
                _SQL = "";
                _SQL = _SQL + "Update ";
                _SQL = _SQL + "    usuario ";
                _SQL = _SQL + "Set ";
                _SQL = _SQL + "    Senha = '" + ConverteSenhaParaMD5.convertPasswordToMD5(usuario.getNovaSenha()) + "' ";
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "    id = " + usuario.getId() + " ";

                return _Banco.Commando(_SQL); 
                
            }else{
                return new Retorno(1, "Senha ou Usuário Inválido !");
            }
        
        }catch(Exception ex){
            return new Retorno(1, "Ocorreu um erro ao validar a senha!");
        }
        
    }
    
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("novasenha")
    public Retorno novaSenha(AlteraUsuario usuario) {

        ConectaBanco _Banco = new ConectaBanco();
        Date _data = new Date();
        Retorno _Retorno;
        
        String _SQL;
        _SQL = "";
        _SQL = _SQL + "Select";
        _SQL = _SQL + "    1 ";
        _SQL = _SQL + "From ";
        _SQL = _SQL + "    usuario ";
        _SQL = _SQL + "Where ";
        _SQL = _SQL + "    email = '" + usuario.getEmail() + "' ";
        _SQL = _SQL + "and link = '" + usuario.getLink() + "' ";
        
        ResultSet rs = _Banco.Consulta(_SQL);

        try{
            if (rs.first()){
                
                _SQL = "";
                _SQL = _SQL + "Update ";
                _SQL = _SQL + "    usuario ";
                _SQL = _SQL + "Set ";
                _SQL = _SQL + "    Senha = '" + ConverteSenhaParaMD5.convertPasswordToMD5(usuario.getSenha()) + "' ";
                _SQL = _SQL + "   ,link = '' ";
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "    email = '" + usuario.getEmail() + "' ";
                
                return _Banco.Commando(_SQL); 
                
            }else{
                return new Retorno(1, "Link Inválido");
            }
        }catch(Exception ex){
            return new Retorno(1, "Ocorreu um erro ao enviar o convite!");
        }
            
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("inativar")
    public Retorno inativa(InformaIdDTO usuario) {
            return facade.inativar(usuario);
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("altera/perfil")
    public Retorno alteraPerfil(UsuarioPerfilEntrada usuarioPerfil) {
            return facade.alteraPerfil(usuarioPerfil);
    }

    @GET
    @Produces(APPLICATION_JSON)
    @Path("usuarios")
    public List<UsuarioPerfilEntrada> usuarios() {
            return facade.usuarios();
    }

    @GET
    @Produces(APPLICATION_JSON)
    @Path("perfis")
    public List<Perfil> perfis() {
            return facade.perfis();
    }

    private boolean criaDiretorio() {
        try {
            File diretorio = new File(UPLOAD_FILE_SERVER);

            if(diretorio.exists())
                return true;
            else
                return new File(UPLOAD_FILE_SERVER).mkdirs();

        }catch (Exception e) {
            e.printStackTrace();
    }
        return false;
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Path("imagem")        
    public Retorno saveImage(ImagemDTO dto) {

        try{

            URL url = new URL(dto.getFileName());
            InputStream is = url.openStream();

            String _id = String.format("%07d", dto.getIdIdeia()); 

            if (criaDiretorio()){
                OutputStream os = new FileOutputStream(UPLOAD_FILE_SERVER + "/" + _id + ".jpg");

                byte[] b = new byte[2048];
                int length;

                while ((length = is.read(b)) != -1) {
                        os.write(b, 0, length);
                }

                is.close();
                os.close();

                return new Retorno(0, "Imagem salva com sucesso !!");
            }else{
                return new Retorno(1, "Erro ao Criar a pasta de destino !!");
            }

        }catch(Exception ex){
            return new Retorno(1, ex.getMessage());
        }
    }
    
    private String enviaImagemParaServidor(String fileName, Map<String, List<InputPart>> map, String _folder) throws IOException {
    
        InputStream inputStream = null;
        List<InputPart> lstInputPart = map.get("uploadedFile");
        String fileServer = null;

        if (lstInputPart != null) {
            for (InputPart inputPart : lstInputPart) {
                if (null != fileName && !"".equalsIgnoreCase(fileName)) {
                    inputStream = inputPart.getBody(InputStream.class, null);
                    fileServer = gravaImagem(inputStream, fileName, _folder);
                    inputStream.close();
                }
            }
        }
        return fileServer;
    }

    private String gravaImagem(InputStream inputStream, String fileName, String _folder) throws IOException {
        
        OutputStream outputStream = null;
        String qualifiedUploadFilePath = _folder + fileName;

        try {
            outputStream = new FileOutputStream(new File(qualifiedUploadFilePath));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
            }
            outputStream.flush();
        } catch (Exception e) {
            return e.getMessage();
        } finally {
            outputStream.close();
        }
        return qualifiedUploadFilePath;
    }    
}
