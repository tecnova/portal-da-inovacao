package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.BrainstormingDTO;
import br.com.portal.inovacao.dto.GrupoBrainstormingDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.facade.BrainstormingFacade;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("brainstorming")
public class BrainstormingService {
	
	@Inject
	private BrainstormingFacade facade;
	
	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;
        
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("curtir")
	public Retorno curtirBrainstorming(BrainstormingDTO dto) {
            
            Retorno _Retorno;
            ConectaBanco _Banco = new ConectaBanco();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date _data = new Date(System.currentTimeMillis());  
            int _Qtde = 0;
            ResultSet rs;


            try{

                String _SQL;

                _SQL = "";
                _SQL = _SQL + "SELECT " 
                                + " count(1) as Qtde "
                            + "FROM  "
                                + "brainstorming_curtir  "
                            + "WHERE "
                                + "idBrainstorming = " + dto.getId() + " ";

                rs = _Banco.Consulta(_SQL);

                if (rs.first()){                
                    _Qtde = rs.getInt("Qtde");
                }
                
                _SQL = "";
                _SQL = _SQL + "Insert Into ";
                _SQL = _SQL + " brainstorming_curtir ";
                _SQL = _SQL + "   ( ";
                _SQL = _SQL + "     idBrainstorming ";
                _SQL = _SQL + "    ,idUsuario  ";
                _SQL = _SQL + "    ,dtCurtir ";
                _SQL = _SQL + "   ) ";
                _SQL = _SQL + " Values ";
                _SQL = _SQL + "   ( ";
                _SQL = _SQL + "     " + dto.getId() + " " ;
                _SQL = _SQL + "   , " + dto.getIdUsuario() + "  " ;
                _SQL = _SQL + "   ,'" + dateFormat.format(_data) + "' ";
                _SQL = _SQL + "   ) ";

                _Retorno = _Banco.Commando(_SQL);

                if (_Retorno.getErro() == 1){

                    _SQL = "";
                    _SQL = _SQL + "Delete ";
                    _SQL = _SQL + "From ";
                    _SQL = _SQL + "   brainstorming_curtir ";
                    _SQL = _SQL + "Where ";
                    _SQL = _SQL + "     idBrainstorming = " + dto.getId() + " " ;
                    _SQL = _SQL + " and idUsuario = " + dto.getIdUsuario() + "  " ;

                    _Retorno = _Banco.Commando(_SQL);
                    
                    return new Retorno(0, "Não Curti !!", _Qtde - 1);
                    
                }else{
                    return new Retorno(0, "Curti !!", _Qtde + 1);
                }
                
            }catch(Exception ex){
                
                return new Retorno(1, "Não foi possível curtir");
            }
                            
	}
        
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("criar")
	public Retorno criaBrainstorming(BrainstormingDTO dto) {

            Retorno _Retorno;
            ConectaBanco _Banco = new ConectaBanco();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date _data = new Date(System.currentTimeMillis());  

            try{

                String _SQL;            
                
                _SQL = "";
                _SQL = _SQL + "Insert Into ";
                _SQL = _SQL + " brainstorming ";
                _SQL = _SQL + "   ( ";
                _SQL = _SQL + "     descricao ";
                _SQL = _SQL + "    ,data_criacao ";
                _SQL = _SQL + "    ,id_historia  ";
                _SQL = _SQL + "    ,id_usuario   ";
                _SQL = _SQL + "   ) ";
                _SQL = _SQL + " Values ";
                _SQL = _SQL + "   ( ";
                _SQL = _SQL + "    '" + dto.getDescricao() + "' " ;
                _SQL = _SQL + "   ,'" + dateFormat.format(_data) + "'  " ;
                _SQL = _SQL + "   , " + dto.getIdHistoria() + " ";
                _SQL = _SQL + "   , " + dto.getIdUsuario() + " ";
                _SQL = _SQL + "   ) ";

                _Retorno = _Banco.InsertIntoReturnID(_SQL);
                
                return _Retorno;
                
            }catch(Exception ex){
                
                return new Retorno(1, "Não foi possível curtir");
            }                
            
        }

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("alterar")
	public Retorno alteraBrainstorming(BrainstormingDTO dto) {
            
            try {

                String _SQL;
                ConectaBanco _Banco = new ConectaBanco();
                ResultSet rs;

                if (dto.getIdUsuario() != null){

                    _SQL = "";
                    _SQL = _SQL + "SELECT " 
                                    + " id_usuario "
                                + "FROM  "
                                    + "brainstorming "
                                + "WHERE "
                                    + "id_usuario = " + dto.getIdUsuario() + " "
                                + "and id = " + dto.getId() + " ";

                    rs = _Banco.Consulta(_SQL);

                    if (rs.first()){                

                        _SQL = "";
                        _SQL =  _SQL + "update ";
                        _SQL =  _SQL + "     brainstorming ";
                        _SQL =  _SQL + "set  ";
                        _SQL =  _SQL + "     descricao = '" + dto.getDescricao() + "' ";
                        if ((dto.getIdGrupoBrainstorming() != null ) && (dto.getIdGrupoBrainstorming() != 0)){
                            _SQL =  _SQL + "     ,id_GrupoBrainstorming = " + dto.getIdGrupoBrainstorming() + " ";
                        }
                        _SQL =  _SQL + "where  ";
                        _SQL =  _SQL + "     id = " + dto.getId() + " ";

                        return _Banco.Commando(_SQL);
                        
                    }else{
                        return new Retorno(1, "Esse Usuário não pode alterar esse brainstorming");
                    }
                
                }else{
                    return new Retorno(1, "Parâmetro 'idUsuario' não informado. ");
                }
                
                
            } catch (Exception ex) {
                return new Retorno(CODIGO_ERRO, ex.getMessage());
            }
        }

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("excluir")
	public Retorno excluiBrainstorming(BrainstormingDTO dto) {
            
            try {

                String _SQL;
                ConectaBanco _Banco = new ConectaBanco();
                ResultSet rs;

                if (dto.getIdUsuario() != null){

                    _SQL = "";
                    _SQL = _SQL + "SELECT " 
                                    + " id_usuario "
                                + "FROM  "
                                    + "brainstorming "
                                + "WHERE "
                                    + "id_usuario = " + dto.getIdUsuario() + " "
                                + "and id = " + dto.getId() + " ";

                    rs = _Banco.Consulta(_SQL);

                    if (rs.first()){                

                        _SQL = "";
                        _SQL =  _SQL + "Delete ";
                        _SQL =  _SQL + "From  ";
                        _SQL =  _SQL + "     brainstorming ";
                        _SQL =  _SQL + "where  ";
                        _SQL =  _SQL + "     id = " + dto.getId() + " ";

                        return _Banco.Commando(_SQL);
                        
                    }else{
                        return new Retorno(1, "Esse Usuário não pode excluir esse brainstorming");
                    }
                
                }else{
                    return new Retorno(1, "Parâmetro 'idUsuario' não informado. ");
                }
                
                
            } catch (Exception ex) {
                return new Retorno(CODIGO_ERRO, ex.getMessage());
            }
        }


	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista/historia")
	public Retorno listaBrainstormingPorHistoria(BrainstormingDTO dto) {

            String _SQL;
            _SQL = "";
            _SQL = _SQL + "Select ";
            _SQL = _SQL + "     bra.* ";
            _SQL = _SQL + "    ,bcu.dtCurtir ";
            _SQL = _SQL + "    ,(Select count(1) From brainstorming_curtir tbl Where tbl.idBrainstorming = bra.id) as Qtde ";
            _SQL = _SQL + "From  ";
            _SQL = _SQL + "     brainstorming bra ";
            _SQL = _SQL + "     left join brainstorming_curtir bcu on (bra.id = bcu.idBrainstorming and bcu.idUsuario = " + dto.getIdUsuario() + " ) ";
            _SQL = _SQL + "where  ";
            _SQL = _SQL + "     id_historia = " + dto.getIdHistoria() + " ";

            ResultSet rs;

            ConectaBanco _Banco = new ConectaBanco();

            rs = _Banco.Consulta(_SQL);
            
            List<BrainstormingDTO> dtos = new ArrayList<>();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM HH:mm");
            
            Date _dtCriacao;
            
            try{
                
                while (rs.next()) {
                    
                    BrainstormingDTO item = new BrainstormingDTO();
                    item.setId(rs.getLong("id"));
                    
                    _dtCriacao = dateFormat.parse(rs.getString("data_criacao"));
                    item.setDataCriacao(newFormat.format(_dtCriacao));
                    
                    item.setDescricao(rs.getString("Descricao"));
                    item.setIdHistoria(rs.getLong("id_historia"));
                    item.setIdUsuario(rs.getLong("id_usuario"));
                    item.setIdGrupoBrainstorming(rs.getLong("id_grupobrainstorming"));
                    
                    if (rs.getDate("dtCurtir") != null){
                        item.setBoCurtir("S");
                    }else{
                        item.setBoCurtir("N");
                    }
                    item.setQtCurtir(rs.getLong("Qtde"));

                    dtos.add(item);
                }
                
            }catch(Exception ex){
                return new Retorno(CODIGO_ERRO, ex.getMessage());
            }

            return new Retorno(CODIGO_SUCESSO, dtos);
	}
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista/grupo")
	public Retorno listaGrupoBrainstormingPorHistoria(GrupoBrainstormingDTO dto) {

            GrupoBrainstormingDTO _Result = new GrupoBrainstormingDTO();
            BrainstormingDTO _item;
            ResultSet rs;
            ConectaBanco _Banco = new ConectaBanco();
            
            String _SQL;
            
            _SQL = "";
            _SQL = _SQL + "SELECT  ";
            _SQL = _SQL + "    * ";
            _SQL = _SQL + "FROM ";
            _SQL = _SQL + "    grupo_brainstorming ";
            _SQL = _SQL + "Where ";
            _SQL = _SQL + "	id = " + dto.getId();
                
            rs = _Banco.Consulta(_SQL);

            try{
                if (rs.first()){
                    
                    _Result.setId(rs.getLong("id"));
                    _Result.setDescricao(rs.getString("descricao"));
                    _Result.setIdHistoria(rs.getLong("id_historia"));
                    
                    _SQL = "";
                    _SQL = _SQL + "SELECT  ";
                    _SQL = _SQL + "    * ";
                    _SQL = _SQL + "FROM ";
                    _SQL = _SQL + "    brainstorming ";
                    _SQL = _SQL + "WHERE ";
                    _SQL = _SQL + "    id_GrupoBrainstorming = " + dto.getId();

                    rs = _Banco.Consulta(_SQL);

                    List<BrainstormingDTO> _lstBrainstorming = new ArrayList();
                    
                    while (rs.next()){
                        _item = new BrainstormingDTO();
                        _item.setId(rs.getLong("id"));
                        _item.setDescricao(rs.getString("descricao"));
                        _item.setDataCriacao(rs.getString("data_criacao"));
                        
                        _lstBrainstorming.add(_item);
                    }
                    
                    _SQL = "";
                    _SQL = _SQL + "SELECT  ";
                    _SQL = _SQL + "    * ";
                    _SQL = _SQL + "FROM ";
                    _SQL = _SQL + "    brainstorming ";
                    _SQL = _SQL + "WHERE ";
                    _SQL = _SQL + "    id_Historia = " + dto.getIdHistoria() + " ";
                    _SQL = _SQL + "and isnull(id_GrupoBrainstorming)" ;

                    rs = _Banco.Consulta(_SQL);

                    List<BrainstormingDTO> _lstBrainstormingDisponivel = new ArrayList();
                    
                    while (rs.next()){
                        _item = new BrainstormingDTO();
                        _item.setId(rs.getLong("id"));
                        _item.setDescricao(rs.getString("descricao"));
                        _item.setDataCriacao(rs.getString("data_criacao"));
                        
                        _lstBrainstormingDisponivel.add(_item);
                    }
                    
                    _Result.setLstBrainstorming(_lstBrainstorming);
                    _Result.setLstBrainstormingDisponivel(_lstBrainstormingDisponivel);
                    
                    return new Retorno(0, "Listagem Efetuada com Sucesso", _Result);
                }else{
                    return new Retorno(1, "Grupo não Encontrado !!");
                }
            }catch(Exception ex){
                return new Retorno(1, "Não foi possível Listar os Itens");
            }            
        }
        
        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("curtir")
	public Retorno curtir() {
            //return facade.criaCanvas(dto);
            return new Retorno(0, "Retornei Alguma Coisa");
	}        

	
}
