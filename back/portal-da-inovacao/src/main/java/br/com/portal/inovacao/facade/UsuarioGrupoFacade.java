package br.com.portal.inovacao.facade;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.EmpresaGrupoDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dao.UsuarioGrupoDAO;
import br.com.portal.inovacao.dto.AdicionaUsuarioNoGrupoDTO;
import br.com.portal.inovacao.model.EmpresaGrupo;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.model.UsuarioGrupo;
import br.com.portal.inovacao.utils.Retorno;

public class UsuarioGrupoFacade {
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	@Inject
	private UsuarioGrupoDAO usuarioGrupoDAO;

	@Inject
	private EmpresaGrupoDAO empresaGrupoDAO;
	
	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;
	
	private static final String MSG_USUARIO_ADICIONADO_COM_SUCESSO_AO_GRUPO = "Usuário adicionado com sucesso ao grupo.";
	
	private UsuarioGrupo criaUsuarioGrupo(AdicionaUsuarioNoGrupoDTO dto) {
		Usuario usuario = usuarioDAO.pesquisaPorId(dto.getIdUsuario());
		EmpresaGrupo empresaGrupo = empresaGrupoDAO.pesquisaPorId(dto.getIdGrupo());
		
		UsuarioGrupo usuarioGrupo = new UsuarioGrupo();
		usuarioGrupo.setIdUsuario(usuario);
		usuarioGrupo.setIdGrupo(empresaGrupo);
		return usuarioGrupo;
	}
	
	public Retorno adicionaUsuarioAoGrupo(AdicionaUsuarioNoGrupoDTO dto) {
		try {
			UsuarioGrupo grupo = criaUsuarioGrupo(dto);
			usuarioGrupoDAO.salvar(grupo);
			return new Retorno(CODIGO_SUCESSO, MSG_USUARIO_ADICIONADO_COM_SUCESSO_AO_GRUPO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
}
