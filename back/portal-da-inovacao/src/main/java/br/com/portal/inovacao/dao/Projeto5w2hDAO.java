package br.com.portal.inovacao.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.portal.inovacao.model.Projeto5w2h;

public class Projeto5w2hDAO {
	
	@Inject
	private EntityManager maneger;
	
	public void salvar(Projeto5w2h projeto5w2h) {
		maneger.getTransaction().begin();
		maneger.merge(projeto5w2h);
		maneger.getTransaction().commit();
	}
	
	public Projeto5w2h pesquisaPorId(Long id) {
		return maneger.find(Projeto5w2h.class, id);
	}
	
	public void alterar(Projeto5w2h projeto5w2h) {
		maneger.getTransaction().begin();
		maneger.merge(projeto5w2h);
		maneger.getTransaction().commit();
	}
	
	public void excluir(Projeto5w2h projeto5w2h) {
		maneger.getTransaction().begin();
		maneger.remove(projeto5w2h);
		maneger.getTransaction().commit();
	}
	
	public void vinculaGrupo(Projeto5w2h projeto5w2h) {
		maneger.getTransaction().begin();
		maneger.merge(projeto5w2h);
		maneger.getTransaction().commit();
	}
}
