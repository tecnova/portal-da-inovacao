package br.com.portal.inovacao.service;

import br.com.portal.inovacao.dto.BrainstormingDTO;
import br.com.portal.inovacao.dto.EmpresaUsuarioDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.Projeto5w2hDTO;
import br.com.portal.inovacao.dto.UsuarioDTO;
import br.com.portal.inovacao.utils.ConectaBanco;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.utils.Retorno;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Path("projeto")
public class Projeto5w2hService {
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("salvar")
	public Retorno cria5w2h(Projeto5w2hDTO dto) {

            try {

                Retorno _Retorno;

                long _idProjeto5w2h;
                long _idProjeto5w2hItem;
                
                ConectaBanco _Banco = new ConectaBanco();
                
                String _SQL;
                
                _SQL = "";
                _SQL = _SQL + "Select ";
                _SQL = _SQL + "   id ";
                _SQL = _SQL + "From  ";
                _SQL = _SQL + "   projeto5w2h ";
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "   id_brainstorming = " + dto.getIdbrainstorming();
                
                ResultSet rs = _Banco.Consulta(_SQL);
                
                if (rs.first()){
                    _idProjeto5w2h = rs.getLong("id");
                    
                    _SQL = "";
                    _SQL = _SQL + "Update ";
                    _SQL = _SQL + "     projeto5w2h ";
                    _SQL = _SQL + "Set ";
                    _SQL = _SQL + "     Assunto = '" + dto.getAssunto() + "' ";
                    _SQL = _SQL + "    ,Ataque = '" + dto.getAtaque() + "' ";
                    _SQL = _SQL + "    ,id_brainstorming  = " + dto.getIdbrainstorming() + " ";
                    _SQL = _SQL + "Where ";
                    _SQL = _SQL + "     id = " + _idProjeto5w2h;

                    _Retorno = _Banco.Commando(_SQL);
                    
                }else {

                    _SQL = "";
                    _SQL = _SQL + "Insert Into ";
                    _SQL = _SQL + " projeto5w2h ";
                    _SQL = _SQL + "   ( ";
                    _SQL = _SQL + "     Assunto ";
                    _SQL = _SQL + "    ,Ataque  ";
                    _SQL = _SQL + "    ,id_brainstorming  ";
                    _SQL = _SQL + "   ) ";
                    _SQL = _SQL + " Values ";
                    _SQL = _SQL + "   ( ";
                    _SQL = _SQL + "    '" + dto.getAssunto() + "' ";
                    _SQL = _SQL + "   ,'" + dto.getAtaque() + "' ";
                    _SQL = _SQL + "   , " + dto.getIdbrainstorming() + " ";
                    _SQL = _SQL + "   ) ";

                    _Retorno = _Banco.InsertIntoReturnID(_SQL);

                    if (_Retorno.getErro() > 0 ){
                        return new Retorno(1, _Retorno.getMensagem());
                    }else{
                        _idProjeto5w2h = (long)_Retorno.getObjeto();
                    }
                    
                }
                

                if ( _idProjeto5w2h > 0) {

                    java.util.Date _date;
                    //SimpleDateFormat dateFormatTela = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    
                    if (dto.getIdprojeto5w2hitem() > 0){
                        
                        _SQL = "";
                        _SQL = _SQL + "Update ";
                        _SQL = _SQL + "     projeto5w2hitem ";
                        _SQL = _SQL + "Set ";
                        _SQL = _SQL + "    ,descricao = '" + dto.getAcao() + "' " ;
                        _SQL = _SQL + "    ,motivo = '" + dto.getMotivo() + "' " ;
                        _SQL = _SQL + "    ,local = '" + dto.getLocal() + "' " ;
                        
                        _date = dateFormat.parse(dto.getDtInicio());
                        _SQL = _SQL + "    ,dtInicio = '" + dateFormat.format(_date) + "' " ;
                        
                        _date = dateFormat.parse(dto.getDtTermino());
                        _SQL = _SQL + "    ,dtTermino = '" + dateFormat.format(_date) + "' " ;
                        
                        _SQL = _SQL + "    ,detalhamento = '" + dto.getDetalhamento() + "' " ;
                        _SQL = _SQL + "    ,valor = " + dto.getValor() + "  " ;
                        _SQL = _SQL + "Where ";
                        _SQL = _SQL + "     getIdprojeto5w2hitem = " + dto.getIdprojeto5w2hitem();                        
                        
                        _Retorno = _Banco.Commando(_SQL);
                        
                    }else{
                        _SQL = "";
                        _SQL = _SQL + "Insert Into ";
                        _SQL = _SQL + " projeto5w2hitem ";
                        _SQL = _SQL + "   ( ";
                        _SQL = _SQL + "     idProjeto5w2h ";
                        _SQL = _SQL + "    ,descricao  ";
                        _SQL = _SQL + "    ,motivo  ";
                        _SQL = _SQL + "    ,local  ";
                        _SQL = _SQL + "    ,dtInicio  ";
                        _SQL = _SQL + "    ,dtTermino  ";
                        _SQL = _SQL + "    ,detalhamento  ";
                        _SQL = _SQL + "    ,valor  ";
                        _SQL = _SQL + "   ) ";
                        _SQL = _SQL + " Values ";
                        _SQL = _SQL + "   ( ";
                        _SQL = _SQL + "    '" + _idProjeto5w2h + "' " ;
                        _SQL = _SQL + "   ,'" + dto.getAcao() + "' " ;
                        _SQL = _SQL + "   ,'" + dto.getMotivo() + "' " ;
                        _SQL = _SQL + "   ,'" + dto.getLocal() + "' " ;

                        _date = dateFormat.parse(dto.getDtInicio());
                        _SQL = _SQL + "   ,'" + dateFormat.format(_date) + "' " ;

                        _date = dateFormat.parse(dto.getDtTermino());
                        _SQL = _SQL + "   ,'" + dateFormat.format(_date) + "' " ;

                        _SQL = _SQL + "   ,'" + dto.getDetalhamento() + "' " ;
                        _SQL = _SQL + "   , " + dto.getValor() + "  " ;
                        _SQL = _SQL + "   ) ";

                        _Retorno = _Banco.InsertIntoReturnID(_SQL);
                    }
                    
                    if (_Retorno.getErro() == 1){
                        return new Retorno(1, _Retorno.getMensagem());
                    }else{
                        _idProjeto5w2hItem = (long)_Retorno.getObjeto();
                    }
           
                    if (_idProjeto5w2hItem > 0) {
                        
                        List<UsuarioDTO> _equipe;

                        String _idUsuario;
                        
                        _equipe = dto.getLstUsuarioAcao();
                        
                        //return new Retorno(1, _equipe);
                        
                        _SQL = "";
                        _SQL = _SQL + "Delete ";
                        _SQL = _SQL + "From ";
                        _SQL = _SQL + "     projeto5w2hitemequipe ";
                        _SQL = _SQL + "Where ";
                        _SQL = _SQL + "     idProjeto5w2hitem = " + _idProjeto5w2hItem;

                        _Retorno = _Banco.Commando(_SQL);                        
                        
                        for (int i = 0; i < _equipe.size(); i++){
                            
                            _idUsuario = _equipe.get(i).getId();

                            _SQL = "";
                            _SQL = _SQL + "Insert Into ";
                            _SQL = _SQL + " projeto5w2hitemequipe ";
                            _SQL = _SQL + "   ( ";
                            _SQL = _SQL + "     idProjeto5w2hitem ";
                            _SQL = _SQL + "    ,idUsuario  ";
                            _SQL = _SQL + "   ) ";
                            _SQL = _SQL + " Values ";
                            _SQL = _SQL + "   ( ";
                            _SQL = _SQL + "    '" + _idProjeto5w2hItem + "' " ;
                            _SQL = _SQL + "   ,'" + _idUsuario + "' " ;
                            _SQL = _SQL + "   ) ";
                            
                            _Retorno = _Banco.Commando(_SQL);
                            
                            if (_Retorno.getErro() == 1){
                                return new Retorno(1, _SQL + " **** " + _Retorno.getMensagem());
                            }

                        }
                        
                        return new Retorno(0, "Gravação Realizada com Sucesso !!");
                        
                    }else{
                        return new Retorno(1, "Não foi possível Gravar ");
                    }
                }else {
                    
                    return new Retorno(1, "Não foi possível concluir a opera��o");
                    
                }

            } catch (Exception ex) {
                return new Retorno(8, ex.getMessage());
            }  
            
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("alterar")
	public Retorno alterar(Projeto5w2hDTO dto) {

            try {

                Retorno _Retorno;

                long _idProjeto5w2h;
                long _idProjeto5w2hItem;
                
                ConectaBanco _Banco = new ConectaBanco();
                
                String _SQL;
                
                //Procurando o Id do Projeto a Partir do id_brainstorming
                
                _SQL = "";
                _SQL = _SQL + "Select ";
                _SQL = _SQL + "   id ";
                _SQL = _SQL + "From  ";
                _SQL = _SQL + "   projeto5w2h ";
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "   id_brainstorming = " + dto.getIdbrainstorming();
                
                ResultSet rs = _Banco.Consulta(_SQL);
                
                if (rs.first()){
                    _idProjeto5w2h = rs.getLong("id");
                }else {
                    return new Retorno(1, "Não Conseguiu localizar o projeto");
                }
                        
                _SQL = "";
                _SQL = _SQL + "Update ";
                _SQL = _SQL + " projeto5w2h ";
                _SQL = _SQL + "set ";
                _SQL = _SQL + "     Assunto = '" + dto.getAssunto() + "' "; 
                _SQL = _SQL + "    ,Ataque  = '" + dto.getAtaque() + "' ";
                _SQL = _SQL + "where ";
                _SQL = _SQL + "    id = " + dto.getIdbrainstorming();

                _Retorno = _Banco.Commando(_SQL);
                
                if (_Retorno.getErro() > 0 ){
                    return new Retorno(1, _Retorno.getMensagem());
                }

                if ((_idProjeto5w2h > 0) && (dto.getIdprojeto5w2hitem() > 0)) {

                    java.util.Date _date;
                    //SimpleDateFormat dateFormatTela = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    
                    _SQL = "";
                    _SQL = _SQL + "Update ";
                    _SQL = _SQL + " projeto5w2hitem ";
                    _SQL = _SQL + "Set ";
                    _SQL = _SQL + "    descricao    = '" + dto.getAcao() + "' ";
                    _SQL = _SQL + "   ,motivo       = '" + dto.getMotivo() + "' ";
                    _SQL = _SQL + "   ,local        = '" + dto.getLocal() + "' ";
                    
                    _date = dateFormat.parse(dto.getDtInicio());
                    _SQL = _SQL + "   ,dtInicio     = '" + dateFormat.format(_date) + "' " ;

                    _date = dateFormat.parse(dto.getDtTermino());
                    _SQL = _SQL + "   ,dtTermino    = '" + dateFormat.format(_date) + "' " ;
                    
                    _SQL = _SQL + "   ,detalhamento = '" + dto.getDetalhamento() + "' ";
                    _SQL = _SQL + "   ,valor        =  " + dto.getValor() + " ";
                    _SQL = _SQL + "Where ";
                    _SQL = _SQL + "    id = " + dto.getIdprojeto5w2hitem() + " ";
                    
                    _Retorno = _Banco.Commando(_SQL);
                    
                    if (_Retorno.getErro() == 1){
                        return new Retorno(1, _Retorno.getMensagem());
                    }else{
                        return new Retorno(0, "Alteração Concluída com Sucesso !");
                    }
           
                }else {
                    return new Retorno(1, "Não foi possível concluir a operação ! idProjeto : " + _idProjeto5w2h + ", idItem : " + dto.getIdprojeto5w2hitem());
                }

            } catch (Exception ex) {
                return new Retorno(1, ex.getMessage());
            }  
            
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("excluir/projeto")
	public Retorno excluirprojeto(Projeto5w2hDTO dto) {

            try {

                Retorno _Retorno;

                ConectaBanco _Banco = new ConectaBanco();
                
                String _SQL;

                _SQL = "";
                _SQL = _SQL + "DELETE FROM projeto5w2h  ";
                _SQL = _SQL + "WHERE ";
                _SQL = _SQL + "    id > 0 ";
                _SQL = _SQL + "and id_brainstorming = " + dto.getIdbrainstorming() ;

                _Retorno = _Banco.Commando(_SQL);
         
                if (_Retorno.getErro() == 0){
                    return new Retorno(0, "Exclus�o Realizada com Sucesso !");
                }else{
                    return new Retorno(1, _Retorno.getMensagem());
                }
                
            } catch (Exception ex) {
                return new Retorno(1, ex.getMessage());
            }  
            
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("excluir/acao")
	public Retorno excluiracao(Projeto5w2hDTO dto) {

            try {

                Retorno _Retorno;

                ConectaBanco _Banco = new ConectaBanco();
                
                String _SQL;

                _SQL = "";
                _SQL = _SQL + "DELETE FROM projeto5w2hitem  ";
                _SQL = _SQL + "WHERE ";
                _SQL = _SQL + "    id = " + dto.getIdprojeto5w2hitem() ;

                _Retorno = _Banco.Commando(_SQL);
         
                if (_Retorno.getErro() == 0){
                    return new Retorno(0, "Exclus�o Realizada com Sucesso !");
                }else{
                    return new Retorno(1, _Retorno.getMensagem());
                }
                
            } catch (Exception ex) {
                return new Retorno(1, ex.getMessage());
            }  
            
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("excluir/equipe")
	public Retorno excluirequipe(Projeto5w2hDTO dto) {

            try {

                Retorno _Retorno;

                ConectaBanco _Banco = new ConectaBanco();
                
                String _SQL;

                _SQL = "";
                _SQL = _SQL + "DELETE FROM projeto5w2hitemequipe  ";
                _SQL = _SQL + "WHERE ";
                _SQL = _SQL + "    idprojeto5w2hitem = " + dto.getIdprojeto5w2hitem() + " " ;
                _SQL = _SQL + "and idusuario = " + dto.getEquipe();

                _Retorno = _Banco.Commando(_SQL);
         
                if (_Retorno.getErro() == 0){
                    return new Retorno(0, "Exclusão Realizada com Sucesso !");
                }else{
                    return new Retorno(1, _Retorno.getMensagem());
                }
                
            } catch (Exception ex) {
                return new Retorno(1, ex.getMessage());
            }  
            
	}
        
        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("brainstorming")
	public Retorno listaacoes(InformaIdDTO idIdeia) {

            try {

                ConectaBanco _Banco = new ConectaBanco();
                BrainstormingDTO _brainstorming;
                List<BrainstormingDTO> _lstBrainstorming = new ArrayList<>();
                SimpleDateFormat _dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                
                String _SQL;
                
                _SQL = "";
                _SQL = _SQL + "SELECT  ";
                _SQL = _SQL + "    bra.* ";
                _SQL = _SQL + "FROM ";
                _SQL = _SQL + "    brainstorming bra ";
                _SQL = _SQL + "WHERE ";
                _SQL = _SQL + "    bra.id_historia = " + idIdeia.getId() + " ";
                
                ResultSet rsBrainstorming;
                rsBrainstorming = _Banco.Consulta(_SQL);

                while (rsBrainstorming.next()){
                    
                    _brainstorming = new BrainstormingDTO();

                    _brainstorming.setId(rsBrainstorming.getLong("id"));
                    _brainstorming.setDescricao(rsBrainstorming.getString("descricao"));
                    _brainstorming.setIdHistoria(rsBrainstorming.getLong("id_historia"));
                    _brainstorming.setIdGrupoBrainstorming(rsBrainstorming.getLong("id_GrupoBrainstorming"));
                    _brainstorming.setDataCriacao(_dateFormat.format(rsBrainstorming.getDate("data_criacao")));
                    _brainstorming.setIdUsuario(rsBrainstorming.getLong("id_usuario"));
                    
                    _SQL = "";
                    _SQL = _SQL + "SELECT  ";
                    _SQL = _SQL + "    usr.* ";
                    _SQL = _SQL + "FROM ";
                    _SQL = _SQL + "    usuario usr, ";
                    _SQL = _SQL + "    ideia_equipe ieq ";
                    _SQL = _SQL + "WHERE ";
                    _SQL = _SQL + "    usr.id = ieq.id_usuario ";
                    _SQL = _SQL + "and ieq.id_ideia = " + idIdeia.getId() + " ";

                    ResultSet rs = _Banco.Consulta(_SQL);

                    List<UsuarioDTO> _lstUsuario = new ArrayList<>();

                    UsuarioDTO _usuario;

                    while (rs.next()) {

                        _usuario = new UsuarioDTO();

                        _usuario.setId(rs.getString("id"));
                        _usuario.setNome(rs.getString("nome"));
                        _usuario.setEmail(rs.getString("email"));

                        _lstUsuario.add(_usuario);
                    }

                    _brainstorming.setLstUsuario(_lstUsuario);

                    _lstBrainstorming.add(_brainstorming);

                }

                return new Retorno(0, "Listagem Gerada com Sucesso", _lstBrainstorming);
                    
            } catch (Exception ex) {
                return new Retorno(1, "Ta dando Nada Não " + ex.getMessage());
            }  
            
	}
        
        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("acao")
	public Retorno listaacoes(Projeto5w2hDTO dto) {

            try {

                Retorno _Retorno;

                long _idProjeto5w2h;
                long _idProjeto5w2hItem;
                
                ConectaBanco _Banco = new ConectaBanco();
                
                String _SQL;
                
                //Procurando o Id do Projeto a Partir do id_brainstorming
                
                _SQL = "";
                _SQL = _SQL + "Select ";
                _SQL = _SQL + "    item.* ";
                _SQL = _SQL + "From  ";
                _SQL = _SQL + "    projeto5w2h      prj  ";
                _SQL = _SQL + "   ,projeto5w2hitem  item ";
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "    item.idprojeto5w2h = prj.id ";
                
                if (dto.getIdbrainstorming() > 0) {
                    _SQL = _SQL + "and prj.id_brainstorming = " + dto.getIdbrainstorming();
                }else if (dto.getIdusuario() > 0) {
                    _SQL = _SQL + "and exists (Select 1 From projeto5w2hitemequipe equ Where equ.idProjeto5w2hitem = item.id and equ.idUsuario = " + dto.getIdusuario() + ") ";
                }
                
                ResultSet _item = _Banco.Consulta(_SQL);

                List<Projeto5w2hDTO> dtos = new ArrayList<>();

                try{
                    while (_item.next()) {
                
                        _idProjeto5w2hItem = _item.getLong("id");

                        Projeto5w2hDTO _itemdto = new Projeto5w2hDTO();
                        
                        _itemdto.setId(_item.getLong("id"));
                        _itemdto.setIdprojeto5w2h(_item.getLong("idProjeto5w2h"));
                        _itemdto.setIdprojeto5w2hitem(_item.getLong("id"));
                        _itemdto.setIdbrainstorming(dto.getIdbrainstorming());
                        _itemdto.setAcao(_item.getString("descricao"));
                        _itemdto.setMotivo(_item.getString("Motivo"));
                        _itemdto.setLocal(_item.getString("Local"));
                        _itemdto.setDtInicio(_item.getString("dtInicio"));
                        _itemdto.setDtTermino(_item.getString("dtTermino"));
                        _itemdto.setDetalhamento(_item.getString("detalhamento"));
                        _itemdto.setValor(_item.getDouble("valor"));

                        _SQL = "";
                        _SQL = _SQL + "Select ";
                        _SQL = _SQL + "    equ.idusuario ";
                        _SQL = _SQL + "   ,usu.Nome ";
                        _SQL = _SQL + "   ,usu.email ";
                        _SQL = _SQL + "From  ";
                        _SQL = _SQL + "   projeto5w2hitemequipe  equ";
                        _SQL = _SQL + "  ,usuario usu ";
                        _SQL = _SQL + "Where ";
                        _SQL = _SQL + "    equ.idusuario = usu.id ";
                        _SQL = _SQL + "and equ.idprojeto5w2hitem = " + _idProjeto5w2hItem;

                        UsuarioDTO _usuario;

                        ResultSet _Equipe = _Banco.Consulta(_SQL);
                       
                        _usuario  = new UsuarioDTO();
                        List<UsuarioDTO> lstUsuario = new ArrayList<>();
                       
                        while (_Equipe.next()){
                            _usuario = new UsuarioDTO();
                            _usuario.setId(_Equipe.getString("idusuario"));
                            _usuario.setNome(_Equipe.getString("nome"));
                            _usuario.setEmail(_Equipe.getString("email"));
                           
                            lstUsuario.add(_usuario);
                        }
                       
                        _itemdto.setLstUsuarioAcao(lstUsuario);

                        
                        _SQL = "";
                        _SQL = _SQL + "Select ";
                        _SQL = _SQL + "   usr.* ";
                        _SQL = _SQL + "From ";
                        _SQL = _SQL + "    usuario usr ";
                        _SQL = _SQL + "   ,ideia_equipe ieq ";
                        _SQL = _SQL + "   ,brainstorming bra ";
                        _SQL = _SQL + "Where ";
                        _SQL = _SQL + "    bra.id = " + dto.getIdbrainstorming() + " ";
                        _SQL = _SQL + "and bra.id_historia = ieq.id_ideia ";
                        _SQL = _SQL + "and ieq.id_usuario = usr.id ";
                        _SQL = _SQL + "and usr.id not in ( ";
                        _SQL = _SQL + "Select ";
                        _SQL = _SQL + "    equ.idusuario ";
                        _SQL = _SQL + "From  ";
                        _SQL = _SQL + "   projeto5w2hitemequipe  equ";
                        _SQL = _SQL + "  ,usuario usu ";
                        _SQL = _SQL + "Where ";
                        _SQL = _SQL + "    equ.idusuario = usu.id ";
                        _SQL = _SQL + "and equ.idprojeto5w2hitem = " + _idProjeto5w2hItem + ") ";

                        ResultSet _EquipeHistoria = _Banco.Consulta(_SQL);
                       
                        _usuario  = new UsuarioDTO();
                        List<UsuarioDTO> lstUsuarioHistoria = new ArrayList<>();
                       
                        while (_EquipeHistoria.next()){
                            _usuario = new UsuarioDTO();
                            _usuario.setId(_EquipeHistoria.getString("id"));
                            _usuario.setNome(_EquipeHistoria.getString("nome"));
                            _usuario.setEmail(_EquipeHistoria.getString("email"));
                           
                            lstUsuarioHistoria.add(_usuario);
                        }
                       
                        _itemdto.setLstUsuarioHistoria(lstUsuarioHistoria);
                        
                        dtos.add(_itemdto);
                        
                    }
                    
                    return new Retorno(0, "Listagem Gerada com Sucesso", dtos);
                    
                }catch(Exception ex){
                    return new Retorno(0, ex.getMessage());
                }

            } catch (Exception ex) {
                return new Retorno(1, "Ta dando Nada Não " + ex.getMessage());
            }  
            
	}

        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("acao/feedback")
	public Retorno fechamentoacao(Projeto5w2hDTO dto) {

            try {

                java.util.Date _date;
                    
                Retorno _Retorno;

                String _SQL;
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                _date = dateFormat.parse(dto.getDtFeedback());

                _SQL = "";
                _SQL = _SQL + "Update ";
                _SQL = _SQL + " projeto5w2hitemequipe ";
                _SQL = _SQL + "Set ";
                _SQL = _SQL + "    feedback    = '" + dto.getFeedback() + "' ";
                _SQL = _SQL + "   ,dtFeedback   = '" + dateFormat.format(_date) + "' " ;
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "    idprojeto5w2hitem = " + dto.getIdprojeto5w2hitem() + " ";
                _SQL = _SQL + "and idUsuario = " + dto.getIdusuario() + " ";
                    
                ConectaBanco _Banco = new ConectaBanco();
                
                _Retorno = _Banco.Commando(_SQL);

                return _Retorno;
                
            } catch (Exception ex) {
                return new Retorno(1, "Ta dando Nada Não " + ex.getMessage());
            }  

	}
        
        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("incluir/usuario")
	public Retorno AdicionarUsuario(Projeto5w2hDTO dto) {

            try {
        
                String _SQL;
                
                _SQL = "";
                _SQL = _SQL + "Insert Into ";
                _SQL = _SQL + " projeto5w2hitemequipe ";
                _SQL = _SQL + "   ( ";
                _SQL = _SQL + "     idProjeto5w2hitem ";
                _SQL = _SQL + "    ,idUsuario  ";
                _SQL = _SQL + "   ) ";
                _SQL = _SQL + " Values ";
                _SQL = _SQL + "   ( ";
                _SQL = _SQL + "    '" + dto.getIdprojeto5w2hitem() + "' " ;
                _SQL = _SQL + "   ,'" + dto.getEquipe() + "' " ;
                _SQL = _SQL + "   ) ";

                Retorno _Retorno;
                
                ConectaBanco _Banco = new ConectaBanco();
                
                _Retorno = _Banco.Commando(_SQL);

                if (_Retorno.getErro() == 1){
                    return new Retorno(1, "Não foi possível incluir o usuário !");
                }else{
                    return new Retorno(0, "Usuário incluido na equipe com sucesso !");
                }

            } catch (Exception ex) {
                return new Retorno(1, ex.getMessage());
            }  
            
	}                            
                            
        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("teste")
	public Retorno teste() {
		//return facade.criaCanvas(dto);
                return new Retorno(0, "Retornei Alguma Coisa");
	}

	
}
