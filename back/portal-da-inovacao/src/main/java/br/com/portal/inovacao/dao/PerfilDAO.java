package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.Perfil;

public class PerfilDAO {
	
	@Inject
	private EntityManager manager;
	
	public PerfilDAO() {}
	
	@SuppressWarnings("unchecked")
	public List<Perfil> pesquisaPerfilUsuario(String sigla) {
		Query query = manager.createQuery("from Perfil where sigla = '" + sigla + "'");
		return query.getResultList();
	}
	
	public List<Perfil> todos() {
		return manager.createQuery("from Perfil", Perfil.class).getResultList();
	}
	
	public Perfil pesquisaPorId(Long id) {
		return manager.find(Perfil.class, id);
	}
	
//	@SuppressWarnings("unchecked")
//	public List<Perfil> pesquisaPerfilAdministradorEmpresa() {
//		Query query = manager.createQuery("from Perfil where sigla = 'AE'");
//		return query.getResultList();
//	}
}
