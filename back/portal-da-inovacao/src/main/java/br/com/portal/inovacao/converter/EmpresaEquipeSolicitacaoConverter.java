package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.EmpresaEquipeSolicitacaoSaidaDTO;
import br.com.portal.inovacao.model.EmpresaEquipe;

public class EmpresaEquipeSolicitacaoConverter {
	
	public static EmpresaEquipeSolicitacaoSaidaDTO converterSolicitacaoEmpresaEquipe(EmpresaEquipe empresaEquipe) {
		EmpresaEquipeSolicitacaoSaidaDTO dto = new EmpresaEquipeSolicitacaoSaidaDTO();
		dto.setId_usuario(empresaEquipe.getUsuario().getId());
		dto.setEmail(empresaEquipe.getUsuario().getEmail());
		return dto;
	}
}
