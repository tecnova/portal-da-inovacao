package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.Usuario;

public class UsuarioDAO {
	
	@Inject
	private EntityManager manager;
	
	public UsuarioDAO() {}
	
	public void salva(Usuario usuario) {
		manager.getTransaction().begin();
		manager.persist(usuario);
		manager.getTransaction().commit();
	}

	public void atualiza(Usuario usuario) {
		manager.getTransaction().begin();
		manager.merge(usuario);
		manager.getTransaction().commit();
	}

	public void apaga(Usuario usuario) {
		manager.getTransaction().begin();
		manager.remove(manager.getReference(Usuario.class, usuario.getId()));
		manager.getTransaction().commit();
	}
	
	public void flush(Usuario usuario) {
		manager.getTransaction().begin();
		manager.persist(usuario);
	}
	
	public void commit() {
		manager.getTransaction().commit();
	}
	
	public Usuario pesquisaPorId(Long id) {
		return manager.find(Usuario.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> pesquisaIdFacebook(String idFacebook) {
		Query query = manager.createQuery("from Usuario where id_facebook = :idFacebook");
		query.setParameter("idFacebook", idFacebook);
		return query.getResultList(); 
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> pesquisaIdGooglePlus(String idGoogle) {
		Query query = manager.createQuery("from Usuario where id_google_plus = :idGoogle");
		query.setParameter("idGoogle", idGoogle);
		return query.getResultList(); 
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> pesquisaEmail(String email) {
		Query query = manager.createQuery("from Usuario where email = :email");
		query.setParameter("email", email);
		return query.getResultList(); 
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> pesquisaLoginESenha(String email, String senha) {
		Query query = manager.createQuery("from Usuario where email = :email and senha = :senha");
		query.setParameter("email", email);
		query.setParameter("senha", senha);
		return query.getResultList(); 
	}
	
	public List<Usuario> todos() {
		return manager.createQuery("from Usuario", Usuario.class).getResultList(); 
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> buscaUsuarios(Long idUsuario, String pesquisa) {
		Query query = manager.createQuery("from Usuario where (nome like'%" + pesquisa + "%'" + " OR email like'%" + pesquisa + "%')" + " and id <> :idUsuario");
		query.setParameter("idUsuario", idUsuario);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> pesquisaPorNome(String nome) {
		Query query = manager.createQuery("from Usuario where nome = :nome");
		query.setParameter("nome", nome);
		return query.getResultList();
	}
}
