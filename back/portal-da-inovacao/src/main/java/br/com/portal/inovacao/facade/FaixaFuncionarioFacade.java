package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.FaixaFuncionarioConverter.converterFaixaFuncionario;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.FaixaFuncionarioDAO;
import br.com.portal.inovacao.dto.FaixaFuncionarioDTO;
import br.com.portal.inovacao.model.FaixaFuncionario;

public class FaixaFuncionarioFacade {
	
	@Inject
	public FaixaFuncionarioDAO faixaFuncionarioDAO;
	
	public List<FaixaFuncionarioDTO> listaFaixaFuncionario() {
		List<FaixaFuncionario> todos = faixaFuncionarioDAO.todos();
		List<FaixaFuncionarioDTO> dtos = new ArrayList<FaixaFuncionarioDTO>();
		
		for (FaixaFuncionario faixaFuncionario : todos) {
			dtos.add(converterFaixaFuncionario(faixaFuncionario));
		}
		return dtos;
	}
}
