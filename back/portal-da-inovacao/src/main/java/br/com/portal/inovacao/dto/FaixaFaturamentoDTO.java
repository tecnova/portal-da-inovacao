package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class FaixaFaturamentoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private String valorInicial;
	private String valorFinal;

	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getValorInicial() {
		return valorInicial;
	}

	public String getValorFinal() {
		return valorFinal;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setValorInicial(String valorInicial) {
		this.valorInicial = valorInicial;
	}

	public void setValorFinal(String valorFinal) {
		this.valorFinal = valorFinal;
	}
}
