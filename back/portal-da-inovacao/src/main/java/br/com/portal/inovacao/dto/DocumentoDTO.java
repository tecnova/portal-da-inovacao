package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class DocumentoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long idEmpresa;
    private Long idHistoria;
    private String tpDocumento;
    private String descricao;
    private String documento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Long getIdHistoria() {
        return idHistoria;
    }

    public void setIdHistoria(Long idHistoria) {
        this.idHistoria = idHistoria;
    }

    public String getTpDocumento() {
        return tpDocumento;
    }

    public void setTpDocumento(String tpDocumento) {
        this.tpDocumento = tpDocumento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String Descricao) {
        this.descricao = Descricao;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String Documento) {
        this.documento = Documento;
    }

    
    
}
