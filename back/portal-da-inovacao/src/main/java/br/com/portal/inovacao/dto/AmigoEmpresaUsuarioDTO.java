package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class AmigoEmpresaUsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idUsuario;
	private Long idAmigoEmpresa;
	private Long idEmpresa;

	public Long getIdUsuario() {
		return idUsuario;
	}

	public Long getIdAmigoEmpresa() {
		return idAmigoEmpresa;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setIdAmigoEmpresa(Long idAmigoEmpresa) {
		this.idAmigoEmpresa = idAmigoEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
