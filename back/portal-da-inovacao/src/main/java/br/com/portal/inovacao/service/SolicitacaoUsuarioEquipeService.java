package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.SolicitacaoUsuarioEquipeDTO;
import br.com.portal.inovacao.facade.SolicitacaoUsuarioEquipeFacade;
import br.com.portal.inovacao.utils.Retorno;

@Path("solicitacao/equipe")
public class SolicitacaoUsuarioEquipeService {
	
	@Inject
	private SolicitacaoUsuarioEquipeFacade facade;
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("solicitar")
	public Retorno solicitaInclusaoUsuario(SolicitacaoUsuarioEquipeDTO dto) {
		return facade.criaSolicitacao(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("solicitacoes/pendentes")
	public Retorno solicitacoesPendentes(InformaIdDTO dto) {
		return facade.solicitacoesPendentes(dto);
	}
}
