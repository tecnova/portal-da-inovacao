package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class ImagemNoticia implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private Long id_noticia;

	public ImagemNoticia() {
	}

	public ImagemNoticia(Long id, String nome, Long id_noticia) {
		super();
		this.id = id;
		this.nome = nome;
		this.id_noticia = id_noticia;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public Long getId_noticia() {
		return id_noticia;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setId_noticia(Long id_noticia) {
		this.id_noticia = id_noticia;
	}
}
