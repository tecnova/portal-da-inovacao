package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.FaixaFuncionarioDTO;
import br.com.portal.inovacao.facade.FaixaFuncionarioFacade;

@Path("faixa/funcionario")
public class FaixaFuncionarioService {
	
	@Inject
	private FaixaFuncionarioFacade faixaFuncionarioFacade;
	
	@GET
	@Produces(APPLICATION_JSON)
	@Path("todos")
	public List<FaixaFuncionarioDTO> listaFaixaFuncionario() {
		return faixaFuncionarioFacade.listaFaixaFuncionario();
	}
}
