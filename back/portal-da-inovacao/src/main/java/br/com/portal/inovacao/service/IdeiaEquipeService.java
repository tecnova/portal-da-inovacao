package br.com.portal.inovacao.service;

import br.com.portal.inovacao.dto.HistoriaUsuarioEmpresaSaidaDTO;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.IdeiaEquipeDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.UsuarioDTO;
import br.com.portal.inovacao.facade.IdeiaEquipeFacade;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Path("ideia/equipe")
public class IdeiaEquipeService {
	
	@Inject
	private IdeiaEquipeFacade facade;
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("inserir")
	public Retorno adicionaUsuarioNaIdeiaEquipe(IdeiaEquipeDTO dto) {
		return facade.adicionaUsuarioNaIdeiaEquipe(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("excluir")
	public Retorno excluiUsuarioDaIdeia(IdeiaEquipeDTO dto) {
		return facade.excluiUsuarioDaIdeia(dto);
	}
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("listar")
	public Retorno buscaUsuariosDaIdeia(InformaIdDTO dto) {
            
            try {

                ConectaBanco _Banco = new ConectaBanco();

                String _SQL;

                //Procurando o Id do Projeto a Partir do id_brainstorming

                try{

                    IdeiaEquipeDTO _result = new IdeiaEquipeDTO();

                    List<UsuarioDTO> _lstUsuarioIdeia = new ArrayList<>();
                    List<UsuarioDTO> _lstUsuarioEmpresa = new ArrayList<>();

                    UsuarioDTO _itemdto;

                        
                    //Equipe da História
                    _SQL = "";
                    _SQL = _SQL + "SELECT " 
                                + "     usr.*  " 
                                + "FROM "
                                + "     usuario usr, "
                                + "     ideia_equipe ieq "
                                + "WHERE "
                                + "    ieq.id_usuario = usr.id  "
                                + "and ieq.id_ideia = " + dto.getId() + " ";

                    ResultSet _item = _Banco.Consulta(_SQL);

                    while (_item.next()) {

                        _itemdto = new UsuarioDTO();

                        _itemdto.setId(_item.getString("id"));
                        _itemdto.setNome(_item.getString("nome"));
                        _itemdto.setEmail(_item.getString("email"));
                        _itemdto.setImagem("");

                        _lstUsuarioIdeia.add(_itemdto);
                    }

                    _result.setLstUsuarioIdeia(_lstUsuarioIdeia);


                    //Equipe da Empresa
                    _SQL = "";
                    _SQL = _SQL + "SELECT " 
                                + "     usr.*  " 
                                + "FROM "
                                + "     usuario usr, "
                                + "     empresa_equipe eeq, "
                                + "     ideia ide "
                                + "WHERE "
                                + "    eeq.id_usuario = usr.id  "
                                + "and ide.id_empresa = eeq.id_empresa "
                                + "and ide.id = " + dto.getId() + " " 
                                + "and usr.id not in ( "
                                + "                     SELECT " 
                                + "                         ieq.id_usuario  " 
                                + "                     FROM "
                                + "                         ideia_equipe ieq "
                                + "                     WHERE "
                                + "                         ieq.id_ideia = ide.id ) ";

                    _item = _Banco.Consulta(_SQL);

                    while (_item.next()) {

                        _itemdto = new UsuarioDTO();

                        _itemdto.setId(_item.getString("id"));
                        _itemdto.setNome(_item.getString("nome"));
                        _itemdto.setEmail(_item.getString("email"));
                        _itemdto.setImagem("");

                        _lstUsuarioEmpresa.add(_itemdto);
                    }

                    _result.setLstUsuarioEmpresa(_lstUsuarioEmpresa);

                    return new Retorno(0, "Listagem Gerada com Sucesso", _result);

                }catch(Exception ex){
                    return new Retorno(0, ex.getMessage());
                }

            } catch (Exception ex) {
                return new Retorno(1, "Ta dando Nada Não " + ex.getMessage());
            }  
            
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("ideias/usuario")
	public Retorno buscaIdeiasDoUsuario(InformaIdDTO dto) {
		return facade.buscaIdeiasDoUsuario(dto);
	}
}