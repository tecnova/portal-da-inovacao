package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.IdeiaLinkConverter.converterIdeiaLink;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.IdeiaDAO;
import br.com.portal.inovacao.dao.IdeiaLinkDAO;
import br.com.portal.inovacao.dto.IdeiaLinkDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.model.Ideia;
import br.com.portal.inovacao.model.IdeiaLink;
import br.com.portal.inovacao.utils.Retorno;

public class IdeiaLinkFacade {

	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;
	private static final String MSG_NAO_E_POSSIVEL_ADICIONAR_LINK_SEM_TER_UMA_IDEIA_CRIADA = "não é possével adicionar link sem ter uma idéia criada.";
	private static final String MSG_LINK_ADICIONADO_COM_SUCESSO = "Link adicionado com sucesso.";
	private static final String MSG_LINK_REMOVIDO_COM_SUCESSO = "Link removido com sucesso.";
	private static final String MSG_LINK_NAO_ENCONTRADO = "Link não encontrado no banco de dados.";

	@Inject
	private IdeiaDAO ideiaDAO;

	@Inject
	private IdeiaLinkDAO dao;

	public Retorno criaIdeiaLink(IdeiaLinkDTO dto) {
		try {
			Ideia idIdeia = validaSeExisteIdeia(dto.getIdeia());

			IdeiaLink ideiaLink = new IdeiaLink();
			ideiaLink.setLink(dto.getLink());
			ideiaLink.setIdeia(idIdeia);
			dao.salva(ideiaLink);
			return new Retorno(CODIGO_SUCESSO, MSG_LINK_ADICIONADO_COM_SUCESSO,	converterIdeiaLink(ideiaLink));
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}

	private Ideia validaSeExisteIdeia(Long id) throws Exception {
		Ideia idIdeia = ideiaDAO.pesquisaPorId(id);

		if (idIdeia == null)
			throw new Exception(MSG_NAO_E_POSSIVEL_ADICIONAR_LINK_SEM_TER_UMA_IDEIA_CRIADA);

		return idIdeia;
	}

	public Retorno listaLinksDaIdeia(IdeiaLinkDTO dto) {
		List<IdeiaLink> linksPorIdeia = dao.pesquisaLinksPorIdeia(dto.getIdeia());
		List<IdeiaLinkDTO> dtos = new ArrayList<IdeiaLinkDTO>();
		
		for (IdeiaLink ideiaLink : linksPorIdeia) {
			dtos.add(converterIdeiaLink(ideiaLink));
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
	
	public Retorno excluiLink(InformaIdDTO dto) {
		IdeiaLink ideiaLink = dao.pesquisaPorId(dto.getId());
		
		if (ideiaLink == null)
			return new Retorno(CODIGO_ERRO, MSG_LINK_NAO_ENCONTRADO);

		try {
			dao.remove(ideiaLink);
			return new Retorno(CODIGO_SUCESSO, MSG_LINK_REMOVIDO_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
}
