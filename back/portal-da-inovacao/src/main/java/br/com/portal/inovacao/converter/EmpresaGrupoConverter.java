package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.EmpresaGrupoDTO;
import br.com.portal.inovacao.model.EmpresaGrupo;

public class EmpresaGrupoConverter {
	
	public static EmpresaGrupoDTO converteEmpresaGrupo(EmpresaGrupo empresaGrupo) {
		EmpresaGrupoDTO empresaGrupoDTO = new EmpresaGrupoDTO();
		empresaGrupoDTO.setId(empresaGrupo.getId());
		empresaGrupoDTO.setNome(empresaGrupo.getNome());
		empresaGrupoDTO.setIdEmpresa(empresaGrupo.getIdEmpresa().getId());
		return empresaGrupoDTO;
	}
}
