package br.com.portal.inovacao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "brainstorming")
public class Brainstorming {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 255)
	private String descricao;

	@ManyToOne
	@JoinColumn(name = "id_historia")
	private Ideia idHistoria;

	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario idUsuario;

	@ManyToOne
	@JoinColumn(name = "id_GrupoBrainstorming")
	private GrupoBrainstorming idGrupoBrainstorming;
	
	@Column(name = "data_criacao")
	private Date dataCriacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Ideia getIdHistoria() {
		return idHistoria;
	}

	public void setIdHistoria(Ideia idHistoria) {
		this.idHistoria = idHistoria;
	}

	public Usuario getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Usuario idUsuario) {
		this.idUsuario = idUsuario;
	}

	public GrupoBrainstorming getIdGrupoBrainstorming() {
		return idGrupoBrainstorming;
	}

	public void setIdGrupoBrainstorming(GrupoBrainstorming idGrupoBrainstorming) {
		this.idGrupoBrainstorming = idGrupoBrainstorming;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	
}
