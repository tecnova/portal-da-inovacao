package br.com.portal.inovacao.converter;

import java.text.SimpleDateFormat;

import br.com.portal.inovacao.dto.IdeiaDTO;
import br.com.portal.inovacao.model.Ideia;

public class IdeiaConverter {
	
	public static IdeiaDTO converterIdeia(Ideia ideia) {
            
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat dateReturn = new SimpleDateFormat("yyyy-MM-dd");
                
                
		
		IdeiaDTO dto = new IdeiaDTO();
		dto.setId(ideia.getId());
		dto.setTitulo(ideia.getTitulo());
		dto.setDescricao(ideia.getDescricao());
		dto.setUsuario(ideia.getUsuario().getId());
		dto.setDataCriacao(dateFormat.format(ideia.getDataCriacao()));
                
                if (ideia.getDataLimite() != null){
                    dto.setDataLimite(dateFormat.format(ideia.getDataLimite()));
                }else{
                    dto.setDataLimite(null);
                }
                    
		dto.setTipoIdeia(ideia.getTipoIdeia());
		dto.setEmpresa(ideia.getIdEmpresa().getId());
                dto.setPrioridade(ideia.getPrioridade());
		return dto;
	}
}
