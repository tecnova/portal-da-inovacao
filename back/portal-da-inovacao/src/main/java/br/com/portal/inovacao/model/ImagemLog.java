package br.com.portal.inovacao.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "imagem_log")
public class ImagemLog implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;

	@Column(name = "data_alteracao")
	private Date data;

	@ManyToOne
	private Noticia noticia;

	@ManyToOne
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public Date getData() {
		return data;
	}

	public Noticia getNoticia() {
		return noticia;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setNoticia(Noticia noticia) {
		this.noticia = noticia;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((noticia == null) ? 0 : noticia.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImagemLog other = (ImagemLog) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (noticia == null) {
			if (other.noticia != null)
				return false;
		} else if (!noticia.equals(other.noticia))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Imagem Log [id=" + id + ", data=" + data + ", noticia=" + noticia + ", usuario" + usuario + "]";
	}
}
