package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class EmpresaGrupoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long idEmpresa;
	private String nome;

	public Long getId() {
		return id;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public String getNome() {
		return nome;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
