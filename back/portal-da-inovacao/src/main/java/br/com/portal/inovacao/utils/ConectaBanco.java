/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.portal.inovacao.utils;

import java.sql.*;

/**
 *
 * @author notebook-inova
 */
public class ConectaBanco {
    
    private static final String _Driver = "com.mysql.jdbc.Driver";
//    private static final String _Connection = "jdbc:mysql://127.0.0.1:3306/portal_inovacao";
//    private static final String _User = "root";
//    private static final String _Password = "ipK23121428";

    private static final String _Connection = "jdbc:mysql://192.168.1.209:3306/portal_inovacao";
    private static final String _User = "root";
    private static final String _Password = "aw2000";
    private Statement stmt;
    private Connection conn;
    
    public ConectaBanco() {
        try{
            
            conn = null;

            Class.forName(_Driver);
            conn = DriverManager.getConnection(_Connection, _User, _Password);

            stmt = conn.createStatement();
            
        }catch(ClassNotFoundException | SQLException ex)
        {
            
        }
    }

    public Retorno Commando(String _SQL){
         
        try{
            
            stmt.executeUpdate(_SQL);
            
            return new Retorno(0, "Operação Efetuada com Sucesso !!");
            
        }catch(Exception ex)
        {
            return new Retorno(1, ex.getMessage());
        }
    }
    
    public ResultSet Consulta(String _SQL){
        
        try{
            
            ResultSet rs = stmt.executeQuery(_SQL);
            
            return rs;
            
        }catch(Exception ex)
        {
            return null;
        }
        
    }
    
    public Retorno InsertIntoReturnID(String _SQL){
        
        try{
            
           
            long _id;
            
            _id = 0;
            
            PreparedStatement pstmt = conn.prepareStatement(_SQL,
                                      Statement.RETURN_GENERATED_KEYS);
            
            int affectedRows = pstmt.executeUpdate();
            
            if (affectedRows > 0){
                
                try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        _id = generatedKeys.getLong(1);
                    }else {
                        return new Retorno(1,"Creating user failed, no ID obtained.");
                    }
                }
                
            }
            
            return new Retorno(0,"Operação Efetuada com Sucesso !!!", _id);
            
        }catch(Exception ex)
        {
            
            return new Retorno(1, ex.getMessage());
        }

    }
}
