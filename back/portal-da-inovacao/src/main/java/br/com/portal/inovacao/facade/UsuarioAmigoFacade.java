package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.UsuarioAmigoConverter.converterAmigo;
import static br.com.portal.inovacao.converter.UsuarioAmigoConverter.converterAmigos;
import static br.com.portal.inovacao.converter.UsuarioAmigoConverter.converterSolicitacoesPendentesAmigo;
import static br.com.portal.inovacao.converter.UsuarioAmigoConverter.converterSolicitacoesPendentesUsuario;
import static br.com.portal.inovacao.utils.DecodificaParaUTF8.decodificaParaUTF8;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.UsuarioAmigoDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.UsuarioDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.PesquisaUsuario;
import br.com.portal.inovacao.dto.SolicitacaoAmigoDTO;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.model.UsuarioAmigo;
import br.com.portal.inovacao.utils.Retorno;

public class UsuarioAmigoFacade {
	
	private static final int CODIGO_ERRO = 1;
	private static final int CODIGO_SUCESSO = 0;
	private static final String MSG_SOLICITACAO_ENVIADA_COM_SUCESSO = "Solicitação de amizade enviada com sucesso.";
	private static final String MSG_SOLICITACAO_DE_AMIZADE_JA_ENVIADA = "Solicitação de amizade já enviada.";
	private static final String MSG_AGORA_VOCES_SAO_AMIGOS = "Agora vocês são amigos";
	private static final String MSG_SNIF_UMA_PENA_NAO_TER_SUA_AMIZADE = "Snif! é uma pena não ter sua amizade";

	@Inject
	private UsuarioAmigoDAO dao;
	
	@Inject 
	private UsuarioDAO usuarioDao;
	
	public Retorno criaSolicitacao(SolicitacaoAmigoDTO solicitacaoAmigo) {
		try {
			validaSeSolicitacaoJaFoiEnviada(solicitacaoAmigo);
			UsuarioAmigo amigo = criaUsuarioAmigo(solicitacaoAmigo);
			dao.salva(amigo);
			return new Retorno(CODIGO_SUCESSO, MSG_SOLICITACAO_ENVIADA_COM_SUCESSO);
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}

	private void validaSeSolicitacaoJaFoiEnviada(SolicitacaoAmigoDTO solicitacaoAmigo) throws Exception {
		List<UsuarioAmigo> existeSolicitacao = dao.pesquisaSeExisteSolicitacao(solicitacaoAmigo.getUsuario().getId(), solicitacaoAmigo.getAmigo().getId());
		try {
			if (!existeSolicitacao.isEmpty())
			throw new Exception(MSG_SOLICITACAO_DE_AMIZADE_JA_ENVIADA);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	private UsuarioAmigo criaUsuarioAmigo(SolicitacaoAmigoDTO solicitacao) {
		UsuarioAmigo amigo = new UsuarioAmigo();
		amigo.setUsuario(usuarioDao.pesquisaPorId(solicitacao.getUsuario().getId()));
		amigo.setAmigo(usuarioDao.pesquisaPorId(solicitacao.getAmigo().getId()));
		amigo.setDataSolicitacao(new Date());
		amigo.setAprovado(false);
		amigo.setRecusado(false);
		return amigo;
	}
	
	public Retorno solicitacoesPendentesDoUsuario(SolicitacaoAmigoDTO idSolicitacao) {
		return new Retorno(CODIGO_SUCESSO, converterSolicitacoesPendentesUsuario(dao.pesquisaSolicitacoesPendentesDoUsuario(idSolicitacao.getUsuario().getId())));
	}
	
	public Retorno solicitacoesPendentesDoAmigo(SolicitacaoAmigoDTO idSolicitacao) {
		return new Retorno(CODIGO_SUCESSO, converterSolicitacoesPendentesAmigo(dao.pesquisaSolicitacoesPendentesDoAmigo(idSolicitacao.getAmigo().getId())));
	}
	
	public Retorno aprovaSolicitacaoAmizade(SolicitacaoAmigoDTO solicitacaoAprovada) {
		Usuario usuario = usuarioDao.pesquisaPorId(solicitacaoAprovada.getUsuario().getId());
		Usuario amigo = usuarioDao.pesquisaPorId(solicitacaoAprovada.getAmigo().getId());
		
		List<UsuarioAmigo> usuarioAmigo = dao.pesquisaSeExisteSolicitacao(usuario.getId(), amigo.getId());
		
		if (usuarioAmigo != null && solicitacaoAprovada.isAprovado()) {
			usuarioAmigo.get(0).setAprovado(true);
			usuarioAmigo.get(0).setDataAprovacao(new Date());
			dao.salva(usuarioAmigo.get(0));
		}
		return new Retorno(CODIGO_SUCESSO, MSG_AGORA_VOCES_SAO_AMIGOS, converterAmigo(amigo));
	}
	
	public Retorno recusaSolicitacaoAmizade(SolicitacaoAmigoDTO solicitacaoRecusada) {
		Usuario usuario = usuarioDao.pesquisaPorId(solicitacaoRecusada.getUsuario().getId());
		Usuario amigo = usuarioDao.pesquisaPorId(solicitacaoRecusada.getAmigo().getId());
		
		List<UsuarioAmigo> usuarioAmigo = dao.pesquisaSeExisteSolicitacao(usuario.getId(), amigo.getId());
		
		if (usuarioAmigo != null && solicitacaoRecusada.isRecusado()) {
			usuarioAmigo.get(0).setRecusado(true);
			usuarioAmigo.get(0).setDataRecusa(new Date());
			dao.salva(usuarioAmigo.get(0));
		}
		return new Retorno(CODIGO_SUCESSO, MSG_SNIF_UMA_PENA_NAO_TER_SUA_AMIZADE);
	}
	
	public Retorno listaAmigos(SolicitacaoAmigoDTO solicitacaoAmigo) {
		return new Retorno(CODIGO_SUCESSO, converterAmigos(dao.buscaAmigos(solicitacaoAmigo.getUsuario().getId())));
	}
	
	public List<Usuario> pesquisaUsuarios(PesquisaUsuario pesquisa) throws UnsupportedEncodingException {
		return usuarioDao.buscaUsuarios(pesquisa.getId(), decodificaParaUTF8(pesquisa.getPesquisa()));
	}
	
	public Retorno mostraAmigosDoUsuario(InformaIdDTO id) {
		List<Usuario> amigosDoUsuario = dao.buscaAmigosDoUsuario(id.getId());
		List<UsuarioDTO> amigos = new ArrayList<UsuarioDTO>();
		
		for (Usuario usuario : amigosDoUsuario) {
			amigos.add(converterAmigo(usuario));
		}
		return new Retorno(CODIGO_SUCESSO, amigos);
	}
}
