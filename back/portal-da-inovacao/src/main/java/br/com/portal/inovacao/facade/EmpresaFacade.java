package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.AmigoNaEmpresaConverter.converteAmigoEmpresa;
import static br.com.portal.inovacao.converter.CnaeConverter.converterCnae;
import static br.com.portal.inovacao.converter.EmpresaConverter.converterEmpresa;
import static br.com.portal.inovacao.converter.EmpresaPerfilUsuarioConverter.converterEmpresaPerfilUsuario;
import static br.com.portal.inovacao.converter.EmpresaUsuarioConverter.converterEmpresaUsuario;
import static br.com.portal.inovacao.converter.NotificaSolicitacaoEquipeEmpresaConverter.converterNotificacaoEquipeEmpresa;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.CnaeDAO;
import br.com.portal.inovacao.dao.EmpresaDAO;
import br.com.portal.inovacao.dao.EmpresaEquipeDAO;
import br.com.portal.inovacao.dao.PerfilDAO;
import br.com.portal.inovacao.dao.PerfilEmpresaDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.AmigoEmpresaDTO;
import br.com.portal.inovacao.dto.AmigoEmpresaUsuarioDTO;
import br.com.portal.inovacao.dto.CnaeDTO;
import br.com.portal.inovacao.dto.EmpresaDTO;
import br.com.portal.inovacao.dto.EmpresaPerfilUsuarioDTO;
import br.com.portal.inovacao.dto.EmpresaUsuarioDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.NotificaSolicitacaoEquipeEmpresaDTO;
import br.com.portal.inovacao.dto.PerfilAmigoEmpresaDTO;
import br.com.portal.inovacao.dto.PerfilDTO;
import br.com.portal.inovacao.dto.UsuarioDTO;
import br.com.portal.inovacao.model.Cnae;
import br.com.portal.inovacao.model.Empresa;
import br.com.portal.inovacao.model.EmpresaEquipe;
import br.com.portal.inovacao.model.Perfil;
import br.com.portal.inovacao.model.PerfilEmpresa;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.utils.Retorno;

public class EmpresaFacade {

	@Inject
	private EmpresaDAO empresaDAO;
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	@Inject
	private PerfilDAO perfilDAO;
	
	@Inject
	private PerfilEmpresaDAO perfilEmpresaDAO;
	
	@Inject
	private EmpresaEquipeDAO empresaEquipeDAO;
	
	@Inject
	private CnaeDAO cnaeDAO;
	
	private static final String MSG_CAMPO_RAZAO_SOCIAL_OBRIGATORIO = "Campo razão social é obrigatório.";
	private static final String MSG_CAMPO_CNPJ_OBRIGATORIO = "Campo CNPJ é obrigatório.";
	private static final String MSG_EMPRESA_SALVA_COM_SUCESSO = "Empresa salva com sucesso!";
	private static final String MSG_NENHUM_PERFIL_DE_EMPRESA_ENCONTRADO = "Nenhum perfil de empresa encontrado.";
	private static final String MSG_EMPRESA_JA_CADASTRADA = "Empresa já cadastrada.";
	private static final String MSG_EMPRESA_NAO_ENCONTRADA = "Empresa não encontrada.";
	private static final String MSG_EMPRESA_ALTERADA_COM_SUCESSO = "Empresa alterada com sucesso!";
	private static final String MSG_EMPRESA_EXCLUIDA_COM_SUCESSO = "Empresa excluída com sucesso!";
	private static final String MSG_NAO_FOI_ENCONTRADO_NENHUMA_EMPRESA_PARA_SER_EXCLUIDA = "Não foi encontrado nenhuma empresa para ser excluída.";
	private static final String MSG_NENHUMA_EMPRESA_ENCONTRADA = "Nenhuma empresa encontrada.";
	private static final String MSG_USUARIO_SEM_PERMISSAO = "Usuário sem permissão!";
	private static final String MSG_PERFIL_ALTERADO_COM_SUCESSO = "Perfil alterado com sucesso!";
	private static final String MSG_ERRO_AO_ALTERAR_O_PERFIL = "Erro ao alterar o perfil.";
	private static final String MSG_EMPRESA_DO_USUARIO_NAO_ENCONTRADA = "Empresa do usu�rio n�o encontrada.";
	private static final String MSG_USUARIO_REMOVIDO_DO_GRUPO_COM_SUCESSO = "Usuário removido do grupo com sucesso.";
	private static final String MSG_CONVITE_ACEITO = "Você aceitou o convite para participar do grupo da empresa.";
	private static final String MSG_CONVITE_RECUSADO = "Você recusou o convite para participar do grupo da empresa.";
	
	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;
	
	private Empresa criaEmpresa(EmpresaDTO dto) throws Exception {
		Empresa empresa = new Empresa();
		empresa.setCnpj(dto.getCnpj());
		empresa.setRazaoSocial(dto.getRazaoSocial());
		empresa.setLogradouro(dto.getLogradouro());
		empresa.setNumero(dto.getNumero());
		empresa.setComplemento(dto.getComplemento());
		empresa.setBairro(dto.getBairro());
		empresa.setMunicipio(dto.getMunicipio());
		empresa.setUf(dto.getUf());
		empresa.setCep(dto.getCep());
		empresa.setIdCnae(dto.getIdCnae());
		empresa.setIdFaixaFuncionario(dto.getIdFaixaFuncionario());
		empresa.setIdFaixaFaturamento(dto.getIdFaixaFaturamento());
		empresa.setAtivo(true);
		empresa.setImagem(false);
		return empresa;
	}
	
	public Retorno salvaEmpresa(EmpresaDTO dto) {
		try {
			validaDadosDeEntrada(dto);
			
			List<Empresa> cnpj = empresaDAO.pesquisaPorCnpj(dto.getCnpj());
			
			if (!cnpj.isEmpty())
				throw new Exception(MSG_EMPRESA_JA_CADASTRADA);
			
			Empresa empresa = criaEmpresa(dto);
			empresaDAO.flush(empresa);

			EmpresaEquipe empresaEquipe = criaEmpresaEquipe(dto, empresa);
			empresaEquipeDAO.salvar(empresaEquipe);

			empresaDAO.salvar(empresa);
			
			adicionaPerfilAdministradorEmpresaParaUsuario(dto);
			
			return new Retorno(CODIGO_SUCESSO, MSG_EMPRESA_SALVA_COM_SUCESSO, converterEmpresa(empresa));
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}

	private void adicionaPerfilAdministradorEmpresaParaUsuario(EmpresaDTO dto) {
		List<Perfil> perfilAdministradorEmpresa = perfilDAO.pesquisaPerfilUsuario("AE");
		Usuario usuario = usuarioDAO.pesquisaPorId(dto.getUsuario());

		if (!usuario.getPerfis().contains(perfilAdministradorEmpresa.get(0))) {
			usuario.getPerfis().addAll(perfilAdministradorEmpresa);
			usuarioDAO.atualiza(usuario);
		}
	}
	
	private void validaDadosDeEntrada(EmpresaDTO empresaDTO) throws Exception {
		if (empresaDTO.getCnpj() == null || empresaDTO.getCnpj().trim().equals(MSG_EMPRESA_DO_USUARIO_NAO_ENCONTRADA))
			throw new Exception(MSG_CAMPO_CNPJ_OBRIGATORIO);
		
		if (empresaDTO.getRazaoSocial() == null || empresaDTO.getRazaoSocial().trim().equals(MSG_EMPRESA_DO_USUARIO_NAO_ENCONTRADA))
			throw new Exception(MSG_CAMPO_RAZAO_SOCIAL_OBRIGATORIO);
	}
	
	private EmpresaEquipe criaEmpresaEquipe(EmpresaDTO dto, Empresa empresa) throws Exception {
		List<PerfilEmpresa> perfilAdministrador = perfilEmpresaDAO.pesquisaPerfilAdministrador("AD");
		
		if (perfilAdministrador.isEmpty())
			throw new Exception(MSG_NENHUM_PERFIL_DE_EMPRESA_ENCONTRADO);
		
		EmpresaEquipe empresaEquipe = new EmpresaEquipe();
		empresaEquipe.setEmpresa(empresaDAO.pesquisaPorId(empresa.getId()));
		empresaEquipe.setUsuario(usuarioDAO.pesquisaPorId(dto.getUsuario()));
		empresaEquipe.setPerfilEmpresa(perfilAdministrador.get(0));
		empresaEquipe.setSolicitacaoAprovada(true);
		empresaEquipe.setDataAprovacao(new Date());
		empresaEquipe.setDataSolicitacao(new Date());
		empresaEquipe.setEnvioEmail(false);
		return empresaEquipe;
	}
	
	@SuppressWarnings("unused")
	public Retorno alteraEmpresa(EmpresaDTO dto) {
		Empresa empresa = empresaDAO.pesquisaPorId(dto.getIdEmpresa());

		List<EmpresaEquipe> perfilAdministrador = empresaEquipeDAO.buscaPerfilAdministrador(dto.getUsuario(), empresa.getId());
		
		if (empresa == null)
			return new Retorno(CODIGO_ERRO, MSG_EMPRESA_NAO_ENCONTRADA);
		
		if (perfilAdministrador.isEmpty())
			return new Retorno(CODIGO_ERRO, MSG_USUARIO_SEM_PERMISSAO);
			
		if (dto.getCnpj() != null)
			empresa.setCnpj(dto.getCnpj());
		
		if (dto.getRazaoSocial() != null)
			empresa.setRazaoSocial(dto.getRazaoSocial());
		
		if (dto.getLogradouro() != null)
			empresa.setLogradouro(dto.getLogradouro());
		
		if (dto.getNumero() != null)
			empresa.setNumero(dto.getNumero());
		
		if (dto.getComplemento() != null)
			empresa.setComplemento(dto.getComplemento());
		
		if (dto.getBairro() != null)
			empresa.setBairro(dto.getBairro());
		
		if (dto.getMunicipio() != null)
			empresa.setMunicipio(dto.getMunicipio());
		
		if (dto.getUf() != null)
			empresa.setUf(dto.getUf());
		
		if (dto.getCep() != null)
			empresa.setCep(dto.getCep());
		
		if (dto.getIdCnae() != null)
			empresa.setIdCnae(dto.getIdCnae());
		
		if (dto.getIdFaixaFuncionario() != null)
			empresa.setIdFaixaFuncionario(dto.getIdFaixaFuncionario());
		
		if (dto.getIdFaixaFaturamento() != null)
			empresa.setIdFaixaFaturamento(dto.getIdFaixaFaturamento());
		
		empresaDAO.salvar(empresa);
		
		return new Retorno(CODIGO_SUCESSO, MSG_EMPRESA_ALTERADA_COM_SUCESSO);
	}
	
	@SuppressWarnings("unused")
	public Retorno inativaEmpresa(EmpresaUsuarioDTO dto) {
		Empresa empresa = empresaDAO.pesquisaPorId(dto.getIdEmpresa());
		
		List<EmpresaEquipe> perfilAdministrador = empresaEquipeDAO.buscaPerfilAdministrador(dto.getIdUsuario(), empresa.getId());
		
		if (perfilAdministrador.isEmpty())
			return new Retorno(CODIGO_ERRO, MSG_USUARIO_SEM_PERMISSAO);
		
		if (empresa != null) {
			empresa.setAtivo(false);
			empresaDAO.salvar(empresa);
			return new Retorno(CODIGO_SUCESSO, MSG_EMPRESA_EXCLUIDA_COM_SUCESSO);
		}
		return new Retorno(CODIGO_ERRO, MSG_NAO_FOI_ENCONTRADO_NENHUMA_EMPRESA_PARA_SER_EXCLUIDA);
	}
	
	public List<CnaeDTO> listaCnaes() {
		List<Cnae> cnaes = cnaeDAO.todos();
		List<CnaeDTO> dtos = new ArrayList<CnaeDTO>();
		
		for (Cnae cnae : cnaes) {
			dtos.add(converterCnae(cnae));
		}
		return dtos;
	}
	
	@SuppressWarnings("rawtypes")
	public Retorno buscaEmpresasEPerfilDoUsuario(InformaIdDTO dto) {
		Usuario usuario = usuarioDAO.pesquisaPorId(dto.getId());
		List empresaDoUsuarioComPerfil = empresaEquipeDAO.buscaEmpresaDoUsuarioComPerfil(usuario.getId());
		List<EmpresaPerfilUsuarioDTO> dtoss = new ArrayList<EmpresaPerfilUsuarioDTO>();
		List<EmpresaEquipe> empresaEquipes = new ArrayList<EmpresaEquipe>();
		
		for (int i = 0; i < empresaDoUsuarioComPerfil.size(); i++) {
			Object[] a = (Object[]) empresaDoUsuarioComPerfil.get(i);
			EmpresaEquipe empresaEquipe = new EmpresaEquipe();
			
			for (Object object : a) {
				if (object instanceof BigInteger) {
					Empresa empresa = empresaDAO.pesquisaPorId(((BigInteger) object).longValue());
					
					if (empresa != null)
						empresaEquipe.setEmpresa(empresa);
				} else {
					List<Usuario> usr = usuarioDAO.pesquisaPorNome((String)object);
					List<PerfilEmpresa> perfil = perfilEmpresaDAO.pesquisaPorNome((String)object);
					
					if (!usr.isEmpty())
						empresaEquipe.setUsuario(usr.get(0));
					
					if (!perfil.isEmpty())
						empresaEquipe.setPerfilEmpresa(perfil.get(0));
				}
			}
			empresaEquipes.add(empresaEquipe);
		}
		
		for (EmpresaEquipe empresaEquipe : empresaEquipes) {
			dtoss.add(converterEmpresaPerfilUsuario(empresaEquipe));
		}
		return new Retorno(CODIGO_SUCESSO, dtoss);
	}
	
	public Retorno buscaEmpresaDoUsuario(EmpresaUsuarioDTO dto) {
		List<EmpresaEquipe> empresaDoUsuario = empresaEquipeDAO.buscaEmpresaDoUsuario(dto.getIdUsuario(), dto.getIdEmpresa());
		Empresa empresa = empresaDAO.pesquisaPorId(dto.getIdEmpresa());
		
		if (empresaDoUsuario.isEmpty())
			return new Retorno(CODIGO_ERRO, MSG_NENHUMA_EMPRESA_ENCONTRADA);
		
		return new Retorno(CODIGO_SUCESSO, converterEmpresaUsuario(empresaDoUsuario.get(0),
				empresaDoUsuario.get(0).getEmpresa().getIdFaixaFaturamento(), 
				empresaDoUsuario.get(0).getEmpresa().getIdFaixaFuncionario(), 
				empresaDoUsuario.get(0).getEmpresa().getIdCnae(),
				empresa.isImagem()));
	}
	
	@SuppressWarnings("rawtypes")
	public Retorno buscaAmigosNaEmpresa(EmpresaUsuarioDTO dto) {
            List<EmpresaEquipe> usuarioNaEmpresa = empresaEquipeDAO.buscaUsuarioNaEmpresa(dto.getIdEmpresa(), dto.getIdUsuario());

            if (usuarioNaEmpresa.isEmpty())
                return new Retorno(CODIGO_ERRO, MSG_USUARIO_SEM_PERMISSAO);

            List amigosNaEmpresa = empresaEquipeDAO.buscaAmigosNaEmpresa(dto.getIdEmpresa());
            List<AmigoEmpresaDTO> dtoss = new ArrayList<AmigoEmpresaDTO>();
            List<Usuario> usuarios = new ArrayList<Usuario>();
            List<EmpresaEquipe> empresaEquipes = new ArrayList<EmpresaEquipe>();

            for (int i = 0; i < amigosNaEmpresa.size(); i++) {
                Object[] a = (Object[]) amigosNaEmpresa.get(i);
                EmpresaEquipe empresaEquipe = new EmpresaEquipe();
                Usuario usuario = new Usuario();

                for (Object object : a) {
                    if (object instanceof BigInteger) {
                        Usuario user = usuarioDAO.pesquisaPorId(((BigInteger)object).longValue());

                        if (user != null)
                                usuario.setId(user.getId());
                    } else {
                        List<Usuario> usr = usuarioDAO.pesquisaPorNome((String)object);
                        List<PerfilEmpresa> perfil = perfilEmpresaDAO.pesquisaPorNome((String)object);

                        if (!usr.isEmpty())
                                usuario.setNome(usr.get(0).getNome());

                        if (!usr.isEmpty())
                                usuario.setEmail(usr.get(0).getEmail());

                        if (!usr.isEmpty())
                                empresaEquipe.setUsuario(usr.get(0));

                        if (!perfil.isEmpty())
                                empresaEquipe.setPerfilEmpresa(perfil.get(0));
                    }
                }
                usuarios.add(usuario);
                empresaEquipes.add(empresaEquipe);
            }

            for (EmpresaEquipe empresaEquipe : empresaEquipes) {
                    dtoss.add(converteAmigoEmpresa(empresaEquipe.getUsuario(), empresaEquipe));
            }
            return new Retorno(CODIGO_SUCESSO, dtoss);
	}
	
	public Retorno alteraPerfil(PerfilAmigoEmpresaDTO perfilAmigoEmpresa) {
		List<EmpresaEquipe> perfilAdministrador = empresaEquipeDAO.buscaPerfilAdministrador(perfilAmigoEmpresa.getIdUsuario(), perfilAmigoEmpresa.getIdEmpresa());
		
		if (perfilAdministrador.isEmpty())
			return new Retorno(CODIGO_ERRO, MSG_USUARIO_SEM_PERMISSAO);
		
		Usuario amigoEmpresa = usuarioDAO.pesquisaPorId(perfilAmigoEmpresa.getIdAmigoEmpresa());
		List<EmpresaEquipe> empresaDoUsuario = empresaEquipeDAO.buscaEmpresaDoUsuario(perfilAmigoEmpresa.getIdAmigoEmpresa(), perfilAmigoEmpresa.getIdEmpresa());
		
		if (amigoEmpresa != null) {
			for (PerfilDTO dto : perfilAmigoEmpresa.getPerfis()) {
				PerfilEmpresa perfilEmpresa = perfilEmpresaDAO.pesquisaPorId(dto.getId());
				empresaDoUsuario.get(0).setPerfilEmpresa(perfilEmpresa);
			}
			empresaEquipeDAO.salvar(empresaDoUsuario.get(0));
			return new Retorno(CODIGO_SUCESSO, MSG_PERFIL_ALTERADO_COM_SUCESSO);
		}
		return new Retorno(CODIGO_ERRO, MSG_ERRO_AO_ALTERAR_O_PERFIL);
	}
	
	public Retorno removeAmigoDaEmpresa(AmigoEmpresaUsuarioDTO amigoEmpresaUsuarioDTO) {
		List<EmpresaEquipe> perfilAdministrador = empresaEquipeDAO.buscaPerfilAdministrador(
				amigoEmpresaUsuarioDTO.getIdUsuario(), 
				amigoEmpresaUsuarioDTO.getIdEmpresa());
		
		if (perfilAdministrador.isEmpty())
			return new Retorno(CODIGO_ERRO, MSG_USUARIO_SEM_PERMISSAO);
		
		List<EmpresaEquipe> empresaDoUsuario = empresaEquipeDAO.buscaEmpresaDoUsuario(amigoEmpresaUsuarioDTO.getIdAmigoEmpresa(), amigoEmpresaUsuarioDTO.getIdEmpresa());
		
		if (empresaDoUsuario.isEmpty())
			return new Retorno(CODIGO_ERRO, MSG_EMPRESA_DO_USUARIO_NAO_ENCONTRADA);
		
		empresaEquipeDAO.excluir(empresaDoUsuario.get(0));
		return new Retorno(CODIGO_SUCESSO, MSG_USUARIO_REMOVIDO_DO_GRUPO_COM_SUCESSO);
	}
	
	public Retorno aprovaSolicitacaoGrupo(EmpresaUsuarioDTO empresaUsuarioDTO) {
            Usuario usuario = usuarioDAO.pesquisaPorId(empresaUsuarioDTO.getIdUsuario());
            Empresa empresa = empresaDAO.pesquisaPorId(empresaUsuarioDTO.getIdEmpresa());

            List<EmpresaEquipe> solicitacao = empresaEquipeDAO.pesquisaSeExisteSolicitacao(usuario.getId(), empresa.getId());
            List<EmpresaEquipe> empresaEquipe = empresaEquipeDAO.buscaEmpresaDoUsuario(usuario.getId(), empresa.getId());

            if (!(solicitacao.isEmpty() && empresaEquipe.isEmpty())) {
                empresaEquipe.get(0).setSolicitacaoAprovada(true);
                empresaEquipe.get(0).setDataAprovacao(new Date());
                empresaEquipeDAO.salvar(empresaEquipe.get(0));
                return new Retorno(CODIGO_SUCESSO, MSG_CONVITE_ACEITO);
            }else{
                return new Retorno(CODIGO_ERRO, "N�o foi poss�vel gravar a solicita��o !!");
            }
	}

	public Retorno recusaSolicitacaoGrupo(EmpresaUsuarioDTO empresaUsuarioDTO) {
		Usuario usuario = usuarioDAO.pesquisaPorId(empresaUsuarioDTO.getIdUsuario());
		Empresa empresa = empresaDAO.pesquisaPorId(empresaUsuarioDTO.getIdEmpresa());
		
		List<EmpresaEquipe> solicitacao = empresaEquipeDAO.pesquisaSeExisteSolicitacao(usuario.getId(), empresa.getId());
		List<EmpresaEquipe> empresaEquipe = empresaEquipeDAO.buscaEmpresaDoUsuario(usuario.getId(), empresa.getId());
		
		if (!(solicitacao.isEmpty() && empresaEquipe.isEmpty())) {
			empresaEquipe.get(0).setDataRecusa(new Date());
			empresaEquipeDAO.salvar(empresaEquipe.get(0));
		}
		return new Retorno(CODIGO_SUCESSO, MSG_CONVITE_RECUSADO);
	}
	
	@SuppressWarnings("rawtypes")
	public Retorno notificaSolicitacaoEquipeEmpresa(InformaIdDTO idUsuario) {
		List solicitacaoEquipeEmpresa = empresaEquipeDAO.notificaSolicitacaoEquipeEmpresa(idUsuario.getId());
		List<NotificaSolicitacaoEquipeEmpresaDTO> dtos = new ArrayList<NotificaSolicitacaoEquipeEmpresaDTO>();		
		
		for (int i = 0; i < solicitacaoEquipeEmpresa.size(); i++) {
			Object[] a = (Object[]) solicitacaoEquipeEmpresa.get(i);
			EmpresaEquipe empresaEquipe = new EmpresaEquipe();
			
			for (Object object : a) {
				if (object instanceof BigInteger)
					empresaEquipe.setId(((BigInteger)object).longValue());
				else {
					List<Empresa> empresa = empresaDAO.pesquisaPorRazaoSocial((String)object);
					
					if (!empresa.isEmpty())
						empresaEquipe.setEmpresa(empresa.get(0));
				}
			}
			dtos.add(converterNotificacaoEquipeEmpresa(empresaEquipe));
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
	
	public Retorno listaUsuariosDaEmpresa(InformaIdDTO idEmpresa) {
		List<EmpresaEquipe> usuariosDaEmpresa = empresaEquipeDAO.listaUsuariosDaEmpresa(idEmpresa.getId());
		List<UsuarioDTO> dtos = new ArrayList<UsuarioDTO>();
		
		for (EmpresaEquipe empresaEquipe : usuariosDaEmpresa) {
			UsuarioDTO dto = new UsuarioDTO();
			dto.setId(empresaEquipe.getUsuario().getId().toString());
			dto.setNome(empresaEquipe.getUsuario().getNome());
			dto.setEmail(empresaEquipe.getUsuario().getEmail());
			dtos.add(dto);
		}
		return new Retorno(CODIGO_SUCESSO, dtos);
	}
}
