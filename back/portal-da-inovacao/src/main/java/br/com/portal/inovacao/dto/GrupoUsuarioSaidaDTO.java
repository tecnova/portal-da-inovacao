package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.List;

public class GrupoUsuarioSaidaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idGrupo;
    private String grupo;
    private Long idEmpresa;
    private List<UsuarioDTO> lstUsuarioEmpresa;
    private List<UsuarioDTO> lstUsuarioGrupo;

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public Long getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Long idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public List<UsuarioDTO> getLstUsuarioEmpresa() {
        return lstUsuarioEmpresa;
    }

    public void setLstUsuarioEmpresa(List<UsuarioDTO> lstUsuarioEmpresa) {
        this.lstUsuarioEmpresa = lstUsuarioEmpresa;
    }

    public List<UsuarioDTO> getLstUsuarioGrupo() {
        return lstUsuarioGrupo;
    }

    public void setLstUsuarioGrupo(List<UsuarioDTO> lstUsuarioGrupo) {
        this.lstUsuarioGrupo = lstUsuarioGrupo;
    }


    
    
}
