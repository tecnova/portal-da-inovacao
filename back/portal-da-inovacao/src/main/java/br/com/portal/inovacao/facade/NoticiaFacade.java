package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.utils.DecodificaParaUTF8.decodificaParaUTF8;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.portal.inovacao.dao.ImagemDAO;
import br.com.portal.inovacao.dao.NoticiaDAO;
import br.com.portal.inovacao.dao.NoticiaLogDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.BuscaNoticiaPorId;
import br.com.portal.inovacao.dto.CriaNoticia;
import br.com.portal.inovacao.dto.InativaNoticia;
import br.com.portal.inovacao.model.Imagem;
import br.com.portal.inovacao.model.Noticia;
import br.com.portal.inovacao.model.NoticiaLog;
import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.utils.Retorno;

public class NoticiaFacade {

	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;

	private static final String MSG_TITULO_NULO = "Título não pode ser vazio.";
	private static final String MSG_DESCRICAO_NULO = "Descrição não pode ser vazia.";
	private static final String MSG_TEXTO_NULO = "Texto não pode ser vazio.";
	private static final String MSG_AUTOR_NULO = "Autor não pode ser vazio.";
	private static final String MSG_CATEGORIA_NULO = "Categoria não pode ser vazia.";
	private static final String MSG_SETOR_NULO = "Setor não pode ser vazio.";

	private static String UPLOAD_FILE_SERVER = "/var/www/html/imagens/noticia/";
	
	private static String ACAO_ALTERAR = "ALTERA��O";
	private static String ACAO_REMOVER = "REMO��O";

	@Inject
	private NoticiaDAO noticiaDao;

	@Inject
	private UsuarioDAO usuarioDao;

	@Inject
	private ImagemDAO imagemDao;

	@Inject
	private NoticiaLogDAO noticiaLogDao;
	
	@Inject
	private EntityManager em;

	private String validaDadosDeEntrada(CriaNoticia dto) throws Exception {
		if (dto.getTitulo().equals(""))
			throw new Exception(MSG_TITULO_NULO);
		if (dto.getDescricao().equals(""))
			throw new Exception(MSG_DESCRICAO_NULO);
		if (dto.getTexto().equals(""))
			throw new Exception(MSG_TEXTO_NULO);
		if (dto.getAutor().equals(""))
			throw new Exception(MSG_AUTOR_NULO);
		if (dto.getCategoria() == 0)
			throw new Exception(MSG_CATEGORIA_NULO);
		if (noticiaDao.pesquisaCategoriaPorId((long) dto.getCategoria()) == null)
			throw new Exception("ID CATEGORIA NAO EXISTE NO BD");
		if (dto.getSetor() == 0)
			throw new Exception(MSG_SETOR_NULO);
		if (noticiaDao.pesquisaSetorPorId((long) dto.getSetor()) == null)
			throw new Exception("ID SETOR NAO EXISTE NO BD");
		return "Sucesso";
	}

	public Retorno inativaNoticia(InativaNoticia dto) {
		Noticia noticia = noticiaDao.pesquisaNoticiaPorId(dto.getId());
		if (noticia != null) {
			try {
				noticia.setAtivo(false);
				NoticiaLog noticiaLog = preparaLog(ACAO_REMOVER, noticia, dto.getUsuario());
				noticiaLogDao.salva(noticiaLog);
				noticiaDao.inativaNoticia(noticia);
				return new Retorno(CODIGO_SUCESSO, "Not�cia removida com sucesso!");
			} catch (Exception e) {
				return new Retorno(CODIGO_ERRO, e.getMessage());
			}
		}
		return new Retorno(0, "N�o foi encontrada nenhuma not�cia para ser removida.");
	}

	public Noticia noticiaPorId(BuscaNoticiaPorId dto) {
		return noticiaDao.pesquisaNoticiaPorId(dto.getId());
	}

	public Noticia criaNoticia(CriaNoticia dto) throws UnsupportedEncodingException {
		Date date = new Date();
		java.sql.Date data = new java.sql.Date(date.getTime());

		Noticia noticia = new Noticia();
		noticia.setTitulo(decodificaParaUTF8(dto.getTitulo()));
		noticia.setDescricao(decodificaParaUTF8(dto.getDescricao()));
		noticia.setTexto(dto.getTexto());
		noticia.setLink(dto.getLink());
		noticia.setAutor(decodificaParaUTF8(dto.getAutor()));
		noticia.setCategoria(noticiaDao.pesquisaCategoriaPorId(dto.getCategoria()));
		noticia.setSetor(noticiaDao.pesquisaSetorPorId(dto.getSetor()));
		noticia.setData(data);
		noticia.setUsuario(usuarioDao.pesquisaPorId(dto.getUsuario()));
		noticia.setPrivado(dto.isPrivado());
		noticia.setAtivo(true);
		return noticia;
	}

	private Imagem criaImagem(Noticia noticia) {
		Date date = new Date();
		java.sql.Date data = new java.sql.Date(date.getTime());

		Imagem imagem = new Imagem();
		imagem.setNome(noticia.getTitulo());
		imagem.setNoticia(noticiaDao.pesquisaNoticiaPorId(noticia.getId()));
		imagem.setData(data);
		return imagem;
	}

	public Retorno salvaNoticia(MultipartFormDataInput multipartFormDataInput) throws Exception {
		String fileName = null;

		try {
			Map<String, List<InputPart>> map = multipartFormDataInput.getFormDataMap();

			CriaNoticia criaNoticia = criaNoticiaDTO(map);

			validaDadosDeEntrada(criaNoticia);

			Noticia noticia = criaNoticia(criaNoticia);
			noticiaDao.flush(noticia);
			em.getTransaction().commit();

			if (map.get("uploadedFile") != null) {
				Imagem imagem = criaImagem(noticia);
				imagemDao.flush(imagem);
				em.getTransaction().commit();
				
				fileName = imagem.getId() + ".jpg";
				
				enviaImagemParaServidor(fileName, map);
				
				noticia.setImagem(fileName);
				noticiaDao.atualiza(noticia);
			}
			return new Retorno(CODIGO_SUCESSO, "Not�cia salva com sucesso!");
		} catch (Exception ioe) {
			return new Retorno(CODIGO_ERRO, ioe.getMessage());
		}
	}

	private void enviaImagemParaServidor(String fileName, Map<String, List<InputPart>> map) throws IOException {
		InputStream inputStream;
		List<InputPart> lstInputPart = map.get("uploadedFile");

		if (lstInputPart != null) {
			for (InputPart inputPart : lstInputPart) {
				if (null != fileName && !"".equalsIgnoreCase(fileName)) {
					inputStream = inputPart.getBody(InputStream.class, null);
					writeToFileServer(inputStream, fileName);
					inputStream.close();
				}
			}
		}
	}

	private CriaNoticia criaNoticiaDTO(Map<String, List<InputPart>> map) throws IOException {
		CriaNoticia criaNoticia = new CriaNoticia();
		criaNoticia.setCategoria(Long.parseLong(map.get("categoria").get(0).getBodyAsString()));
		criaNoticia.setTitulo(map.get("titulo").get(0).getBodyAsString());
		criaNoticia.setSetor(Long.parseLong(map.get("setor").get(0).getBodyAsString()));
		criaNoticia.setAutor(map.get("autor").get(0).getBodyAsString());
		criaNoticia.setTexto(map.get("texto").get(0).getBodyAsString());
		criaNoticia.setUsuario(Long.parseLong(map.get("usuario").get(0).getBodyAsString()));
		criaNoticia.setLink(map.get("link").get(0).getBodyAsString());
		criaNoticia.setDescricao(map.get("descricao").get(0).getBodyAsString());
		criaNoticia.setPrivado(Boolean.parseBoolean(map.get("isPrivado").get(0).getBodyAsString()));
		return criaNoticia;
	}

	private String writeToFileServer(InputStream inputStream, String fileName) throws IOException {
		OutputStream outputStream = null;
		String qualifiedUploadFilePath = UPLOAD_FILE_SERVER + fileName;

		try {
			outputStream = new FileOutputStream(new File(qualifiedUploadFilePath));
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.flush();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			outputStream.close();
		}
		return qualifiedUploadFilePath;
	}
	
	public Retorno alteraNoticia(MultipartFormDataInput multipartFormDataInput) throws Exception, IOException {
		Map<String, List<InputPart>> map = multipartFormDataInput.getFormDataMap();
		
		Noticia noticia = noticiaDao.pesquisaNoticiaPorId(Long.parseLong(map.get("id").get(0).getBodyAsString()));
		
		if (noticia == null)
			new Retorno(CODIGO_ERRO, "Id informado inexistente.");
		
		try {
			if (map.get("titulo") != null)
				noticia.setTitulo(decodificaParaUTF8(map.get("titulo").get(0).getBodyAsString()));                           
			
			if (map.get("descricao") != null)
				noticia.setDescricao(decodificaParaUTF8(map.get("descricao").get(0).getBodyAsString()));                     

			if (map.get("texto") != null)
				noticia.setTexto(map.get("texto").get(0).getBodyAsString());                           
			
			if (map.get("autor") != null)
				noticia.setAutor(decodificaParaUTF8(map.get("autor").get(0).getBodyAsString()));                           
			
			if (map.get("link") != null)
				noticia.setLink(map.get("link").get(0).getBodyAsString());                               
			
			if (map.get("isPrivado") != null)
				noticia.setPrivado(Boolean.parseBoolean(map.get("isPrivado").get(0).getBodyAsString())); 
			
			if (map.get("setor") != null)
				noticia.setSetor(noticiaDao.pesquisaSetorPorId(Long.parseLong(map.get("setor").get(0).getBodyAsString())));           
			
			if (map.get("categoria") != null)
				noticia.setCategoria(noticiaDao.pesquisaCategoriaPorId(Long.parseLong(map.get("categoria").get(0).getBodyAsString())));
			
			NoticiaLog noticiaLog = preparaLog(ACAO_ALTERAR, noticia, map);
			noticiaLogDao.salva(noticiaLog);
			noticiaDao.atualiza(noticia);

			return new Retorno(CODIGO_SUCESSO, "Not�cia alterada com sucesso!");
		} catch (Exception e) {
			return new Retorno(CODIGO_ERRO, e.getMessage());
		}
	}
	
	private NoticiaLog preparaLog(String acao, Noticia noticia, Map<String, List<InputPart>> map) throws Exception {
		Date date = new Date();
		java.sql.Date data = new java.sql.Date(date.getTime());

		NoticiaLog noticiaLog = new NoticiaLog();
		noticiaLog.setAcao(acao);
		noticiaLog.setData(data);
		noticiaLog.setNoticia(noticiaDao.pesquisaNoticiaPorId(noticia.getId()));
		noticiaLog.setUsuario(usuarioDao.pesquisaPorId(Long.parseLong(map.get("usuario").get(0).getBodyAsString())));
		return noticiaLog;
	}
	
	private NoticiaLog preparaLog(String acao, Noticia noticia, Usuario usuario) throws Exception {
		Date date = new Date();
		java.sql.Date data = new java.sql.Date(date.getTime());

		NoticiaLog noticiaLog = new NoticiaLog();
		noticiaLog.setAcao(acao);
		noticiaLog.setData(data);
		noticiaLog.setNoticia(noticiaDao.pesquisaNoticiaPorId(noticia.getId()));
		noticiaLog.setUsuario(usuarioDao.pesquisaPorId(usuario.getId()));
		return noticiaLog;
	}
}
