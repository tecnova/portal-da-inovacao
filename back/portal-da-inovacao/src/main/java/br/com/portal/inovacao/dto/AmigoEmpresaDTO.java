package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class AmigoEmpresaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idUsuario;
    private String perfil;
    private String nome;
    private String email;
    private String boConfirmado;

    public Long getIdUsuario() {
            return idUsuario;
    }

    public String getPerfil() {
            return perfil;
    }

    public String getNome() {
            return nome;
    }

    public String getEmail() {
            return email;
    }

    public void setIdUsuario(Long idUsuario) {
            this.idUsuario = idUsuario;
    }

    public void setPerfil(String perfil) {
            this.perfil = perfil;
    }

    public void setNome(String nome) {
            this.nome = nome;
    }

    public void setEmail(String email) {
            this.email = email;
    }

    public String getBoConfirmado() {
        return boConfirmado;
    }

    public void setBoConfirmado(String boConfirmado) {
        this.boConfirmado = boConfirmado;
    }
    
    
}
