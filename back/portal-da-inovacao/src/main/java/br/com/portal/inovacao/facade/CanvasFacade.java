package br.com.portal.inovacao.facade;

import static br.com.portal.inovacao.converter.CanvasConverter.converterCanvas;

import javax.inject.Inject;

import br.com.portal.inovacao.dao.CanvasDAO;
import br.com.portal.inovacao.dao.IdeiaDAO;
import br.com.portal.inovacao.dao.UsuarioDAO;
import br.com.portal.inovacao.dto.CanvasDTO;
import br.com.portal.inovacao.model.Canvas;
import br.com.portal.inovacao.model.Ideia;
import br.com.portal.inovacao.utils.Retorno;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CanvasFacade {

	private static final int CODIGO_SUCESSO = 0;
	private static final int CODIGO_ERRO = 1;

	private static final String MSG_CANVAS_SALVO_COM_SUCESSO = "Canvas salva com sucesso.";
	private static final String MSG_DESCRICAO_DEVE_SER_PREENCHIDO = "Descrião deve ser preenchido.";
	private static final String MSG_IDEIA_ALTERADA_COM_SUCESSO = "Idéia alterada com sucesso.";
	private static final String MSG_NAO_FOI_ENCONTRADA_NENHUMA_IDEIA_PARA_SER_ALTERADA = "Não foi encontrada nenhuma idéia para ser alterada.";
	//private static final String MSG_NAO_FOI_ENCONTRADA_NENHUMA_CANVAS_PARA_SER_REMOVIDO = "N�o foi encontrad nenhum canvas para ser removido.";
	//private static final String MSG_CANVAS_EXCLUIDO_COM_SUCESSO = "Item Canvas excluida com sucesso.";
	private static final String MSG_HISTORIA_NAO_ENCONTRADA = "Hist�ria não encontrada.";

	@Inject
	private CanvasDAO canvasDAO;

	@Inject
	private IdeiaDAO ideiaDAO;

	@Inject
	private UsuarioDAO usuarioDAO;
	
	public Retorno criaCanvas(CanvasDTO dto) {
		Ideia ideia = ideiaDAO.pesquisaPorId(dto.getIdIdeia());
		
		if (ideia == null)
			return new Retorno(CODIGO_ERRO, MSG_HISTORIA_NAO_ENCONTRADA);
		
		if (dto.getDescricao() != null && !(dto.getIdUsuario().equals("")) && dto.getNr_item() != null ) {
			Canvas canvas = new Canvas();
			canvas.setIdIdeia(ideiaDAO.pesquisaPorId(dto.getIdIdeia()));
			canvas.setIdUsuario(usuarioDAO.pesquisaPorId(dto.getIdUsuario()));
			canvas.setNr_item(dto.getNr_item());
			canvas.setDescricao(dto.getDescricao());
			canvas.setValor(dto.getValor());
			canvas.setData_cadastro(new Date(System.currentTimeMillis()));
			canvasDAO.salva(canvas);
			return new Retorno(CODIGO_SUCESSO, MSG_CANVAS_SALVO_COM_SUCESSO, converterCanvas(canvas));
		} else
			return new Retorno(CODIGO_ERRO, MSG_DESCRICAO_DEVE_SER_PREENCHIDO);
	}
	
        public List<CanvasDTO> listaCanvas(CanvasDTO dto) {
            List<Canvas> lstCanvas = canvasDAO.byHistoriaByNrItem(dto);
            List<CanvasDTO> canvasConvertidas = new ArrayList<CanvasDTO>();

            for (Canvas canvas : lstCanvas) {
                    canvasConvertidas.add(converterCanvas(canvas));
            }
            return canvasConvertidas;
	}
}