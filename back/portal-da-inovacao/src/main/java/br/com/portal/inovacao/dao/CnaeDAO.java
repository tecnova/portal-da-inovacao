package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.portal.inovacao.model.Cnae;

public class CnaeDAO {
	
	@Inject
	private EntityManager em;
	
	public Cnae pesquisaPorId(Long id) {
		return em.find(Cnae.class, id);
	}
	
	public List<Cnae> todos() {
		return em.createQuery("from Cnae", Cnae.class).getResultList();
	}
}
