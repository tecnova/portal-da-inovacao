package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.UsuarioDTO;
import br.com.portal.inovacao.model.IdeiaEquipe;

public class IdeiaEquipeConverter {
	
	public static UsuarioDTO converterUsuarioIdeiaEquipe(IdeiaEquipe ideiaEquipe) {
		UsuarioDTO dto = new UsuarioDTO();
		dto.setId(ideiaEquipe.getUsuario().getId().toString());
		dto.setNome(ideiaEquipe.getUsuario().getNome());
		dto.setEmail(ideiaEquipe.getUsuario().getEmail());
		return dto;
	}
}
