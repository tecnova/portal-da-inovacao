package br.com.portal.inovacao.dto;

import java.util.Date;

public class CanvasDTO {

	private Long id;
	private Long idIdeia;
	private Long idUsuario;
	private Long nr_item;
	private String descricao;
	private Double valor;
	private Date data_cadastro;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdIdeia() {
		return idIdeia;
	}
	public void setIdIdeia(Long ideia) {
		this.idIdeia = ideia;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Long getNr_item() {
		return nr_item;
	}
	public void setNr_item(Long nr_item) {
		this.nr_item = nr_item;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Date getData_cadastro() {
		return data_cadastro;
	}
	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}
	

}
