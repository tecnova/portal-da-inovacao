package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.Empresa;

public class EmpresaDAO {
	
	@Inject
	private EntityManager em;
	
	public void salvar(Empresa empresa) {
		em.getTransaction().begin();
		em.merge(empresa);
		em.getTransaction().commit();
	}
	
	public Empresa pesquisaPorId(Long id) {
		return em.find(Empresa.class, id);
	}
	
	public void flush(Empresa empresa) {
		em.getTransaction().begin();
		em.persist(empresa);
		em.flush();
		em.getTransaction().commit();
	}
	
	@SuppressWarnings("unchecked")
	public List<Empresa> pesquisaPorCnpj(String cnpj) {
		Query query = em.createQuery("from Empresa where cnpj = :cnpj", Empresa.class);
		query.setParameter("cnpj", cnpj);
		return query.getResultList();
	}
	
	public List<Empresa> todos() {
		return em.createQuery("from Empresa where is_empresa_ativa = 1", Empresa.class).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Empresa> pesquisaPorRazaoSocial(String razaoSocial) {
		Query query = em.createQuery("from Empresa where razao_social = :razaoSocial", Empresa.class);
		query.setParameter("razaoSocial", razaoSocial);
		return query.getResultList();
	}
}
