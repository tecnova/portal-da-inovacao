package br.com.portal.inovacao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "faixa_funcionario")
public class FaixaFuncionario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 255)
	private String descricao;

	@Column(name = "quantidade_inicial")
	private Integer quantidadeInicial;

	@Column(name = "quantidade_final")
	private Integer quantidadeFinal;

	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public Integer getQuantidadeInicial() {
		return quantidadeInicial;
	}

	public Integer getQuantidadeFinal() {
		return quantidadeFinal;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setQuantidadeInicial(Integer quantidadeInicial) {
		this.quantidadeInicial = quantidadeInicial;
	}

	public void setQuantidadeFinal(Integer quantidadeFinal) {
		this.quantidadeFinal = quantidadeFinal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result	+ ((quantidadeFinal == null) ? 0 : quantidadeFinal.hashCode());
		result = prime * result + ((quantidadeInicial == null) ? 0 : quantidadeInicial.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FaixaFuncionario other = (FaixaFuncionario) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (quantidadeFinal == null) {
			if (other.quantidadeFinal != null)
				return false;
		} else if (!quantidadeFinal.equals(other.quantidadeFinal))
			return false;
		if (quantidadeInicial == null) {
			if (other.quantidadeInicial != null)
				return false;
		} else if (!quantidadeInicial.equals(other.quantidadeInicial))
			return false;
		return true;
	}
}
