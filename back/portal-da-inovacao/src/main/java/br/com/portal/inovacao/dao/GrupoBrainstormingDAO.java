package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.GrupoBrainstorming;

public class GrupoBrainstormingDAO {
	
	@Inject
	private EntityManager manager;
	
	public void salvar(GrupoBrainstorming grupoBrainstorming) {
		manager.getTransaction().begin();
		manager.merge(grupoBrainstorming);
		manager.getTransaction().commit();
	}

	public void alterar(GrupoBrainstorming grupoBrainstorming) {
		manager.getTransaction().begin();
		manager.merge(grupoBrainstorming);
		manager.getTransaction().commit();
	}
	
	public void excluir(GrupoBrainstorming grupoBrainstorming) {
		manager.getTransaction().begin();
		manager.remove(manager.getReference(GrupoBrainstorming.class, grupoBrainstorming.getId()));
		manager.getTransaction().commit();
	}

	public GrupoBrainstorming pesquisaPorId(Long id) {
		return manager.find(GrupoBrainstorming.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<GrupoBrainstorming> listaGrupoBrainstormingPorHistoria(Long idHistoria) {
		Query query = manager.createQuery("from GrupoBrainstorming where id_historia = :idHistoria", GrupoBrainstorming.class);
		query.setParameter("idHistoria", idHistoria);
		return query.getResultList();
	}
}
