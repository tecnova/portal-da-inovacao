package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class BrainstormingAlteracaoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private Long idGrupoBrainstorming;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Long getIdGrupoBrainstorming() {
		return idGrupoBrainstorming;
	}
	public void setIdGrupoBrainstorming(Long idGrupoBrainstorming) {
		this.idGrupoBrainstorming = idGrupoBrainstorming;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
