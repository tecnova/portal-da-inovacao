package br.com.portal.inovacao.converter;

import java.text.SimpleDateFormat;

import br.com.portal.inovacao.dto.BrainstormingDTO;
import br.com.portal.inovacao.model.Brainstorming;

public class BrainstormingConverter {
	
	public static BrainstormingDTO converterBrainstorming(Brainstorming brainstorming) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		BrainstormingDTO dto = new BrainstormingDTO();
		dto.setId(brainstorming.getId());
		dto.setDescricao(brainstorming.getDescricao());
		dto.setIdUsuario(brainstorming.getIdUsuario().getId());
		dto.setDataCriacao(dateFormat.format(brainstorming.getDataCriacao()));
                try {
                    dto.setIdGrupoBrainstorming(brainstorming.getIdGrupoBrainstorming().getId());
                } catch (Exception e) {
                    //dto.setIdGrupoBrainstorming((long)0);
                }
		return dto;
	}
}
