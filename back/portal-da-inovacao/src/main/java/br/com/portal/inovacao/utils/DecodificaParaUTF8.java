package br.com.portal.inovacao.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class DecodificaParaUTF8 {
	
	private static final String UTF_8 = "UTF-8";
	
	public static String decodificaParaUTF8(String dto) throws UnsupportedEncodingException {
		return URLDecoder.decode(dto, UTF_8);
	}
}
