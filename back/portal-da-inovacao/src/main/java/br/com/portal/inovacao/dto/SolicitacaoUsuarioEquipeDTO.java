package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class SolicitacaoUsuarioEquipeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id_empresa;
	private Long id_usuario;

	public Long getId_empresa() {
		return id_empresa;
	}

	public Long getId_usuario() {
		return id_usuario;
	}

	public void setId_empresa(Long id_empresa) {
		this.id_empresa = id_empresa;
	}

	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}
}
