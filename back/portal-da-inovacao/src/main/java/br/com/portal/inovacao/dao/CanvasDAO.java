package br.com.portal.inovacao.dao;

import br.com.portal.inovacao.dto.CanvasDTO;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.Canvas;

public class CanvasDAO {
	
	@Inject
	private EntityManager manager;
	
	public void salva(Canvas canvas) {
		manager.getTransaction().begin();
		manager.persist(canvas);
		manager.getTransaction().commit();
	}
	
	public Canvas pesquisaPorId(Long id) {
		return manager.find(Canvas.class, id);
	}
	
	public List<Canvas> todos() {
		return manager.createQuery("from Canvas", Canvas.class).getResultList();
	}

        public List<Canvas> byHistoriaByNrItem(CanvasDTO dto) {
                String _SQL;
                _SQL = "from Canvas where id_ideia = :idideia";
                
                Query query = manager.createQuery(_SQL, Canvas.class);
                query.setParameter("idideia", dto.getIdIdeia());
                //query.setParameter("nr_item", dto.getNr_item());
                return query.getResultList();
	}
	//public Retorno buscaCanvasPorUsuario(InformaIdDTO dto) {
	//	List<Canvas> lstcanvas = canvasDAO.buscaCanvasPorUsuario(dto.getId());
	//	List<CanvasDTO> dtos = new ArrayList<CanvasDTO>();
		
	//	for (Canvas canvas : lstcanvas) {
	//		dtos.add(converterCanvas(canvas));
	//	}
	//	return new Retorno(CODIGO_SUCESSO, dtos);
	//}
	
	@SuppressWarnings("unchecked")
	public List<Canvas> buscaCanvasPorHistoria(Long idHistoria){
		Query query = manager.createQuery("from Canvas where id_ideia = :idideia", Canvas.class);
		query.setParameter("idideia", idHistoria);
		return query.getResultList();
	}
	
}
