package br.com.portal.inovacao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ideia")
public class Ideia {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    @Column(length = 255)
    private String titulo;

    @Column(length = 255)
    private String descricao;

    @Column(name = "data_criacao")
    private Date dataCriacao;

    @Column(name = "data_limite")
    private Date dataLimite;
    
    @Column(name = "is_ideia_ativa")
    private boolean ativo;

    @Column(columnDefinition = "CHAR(2)", nullable = false)
    private String tipoIdeia;

    @ManyToOne
    @JoinColumn(name = "id_empresa")
    private Empresa idEmpresa;

    @Column(name = "prioridade")
    private Long prioridade;

    public Long getId() {
        return id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public Date getDataLimite() {
        return dataLimite;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String getTipoIdeia() {
        return tipoIdeia;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public void setDataLimite(Date dataLimite) {
        this.dataLimite = dataLimite;
    }
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setTipoIdeia(String tipoIdeia) {
        this.tipoIdeia = tipoIdeia;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Long getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(Long prioridade) {
        this.prioridade = prioridade;
    }
	
        
        
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (ativo ? 1231 : 1237);
        result = prime * result + ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
        result = prime * result	+ ((descricao == null) ? 0 : descricao.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result	+ ((idEmpresa == null) ? 0 : idEmpresa.hashCode());
        result = prime * result	+ ((tipoIdeia == null) ? 0 : tipoIdeia.hashCode());
        result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
        result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Ideia other = (Ideia) obj;
        if (ativo != other.ativo)
            return false;
        
        if (dataCriacao == null) {
            if (other.dataCriacao != null)
                return false;
        } else if (!dataCriacao.equals(other.dataCriacao))
            return false;

        if (dataLimite == null) {
            if (other.dataLimite != null)
                return false;
        } else if (!dataLimite.equals(other.dataLimite))
            return false;
        
        if (descricao == null) {
            if (other.descricao != null)
                return false;
        } else if (!descricao.equals(other.descricao))
            return false;
        
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
                return false;

        if (idEmpresa == null) {
            if (other.idEmpresa != null)
                return false;
        } else if (!idEmpresa.equals(other.idEmpresa))
            return false;

        if (tipoIdeia == null) {
            if (other.tipoIdeia != null)
                return false;
        } else if (!tipoIdeia.equals(other.tipoIdeia))
            return false;
        
        if (titulo == null) {
            if (other.titulo != null)
                return false;
        } else if (!titulo.equals(other.titulo))
            return false;

        if (usuario == null) {
            if (other.usuario != null)
                return false;
        } else if (!usuario.equals(other.usuario))
                return false;

        if (prioridade == null) {
            if (other.prioridade != null)
                return false;
        } else if (!prioridade.equals(other.prioridade))
                return false;
        
        return true;
    }

    @Override
    public String toString() {
        return "Ideia [id=" + id + ", usuario=" + usuario + ", titulo="	+ titulo + ", descricao=" + descricao + ", dataCriação="
                + dataCriacao + ", ativo=" + ativo + ", tipoIdeia=" + tipoIdeia	+ "]";
    }
}
