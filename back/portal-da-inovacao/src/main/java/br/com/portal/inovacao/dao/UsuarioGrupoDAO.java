package br.com.portal.inovacao.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.portal.inovacao.model.UsuarioGrupo;

public class UsuarioGrupoDAO {
	
	@Inject
	private EntityManager em;
	
	public void salvar(UsuarioGrupo usuarioGrupo) {
		em.getTransaction().begin();
		em.merge(usuarioGrupo);
		em.getTransaction().commit();
	}
}
