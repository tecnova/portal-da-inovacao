package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.FaixaFaturamentoDTO;
import br.com.portal.inovacao.model.FaixaFaturamento;

public class FaixaFaturamentoConverter {
	
	public static FaixaFaturamentoDTO converterFaixaFaturamento(FaixaFaturamento faixaFaturamento) {
		FaixaFaturamentoDTO faturamentoDTO = new FaixaFaturamentoDTO();
		faturamentoDTO.setId(faixaFaturamento.getId());
		faturamentoDTO.setDescricao(faixaFaturamento.getDescricao());
		faturamentoDTO.setValorInicial(faixaFaturamento.getValorInicial().toString());
		faturamentoDTO.setValorFinal(faixaFaturamento.getValorFinal().toString());
		return faturamentoDTO;
	} 
}
