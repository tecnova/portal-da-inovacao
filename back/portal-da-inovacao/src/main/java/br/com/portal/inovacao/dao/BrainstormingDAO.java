package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.Brainstorming;

public class BrainstormingDAO {
	
	@Inject
	private EntityManager maneger;
	
	public void salvar(Brainstorming brainstorming) {
		maneger.getTransaction().begin();
		maneger.merge(brainstorming);
		maneger.getTransaction().commit();
	}
	
	public Brainstorming pesquisaPorId(Long id) {
		return maneger.find(Brainstorming.class, id);
	}
	
	public void alterar(Brainstorming brainstorming) {
		maneger.getTransaction().begin();
		maneger.merge(brainstorming);
		maneger.getTransaction().commit();
	}
	
	public void excluir(Brainstorming brainstorming) {
		maneger.getTransaction().begin();
		maneger.remove(brainstorming);
		maneger.getTransaction().commit();
	}
	
	public void vinculaGrupo(Brainstorming brainstorming) {
		maneger.getTransaction().begin();
		maneger.merge(brainstorming);
		maneger.getTransaction().commit();
	}
	
	@SuppressWarnings("unchecked")
	public List<Brainstorming> listaBrainstormingPorHistoria(Long idHistoria) {
		Query query = maneger.createQuery("from Brainstorming where id_historia = :idHistoria", Brainstorming.class);
		query.setParameter("idHistoria", idHistoria);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Brainstorming> listaBrainstormingPorGrupo(Long idGrupo) {
		Query query = maneger.createQuery("from Brainstorming where id_GrupoBrainstorming = :idGrupo", Brainstorming.class);
		query.setParameter("idGrupo", idGrupo);
		return query.getResultList();
	}
}
