package br.com.portal.inovacao.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;

	@Column(length = 250, nullable = false)
	private String nome;

	@Column(length = 250, unique = true)
	private String email;

	private String senha;

	@Column(name = "data_criacao", nullable = false)
	private Date dataCriacao;

	@Column(length = 250, name = "id_facebook", unique = true)
	private String id_face;

	@Column(length = 250, name = "id_google_plus", unique = true)
	private String id_google;

	@Column(length = 10)
	private String perfil_tema;

	@Column(name = "is_usuario_ativo")
	private boolean ativo;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "usuario_perfil", joinColumns = @JoinColumn(name = "id_usuario"), inverseJoinColumns = @JoinColumn(name = "id_perfil"))
	private List<Perfil> perfis;

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getEmail() {
		return email;
	}

	public String getSenha() {
		return senha;
	}

	public Date getData() {
		return dataCriacao;
	}

	public String getId_face() {
		return id_face;
	}

	public String getId_google() {
		return id_google;
	}

	public String getPerfil_tema() {
		return perfil_tema;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setData(Date data) {
		this.dataCriacao = data;
	}

	public void setId_face(String id_face) {
		this.id_face = id_face;
	}

	public void setId_google(String id_google) {
		this.id_google = id_google;
	}

	public void setPerfil_tema(String perfil_tema) {
		this.perfil_tema = perfil_tema;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (ativo ? 1231 : 1237);
		result = prime * result + ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((id_face == null) ? 0 : id_face.hashCode());
		result = prime * result + ((id_google == null) ? 0 : id_google.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((perfil_tema == null) ? 0 : perfil_tema.hashCode());
		result = prime * result + ((perfis == null) ? 0 : perfis.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (ativo != other.ativo)
			return false;
		if (dataCriacao == null) {
			if (other.dataCriacao != null)
				return false;
		} else if (!dataCriacao.equals(other.dataCriacao))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (id_face == null) {
			if (other.id_face != null)
				return false;
		} else if (!id_face.equals(other.id_face))
			return false;
		if (id_google == null) {
			if (other.id_google != null)
				return false;
		} else if (!id_google.equals(other.id_google))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (perfil_tema == null) {
			if (other.perfil_tema != null)
				return false;
		} else if (!perfil_tema.equals(other.perfil_tema))
			return false;
		if (perfis == null) {
			if (other.perfis != null)
				return false;
		} else if (!perfis.equals(other.perfis))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario: [id=" + id + ", nome=" + nome + ", data_criacao="
				+ dataCriacao + ", senha=" + senha + ", id_face=" + id_face
				+ ", id_google=" + id_google + ", perfil_tema=" + perfil_tema
				+ ", email=" + email + ", ativo=" + ativo + ", perfis="
				+ perfis + "]";
	}
}
