package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.FaixaFaturamentoDTO;
import br.com.portal.inovacao.facade.FaixaFaturamentoFacade;

@Path("faixa/faturamento")
public class FaixaFaturamentoService {
	
	@Inject
	private FaixaFaturamentoFacade faixafaturamentoFacade;
	
	@GET
	@Produces(APPLICATION_JSON)
	@Path("todos")
	public List<FaixaFaturamentoDTO> listaFaixaFaturamento() {
		return faixafaturamentoFacade.listaFaixaFaturamento();
	}
}
