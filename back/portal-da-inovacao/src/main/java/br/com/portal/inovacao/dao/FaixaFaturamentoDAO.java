package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.portal.inovacao.model.FaixaFaturamento;

public class FaixaFaturamentoDAO {
	
	@Inject
	private EntityManager em;
	
	public List<FaixaFaturamento> todos() {
		return em.createQuery("from FaixaFaturamento", FaixaFaturamento.class).getResultList();
	}
	
	public FaixaFaturamento pesquisaPorId(Long id) {
		return em.find(FaixaFaturamento.class, id);
	}
}
