package br.com.portal.inovacao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "empresa_equipe")
public class EmpresaEquipe {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_empresa")
	private Empresa empresa;

	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "id_perfil_empresa")
	private PerfilEmpresa perfilEmpresa;
	
	@Column(name = "solicitacao_aprovada")
	private boolean solicitacaoAprovada;
	
	@Column(name = "data_aprovacao")
	private Date dataAprovacao;
	
	@Column(name = "data_recusa")
	private Date dataRecusa;
	
	@Column(name = "data_solicitacao")
	private Date dataSolicitacao;
	
	@Column(name = "is_email_enviado")
	private boolean envioEmail;

	public Long getId() {
		return id;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public PerfilEmpresa getPerfilEmpresa() {
		return perfilEmpresa;
	}

	public boolean isSolicitacaoAprovada() {
		return solicitacaoAprovada;
	}

	public Date getDataAprovacao() {
		return dataAprovacao;
	}

	public Date getDataRecusa() {
		return dataRecusa;
	}

	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}

	public boolean isEnvioEmail() {
		return envioEmail;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setPerfilEmpresa(PerfilEmpresa perfilEmpresa) {
		this.perfilEmpresa = perfilEmpresa;
	}

	public void setSolicitacaoAprovada(boolean solicitacaoAprovada) {
		this.solicitacaoAprovada = solicitacaoAprovada;
	}

	public void setDataAprovacao(Date dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}

	public void setDataRecusa(Date dataRecusa) {
		this.dataRecusa = dataRecusa;
	}

	public void setDataSolicitacao(Date dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	public void setEnvioEmail(boolean envioEmail) {
		this.envioEmail = envioEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result	+ ((dataAprovacao == null) ? 0 : dataAprovacao.hashCode());
		result = prime * result	+ ((dataRecusa == null) ? 0 : dataRecusa.hashCode());
		result = prime * result	+ ((dataSolicitacao == null) ? 0 : dataSolicitacao.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + (envioEmail ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result	+ ((perfilEmpresa == null) ? 0 : perfilEmpresa.hashCode());
		result = prime * result + (solicitacaoAprovada ? 1231 : 1237);
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpresaEquipe other = (EmpresaEquipe) obj;
		if (dataAprovacao == null) {
			if (other.dataAprovacao != null)
				return false;
		} else if (!dataAprovacao.equals(other.dataAprovacao))
			return false;
		if (dataRecusa == null) {
			if (other.dataRecusa != null)
				return false;
		} else if (!dataRecusa.equals(other.dataRecusa))
			return false;
		if (dataSolicitacao == null) {
			if (other.dataSolicitacao != null)
				return false;
		} else if (!dataSolicitacao.equals(other.dataSolicitacao))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (envioEmail != other.envioEmail)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (perfilEmpresa == null) {
			if (other.perfilEmpresa != null)
				return false;
		} else if (!perfilEmpresa.equals(other.perfilEmpresa))
			return false;
		if (solicitacaoAprovada != other.solicitacaoAprovada)
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
}
