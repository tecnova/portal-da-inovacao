package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.model.Usuario;
import br.com.portal.inovacao.model.UsuarioAmigo;

public class UsuarioAmigoDAO {
	
	@Inject
	private EntityManager manager;
	
	public UsuarioAmigoDAO() {}
	
	public void salva(UsuarioAmigo usuarioAmigo) {
		manager.getTransaction().begin();
		manager.persist(usuarioAmigo);
		manager.getTransaction().commit();
	}

	public void atualiza(UsuarioAmigo usuarioAmigo) {
		manager.getTransaction().begin();
		manager.merge(usuarioAmigo);
		manager.getTransaction().commit();
	}

	public void apaga(UsuarioAmigo usuarioAmigo) {
		manager.getTransaction().begin();
		manager.remove(manager.getReference(UsuarioAmigo.class, usuarioAmigo.getId()));
		manager.getTransaction().commit();
	}
	
	public void pesquisaPorId(Long id) {
		manager.find(UsuarioAmigo.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioAmigo> pesquisaSolicitacoesPendentesDoUsuario(Long id) {
		Query query = manager.createQuery("from UsuarioAmigo where id_usuario = :id and data_aprovacao is null and data_recusa is null");
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioAmigo> pesquisaSolicitacoesPendentesDoAmigo(Long id) {
		Query query = manager.createQuery("from UsuarioAmigo where id_amigo = :id and data_aprovacao is null and data_recusa is null");
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioAmigo> pesquisaSeExisteSolicitacao(Long idUsuario, Long idAmigo) {
		Query query = manager.createQuery("from UsuarioAmigo where id_usuario = :idUsuario and id_amigo = :idAmigo and data_aprovacao is null and data_recusa is null");
		query.setParameter("idUsuario", idUsuario);
		query.setParameter("idAmigo", idAmigo);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioAmigo> buscaAmigos(Long idUsuario) {
		Query query = manager.createQuery("from UsuarioAmigo where id_usuario = :idUsuario and data_aprovacao is not null");
		query.setParameter("idUsuario", idUsuario);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> buscaAmigosDoUsuario(Long id) {
		Query query = manager.createNativeQuery("SELECT u1.* FROM usuario u1, usuario_amigo ua1 WHERE u1.id = ua1.id_amigo AND ua1.id_usuario = :id AND ua1.data_aprovacao is not null UNION SELECT u2.* FROM usuario u2, usuario_amigo ua2 WHERE u2.id = ua2.id_usuario AND ua2.id_amigo = :id AND ua2.data_aprovacao is not null", Usuario.class);
		query.setParameter("id", id);
		return query.getResultList();
	}
}