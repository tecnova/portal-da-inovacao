package br.com.portal.inovacao.converter;

import br.com.portal.inovacao.dto.EmpresaUsuarioSaidaDTO;
import br.com.portal.inovacao.model.EmpresaEquipe;

public class EmpresaUsuarioConverter {
	
	public static EmpresaUsuarioSaidaDTO converterEmpresaUsuario(EmpresaEquipe empresaEquipe, Long faixaFaturamento, Long faixaFuncionario, Long idCane, boolean imagem) {
		EmpresaUsuarioSaidaDTO dto = new EmpresaUsuarioSaidaDTO();
		dto.setIdEmpresa(empresaEquipe.getEmpresa().getId());
		dto.setCnpj(empresaEquipe.getEmpresa().getCnpj());
		dto.setRazaoSocial(empresaEquipe.getEmpresa().getRazaoSocial());
		dto.setLogradouro(empresaEquipe.getEmpresa().getLogradouro());
		dto.setNumero(empresaEquipe.getEmpresa().getNumero());
		dto.setComplemento(empresaEquipe.getEmpresa().getComplemento());
		dto.setBairro(empresaEquipe.getEmpresa().getBairro());
		dto.setMunicipio(empresaEquipe.getEmpresa().getMunicipio());
		dto.setUf(empresaEquipe.getEmpresa().getUf());
		dto.setCep(empresaEquipe.getEmpresa().getCep());
		dto.setIdCnae(idCane);
		dto.setIdFaixaFuncionario(faixaFuncionario);
		dto.setIdFaixaFaturamento(faixaFaturamento);
		dto.setPerfilUsuario(empresaEquipe.getPerfilEmpresa().getNome());
		dto.setImagem(imagem);
		return dto;
	}
}
