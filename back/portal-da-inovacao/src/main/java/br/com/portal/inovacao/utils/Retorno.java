package br.com.portal.inovacao.utils;

public class Retorno {

	private int erro;
	private String mensagem;
	private Object objeto;

	public Retorno() {}

	public Retorno(int erro, String mensagem, Object objeto) {
		this.erro = erro;
		this.mensagem = mensagem;
		this.objeto = objeto;
	}

	public Retorno(int erro, String mensagem) {
		this.erro = erro;
		this.mensagem = mensagem;
	}

	public Retorno(int erro, Object objeto) {
		this.erro = erro;
		this.objeto = objeto;
	}

	public int getErro() {
		return erro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public Object getObjeto() {
		return objeto;
	}

	public void setErro(int erro) {
		this.erro = erro;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}
}