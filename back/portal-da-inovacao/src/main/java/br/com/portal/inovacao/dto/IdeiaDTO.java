package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class IdeiaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long usuario;
    private Long empresa;
    private String titulo;
    private String descricao;
    private String dataCriacao;
    private String dataLimite;
    private String tipoIdeia;
    private Long prioridade;
    private Long ativo;
    private String link;
    private List<String> lstLink;
    private List<String> lstImagem;

    public Long getId() {
        return id;
    }

    public Long getUsuario() {
        return usuario;
    }

    public Long getEmpresa() {
        return empresa;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getDataCriacao() {
        return dataCriacao;
    }

    public String getDataLimite() {
        return dataLimite;
    }

    public void setDataLimite(String dataLimite) {
        this.dataLimite = dataLimite;
    }

    public Long getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(Long prioridade) {
        this.prioridade = prioridade;
    }

    public String getTipoIdeia() {
        return tipoIdeia;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsuario(Long usuario) {
        this.usuario = usuario;
    }

    public void setEmpresa(Long empresa) {
        this.empresa = empresa;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public void setTipoIdeia(String tipoIdeia) {
        this.tipoIdeia = tipoIdeia;
    }

    public Long getAtivo() {
        return ativo;
    }

    public void setAtivo(Long ativo) {
        this.ativo = ativo;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<String> getLstLink() {
        return lstLink;
    }

    public void setLstLink(List<String> lstLink) {
        this.lstLink = lstLink;
    }

    public List<String> getLstImagem() {
        return lstImagem;
    }

    public void setLstImagem(List<String> lstImagem) {
        this.lstImagem = lstImagem;
    }
    
    
}