package br.com.portal.inovacao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "grupo_brainstorming")
public class GrupoBrainstorming {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 255)
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "id_historia")
	private Ideia idHistoria;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Ideia getIdHistoria() {
		return idHistoria;
	}

	public void setIdHistoria(Ideia idHistoria) {
		this.idHistoria = idHistoria;
	}
}
