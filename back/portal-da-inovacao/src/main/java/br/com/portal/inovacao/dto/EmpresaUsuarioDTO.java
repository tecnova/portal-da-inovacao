package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class EmpresaUsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idEmpresa;
	private Long idUsuario;

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
}
