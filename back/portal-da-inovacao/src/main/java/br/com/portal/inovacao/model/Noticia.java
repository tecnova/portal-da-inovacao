package br.com.portal.inovacao.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "noticia")
public class Noticia implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;

	@Column(length = 250, nullable = false)
	private String titulo;

	@Column(length = 250, nullable = false)
	private String descricao;

	@Type(type = "text")
	private String texto;

	@Column(length = 250)
	private String link;

	@Column(length = 250, nullable = false)
	private String autor;

	@Column(name = "data_publicacao")
	private Date data;

	@Column(name = "is_noticia_privada")
	private boolean privado;

	@Column(name = "is_noticia_ativa")
	private boolean ativo;

	@ManyToOne
	private Usuario usuario;

	@ManyToOne
	private NoticiaSetor setor;

	@ManyToOne
	private NoticiaCategoria categoria;

	@Column(name = "imagem_principal")
	private String imagem;

	public Long getId() {
		return id;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getTexto() {
		return texto;
	}

	public String getLink() {
		return link;
	}

	public String getAutor() {
		return autor;
	}

	public Date getData() {
		return data;
	}

	public boolean isPrivado() {
		return privado;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public NoticiaSetor getSetor() {
		return setor;
	}

	public NoticiaCategoria getCategoria() {
		return categoria;
	}

	public String getImagem() {
		return imagem;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setPrivado(boolean privado) {
		this.privado = privado;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setSetor(NoticiaSetor setor) {
		this.setor = setor;
	}

	public void setCategoria(NoticiaCategoria categoria) {
		this.categoria = categoria;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (ativo ? 1231 : 1237);
		result = prime * result + ((autor == null) ? 0 : autor.hashCode());
		result = prime * result
				+ ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imagem == null) ? 0 : imagem.hashCode());
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		result = prime * result + (privado ? 1231 : 1237);
		result = prime * result + ((setor == null) ? 0 : setor.hashCode());
		result = prime * result + ((texto == null) ? 0 : texto.hashCode());
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Noticia other = (Noticia) obj;
		if (ativo != other.ativo)
			return false;
		if (autor == null) {
			if (other.autor != null)
				return false;
		} else if (!autor.equals(other.autor))
			return false;
		if (categoria == null) {
			if (other.categoria != null)
				return false;
		} else if (!categoria.equals(other.categoria))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imagem == null) {
			if (other.imagem != null)
				return false;
		} else if (!imagem.equals(other.imagem))
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		if (privado != other.privado)
			return false;
		if (setor == null) {
			if (other.setor != null)
				return false;
		} else if (!setor.equals(other.setor))
			return false;
		if (texto == null) {
			if (other.texto != null)
				return false;
		} else if (!texto.equals(other.texto))
			return false;
		if (titulo == null) {
			if (other.titulo != null)
				return false;
		} else if (!titulo.equals(other.titulo))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Noticia: [id=" + id + ", titulo=" + titulo + ", descricao="
				+ descricao + ", texto=" + texto + ", link=" + link
				+ ", autor=" + autor + ", setor=" + setor + ", categoria="
				+ categoria + ", privado=" + privado + ", data=" + data
				+ ", usuario=" + usuario + ", ativo=" + ativo + ",imagem="
				+ imagem + "]";
	}
}