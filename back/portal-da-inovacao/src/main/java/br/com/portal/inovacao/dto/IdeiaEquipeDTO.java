package br.com.portal.inovacao.dto;

import java.io.Serializable;
import java.util.List;

public class IdeiaEquipeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long idUsuario;
    private Long idEmpresa;
    private Long idIdeia;
    private List<UsuarioDTO> lstUsuarioIdeia;
    private List<UsuarioDTO> lstUsuarioEmpresa;

    public Long getId() {
        return id;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public Long getIdIdeia() {
        return idIdeia;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setIdIdeia(Long idIdeia) {
        this.idIdeia = idIdeia;
    }

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public List<UsuarioDTO> getLstUsuarioIdeia() {
        return lstUsuarioIdeia;
    }

    public void setLstUsuarioIdeia(List<UsuarioDTO> lstUsuarioIdeia) {
        this.lstUsuarioIdeia = lstUsuarioIdeia;
    }

    public List<UsuarioDTO> getLstUsuarioEmpresa() {
        return lstUsuarioEmpresa;
    }

    public void setLstUsuarioEmpresa(List<UsuarioDTO> lstUsuarioEmpresa) {
        this.lstUsuarioEmpresa = lstUsuarioEmpresa;
    }
    
    
}