package br.com.portal.inovacao.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.portal.inovacao.dto.IdeiaEquipeDTO;
import br.com.portal.inovacao.model.IdeiaEquipe;

public class IdeiaEquipeDAO {
	
	@Inject
	private EntityManager em;
	
	public void salvar(IdeiaEquipe ideiaEquipe) {
		em.getTransaction().begin();
		em.merge(ideiaEquipe);
		em.getTransaction().commit();
	}
	
	public int quantidadeDoMesmoUsuarioNaIdeia(Long idIdeia, Long idUsuario) {
		Query query = em.createQuery("from IdeiaEquipe where id_ideia = :idIdeia and id_usuario = :idUsuario", IdeiaEquipe.class);
		query.setParameter("idIdeia", idIdeia);
		query.setParameter("idUsuario", idUsuario);
		return query.getResultList().size();
	}
	
	public IdeiaEquipe buscaUsuarioNaIdeia(Long idIdeia, Long idUsuario) {
		Query query = em.createQuery("from IdeiaEquipe where id_ideia = :idIdeia and id_usuario = :idUsuario", IdeiaEquipe.class);
		query.setParameter("idIdeia", idIdeia);
		query.setParameter("idUsuario", idUsuario);
		return (IdeiaEquipe) query.getSingleResult();
	}
	
	public void excluir(IdeiaEquipeDTO dto) {
		IdeiaEquipe usuarioNaIdeia = buscaUsuarioNaIdeia(dto.getIdIdeia(), dto.getIdUsuario());
		
		em.getTransaction().begin();
		em.remove(usuarioNaIdeia);
		em.getTransaction().commit();
	}
	
	@SuppressWarnings("unchecked")
	public List<IdeiaEquipe> buscaUsuariosDaIdeia(Long idIdeia) {
		Query query = em.createQuery("from IdeiaEquipe where id_ideia = :idIdeia", IdeiaEquipe.class);
		query.setParameter("idIdeia", idIdeia);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<IdeiaEquipe> buscaIdeiasDoUsuario(Long idUsuario) {
		Query query = em.createQuery("from IdeiaEquipe where id_usuario = :idUsuario", IdeiaEquipe.class);
		query.setParameter("idUsuario", idUsuario);
		return query.getResultList();
	}
}
