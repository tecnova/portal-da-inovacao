package br.com.portal.inovacao.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.portal.inovacao.dto.AdicionaUsuarioNoGrupoDTO;
import br.com.portal.inovacao.dto.EmpresaGrupoDTO;
import br.com.portal.inovacao.dto.GrupoUsuarioSaidaDTO;
import br.com.portal.inovacao.dto.HistoriaUsuarioEmpresaSaidaDTO;
import br.com.portal.inovacao.dto.InformaIdDTO;
import br.com.portal.inovacao.dto.UsuarioDTO;
import br.com.portal.inovacao.facade.EmpresaGrupoFacade;
import br.com.portal.inovacao.facade.UsuarioGrupoFacade;
import br.com.portal.inovacao.utils.ConectaBanco;
import br.com.portal.inovacao.utils.Retorno;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Path("empresa/grupo")
public class EmpresaGrupoService {
	
	@Inject
	private EmpresaGrupoFacade facade;
	
	@Inject
	private UsuarioGrupoFacade usuarioGrupoFacade; 
	
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("criar")
	public Retorno criaEmpresaGrupo(EmpresaGrupoDTO dto) {
		return facade.criaGrupo(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("excluir")
	public Retorno excluiEmpresaGrupo(InformaIdDTO dto) {
		return facade.excluiGrupo(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("alterar")
	public Retorno alteraEmpresaGrupo(EmpresaGrupoDTO dto) {
		return facade.alteraGrupo(dto);
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("adicionar/usuario")
	public Retorno adicionaUsuarioAoGrupo(AdicionaUsuarioNoGrupoDTO dto) {
		return usuarioGrupoFacade.adicionaUsuarioAoGrupo(dto);
	}
        
        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("remover/usuario")
	public Retorno removeUsuarioAoGrupo(AdicionaUsuarioNoGrupoDTO dto) {

            ConectaBanco _Banco = new ConectaBanco();

            String _SQL;

            try{

                _SQL = "";
                _SQL = _SQL + "Delete ";
                _SQL = _SQL + "From ";
                _SQL = _SQL + "    usuario_grupo ";
                _SQL = _SQL + "Where ";
                _SQL = _SQL + "    id_grupo = " + dto.getIdGrupo() + " ";
                _SQL = _SQL + "and id_usuario = " + dto.getIdUsuario() + " ";

                return _Banco.Commando(_SQL); 

            }catch(Exception ex){
                return new Retorno(1, "Ocorreu um erro ao enviar o convite!");
            }
            
        }

        @POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista/grupo/usuario")
	public Retorno listaGrupoEUsuarios(InformaIdDTO dto) {
                return facade.listaGrupoEUsuarios(dto);
        }
        
	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista/usuario")
	public Retorno listaUsuarioGrupo(InformaIdDTO dto) {
            
            try {
                GrupoUsuarioSaidaDTO _result = new GrupoUsuarioSaidaDTO();
                List<UsuarioDTO> _lstUsuarioGrupo = new ArrayList<>();
                List<UsuarioDTO> _lstUsuarioEmpresa = new ArrayList<>();
                UsuarioDTO _Usuario;


                ConectaBanco _Banco = new ConectaBanco();

                String _SQL;

                //Procurando o Id do Projeto a Partir do id_brainstorming

                _SQL = "";
                _SQL = _SQL + "SELECT " 
                                + " * "
                            + "FROM empresa_grupo "
                            + "WHERE "
                                + "id = " + dto.getId() + " ";

                ResultSet _item = _Banco.Consulta(_SQL);

                if (_item.first()){
                    _result.setIdGrupo(_item.getLong("id"));
                    _result.setIdEmpresa(_item.getLong("id_empresa"));
                    _result.setGrupo(_item.getString("nome"));
                    
                    _SQL = "";
                    _SQL = _SQL + "SELECT " 
                                    + " usr.* "
                                + "FROM " 
                                    + " usuario usr "
                                    + ",usuario_grupo ugr "   
                                + "WHERE "
                                    + "    usr.id = ugr.id_usuario "
                                    + "and ugr.id_grupo = " + dto.getId() + " ";

                    _item = _Banco.Consulta(_SQL);
                    
                    while (_item.next()){
                        _Usuario = new UsuarioDTO();
                        _Usuario.setId(_item.getString("id"));
                        _Usuario.setNome(_item.getString("nome"));
                        _Usuario.setEmail(_item.getString("email"));
                        _Usuario.setImagem("");
                        
                        _lstUsuarioGrupo.add(_Usuario);
                    }
                    

                    _SQL = "";
                    _SQL = _SQL + "SELECT "
                                    + "u.* "
                                + "FROM "
                                    + " usuario u, "
                                    + "empresa_equipe eeq "
                                + "WHERE " 
                                    + "      u.id = eeq.id_usuario "
                                    + "and eeq.id_empresa = " + _result.getIdEmpresa() + " "
                                    + "and   u.id not in ( "   
                                                        + "SELECT " 
                                                            + " usr.id "
                                                        + "FROM " 
                                                            + " usuario usr "
                                                            + ",usuario_grupo ugr "   
                                                        + "WHERE "
                                                            + "    usr.id = ugr.id_usuario "
                                                            + "and ugr.id_grupo = " + dto.getId() + ") ";

                    _item = _Banco.Consulta(_SQL);
                    
                    while (_item.next()){
                        _Usuario = new UsuarioDTO();
                        _Usuario.setId(_item.getString("id"));
                        _Usuario.setNome(_item.getString("nome"));
                        _Usuario.setEmail(_item.getString("email"));
                        _Usuario.setImagem("");
                        
                        _lstUsuarioEmpresa.add(_Usuario);
                    }
                    
                    _result.setLstUsuarioGrupo(_lstUsuarioGrupo);
                    _result.setLstUsuarioEmpresa(_lstUsuarioEmpresa);

                    return new Retorno(0, "Lista Gerada com Sucesso", _result);
                    
                }else{
                    return new Retorno(1, "Grupo Não Localizado");
                }
                
            } catch (Exception ex) {
                return new Retorno(1, "Ta dando Nada Não " + ex.getMessage());
            }  

        }

	@POST
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	@Path("lista/grupos/empresa")
	public Retorno listaGruposDaEmpresa(InformaIdDTO idEmpresa) {
		return facade.listaGruposDaEmpresa(idEmpresa);
	}
}
