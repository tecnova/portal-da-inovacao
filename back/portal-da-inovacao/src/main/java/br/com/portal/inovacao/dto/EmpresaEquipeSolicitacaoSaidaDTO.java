package br.com.portal.inovacao.dto;

import java.io.Serializable;

public class EmpresaEquipeSolicitacaoSaidaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id_usuario;
	private String email;

	public Long getId_usuario() {
		return id_usuario;
	}

	public String getEmail() {
		return email;
	}

	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}