/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.rest.application.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author notebook-inova
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        // following code can be used to customize Jersey 1.x JSON provider:
        try {
            Class jacksonProvider = Class.forName("org.codehaus.jackson.jaxrs.JacksonJsonProvider");
            resources.add(jacksonProvider);
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(br.com.portal.inovacao.service.BrainstormingService.class);
        resources.add(br.com.portal.inovacao.service.CanvasService.class);
        resources.add(br.com.portal.inovacao.service.DocumentoService.class);
        resources.add(br.com.portal.inovacao.service.EmailService.class);
        resources.add(br.com.portal.inovacao.service.EmpresaGrupoService.class);
        resources.add(br.com.portal.inovacao.service.EmpresaImagemService.class);
        resources.add(br.com.portal.inovacao.service.EmpresaService.class);
        resources.add(br.com.portal.inovacao.service.EnviarEmailService.class);
        resources.add(br.com.portal.inovacao.service.FaixaFaturamentoService.class);
        resources.add(br.com.portal.inovacao.service.FaixaFuncionarioService.class);
        resources.add(br.com.portal.inovacao.service.GamificacaoService.class);
        resources.add(br.com.portal.inovacao.service.GrupoBrainstormingService.class);
        resources.add(br.com.portal.inovacao.service.IdeiaEquipeService.class);
        resources.add(br.com.portal.inovacao.service.IdeiaLinkService.class);
        resources.add(br.com.portal.inovacao.service.IdeiaService.class);
        resources.add(br.com.portal.inovacao.service.NoticiaService.class);
        resources.add(br.com.portal.inovacao.service.Projeto5w2hService.class);
        resources.add(br.com.portal.inovacao.service.SimpleService.class);
        resources.add(br.com.portal.inovacao.service.SolicitacaoAmigoService.class);
        resources.add(br.com.portal.inovacao.service.SolicitacaoUsuarioEquipeService.class);
        resources.add(br.com.portal.inovacao.service.SwotService.class);
        resources.add(br.com.portal.inovacao.service.UsuarioService.class);
    }
    
}
